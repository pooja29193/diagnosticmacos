//
//  AudioTestCollectionViewCell.swift
//  DiagnosticMac
//
//  Created by MacOS on 25/03/22.
//

import Cocoa

class AudioTestCollectionViewCell: NSCollectionViewItem {

    @IBOutlet weak var buttonOutlet: NSButton!
    @IBOutlet weak var labelOutlet: NSTextField!
    
    @IBOutlet weak var labelLeftTitleOutlet: NSTextField!
    @IBOutlet weak var labelLeftSubtitleOutlet: NSTextField!
    @IBOutlet weak var leftActivityIndicator: NSProgressIndicator!
    @IBOutlet weak var leftImageViewOutlet: NSImageView!
    @IBOutlet weak var buttonLeftYesOutlet: NSButton!
    @IBOutlet weak var buttonLeftNoOutlet: NSButton!
    @IBOutlet weak var buttonLeftBalanceIssueOutlet: NSButton!

    
    @IBOutlet weak var labelRightTitleOutlet: NSTextField!
    @IBOutlet weak var labelRightSubtitleOutlet: NSTextField!
    @IBOutlet weak var rightActivityIndicator: NSProgressIndicator!
    @IBOutlet weak var rightImageViewOutlet: NSImageView!
    @IBOutlet weak var buttonRightYesOutlet: NSButton!
    @IBOutlet weak var buttonRightNoOutlet: NSButton!
    @IBOutlet weak var buttonRightBalanceIssueOutlet: NSButton!


    
    @IBOutlet weak var labelBalanceTitleOutlet: NSTextField!
    @IBOutlet weak var labelBalanceSubtitleOutlet: NSTextField!
    @IBOutlet weak var balanceActivityIndicator: NSProgressIndicator!
    @IBOutlet weak var balanceImageViewOutlet: NSImageView!
    @IBOutlet weak var buttonBalanceYesOutlet: NSButton!
    @IBOutlet weak var buttonBalanceNoOutlet: NSButton!
    @IBOutlet weak var buttonBalanceIssueOutlet: NSButton!

    
    @IBOutlet weak var buttonRunAgainOUtlet: NSButton!
    @IBOutlet weak var labelStatusOutlet: NSTextField!
    @IBOutlet weak var buttonSendReportOutlet: NSButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
}
