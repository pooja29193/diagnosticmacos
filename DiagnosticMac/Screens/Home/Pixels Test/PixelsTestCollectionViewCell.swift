//
//  PixelsTestCollectionViewCell.swift
//  DiagnosticMac
//
//  Created by MacOS on 24/03/22.
//

import Cocoa

class PixelsTestCollectionViewCell: NSCollectionViewItem {
    @IBOutlet weak var buttonOutlet: NSButton!
    
    @IBOutlet weak var labelOutlet: NSTextField!

    @IBOutlet weak var labelTitleOutlet: NSTextField!
    @IBOutlet weak var buttonYesOutlet: NSButton!
    @IBOutlet weak var buttonNoOutlet: NSButton!

    @IBOutlet weak var buttonRunAgainOUtlet: NSButton!
    @IBOutlet weak var labelStatusOutlet: NSTextField!
    @IBOutlet weak var buttonSendReportOutlet: NSButton!
    @IBOutlet weak var activityIndicator: NSProgressIndicator!
    @IBOutlet weak var imageViewOutlet: NSImageView!
    @IBOutlet weak var labelSubtitleOutlet: NSTextField!


    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
}
