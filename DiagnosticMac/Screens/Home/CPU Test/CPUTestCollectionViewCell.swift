//
//  CPUTestCollectionViewCell.swift
//  DiagnosticMac
//
//  Created by MacOS on 26/03/22.
//

import Cocoa

class CPUTestCollectionViewCell: NSCollectionViewItem {
    @IBOutlet weak var viewOutlet: NSView!
    @IBOutlet weak var labelTitleSevenOutlet: NSTextField!
    
    @IBOutlet weak var activityIndicator: NSProgressIndicator!
    @IBOutlet weak var labelTenOutlet: NSTextField!
    @IBOutlet weak var labelTitleTenOutlet: NSTextField!
    @IBOutlet weak var labelNineOutlet: NSTextField!
    @IBOutlet weak var labelTitleNineOutlet: NSTextField!
    @IBOutlet weak var labelEightOutlet: NSTextField!
    @IBOutlet weak var labelTitleEightOutlet: NSTextField!
    @IBOutlet weak var labelSevenOutlet: NSTextField!
    @IBOutlet weak var labelSixOutlet: NSTextField!
    @IBOutlet weak var labelTitleSixOutlet: NSTextField!
    @IBOutlet weak var labelFiveOutlet: NSTextField!
    @IBOutlet weak var labelTitleFiveOutlet: NSTextField!
    @IBOutlet weak var imageViewOutlet: NSImageView!
    @IBOutlet weak var labelOutlet: NSTextField!
    @IBOutlet weak var labelTitleOneOutlet: NSTextField!
    @IBOutlet weak var labelTitleOutlet: NSTextField!
    @IBOutlet weak var labelTitleTwoOutlet: NSTextField!
    @IBOutlet weak var labelTwoOutlet: NSTextField!
    @IBOutlet weak var labelTitleThreeOutlet: NSTextField!
    @IBOutlet weak var labelThreeOutlet: NSTextField!
    @IBOutlet weak var labelTitleFourOutlet: NSTextField!
    @IBOutlet weak var labelFourOutlet: NSTextField!


    
    @IBOutlet weak var buttonSendReportTrailingConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var buttonSendReportWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonSendReportOutlet: NSButton!



    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
}
