import Cocoa
import Carbon
import Carbon.HIToolbox.Events
import Alamofire
import IOKit.ps
import AVFoundation
import CoreBluetooth
import CoreWLAN
import PDFKit
import Vision
import Files
import CoreAudio
class HomeVC: NSViewController, NSTableViewDataSource, NSCollectionViewDelegate, CBCentralManagerDelegate, AVCaptureVideoDataOutputSampleBufferDelegate, AVCapturePhotoCaptureDelegate, AVAudioRecorderDelegate {
    
    // MARK: - Outlets
    @IBOutlet weak var tableView: NSTableView!
    @IBOutlet weak var collectionView: NSCollectionView!
    @IBOutlet weak var scrollView: NSScrollView!
    @IBOutlet weak var collectionViewWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var viewPixelsOutlet: NSView!
    
    @IBOutlet weak var labelHeaderEmailTitleOutlet: NSTextField!
    @IBOutlet weak var labelHeaderTitleOutlet: NSTextField!
    //MARK: - Keyboard Outlets
    @IBOutlet weak var stackExtra: NSStackView!
    @IBOutlet weak var collectionViewTest: NSCollectionView!
    @IBOutlet weak var collectionViewSubmenu: NSCollectionView!
    //MARK: - Charger Test Outlets
    
    @IBOutlet weak var viewChargerOutlet: NSView!
    @IBOutlet weak var activityIndicator: NSProgressIndicator!
    @IBOutlet weak var reportsActivityIndicator: NSProgressIndicator!
    
    @IBOutlet weak var previewView: NSView!
    @IBOutlet weak var imageViewCameraPictureOutlet: NSImageView!
    @IBOutlet var audioMeter:AudioMeter!

    @IBOutlet weak var labelDataEraserOutlet: NSTextField!
    @IBOutlet weak var dataEraserIndicatorOutlet: NSProgressIndicator!
    @IBOutlet weak var viewDataEraserOutlet: NSView!

    
    @IBOutlet weak var labelDeviceSizeOutlet: NSTextField!
    @IBOutlet weak var labelDeviceSizeTitleOutlet: NSTextField!
    @IBOutlet weak var labelDeviceNameOutlet: NSTextField!
    @IBOutlet weak var labelDeviceNameTitleOutlet: NSTextField!
    
    @IBOutlet weak var buttonDataEraserOutlet: NSButton!
    @IBOutlet weak var buttonDataEraserCancelOutlet: NSButton!
    
    @IBOutlet var pdfView: PDFView!
    //MARK: - Variables And Constant
//        var arrayData = [["name":"Dashboard","image": #imageLiteral(resourceName: "home")],["name":"Diagnostic","image": nil],["name":"Interactive","image": #imageLiteral(resourceName: "interaction")],["name":"Non - Interactive","image": #imageLiteral(resourceName: "automatic")],["name":"Reports Interactive","image": #imageLiteral(resourceName: "info")],["name":"Reports Non - Interactive","image": #imageLiteral(resourceName: "info")],["name":"Logout","image": #imageLiteral(resourceName: "Logout")]]
    var arrayData = [["name":"Dashboard","image": #imageLiteral(resourceName: "home")],["name":"Diagnostic","image": nil],["name":"Interactive","image": #imageLiteral(resourceName: "interaction")],["name":"Non - Interactive","image": #imageLiteral(resourceName: "automatic")],["name":"Reports Interactive","image": #imageLiteral(resourceName: "info")],["name":"Reports Non - Interactive","image": #imageLiteral(resourceName: "info")],["name":"Logout","image": #imageLiteral(resourceName: "Logout")],["name":"Language","image": #imageLiteral(resourceName: "language")],["name":"Device Info","image": #imageLiteral(resourceName: "detailed")],["name":"Review Test","image": #imageLiteral(resourceName: "reviewtest")],["name":"Report History","image": #imageLiteral(resourceName: "folder")]] //,["name":"Data Eraser","image": #imageLiteral(resourceName: "folder")]
    var arrayDataArabic = [["name":"لوحة القيادة","image": #imageLiteral(resourceName: "home")],["name":"التشخيص","image": nil],["name":"تفاعلي","image": #imageLiteral(resourceName: "interaction")],["name":"غير - تفاعلي","image": #imageLiteral(resourceName: "automatic")],["name":"التقارير التفاعلية","image": #imageLiteral(resourceName: "info")],["name":"تقرير غير تفاعلي","image": #imageLiteral(resourceName: "info")],["name":"تسجيل خروج","image": #imageLiteral(resourceName: "Logout")],["name":"لغة","image": #imageLiteral(resourceName: "language")],["name":"معلومات الجهاز","image": #imageLiteral(resourceName: "detailed")],["name":"مراجعة الاختبار","image": #imageLiteral(resourceName: "reviewtest")],["name":"تقرير التاريخ","image": #imageLiteral(resourceName: "folder")]]
//    ,["name":"ممحاة البيانات","image": #imageLiteral(resourceName: "folder")]
    var arrayDataSpanish = [["name":"Panel","image": #imageLiteral(resourceName: "home")],["name":"Diagnóstico","image": nil],["name":"Interactivo","image": #imageLiteral(resourceName: "interaction")],["name":"No - Interactivo","image": #imageLiteral(resourceName: "automatic")],["name":"Informes interactivos","image": #imageLiteral(resourceName: "info")],["name":"Informe no interactivo","image": #imageLiteral(resourceName: "info")],["name":"Cerrar sesión","image": #imageLiteral(resourceName: "Logout")],["name":"Idioma","image": #imageLiteral(resourceName: "language")],["name":"Información del dispositivo","image": #imageLiteral(resourceName: "detailed")],["name":"Prueba de revisión","image": #imageLiteral(resourceName: "reviewtest")],["name":"Historial de informes","image": #imageLiteral(resourceName: "folder")]]
//    ,["name":"Borrador de datos","image": #imageLiteral(resourceName: "folder")]
    var arrayDataPort = [["name":"Painel","image": #imageLiteral(resourceName: "home")],["name":"Diagnóstico","image": nil],["name":"Interativo","image": #imageLiteral(resourceName: "interaction")],["name":"Não - Interativo","image": #imageLiteral(resourceName: "automatic")],["name":"Relatórios Interativos","image": #imageLiteral(resourceName: "info")],["name":"Relatório Não Interativo","image": #imageLiteral(resourceName: "info")],["name":"Sair","image": #imageLiteral(resourceName: "Logout")],["name":"Linguagem","image": #imageLiteral(resourceName: "language")],["name":"Informação do dispositivo","image": #imageLiteral(resourceName: "detailed")],["name":"Teste de Revisão","image": #imageLiteral(resourceName: "reviewtest")], ["name":"Histórico de relatórios","image": #imageLiteral(resourceName: "folder")]]
//    ,["name":"Apagador de dados","image": #imageLiteral(resourceName: "folder")]
    var arrayTestData = [["name":"Defective Pixels Test","image": #imageLiteral(resourceName: "cracked")],["name":"Interactive","image": #imageLiteral(resourceName: "click")],["name":"Non - Interactive","image": #imageLiteral(resourceName: "gear")],["name":"Charging Port Test","image": #imageLiteral(resourceName: "laptop (1)")],["name":"Battery Test","image": #imageLiteral(resourceName: "battery")],["name":"Keyboard Test","image": #imageLiteral(resourceName: "keyboard")],["name":"Audio Test","image": #imageLiteral(resourceName: "listening")]]
    var arrayTestDataArabic = [["name":"اختبار البكسل المعيب","image": #imageLiteral(resourceName: "cracked")],["name":"تفاعلي","image": #imageLiteral(resourceName: "click")],["name":"غير - تفاعلي","image": #imageLiteral(resourceName: "gear")],["name":"اختبار منفذ الشحن","image": #imageLiteral(resourceName: "laptop (1)")],["name":"اختبار البطارية","image": #imageLiteral(resourceName: "battery")],["name":"اختبار لوحة المفاتيح","image": #imageLiteral(resourceName: "keyboard")],["name":"اختبار الصوت","image": #imageLiteral(resourceName: "listening")]]
    var arrayTestDataSpanish = [["name":"Prueba de píxeles defectuosos","image": #imageLiteral(resourceName: "cracked")],["name":"Interactivo","image": #imageLiteral(resourceName: "click")],["name":"No - Interactivo","image": #imageLiteral(resourceName: "gear")],["name":"Prueba del puerto de carga","image": #imageLiteral(resourceName: "laptop (1)")],["name":"Prueba de batería","image": #imageLiteral(resourceName: "battery")],["name":"Prueba de teclado","image": #imageLiteral(resourceName: "keyboard")],["name":"Prueba de sonido","image": #imageLiteral(resourceName: "listening")]]
    var arrayTestDataPort = [["name":"Teste de pixel com defeito","image": #imageLiteral(resourceName: "cracked")],["name":"Interativo","image": #imageLiteral(resourceName: "click")],["name":"Não - Interativo","image": #imageLiteral(resourceName: "gear")],["name":"Teste da porta de carregamento","image": #imageLiteral(resourceName: "laptop (1)")],["name":"Teste de bateria","image": #imageLiteral(resourceName: "battery")],["name":"Keyboard Test","image": #imageLiteral(resourceName: "keyboard")],["name":"Audio Test","image": #imageLiteral(resourceName: "listening")]]
    
    var arrayInterectiveTestData = ["Keyboard","Charging Port Test","Webcam","Volume","Microphone","Defective Pixels","Audio Test"]
    var arrayInterectiveTestDataArabic = ["لوحة المفاتيح","اختبار منفذ الشحن","كاميرا ويب","مقدار","ميكروفون","وحدات البكسل المعيبة","اختبار الصوت"]
    var arrayInterectiveTestDataSpanish = ["Teclado","Prueba del puerto de carga","Cámara web","Volumen","Micrófono","Píxeles defectuosos","Prueba de sonido"]
    var arrayInterectiveTestDataPort = ["Teclado","Teste da porta de carregamento","Webcam","Volume","Microfone","Pixels defeituosos","Teste de áudio"]
    
    var arrayNonInterectiveTestData = ["CPU","GPU","Memory","Storage","Battery","Network","Bluetooth"]
    var arrayNonInterectiveTestDataArabic = ["وحدة المعالجة المركزية","GPU","ذاكرة","تخزين","بطارية","إيثرنت / واي فاي","بلوتوث"]
    var arrayNonInterectiveTestDataSpanish = ["CPU","GPU","Memoria","Almacenamiento","Batería","La red","Bluetooth"]
    var arrayNonInterectiveTestDataPort = ["CPU","GPU","Memória","Armazenar","Bateria","Rede","Bluetooth"]
    
    
    
    
    var isTableViewTag = 0
    var timer = Timer()
    var totalTime = 60
    var counter = 0
    var dateString = ""
    let formatter = DateFormatter()
    var keyboardTestArray = [["testName":"","macAddress": "","email":"","testStatusID":"","testTypeId":0]]
    var keyboardTestStatusID = ""
    var interectiveTestIndex = -1
    let session: AVCaptureSession = AVCaptureSession()
    struct readFile {
        static var arrayFloatValues:[Float] = []
        static var points:[CGFloat] = []
    }
    let audioPlayer: AVAudioPlayerNode = AVAudioPlayerNode()
    let audioPlayer2: AVAudioPlayerNode = AVAudioPlayerNode()
    var SelectArrayString = [String()]
    var currentSelection = -1
    var selectArray = [Int]()
    var isSelect = false
    var isTestStart = false
    var isKeyboardTestStart = false
    var audioControl:AudioControl?
    var isRecording:Bool = false
    var timerNew:DispatchSourceTimer?

    //MARK: - Keyboard Variables And Constant
    private var eventMonitor: Any?
    deinit {
        if let monitor = eventMonitor {
            NSEvent.stopPeriodicEvents()
            NSEvent.removeMonitor(monitor)
        }
    }
    var keyboardKeyArray = [String]()
    var isKeyboardTest = false // use for stop keybaoard interaction while test run
    var isSubMenuSelectedTag = -1
    
    //MARK: - Defective Pixels Variables And Constant
    private let colors: [NSColor] = [
        .red,
        .green,
        .blue,
        .white,
        .black,
        .yellow,
        .cyan,
        .magenta
    ]
    private var currentColorIndex = -1
    private var currentColor: NSColor { colors[currentColorIndex] }
    
    
    //MARK: - Charger Variables And Constant
    var batteryCheckArray = NSArray()
    let blob = IOPSCopyPowerSourcesInfo()
    var chargerTestArray = [["testName":"","macAddress": "","email":"","testStatusID":"","testTypeId":0]]
    var webcamTestArray = [["testName":"","macAddress": "","email":"","testStatusID":"","testTypeId":0]]
    var volumeTestArray = [["testName":"","macAddress": "","email":"","testStatusID":"","testTypeId":0]]
    var micTestArray = [["testName":"","macAddress": "","email":"","testStatusID":"","testTypeId":0]]
    var pixelsTestArray = [["testName":"","macAddress": "","email":"","testStatusID":"","testTypeId":0]]
    var centralManager: CBCentralManager!
    var testTypeArray = NSMutableArray()
    var CPUandGPUArray = NSArray()
    var isChargerTestStart = false
    var isChargerTestSkip = false
    var isChargerStart = 0
    var currentVolume = Float()
    var isVolumeLow = false
    var isVolumeHigh = false
    var isPixelsTestStart = false
    var isPixelsTestFinish = false
    var isFullScreen = false
    public let audioEngine = AVAudioEngine()
    public let audioEngine2 = AVAudioEngine()
    var flag = 0
    var nonInterectiveArray = NSMutableArray()
    var audioTestArray = NSMutableArray()
    var nonInterectiveCounter = 0
    var audioTestIndex = -1
    var audioTestStatus = ""
    var audioTestRightStatus = ""
    var audioTestBalanceStatus = ""
    var testArray = NSMutableArray()
    var isSelectIndex = 0
    var bluethoothStatus = ""
    var buttonRunTitleString = ""
    var buttonSendReportTitleString = ""
    var buttonRunAgainTitleString = ""
    var buttonStopTitleString = ""
    var buttonFinishTitleString = ""
    
    
    var timerMultipleSelction = Timer()
    var counterMultiple = 0

    
    var captureSession = AVCaptureSession()
    var previewLayer = AVCaptureVideoPreviewLayer()
    var stillImageOutput: AVCaptureStillImageOutput?
    var captureConnection: AVCaptureConnection?
    var cameraDevice: AVCaptureDevice?
    var photoOutput: AVCapturePhotoOutput?
    var imageData = Data()
    var dropDownIndex = 0
    var isMultipleTestSelect = false
    var batteryDict = NSDictionary()
    var isDeviceInfo = false
    var recorder:AVAudioRecorder?

    private(set) var popUpInitiallySelectedItem: NSMenuItem?

    enum Network: String {
        case wifi = "en0"
        case cellular = "pdp_ip0"
        //... case ipv4 = "ipv4"
        //... case ipv6 = "ipv6"
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.labelDeviceNameTitleOutlet.stringValue = "Device name :"
        self.labelDeviceNameOutlet.wantsLayer = true
        self.labelDeviceNameOutlet.stringValue = "Macbook"
        self.labelDeviceNameOutlet.layer?.borderColor = .black
        self.labelDeviceNameOutlet.layer?.borderWidth = 1
        self.labelDeviceSizeOutlet.wantsLayer = true
        self.labelDeviceSizeTitleOutlet.stringValue = "Available storage :"
        self.labelDeviceSizeOutlet.stringValue = "121 GB"
        self.labelDeviceSizeOutlet.layer?.borderColor = .black
        self.labelDeviceSizeOutlet.layer?.borderWidth = 1
        
        
        self.viewDataEraserOutlet.isHidden = true
        self.labelHeaderEmailTitleOutlet.stringValue = "\(DataStore.shared.email ?? "")"
        self.labelHeaderTitleOutlet.stringValue = "Welcome \(DataStore.shared.firstName ?? "")"
//        self.view.window =
        if AppLanguage.current == .arabic{
            self.arrayInterectiveTestData.removeAll()
            self.arrayInterectiveTestData = arrayInterectiveTestDataArabic

        }else if AppLanguage.current == .spanish{
            self.arrayInterectiveTestData.removeAll()
            self.arrayInterectiveTestData = arrayInterectiveTestDataSpanish
        }else if AppLanguage.current == .portuguese{
            self.arrayInterectiveTestData.removeAll()
            self.arrayInterectiveTestData = arrayInterectiveTestDataPort
        }
        
        //        if AppLanguage.current == .arabic{
        //            UserDefaults.standard.set("YES", forKey: "AppleTextDirection")
        //            UserDefaults.standard.set("YES", forKey: "NSForceRightToLeftWritingDirection")
        //        }else if AppLanguage.current == .portuguese{
        //            UserDefaults.standard.removeObject(forKey: "AppleTextDirection")
        //            UserDefaults.standard.removeObject(forKey: "NSForceRightToLeftWritingDirection")
        //        }
        //        self.collectionView.reloadData()
        //        self.collectionViewTest.reloadData()
        //        self.collectionViewSubmenu.reloadData()
        //        self.tableView.reloadData()
        

        
        if AppLanguage.current == .english{
            self.buttonRunTitleString = "START"
            self.buttonSendReportTitleString = "Send Report"
            self.buttonRunAgainTitleString = "START AGAIN"
            self.buttonStopTitleString = "Stop"
            self.buttonFinishTitleString = "Finished"
        }else if AppLanguage.current == .arabic{
            self.buttonRunTitleString = "بداية"
            self.buttonSendReportTitleString = "إرسال تقرير"
            self.buttonRunAgainTitleString = "إبدأ من جديد"
            self.buttonStopTitleString = "قف"
            self.buttonFinishTitleString = "تم الانتهاء من"
        }else if AppLanguage.current == .spanish{
            self.buttonRunTitleString = "COMIENZO"
            self.buttonSendReportTitleString = "ENVIAR REPORTE"
            self.buttonRunAgainTitleString = "EMPEZAR DE NUEVO"
            self.buttonStopTitleString = "DETÉNGASE"
            self.buttonFinishTitleString = "ACABADO"
        }else if AppLanguage.current == .portuguese{
            self.buttonRunTitleString = "COMEÇAR"
            self.buttonSendReportTitleString = "ENVIAR RELATÓRIO"
            self.buttonRunAgainTitleString = "COMECE DE NOVO"
            self.buttonStopTitleString = "PARE"
            self.buttonFinishTitleString = "FINALIZADO"
        }
        
        buttonDataEraserOutlet.bezelStyle = .texturedSquare
        buttonDataEraserOutlet.isBordered = false //Important
        buttonDataEraserOutlet.wantsLayer = true
        buttonDataEraserOutlet.bezelStyle = .texturedSquare
        buttonDataEraserOutlet.isBordered = false //Important
        buttonDataEraserOutlet.wantsLayer = true
        buttonDataEraserOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
        buttonDataEraserOutlet.layer?.cornerRadius = 6

        buttonDataEraserCancelOutlet.bezelStyle = .texturedSquare
        buttonDataEraserCancelOutlet.isBordered = false //Important
        buttonDataEraserCancelOutlet.wantsLayer = true
        buttonDataEraserCancelOutlet.bezelStyle = .texturedSquare
        buttonDataEraserCancelOutlet.isBordered = false //Important
        buttonDataEraserCancelOutlet.wantsLayer = true
        buttonDataEraserCancelOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
        buttonDataEraserCancelOutlet.layer?.cornerRadius = 6

        if #available(macOS 10.14, *) {
            buttonDataEraserOutlet.contentTintColor = NSColor.white
            buttonDataEraserCancelOutlet.contentTintColor = NSColor.white
        } else {
            // Fallback on earlier versions
        }
        self.viewPixelsOutlet.isHidden = true
        self.pdfView.isHidden = true
        currentVolume = NSSound.systemVolume
        self.intialSetup()
        print("// MACHINE STATUS")
        print("\( String(format:NSLocalizedString("Used %@", comment: ""), DiskStatus.usedDiskSpace))")
        print("\( String(format:NSLocalizedString("Free %@", comment: ""), DiskStatus.freeDiskSpace))")
        //        Please wait untill test finish
        print("\n-- CPU --")
        print("\tPHYSICAL CORES:  \(System.physicalCores())")
        print("\tLOGICAL CORES:   \(System.logicalCores())")
        
        var sys = System()
        let cpuUsage = sys.usageCPU()
        print("\tSYSTEM:          \(Int(cpuUsage.system))%")
        print("\tUSER:            \(Int(cpuUsage.user))%")
        print("\tIDLE:            \(Int(cpuUsage.idle))%")
        print("\tNICE:            \(Int(cpuUsage.nice))%")
        
        
        print("\n-- MEMORY --")
        print("\tPHYSICAL SIZE:   \(System.physicalMemory())GB")
        
        let memoryUsage = System.memoryUsage()
        func memoryUnit(_ value: Double) -> String {
            if value < 1.0 { return String(Int(value * 1000.0))    + "MB" }
            else           { return NSString(format:"%.2f", value) as String + "GB" }
        }
        
        print("\tFREE:            \(memoryUnit(memoryUsage.free))")
        print("\tWIRED:           \(memoryUnit(memoryUsage.wired))")
        print("\tACTIVE:          \(memoryUnit(memoryUsage.active))")
        print("\tINACTIVE:        \(memoryUnit(memoryUsage.inactive))")
        print("\tCOMPRESSED:      \(memoryUnit(memoryUsage.compressed))")
        
        
        
        
        print("\n-- SYSTEM --")
        print("\tMODEL:           \(System.modelName())")
        
        //let names = System.uname()
        //print("\tSYSNAME:         \(names.sysname)")
        //print("\tNODENAME:        \(names.nodename)")
        //print("\tRELEASE:         \(names.release)")
        //print("\tVERSION:         \(names.version)")
        //print("\tMACHINE:         \(names.machine)")
        
        let uptime = System.uptime()
        print("\tUPTIME:          \(uptime.days)d \(uptime.hrs)h \(uptime.mins)m " +
              "\(uptime.secs)s")
        
        let counts = System.processCounts()
        print("\tPROCESSES:       \(counts.processCount)")
        print("\tTHREADS:         \(counts.threadCount)")
        
        let loadAverage = System.loadAverage().map { NSString(format:"%.2f", $0) }
        print("\tLOAD AVERAGE:    \(loadAverage)")
        print("\tMACH FACTOR:     \(System.machFactor())")
        
        
        print("\n-- POWER --")
        let cpuThermalStatus = System.CPUPowerLimit()
        
        print("\tCPU SPEED LIMIT: \(cpuThermalStatus.processorSpeed)%")
        print("\tCPUs AVAILABLE:  \(cpuThermalStatus.processorCount)")
        print("\tSCHEDULER LIMIT: \(cpuThermalStatus.schedulerTime)%")
        
        print("\tTHERMAL LEVEL:   \(System.thermalLevel().rawValue)")
        
        var battery = Battery()
        if battery.open() != kIOReturnSuccess { exit(0) }
        
        print("\n-- BATTERY --")
        print("\tAC POWERED:      \(battery.isACPowered())")
        print("\tCHARGED:         \(battery.isCharged())")
        print("\tCHARGING:        \(battery.isCharging())")
        print("\tCHARGE:          \(battery.charge())%")
        print("\tCAPACITY:        \(battery.currentCapacity()) mAh")
        print("\tMAX CAPACITY:    \(battery.maxCapactiy()) mAh")
        print("\tDESGIN CAPACITY: \(battery.designCapacity()) mAh")
        print("\tCYCLES:          \(battery.cycleCount())")
        print("\tMAX CYCLES:      \(battery.designCycleCount())")
        print("\tTEMPERATURE:     \(battery.temperature())°C")
        print("\tTIME REMAINING:  \(battery.timeRemainingFormatted())")
        print("\tTIME REMAINING:  \(battery.timeRemainingFormatted())")
        
        _ = battery.close()
        print("------>>>>>isWIFIActive()",currentSSIDs())
        self.centralManager = CBCentralManager(delegate: self, queue: nil)
        let blob = IOPSCopyPowerSourcesInfo()
        let list = IOPSCopyPowerSourcesList(blob?.takeRetainedValue())
        print((((list?.takeRetainedValue()) as? NSArray ?? [])[0] as? NSDictionary ?? [:]).value(forKey: "BatteryHealth"))
        self.audioMeter.isHidden = true
        let audioFilename = getDocumentsDirectory().appendingPathComponent("recording.m4a")
        self.audioControl = AudioControl(path:audioFilename)
//        let documentDirectory = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).last!
//        let allContents = contentsOfDirectoryAtPath(path: documentDirectory)
//        print("allContentsallContentsallContentsallContents",allContents)
//        for i in 0..<(allContents?.count ?? 0){
//            print("The file name is --------->>>>>>>>",(allContents?[i] as? NSString ?? "").lastPathComponent)
//            do {
//                let fileManager = FileManager.default
//                try fileManager.removeItem(atPath: (allContents?[i])!)
//            }
//            catch let error as NSError {
//                print("An error took place: \(error)")
//            }
//            do {
//                let folder = try Folder(path: allContents?[i] as? String ?? "")
//                try folder.delete()
//            }catch{
//                print("------>>>>>The Error is------>>>>>>()",error)
//            }
//        }
      
    }

    
    func isHeadphonesConnected() -> Bool {
        var defaultOutputDeviceID = AudioDeviceID(0)
        var size = UInt32(MemoryLayout<AudioDeviceID>.size)
        var defaultOutputDevicePropertyAddress = AudioObjectPropertyAddress(
            mSelector: kAudioHardwarePropertyDefaultOutputDevice,
            mScope: kAudioObjectPropertyScopeGlobal,
            mElement: kAudioObjectPropertyElementMaster
        )
        
        let result = AudioObjectGetPropertyData(
            AudioObjectID(kAudioObjectSystemObject),
            &defaultOutputDevicePropertyAddress,
            0,
            nil,
            &size,
            &defaultOutputDeviceID
        )
        
        if result != noErr {
            return false
        }
        
        var deviceName: CFString = "" as CFString
        var deviceNameSize = UInt32(MemoryLayout<CFString>.size)
        var deviceNamePropertyAddress = AudioObjectPropertyAddress(
            mSelector: kAudioDevicePropertyDeviceNameCFString,
            mScope: kAudioObjectPropertyScopeGlobal,
            mElement: kAudioObjectPropertyElementMaster
        )
        
        let deviceID = AudioDeviceID(defaultOutputDeviceID)
        let deviceResult = AudioObjectGetPropertyData(
            deviceID,
            &deviceNamePropertyAddress,
            0,
            nil,
            &deviceNameSize,
            &deviceName
        )
        
        if deviceResult != noErr {
            return false
        }
        
        return (deviceName as String).contains("Headphones")
    }

    func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }
    
    @IBAction func buttonDataEraserCancelAction(_ sender: Any) {
        self.viewDataEraserOutlet.isHidden = true
        self.tableView.isHidden = false
        self.collectionView.isHidden = false
        self.collectionViewTest.isHidden = false
        self.collectionViewSubmenu.isHidden = false
        self.dataEraserIndicatorOutlet.isHidden = true
        self.labelDataEraserOutlet.isHidden = true
    }

    @IBAction func buttonDataEraserAction(_ sender: Any) {
        let a = NSAlert()
        if AppLanguage.current == .english{
            a.messageText = "Data Eraser"
            a.informativeText = "Are you sure you want to delete all system? This process will take too much time"
            a.addButton(withTitle: "Erase all data")
            a.addButton(withTitle: "Cancel")
        }else if AppLanguage.current == .arabic{
            a.messageText = "ممحاة البيانات"
            a.informativeText = "هل أنت متأكد من أنك تريد حذف النظام بالكامل؟ ستستغرق هذه العملية الكثير من الوقت"
            a.addButton(withTitle: "محو كل البيانات")
            a.addButton(withTitle: "يلغي")
        }else if AppLanguage.current == .spanish{
            a.messageText = "Borrador de datos"
            a.informativeText = "¿Está seguro de que desea eliminar todo el sistema? Este proceso llevará demasiado tiempo"
            a.addButton(withTitle: "Borrar todos los datos")
            a.addButton(withTitle: "Cancelar")
        }else if AppLanguage.current == .portuguese{
            a.messageText = "Apagador de Dados"
            a.informativeText = "Tem certeza de que deseja excluir todo o sistema? Este processo levará muito tempo"
            a.addButton(withTitle: "Apagar todos os dados")
            a.addButton(withTitle: "Cancelar")
        }
        a.alertStyle = NSAlert.Style.warning
        a.beginSheetModal(for: self.view.window!, completionHandler: { (modalResponse) -> Void in
            if modalResponse == NSApplication.ModalResponse.alertFirstButtonReturn {
                print("Document deleted")
                self.labelDataEraserOutlet.isHidden = false
                self.dataEraserIndicatorOutlet.isHidden = false
                self.dataEraserIndicatorOutlet.isIndeterminate = true
                self.dataEraserIndicatorOutlet.style = .spinning
                self.dataEraserIndicatorOutlet.startAnimation(nil)

            }
        })
    }
    
    func dataEraser(){
        let documentDirectory = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).last!
        let allContents = contentsOfDirectoryAtPath(path: documentDirectory)
        print("allContentsallContentsallContentsallContents",allContents)
        for i in 0..<(allContents?.count ?? 0){
            print("The file name is --------->>>>>>>>",(allContents?[i] as? NSString ?? "").lastPathComponent)
            do {
                let fileManager = FileManager.default
                try fileManager.removeItem(atPath: (allContents?[i])!)
            }
            catch let error as NSError {
                print("An error took place: \(error)")
            }
            do {
                let folder = try Folder(path: allContents?[i] as? String ?? "")
                try folder.delete()
            }catch{
                print("------>>>>>The Error is------>>>>>>()",error)
            }
        }
        
        
        
        let desktopDirectory = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.desktopDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).last!
        let allContentsDesktopDirectory = contentsOfDirectoryAtPath(path: desktopDirectory)
        print("allContentsallContentsallContentsallContents",allContentsDesktopDirectory)
        for i in 0..<(allContentsDesktopDirectory?.count ?? 0){
            print("The file name is --------->>>>>>>>",(allContentsDesktopDirectory?[i] as? NSString ?? "").lastPathComponent)
            do {
                let fileManager = FileManager.default
                try fileManager.removeItem(atPath: (allContentsDesktopDirectory?[i])!)
            }
            catch let error as NSError {
                print("An error took place: \(error)")
            }
            do {
                let folder = try Folder(path: allContentsDesktopDirectory?[i] as? String ?? "")
                try folder.delete()
            }catch{
                print("------>>>>>The Error is------>>>>>>()",error)
            }
        }
        
        
        let picturesDirectory = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.picturesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).last!
        let allContentsPicturesDirectory = contentsOfDirectoryAtPath(path: picturesDirectory)
        print("allContentsallContentsallContentsallContents",allContentsPicturesDirectory)
        for i in 0..<(allContentsPicturesDirectory?.count ?? 0){
            print("The file name is --------->>>>>>>>",(allContentsPicturesDirectory?[i] as? NSString ?? "").lastPathComponent)
            do {
                let fileManager = FileManager.default
                try fileManager.removeItem(atPath: (allContentsPicturesDirectory?[i])!)
            }
            catch let error as NSError {
                print("An error took place: \(error)")
            }
            do {
                let folder = try Folder(path: allContentsPicturesDirectory?[i] as? String ?? "")
                try folder.delete()
            }catch{
                print("------>>>>>The Error is------>>>>>>()",error)
            }
        }
        
        
        let allLibrariesDirectory = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.allLibrariesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).last!
        let allContentsallLibrariesDirectory = contentsOfDirectoryAtPath(path: allLibrariesDirectory)
        print("allContentsallContentsallContentsallContents",allContentsallLibrariesDirectory)
        for i in 0..<(allContentsallLibrariesDirectory?.count ?? 0){
            print("The file name is --------->>>>>>>>",(allContentsallLibrariesDirectory?[i] as? NSString ?? "").lastPathComponent)
            do {
                let fileManager = FileManager.default
                try fileManager.removeItem(atPath: (allContentsallLibrariesDirectory?[i])!)
            }
            catch let error as NSError {
                print("An error took place: \(error)")
            }
            do {
                let folder = try Folder(path: allContentsallLibrariesDirectory?[i] as? String ?? "")
                try folder.delete()
            }catch{
                print("------>>>>>The Error is------>>>>>>()",error)
            }
        }
        
        //        let userDirectory = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.userDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).last!
        //        let allContentsUserDirectory = contentsOfDirectoryAtPath(path: userDirectory)
        //        print("allContentsallContentsallContentsallContents",allContentsUserDirectory)
        //        for i in 0..<(allContentsUserDirectory?.count ?? 0){
        //            print("The file name is --------->>>>>>>>",(allContentsUserDirectory?[i] as? NSString ?? "").lastPathComponent)
        //            do {
        //                let fileManager = FileManager.default
        //                try fileManager.removeItem(atPath: (allContentsUserDirectory?[i])!)
        //            }
        //            catch let error as NSError {
        //                print("An error took place: \(error)")
        //            }
        //            do {
        //                let folder = try Folder(path: allContentsUserDirectory?[i] as? String ?? "")
        //                try folder.delete()
        //            }catch{
        //                print("------>>>>>The Error is------>>>>>>()",error)
        //            }
        //        }
        
        let musicDirectory = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.musicDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).last!
        let allContentsMusicDirectory = contentsOfDirectoryAtPath(path: musicDirectory)
        print("allContentsallContentsallContentsallContents",allContentsMusicDirectory)
        for i in 0..<(allContentsMusicDirectory?.count ?? 0){
            print("The file name is --------->>>>>>>>",(allContentsMusicDirectory?[i] as? NSString ?? "").lastPathComponent)
            do {
                let fileManager = FileManager.default
                try fileManager.removeItem(atPath: (allContentsMusicDirectory?[i])!)
            }
            catch let error as NSError {
                print("An error took place: \(error)")
            }
            do {
                let folder = try Folder(path: allContentsMusicDirectory?[i] as? String ?? "")
                try folder.delete()
            }catch{
                print("------>>>>>The Error is------>>>>>>()",error)
            }
        }
        
        let trashDirectory = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.trashDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).last!
        let allContentsTrashDirectory = contentsOfDirectoryAtPath(path: trashDirectory)
        print("allContentsallContentsallContentsallContents",allContentsTrashDirectory)
        for i in 0..<(allContentsTrashDirectory?.count ?? 0){
            print("The file name is --------->>>>>>>>",(allContentsTrashDirectory?[i] as? NSString ?? "").lastPathComponent)
            do {
                let fileManager = FileManager.default
                try fileManager.removeItem(atPath: (allContentsTrashDirectory?[i])!)
            }
            catch let error as NSError {
                print("An error took place: \(error)")
            }
            do {
                let folder = try Folder(path: allContentsTrashDirectory?[i] as? String ?? "")
                try folder.delete()
            }catch{
                print("------>>>>>The Error is------>>>>>>()",error)
            }
        }
        
        let cachesDirectory = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.cachesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).last!
        let allContentsCachesDirectory = contentsOfDirectoryAtPath(path: cachesDirectory)
        print("allContentsallContentsallContentsallContents",allContentsCachesDirectory)
        for i in 0..<(allContentsCachesDirectory?.count ?? 0){
            print("The file name is --------->>>>>>>>",(allContentsCachesDirectory?[i] as? NSString ?? "").lastPathComponent)
            do {
                let fileManager = FileManager.default
                try fileManager.removeItem(atPath: (allContentsCachesDirectory?[i])!)
            }
            catch let error as NSError {
                print("An error took place: \(error)")
            }
            do {
                let folder = try Folder(path: allContentsCachesDirectory?[i] as? String ?? "")
                try folder.delete()
            }catch{
                print("------>>>>>The Error is------>>>>>>()",error)
            }
        }
        
        let moviesDirectory = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.moviesDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).last!
        let allContentsMoviesDirectory = contentsOfDirectoryAtPath(path: moviesDirectory)
        print("allContentsallContentsallContentsallContents",allContentsMoviesDirectory)
        for i in 0..<(allContentsMoviesDirectory?.count ?? 0){
            print("The file name is --------->>>>>>>>",(allContentsMoviesDirectory?[i] as? NSString ?? "").lastPathComponent)
            do {
                let fileManager = FileManager.default
                try fileManager.removeItem(atPath: (allContentsMoviesDirectory?[i])!)
            }
            catch let error as NSError {
                print("An error took place: \(error)")
            }
            do {
                let folder = try Folder(path: allContentsMoviesDirectory?[i] as? String ?? "")
                try folder.delete()
            }catch{
                print("------>>>>>The Error is------>>>>>>()",error)
            }
        }
        
        let libraryDirectory = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.libraryDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).last!
        let allContentsLibraryDirectory = contentsOfDirectoryAtPath(path: libraryDirectory)
        print("allContentsallContentsallContentsallContents",allContentsLibraryDirectory)
        for i in 0..<(allContentsLibraryDirectory?.count ?? 0){
            print("The file name is --------->>>>>>>>",(allContentsLibraryDirectory?[i] as? NSString ?? "").lastPathComponent)
            do {
                let fileManager = FileManager.default
                try fileManager.removeItem(atPath: (allContentsLibraryDirectory?[i])!)
            }
            catch let error as NSError {
                print("An error took place: \(error)")
            }
            do {
                let folder = try Folder(path: allContentsLibraryDirectory?[i] as? String ?? "")
                try folder.delete()
            }catch{
                print("------>>>>>The Error is------>>>>>>()",error)
            }
        }
        
        let downloadsDirectory = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.downloadsDirectory, FileManager.SearchPathDomainMask.userDomainMask, true).last!
        let allContentsDownloadsDirectory = contentsOfDirectoryAtPath(path: downloadsDirectory)
        print("allContentsallContentsallContentsallContents",allContentsDownloadsDirectory)
        for i in 0..<(allContentsDownloadsDirectory?.count ?? 0){
            print("The file name is --------->>>>>>>>",(allContentsDownloadsDirectory?[i] as? NSString ?? "").lastPathComponent)
            do {
                let fileManager = FileManager.default
                try fileManager.removeItem(atPath: (allContentsDownloadsDirectory?[i])!)
            }
            catch let error as NSError {
                print("An error took place: \(error)")
            }
            do {
                let folder = try Folder(path: allContentsDownloadsDirectory?[i] as? String ?? "")
                try folder.delete()
            }catch{
                print("------>>>>>The Error is------>>>>>>()",error)
            }
        }
        self.viewDataEraserOutlet.isHidden = true
        self.dataEraserIndicatorOutlet.stopAnimation(nil)
        self.tableView.isHidden = false
        self.collectionView.isHidden = false
        self.collectionViewTest.isHidden = false
        self.collectionViewSubmenu.isHidden = false
    }
    func contentsOfDirectoryAtPath(path: String) -> [String]? {
        guard let paths = try? FileManager.default.contentsOfDirectory(atPath: path) else { return nil}
        return paths.map { aContent in (path as NSString).appendingPathComponent(aContent)}
    }
    func hostCPULoadInfo() -> host_cpu_load_info? {

        let  HOST_CPU_LOAD_INFO_COUNT = MemoryLayout<host_cpu_load_info>.stride / MemoryLayout<integer_t>.stride

        var size = mach_msg_type_number_t(HOST_CPU_LOAD_INFO_COUNT)
        let hostInfo = host_cpu_load_info_t.allocate(capacity: 1)

        let result = hostInfo.withMemoryRebound(to: integer_t.self, capacity: HOST_CPU_LOAD_INFO_COUNT) {
            host_statistics(mach_host_self(), HOST_CPU_LOAD_INFO, $0, &size)
        }

        if result != KERN_SUCCESS{
            print("Error  - \(#file): \(#function) - kern_result_t = \(result)")
            return nil
        }
        let data = hostInfo.move()
        hostInfo.deallocate()
        return data
    }

    func pdfViewOpenPDF_URL() {
        if let pdfURL = URL(string: "https://nexaglobal.azurewebsites.net/Content/Pdf/pixel-perfect%20_491c1eb8-c732-45ff-8bcc-d0d4ea306878.pdf") {
                    if let pdfDocument = PDFDocument(url: pdfURL) {
                        pdfView.document = pdfDocument
                    } else {
                        print("Failed to create PDF document.")
                    }
                } else {
                    print("Invalid PDF URL.")
                }
    }
    
    func TakeScreensShots(folderName: String){
        var displayCount: UInt32 = 0;
        var result = CGGetActiveDisplayList(0, nil, &displayCount)
        if (result != CGError.success) {
            print("error: \(result)")
            return
        }
        let allocated = Int(displayCount)
        let activeDisplays = UnsafeMutablePointer<CGDirectDisplayID>.allocate(capacity: allocated)
        result = CGGetActiveDisplayList(displayCount, activeDisplays, &displayCount)
        if (result != CGError.success) {
            print("error: \(result)")
            return
        }
        for i in 1...displayCount {
            let unixTimestamp = CreateTimeStamp()
            let fileUrl = URL(fileURLWithPath: folderName + "\(unixTimestamp)" + "_" + "\(i)" + ".jpg", isDirectory: true)
            //           let screenShot:CGImage = CGDisplayCreateImage(activeDisplays[Int(i-1)])!
            
            let rect = CGRect(x: 480, y: self.collectionViewTest.frame.origin.y, width: ((NSScreen.main?.frame.width ?? 500) - 490), height :self.collectionViewTest.bounds.height);
            let screenShot:CGImage = CGDisplayCreateImage(activeDisplays[Int(i-1)],rect: rect)!
            
            let bitmapRep = NSBitmapImageRep(cgImage: screenShot)
            let myNsImage = NSImage(cgImage: screenShot, size: .zero)
            
            
            
            let pdfDocument = PDFDocument()
            
            // Load or create your NSImage
            
            // Create a PDF page instance from your image
            let pdfPage = PDFPage(image: myNsImage)
            
            // Insert the PDF page into your document
            pdfDocument.insert(pdfPage!, at: 0)
            
            // Get the raw data of your PDF document
            let data = pdfDocument.dataRepresentation()
            
            // The url to save the data to
            let url = URL(fileURLWithPath: "/Users/macos/Downloads")
            
            let printOperation = NSPrintOperation(view: self.collectionViewTest)
            printOperation.showsPrintPanel = false
            printOperation.run()
            
            
            // Save the data to the url
            
            
            //            let pdfDocument = PDFDocument()
            //            let pdfPage = PDFPage(image: myNsImage)
            //            pdfDocument.insert(pdfPage!, at: 0)
            //            let data = pdfDocument.dataRepresentation()
            //
            //
            //            let url = URL(fileURLWithPath: " /Users/macos/Desktop")
            //            try! data!.write(to: url)
            
            // Save the data to the url
            
            let jpegData = bitmapRep.representation(using: NSBitmapImageRep.FileType.jpeg, properties: [:])!
            print("---------->>>>>>>>>>>>>>>>>>>url: \(url)")
            do {
                try data?.write(to: url, options: .atomic)
                
                
                //                try jpegData.write(to: fileUrl, options: .atomic)
            }
            catch {print("error: \(error)")}
        }
    }
    
    
    func CreateTimeStamp() -> Int32
    {
        return Int32(Date().timeIntervalSince1970)
    }
    
    
    //    override func viewDidAppear() {
    //        let presOptions: NSApplication.PresentationOptions = ([.fullScreen,.autoHideMenuBar])
    //        /*These are all of the options for NSApplicationPresentationOptions
    //         .Default
    //         .AutoHideDock              |   /
    //         .AutoHideMenuBar           |   /
    //         .DisableForceQuit          |   /
    //         .DisableMenuBarTransparency|   /
    //         .FullScreen                |   /
    //         .HideDock                  |   /
    //         .HideMenuBar               |   /
    //         .DisableAppleMenu          |   /
    //         .DisableProcessSwitching   |   /
    //         .DisableSessionTermination |   /
    //         .DisableHideApplication    |   /
    //         .AutoHideToolbar
    //         .HideMenuBar               |   /
    //         .DisableAppleMenu          |   /
    //         .DisableProcessSwitching   |   /
    //         .DisableSessionTermination |   /
    //         .DisableHideApplication    |   /
    //         .AutoHideToolbar */
    //
    //        let optionsDictionary = [NSView.FullScreenModeOptionKey.fullScreenModeApplicationPresentationOptions :
    //                                    UInt64(presOptions.rawValue)]
    //
    //        self.view.enterFullScreenMode(NSScreen.main!, withOptions:optionsDictionary)
    //        self.view.wantsLayer = true
    //
    //    }
    
    
    override func viewWillAppear() {
        super.viewWillAppear()
        showDeviceName()
        
        self.intialSetup()
    }
    
    func showDeviceName() {

        // load the current default device
        var deviceId = AudioDeviceID(0);
        var deviceSize = UInt32(MemoryLayout.size(ofValue: deviceId));
        var address = AudioObjectPropertyAddress(mSelector: kAudioHardwarePropertyDefaultInputDevice, mScope: kAudioObjectPropertyScopeGlobal, mElement: kAudioObjectPropertyElementMaster);
        var err = AudioObjectGetPropertyData(AudioObjectID(kAudioObjectSystemObject), &address, 0, nil, &deviceSize, &deviceId);

        if ( err == 0) {
            // change the query property and use previously fetched details
            address.mSelector = kAudioDevicePropertyDeviceNameCFString;
            var deviceName = "" as CFString;
            deviceSize = UInt32(MemoryLayout.size(ofValue: deviceName));
            err = AudioObjectGetPropertyData( deviceId, &address, 0, nil, &deviceSize, &deviceName);
            if (err == 0) {
                print("### current default mic:: \(deviceName) ");
            } else {
                // TODO:: unable to fetch device name
            }
        } else {
            // TODO:: unable to fetch the default input device
        }
    }
    
    
    
    
    func intialSetup(){
        tableView.selectionHighlightStyle = .none
//        tableView.backgroundColor =  NSColor.blue
//        scrollView.backgroundColor = NSColor.blue
        scrollView.backgroundColor = #colorLiteral(red: 0.2722039819, green: 0.3129750192, blue: 0.3694556355, alpha: 1)
        tableView.backgroundColor = #colorLiteral(red: 0.2722039819, green: 0.3129750192, blue: 0.3694556355, alpha: 1)

//        tableView.backgroundColor =   #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.4855034722)
//        scrollView.backgroundColor =  #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.4855034722)

        collectionViewSubmenu.register(ReportsTestCollectionViewCell.self, forItemWithIdentifier: NSUserInterfaceItemIdentifier("ReportsTestCollectionViewCell"))
        collectionViewTest.register(ReportsTestCollectionViewCell.self, forItemWithIdentifier: NSUserInterfaceItemIdentifier("ReportsTestCollectionViewCell"))
        
        collectionViewTest.register(CPUTestCollectionViewCell.self, forItemWithIdentifier: NSUserInterfaceItemIdentifier("CPUTestCollectionViewCell"))
        collectionViewTest.register(AudioTestCollectionViewCell.self, forItemWithIdentifier: NSUserInterfaceItemIdentifier("AudioTestCollectionViewCell"))
        collectionViewTest.register(KeyboardTestCollectionViewCell.self, forItemWithIdentifier: NSUserInterfaceItemIdentifier("KeyboardTestCollectionViewCell"))
        collectionViewTest.register(PixelsTestCollectionViewCell.self, forItemWithIdentifier: NSUserInterfaceItemIdentifier("PixelsTestCollectionViewCell"))
        collectionViewTest.register(MicTestCollectionViewCell.self, forItemWithIdentifier: NSUserInterfaceItemIdentifier("MicTestCollectionViewCell"))
        collectionViewTest.register(TestHeaderCollectionViewCell.self, forItemWithIdentifier: NSUserInterfaceItemIdentifier("TestHeaderCollectionViewCell"))
        collectionViewTest.register(VolumeTestCollectionViewCell.self, forItemWithIdentifier: NSUserInterfaceItemIdentifier("VolumeTestCollectionViewCell"))
        collectionViewSubmenu.register(SubMenuCollectionViewCell.self, forItemWithIdentifier: NSUserInterfaceItemIdentifier("SubMenuCollectionViewCell"))
        collectionViewTest.register(HomeTestCollectionViewCell.self, forItemWithIdentifier: NSUserInterfaceItemIdentifier("HomeTestCollectionViewCell"))
        collectionViewTest.register(SubMenuCollectionViewCell.self, forItemWithIdentifier: NSUserInterfaceItemIdentifier("SubMenuCollectionViewCell"))
        collectionView.register(DeviceInfoCell.self, forItemWithIdentifier: NSUserInterfaceItemIdentifier("DeviceInfoCell"))
        collectionViewTest.register(DeviceInfoCell.self, forItemWithIdentifier: NSUserInterfaceItemIdentifier("DeviceInfoCell"))

        formatter.dateFormat = "MM-dd-yyyy"
        let date = Date()
        dateString = formatter.string(from: date)
        collectionViewSubmenu.isHidden = true
        collectionView.isHidden = false
        tableView.delegate = self
        tableView.dataSource = self
        collectionViewWidthConstraint.constant = 0
        collectionView.delegate = self
        if #available(macOS 10.11, *) {
            collectionView.dataSource = self
            collectionViewTest.dataSource = self
            collectionViewSubmenu.dataSource = self
        } else {
            // Fallback on earlier versions
        }
        collectionViewTest.isHidden = true
        self.configureCollectionView(tag: 3)
        self.configureSideMenuCollectionView()
        collectionViewTest.delegate = self
        collectionViewSubmenu.delegate = self
        collectionViewTest.enclosingScrollView?.borderType = .noBorder
        self.view.wantsLayer = true
        self.viewChargerOutlet.wantsLayer = true
        self.view.layer?.backgroundColor = NSColor.white.cgColor

        viewChargerOutlet.layer?.backgroundColor = NSColor.blue.cgColor
        let ai = shell(launchPath: "/usr/sbin/system_profiler", arguments: ["SPHardwareDataType", "SPDisplaysDataType"])
        self.CPUandGPUArray = ((ai as? String)?.split(separator: "\n") as? NSArray ?? [])
        let list = IOPSCopyPowerSourcesList(blob?.takeRetainedValue())
        self.batteryCheckArray = (list?.takeUnretainedValue() as? NSArray ?? [])
        
    }
}
// MARK: - NSTableViewDelegate
extension HomeVC: NSTableViewDelegate {
    func numberOfRows(in tableView: NSTableView) -> Int {
        if AppLanguage.current == .english{
            return arrayData.count
        }else if AppLanguage.current == .arabic{
            return arrayDataArabic.count
        }else if AppLanguage.current == .spanish{
            return arrayDataSpanish.count
        }else{
            return arrayDataPort.count
        }
        
    }
    
    func tableView(_ tableView: NSTableView, viewFor tableColumn: NSTableColumn?, row: Int) -> NSView? {
        let view = PurchasesDetailView()
        if row == 1{
            view.labelLeadingConstraint.constant = -20
            view.avatarImageView.isHidden = true
        }else{
            view.labelLeadingConstraint.constant = 10
            view.avatarImageView.isHidden = false
        }
        view.viewOutlet.wantsLayer = true
        if row == isSelectIndex{
            view.viewOutlet.layer?.backgroundColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 0.4855034722).cgColor
            view.viewOutlet.layer?.cornerRadius = 15
        }else{
            view.viewOutlet.layer?.backgroundColor = NSColor.clear.cgColor
            view.viewOutlet.layer?.cornerRadius = 0
        }
        view.labelOutlet.lineBreakMode = .byWordWrapping
        view.labelOutlet.maximumNumberOfLines = 0
        view.labelOutlet.sizeToFit()
        if AppLanguage.current == .english{
            view.labelOutlet.stringValue = arrayData[row]["name"] as? String ?? ""
        }else if AppLanguage.current == .arabic{
            view.labelOutlet.stringValue = arrayDataArabic[row]["name"] as? String ?? ""
        }else if AppLanguage.current == .spanish{
            view.labelOutlet.stringValue = arrayDataSpanish[row]["name"] as? String ?? ""
        }else if AppLanguage.current == .portuguese{
            view.labelOutlet.stringValue = arrayDataPort[row]["name"] as? String ?? ""
        }
        view.avatarImageView.image = arrayData[row]["image"] as? NSImage
        return view
    }
    
    func tableView(_ tableView: NSTableView, heightOfRow row: Int) -> CGFloat {
        return 50
    }
    
    
    func tableView(_ tableView: NSTableView, shouldSelectRow row: Int) -> Bool {
        print("-------->>>>>>>>",row)
        self.audioMeter.isHidden = true
        //        self.collectionViewSubmenu.isSelectable = true
        //        self.isKeyboardTest = false
        
        
        //        self.collectionViewTest.isHidden = true
        ////        self.timer.invalidate()
        ////        self.counter = 0
        //        self.nonInterectiveCounter = 0
        if self.isKeyboardTest == true{
            return false
        }else{
            if row != 1{
                self.isDeviceInfo = false
                isSelectIndex = row
                self.configureCollectionView(tag: 3)
                self.tableView.reloadData()
                self.labelHeaderEmailTitleOutlet.stringValue = "\(DataStore.shared.email ?? "")"
                self.labelHeaderTitleOutlet.stringValue = "Welcome \(DataStore.shared.firstName ?? "")"
            }
            if row == 2{
                self.isDeviceInfo = false
                self.isSubMenuSelectedTag = 0
                self.timer.invalidate()
                isTableViewTag = 2
                collectionViewSubmenu.isHidden = false
                collectionView.isHidden = true
                collectionViewWidthConstraint.constant = 240
                self.totalTime = 60
                self.viewChargerOutlet.isHidden = false
                self.collectionViewTest.isHidden = false
                self.interectiveTestIndex = 1
                self.keyboardTestStatusID = ""
                self.isChargerStart = 0
                self.collectionViewTest.reloadData()
                self.configureCollectionView(tag: 1)
                self.collectionViewSubmenu.reloadData()
                self.labelHeaderEmailTitleOutlet.stringValue = ""
                self.labelHeaderTitleOutlet.stringValue = ""
            }else if row == 3{
                self.isDeviceInfo = false
                self.timer.invalidate()
                isTableViewTag = 1
                self.viewChargerOutlet.isHidden = true
                self.collectionViewTest.isHidden = true
                collectionViewSubmenu.isHidden = false
                collectionView.isHidden = true
                self.nonInterectiveCounter = 0
                self.interectiveTestIndex = 9
                self.isSelectIndex = 3
                self.keyboardTestStatusID = ""
                self.isChargerStart = 0
                collectionViewWidthConstraint.constant = 240
                self.startNonInterectiveTest()
                self.isSubMenuSelectedTag = 0
                self.collectionViewSubmenu.reloadData()
                self.labelHeaderEmailTitleOutlet.stringValue = ""
                self.labelHeaderTitleOutlet.stringValue = ""
            }else if row == 4{
                self.isDeviceInfo = false
                self.timer.invalidate()
                isTableViewTag = 3
                collectionViewSubmenu.isHidden = false
                self.viewChargerOutlet.isHidden = true
                self.collectionViewTest.isHidden = true
                collectionViewSubmenu.isHidden = false
                collectionView.isHidden = true
                collectionViewWidthConstraint.constant = 240
                self.isSubMenuSelectedTag = 0
                self.collectionViewSubmenu.reloadData()
                self.labelHeaderEmailTitleOutlet.stringValue = ""
                self.labelHeaderTitleOutlet.stringValue = ""
                if Reachability.isConnectedToNetwork(){
                    DispatchQueue.main.async {
                        self.getDeviceTestResultsAPI(TestingGroupId: 2)
                    }
                }else{
                    if AppLanguage.current == .english{
                        self.confirmAbletonIsReady(question: "Error", text: "Please Check Your Internet Connection Or Wifi")
                    }else if AppLanguage.current == .arabic{
                        self.confirmAbletonIsReady(question: "خطأ", text: "يرجى التحقق من اتصالك بالإنترنت أو شبكة Wifi")
                    }else if AppLanguage.current == .spanish{
                        self.confirmAbletonIsReady(question: "Error", text: "Por favor, compruebe su conexión a Internet o Wifi")
                    }else if AppLanguage.current == .portuguese{
                        self.confirmAbletonIsReady(question: "Erro", text: "Por favor, verifique sua conexão com a Internet ou Wi-Fi")
                    }
                }
            }else if row == 5{
                self.isDeviceInfo = false
                self.timer.invalidate()
                self.isTableViewTag = 4
                self.collectionViewSubmenu.isHidden = false
                self.viewChargerOutlet.isHidden = true
                self.collectionViewTest.isHidden = true
                self.collectionViewSubmenu.isHidden = false
                self.collectionView.isHidden = true
                self.collectionViewWidthConstraint.constant = 240
                self.isSubMenuSelectedTag = 0
                self.collectionViewSubmenu.reloadData()
                self.labelHeaderEmailTitleOutlet.stringValue = ""
                self.labelHeaderTitleOutlet.stringValue = ""
                if Reachability.isConnectedToNetwork(){
                    DispatchQueue.main.async {
                        self.getDeviceTestResultsAPI(TestingGroupId: 1)
                    }
                }else{
                    if AppLanguage.current == .english{
                        self.confirmAbletonIsReady(question: "Error", text: "Please Check Your Internet Connection Or Wifi")
                    }else if AppLanguage.current == .arabic{
                        self.confirmAbletonIsReady(question: "خطأ", text: "يرجى التحقق من اتصالك بالإنترنت أو شبكة Wifi")
                    }else if AppLanguage.current == .spanish{
                        self.confirmAbletonIsReady(question: "Error", text: "Por favor, compruebe su conexión a Internet o Wifi")
                    }else if AppLanguage.current == .portuguese{
                        self.confirmAbletonIsReady(question: "Erro", text: "Por favor, verifique sua conexão com a Internet ou Wi-Fi")
                    }
                }
            }else if row == 6{
                //                self.view.window?.close()
                //                NSApplication.shared.terminate(self)
                if let controller = self.storyboard?.instantiateController(withIdentifier: "ViewController") as? ViewController {
                    self.view.window?.contentViewController = controller
                }
            }else if row == 7{
                self.isDeviceInfo = false
                let alert = NSAlert()
                if AppLanguage.current == .english{
                    alert.messageText = "Change Language"
                    alert.informativeText = "Do you want change language?"
                    alert.addButton(withTitle: "English")
//                    alert.addButton(withTitle: "Arabic")
                    alert.addButton(withTitle: "Spanish")
                    alert.addButton(withTitle: "Portuguese")
                }
//                else if AppLanguage.current == .arabic{
//                    alert.messageText = "تغيير اللغة"
//                    alert.informativeText = "هل تريد تغيير اللغة؟"
//                    alert.addButton(withTitle: "إنجليزي")
//                    alert.addButton(withTitle: "عربي")
//                    alert.addButton(withTitle: "الأسبانية")
//                    alert.addButton(withTitle: "البرتغالية")
//                }
                else if AppLanguage.current == .spanish{
                    alert.messageText = "Cambiar idioma"
                    alert.informativeText = "¿Quieres cambiar de idioma?"
                    alert.addButton(withTitle: "Inglés")
//                    alert.addButton(withTitle: "Arábica")
                    alert.addButton(withTitle: "Español")
                    alert.addButton(withTitle: "Portugués")
                }else if AppLanguage.current == .portuguese{
                    alert.messageText = "Mudar idioma"
                    alert.informativeText = "Você quer mudar de idioma?"
                    alert.addButton(withTitle: "Inglês")
//                    alert.addButton(withTitle: "Árabe")
                    alert.addButton(withTitle: "Espanhol")
                    alert.addButton(withTitle: "Português")
                }
                
                let modalResult = alert.runModal()
                switch modalResult {
                case .alertFirstButtonReturn: //
                    DataStore.shared.appLanguage = .english
                    AppLanguage.current = .english
                    self.ChangeLayout()
                case .alertThirdButtonReturn:
                    DataStore.shared.appLanguage = .spanish
                    AppLanguage.current = .spanish
                    self.ChangeLayout()
                default:
                    DataStore.shared.appLanguage = .portuguese
                    AppLanguage.current = .portuguese
                    self.ChangeLayout()
                }
            }else if row == 8{
                self.labelHeaderEmailTitleOutlet.stringValue = ""
                self.labelHeaderTitleOutlet.stringValue = ""
                self.isPixelsTestStart = false
//                self.totalTime = 60
                self.viewChargerOutlet.isHidden = false
                self.collectionViewTest.isHidden = false
                self.interectiveTestIndex = 25
                self.keyboardTestStatusID = ""
                self.isChargerStart = 0
                self.isSubMenuSelectedTag = 0
                collectionViewWidthConstraint.constant = 0
                self.configureCollectionView(tag: 25)
                self.collectionViewSubmenu.reloadData()
                self.collectionViewTest.reloadData()
            }else if row == 9{
                self.labelHeaderEmailTitleOutlet.stringValue = ""
                self.labelHeaderTitleOutlet.stringValue = ""
                self.isPixelsTestStart = false
//                self.totalTime = 60
                self.viewChargerOutlet.isHidden = false
                self.collectionViewTest.isHidden = false
                self.interectiveTestIndex = 27
                self.keyboardTestStatusID = ""
                self.isChargerStart = 0
                self.isSubMenuSelectedTag = 0
                collectionViewWidthConstraint.constant = 0
                self.configureCollectionView(tag: 27)
                self.collectionViewSubmenu.reloadData()
                self.collectionViewTest.reloadData()
            }else if row == 10{
                
//                MARK: REPORT PDF
               // self.pdfView.isHidden = false
                self.isDeviceInfo = false
                self.timer.invalidate()
                isTableViewTag = 3
                collectionViewSubmenu.isHidden = false
                self.viewChargerOutlet.isHidden = true
                self.collectionViewTest.isHidden = true
                collectionViewSubmenu.isHidden = false
                collectionView.isHidden = true
                collectionViewWidthConstraint.constant = 240
                self.isSubMenuSelectedTag = 0
                self.collectionViewSubmenu.reloadData()
                self.labelHeaderEmailTitleOutlet.stringValue = ""
                self.labelHeaderTitleOutlet.stringValue = ""
                if Reachability.isConnectedToNetwork(){
                    DispatchQueue.main.async {
                        self.getDeviceTestResultsSixTestAPI()
                    }
                }else{
                    if AppLanguage.current == .english{
                        self.confirmAbletonIsReady(question: "Error", text: "Please Check Your Internet Connection Or Wifi")
                    }else if AppLanguage.current == .arabic{
                        self.confirmAbletonIsReady(question: "خطأ", text: "يرجى التحقق من اتصالك بالإنترنت أو شبكة Wifi")
                    }else if AppLanguage.current == .spanish{
                        self.confirmAbletonIsReady(question: "Error", text: "Por favor, compruebe su conexión a Internet o Wifi")
                    }else if AppLanguage.current == .portuguese{
                        self.confirmAbletonIsReady(question: "Erro", text: "Por favor, verifique sua conexão com a Internet ou Wi-Fi")
                    }
                    
                }

            }else{
                isTableViewTag = 0
                collectionViewSubmenu.isHidden = true
                collectionView.isHidden = false
                //                    self.keyboardView.isHidden = true
                //                    tableViewWidthConstraint.constant = 0
                collectionViewWidthConstraint.constant = 0
                self.collectionViewTest.isHidden = true
                self.viewChargerOutlet.isHidden = true
                self.labelHeaderEmailTitleOutlet.stringValue = "\(DataStore.shared.email ?? "")"
                self.labelHeaderTitleOutlet.stringValue = "Welcome \(DataStore.shared.firstName ?? "")"
            }
            if row == 1{
                return false
            }else{
                return true
            }
        }
    }
    
    func ChangeLayout(){
        print("APP ------------------>>>>>>>>>>>",AppLanguage.current)

        if AppLanguage.current == .arabic{
            UserDefaults.standard.set("YES", forKey: "AppleTextDirection")
            UserDefaults.standard.set("YES", forKey: "NSForceRightToLeftWritingDirection")
            if let controller = self.storyboard?.instantiateController(withIdentifier: "HomeVC") as? HomeVC {
                self.present(controller, animator: ReplacePresentationAnimator())
            }

        }else if AppLanguage.current == .portuguese{
            UserDefaults.standard.removeObject(forKey: "AppleTextDirection")
            UserDefaults.standard.removeObject(forKey: "NSForceRightToLeftWritingDirection")
            if let controller = self.storyboard?.instantiateController(withIdentifier: "HomeVC") as? HomeVC {
                self.present(controller, animator: ReplacePresentationAnimator())
            }
        }else if AppLanguage.current == .spanish{
            UserDefaults.standard.removeObject(forKey: "AppleTextDirection")
            UserDefaults.standard.removeObject(forKey: "NSForceRightToLeftWritingDirection")
            if let controller = self.storyboard?.instantiateController(withIdentifier: "HomeVC") as? HomeVC {
                self.present(controller, animator: ReplacePresentationAnimator())
            }
        }else{
            UserDefaults.standard.removeObject(forKey: "AppleTextDirection")
            UserDefaults.standard.removeObject(forKey: "NSForceRightToLeftWritingDirection")
            if let controller = self.storyboard?.instantiateController(withIdentifier: "HomeVC") as? HomeVC {
                self.present(controller, animator: ReplacePresentationAnimator())
            }
        }
    }
}
extension HomeVC: NSCollectionViewDataSource {
    func collectionView(_ collectionView: NSCollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == collectionViewTest {
            if isTableViewTag == 3 || isTableViewTag == 4{
                if self.testArray.count > 0 {
                    return self.testArray.count
                }else{
                    return 1
                }
            }else{
                return 1//arrayTestData.count
            }
        }else if collectionView == collectionViewSubmenu{
            if isTableViewTag == 1{
                return arrayNonInterectiveTestData.count
            }else if isTableViewTag == 2{
                return arrayInterectiveTestData.count + 1
            }else if isTableViewTag == 3 || isTableViewTag == 4{
                if testTypeArray.count > 0 {
                    return testTypeArray.count + 1
                }else{
                    return 1//testTypeArray.count
                }
            }else{
                return 0
            }
        }else{
                if AppLanguage.current == .english{
                    return arrayTestData.count
                }else if AppLanguage.current == .arabic{
                    return arrayTestDataArabic.count
                }else if AppLanguage.current == .spanish{
                    return arrayTestDataSpanish.count
                }else{
                    return arrayTestDataPort.count
            }
        }
    }
    
    func collectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        if collectionView == collectionViewTest {
            if interectiveTestIndex == 3{
                return self.chargerCollectionView(collectionView, itemForRepresentedObjectAt: indexPath)
            }else if interectiveTestIndex == 1{
                return self.keyboardCollectionView(collectionView, itemForRepresentedObjectAt: indexPath)
            }else if interectiveTestIndex == 4{
                return self.cameraCollectionView(collectionView, itemForRepresentedObjectAt: indexPath)
            }else if interectiveTestIndex == 5{
                return self.volumeCollectionView(collectionView, itemForRepresentedObjectAt: indexPath)
            }else if interectiveTestIndex == 6{
                return self.MicCollectionView(collectionView, itemForRepresentedObjectAt: indexPath)
            }else if interectiveTestIndex == 7{
                return self.pixelsCollectionView(collectionView, itemForRepresentedObjectAt: indexPath)
            }else if interectiveTestIndex == 8{
                return self.audioCollectionView(collectionView, itemForRepresentedObjectAt: indexPath)
            }else if interectiveTestIndex == 9{
                return self.CPUCollectionView(collectionView, itemForRepresentedObjectAt: indexPath)
            }else if interectiveTestIndex == 10{
                return self.GPUCollectionView(collectionView, itemForRepresentedObjectAt: indexPath)
            }else if interectiveTestIndex == 11{
                return self.MemoryCollectionView(collectionView, itemForRepresentedObjectAt: indexPath)
            }else if interectiveTestIndex == 12{
                return self.StorageCollectionView(collectionView, itemForRepresentedObjectAt: indexPath)
            }else if interectiveTestIndex == 25{
                return self.DeviceInfoCollectionView(collectionView, itemForRepresentedObjectAt: indexPath)
            }else if interectiveTestIndex == 27{
                return self.DeviceInfoCollectionView(collectionView, itemForRepresentedObjectAt: indexPath)
            }else if interectiveTestIndex == 13{
                return self.BatteryCollectionView(collectionView, itemForRepresentedObjectAt: indexPath)
            }else if interectiveTestIndex == 14{
                return self.wifiCollectionView(collectionView, itemForRepresentedObjectAt: indexPath)
            }else if interectiveTestIndex == 15{
                return self.bluethoothCollectionView(collectionView, itemForRepresentedObjectAt: indexPath)
            }else if interectiveTestIndex == 20{
                var item: NSCollectionViewItem
                if #available(macOS 10.11, *) {
                    item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ReportsTestCollectionViewCell"), for: indexPath) as! ReportsTestCollectionViewCell
                    if let item = item as? ReportsTestCollectionViewCell {
                        item.viewOutlet.isHidden = true
                        item.buttonDropDownStartOutlet.isHidden = true
                        item.buttonDropDownOutlet.isHidden = true
                        item.viewReportResultOutlet.isHidden = false
                        if self.testArray.count > 0 {
                            item.imageViewStatusOutlet.isHidden = false
                            item.labelStatusOutlet.isHidden = false
                            item.labelTestNameOutlet.stringValue = ((testArray[indexPath.item] as? NSDictionary)?.value(forKey: "TestName") as? String ?? "")
                            item.labelStatusOutlet.stringValue = ((testArray[indexPath.item] as? NSDictionary)?.value(forKey: "TestStatus") as? String ?? "")
                            if ((testArray[indexPath.item] as? NSDictionary)?.value(forKey: "TestStatus") as? String) == "Pass"{
                                item.imageViewStatusOutlet.image = NSImage(named: "tick")
                            }else{
                                item.imageViewStatusOutlet.image = NSImage(named: "danger-1")
                            }
                        }else{
                            item.imageViewStatusOutlet.isHidden = true
                            item.labelStatusOutlet.isHidden = true
                            item.labelTestNameOutlet.stringValue = "No Record Found"
                        }
                    }
                    return item
                } else {
                    return NSCollectionViewItem()
                }
            }else{
                var item: NSCollectionViewItem
                if #available(macOS 10.11, *) {
                    item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ReportsTestCollectionViewCell"), for: indexPath) as! ReportsTestCollectionViewCell
                    return item
                } else {
                    return NSCollectionViewItem()
                }
            }
        }else if collectionView == collectionViewSubmenu{
            if isTableViewTag == 3 || isTableViewTag == 4{
                if indexPath.item == 0 {
                    var item: NSCollectionViewItem
                    if #available(macOS 10.11, *) {
                        item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ReportsTestCollectionViewCell"), for: indexPath) as! ReportsTestCollectionViewCell
                        if let item = item as? ReportsTestCollectionViewCell {
                            item.labelNameOutlet.isHidden = true
                            item.labelOutlet.isHidden = true
                            item.labelHeaderTitleOutlet.isHidden = false
                            item.labelTitleOutlet.isHidden = true
                            item.buttonDropDownStartOutlet.isHidden = true
                            item.buttonDropDownOutlet.isHidden = true
                            if isTableViewTag == 3{
                                if testTypeArray.count > 0 {
                                    item.labelHeaderTitleOutlet.stringValue = "Interective"
                                }else{
                                    item.labelHeaderTitleOutlet.stringValue = "No Record Found"
                                }
                            }else{
                                if testTypeArray.count > 0 {
                                    item.labelHeaderTitleOutlet.stringValue = "Non - Interective"
                                }else{
                                    item.labelHeaderTitleOutlet.stringValue = "No Record Found"
                                }
                            }
                        }
                        return item
                    } else {
                        return NSCollectionViewItem()
                    }
                }else{
                    var item: NSCollectionViewItem
                    if #available(macOS 10.11, *) {
                        item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "SubMenuCollectionViewCell"), for: indexPath) as! SubMenuCollectionViewCell
                        if let item = item as? SubMenuCollectionViewCell {
                            item.labelSubtitleOutlet.stringValue = ""

                            if indexPath.item == isSubMenuSelectedTag{
                                item.imageViewOutlet.image = NSImage(named: "left-arrow")
                                item.viewOutlet.wantsLayer = true
                                item.viewOutlet.layer?.backgroundColor = NSColor(hex: "F4F5F9").cgColor
                                
                            }else{
                                item.imageViewOutlet.image = NSImage(named: "right-arrow-1")
                                item.viewOutlet.wantsLayer = true
                                item.viewOutlet.layer?.backgroundColor = NSColor.white.cgColor
                            }
                            item.labelTitleOutlet.stringValue = (testTypeArray[indexPath.item - 1] as? NSDictionary)?.value(forKey: "TestingName") as? String ?? ""
                            
                        }
                        return item
                    } else {
                        return NSCollectionViewItem()
                    }
                    
                }
            }else{
                if isTableViewTag == 1{
                    var item: NSCollectionViewItem
                    if #available(macOS 10.11, *) {
                        item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "SubMenuCollectionViewCell"), for: indexPath) as! SubMenuCollectionViewCell
                        if let item = item as? SubMenuCollectionViewCell {
                            if AppLanguage.current == .english{
                                    item.labelSubtitleOutlet.stringValue = "1 Test"
                               }else if AppLanguage.current == .arabic{
                                    item.labelSubtitleOutlet.stringValue = "1 اختبار"
                            }else if AppLanguage.current == .spanish{
                                    item.labelSubtitleOutlet.stringValue = "1 teste"
                            }else if AppLanguage.current == .portuguese{
                                    item.labelSubtitleOutlet.stringValue = "1 teste"
                            }
                            if isTableViewTag == 1{
                                
                                
                                
                                if indexPath.item == isSubMenuSelectedTag{
                                    item.imageViewOutlet.image = NSImage(named: "left-arrow")
                                    item.viewOutlet.wantsLayer = true
                                    item.viewOutlet.layer?.backgroundColor = NSColor(hex: "F4F5F9").cgColor
                                    
                                }else{
                                    item.imageViewOutlet.image = NSImage(named: "right-arrow-1")
                                    item.viewOutlet.wantsLayer = true
                                    item.viewOutlet.layer?.backgroundColor = NSColor.white.cgColor
                                }
                                
                                if AppLanguage.current == .english{
                                    item.labelTitleOutlet.stringValue = arrayNonInterectiveTestData[indexPath.item]
                                }else if AppLanguage.current == .arabic{
                                    item.labelTitleOutlet.stringValue = arrayNonInterectiveTestDataArabic[indexPath.item]
                                }else if AppLanguage.current == .spanish{
                                    item.labelTitleOutlet.stringValue = arrayNonInterectiveTestDataSpanish[indexPath.item]
                                }else if AppLanguage.current == .portuguese{
                                    item.labelTitleOutlet.stringValue = arrayNonInterectiveTestDataPort[indexPath.item]
                                }
                            }
                        }
                        return item
                    } else {
                        return NSCollectionViewItem()
                    }
                }else{
                    if indexPath.item == 0{
                        var item: NSCollectionViewItem
                        if #available(macOS 10.11, *) {
                            item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "ReportsTestCollectionViewCell"), for: indexPath) as! ReportsTestCollectionViewCell
                            if let item = item as? ReportsTestCollectionViewCell {
                                item.viewOutlet.isHidden = false
                                item.labelHeaderTitleOutlet.isHidden = true
                                item.viewReportResultOutlet.isHidden = false
                                item.imageViewStatusOutlet.isHidden = true
                                item.labelStatusOutlet.isHidden = true
                                item.labelOutlet.isHidden = true
                                item.labelTestNameOutlet.isHidden = true
                                item.buttonDropDownOutlet.isHidden = false
//                                item.buttonDropDownOutlet.selectedite
//                                item.buttonDropDownOutlet.selectItem(at: dropDownIndex)
//                                item.buttonDropDownOutlet.image.color
                                item.buttonDropDownOutlet.action = #selector(buttonDropDownAction)
//                                item.buttonDropDownOutlet.selectItem(at: 2)

                                popUpInitiallySelectedItem = item.buttonDropDownOutlet.selectedItem

                                item.buttonDropDownStartOutlet.action = #selector(buttonDropDownStartAction)

                                
                                
//                                let pstyle = NSMutableParagraphStyle()
//                                pstyle.alignment = .center
//                                item.buttonDropDownStartOutlet.layer?.backgroundColor = NSColor.orange.cgColor
//                                item.buttonDropDownStartOutlet.layer?.cornerRadius = 6
////                                item.buttonDropDownStartOutlet.attributedTitle = NSAttributedString(string: self.buttonRunTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])


                                
                                item.buttonDropDownStartOutlet.bezelStyle = .texturedSquare
                                item.buttonDropDownStartOutlet.isBordered = false //Important
                                item.buttonDropDownStartOutlet.wantsLayer = true
                                                                item.buttonDropDownStartOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                                item.buttonDropDownStartOutlet.layer?.cornerRadius = 6
                                
                                let pstyle = NSMutableParagraphStyle()
                                pstyle.alignment = .center
                                if SelectArrayString.count > 0 && SelectArrayString[0] != ""{
                                    item.buttonDropDownCenterConstraint.constant = -45
                                    item.buttonDropDownStartOutlet.isHidden = false
                                }else{
                                    item.buttonDropDownCenterConstraint.constant = 0
                                    item.buttonDropDownStartOutlet.isHidden = true
                                }
                                if isTestStart == false{
                                    item.buttonDropDownStartOutlet.attributedTitle = NSAttributedString(string: "START", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                                }else{
                                    item.buttonDropDownStartOutlet.attributedTitle = NSAttributedString(string: "STOP", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                                }
                            }
                            return item
                        } else {
                            return NSCollectionViewItem()
                        }
                        
                    }else{
                        var item: NSCollectionViewItem
                        if #available(macOS 10.11, *) {
                            item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "SubMenuCollectionViewCell"), for: indexPath) as! SubMenuCollectionViewCell
                            if let item = item as? SubMenuCollectionViewCell {
                                if AppLanguage.current == .english{
                                    if indexPath.item == 7{
                                        item.labelSubtitleOutlet.stringValue = "3 Tests"
                                    }else{
                                        item.labelSubtitleOutlet.stringValue = "1 Test"
                                    }
                                }else if AppLanguage.current == .arabic{
                                    if indexPath.item == 7{
                                        item.labelSubtitleOutlet.stringValue = "3 اختبارات"
                                    }else{
                                        item.labelSubtitleOutlet.stringValue = "1 اختبار"
                                    }
                                }else if AppLanguage.current == .spanish{
                                    if indexPath.item == 7{
                                        item.labelSubtitleOutlet.stringValue = "3 pruebas"
                                    }else{
                                        item.labelSubtitleOutlet.stringValue = "1 teste"
                                    }
                                }else if AppLanguage.current == .portuguese{
                                    if indexPath.item == 7{
                                        item.labelSubtitleOutlet.stringValue = "3 testes"
                                    }else{
                                        item.labelSubtitleOutlet.stringValue = "1 teste"
                                    }
                                }
                                if isTableViewTag == 2{
                                    if isSelect == true{
                                        if self.selectArray.count > 0{
                                            if self.selectArray[indexPath.item - 1] == indexPath.item - 1{
                                                item.imageViewOutlet.image = NSImage(named: "check-1")
                                            }else{
                                                item.imageViewOutlet.image = NSImage(named: "check-box-empty")
                                            }
                                        }
                                        if (indexPath.item) == isSubMenuSelectedTag{
                                            item.viewOutlet.wantsLayer = true
                                            item.viewOutlet.layer?.backgroundColor = NSColor(hex: "F4F5F9").cgColor
                                        }else{
                                            item.viewOutlet.wantsLayer = true
                                            item.viewOutlet.layer?.backgroundColor = NSColor.white.cgColor
                                        }
                                    }else{
                                        if (indexPath.item) == isSubMenuSelectedTag{
                                            item.imageViewOutlet.image = NSImage(named: "left-arrow")
                                            item.viewOutlet.wantsLayer = true
                                            item.viewOutlet.layer?.backgroundColor = NSColor(hex: "F4F5F9").cgColor
                                            
                                        }else{
                                            item.imageViewOutlet.image = NSImage(named: "right-arrow-1")
                                            item.viewOutlet.wantsLayer = true
                                            item.viewOutlet.layer?.backgroundColor = NSColor.white.cgColor
                                        }
                                    }
                                    if AppLanguage.current == .english{
                                        item.labelTitleOutlet.stringValue = arrayInterectiveTestData[indexPath.item - 1]
                                    }else if AppLanguage.current == .arabic{
                                        item.labelTitleOutlet.stringValue = arrayInterectiveTestDataArabic[indexPath.item - 1]
                                    }else if AppLanguage.current == .spanish{
                                        item.labelTitleOutlet.stringValue = arrayInterectiveTestDataSpanish[indexPath.item - 1]
                                    }else if AppLanguage.current == .portuguese{
                                        item.labelTitleOutlet.stringValue = arrayInterectiveTestDataPort[indexPath.item - 1]
                                    }
                                }
                            }
                            return item
                        } else {
                            return NSCollectionViewItem()
                        }
                    }
                }
            }
        }else{
            if isDeviceInfo == true{
                var item: NSCollectionViewItem
                if #available(macOS 10.11, *) {
                    item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "DeviceInfoCell"), for: indexPath) as! DeviceInfoCell
                    if let item = item as? DeviceInfoCell {
                        
                        
                        
                        
                        
                        let fullNameArr = (CPUandGPUArray[2] as? String)?.components(separatedBy: ":")
                        item.labelModelOutlet.stringValue = "\(fullNameArr?[1] ?? "")"
                        let fullNameArrGPU = (CPUandGPUArray[3] as? String)?.components(separatedBy: ":")
                        item.labelYearOutlet.stringValue = "\(fullNameArrGPU?[1] ?? "")"
                        let fullNameArrCPU = (CPUandGPUArray[4] as? String)?.components(separatedBy: ":")
                        let fullNameArrSpeed = (CPUandGPUArray[5] as? String)?.components(separatedBy: ":")
                        item.labelCPUOutlet.stringValue = "\(fullNameArrSpeed?[1] ?? "") \(fullNameArrCPU?[1] ?? "")"
                        let fullNameArrGPUTest = (CPUandGPUArray[21] as? String)?.components(separatedBy: ":")
                        let fullNameArrVRAM = (CPUandGPUArray[24] as? String)?.components(separatedBy: ":")
                        item.labelGPUOutlet.stringValue = "\(fullNameArrGPUTest?[1] ?? "") \(fullNameArrVRAM?[1] ?? "")"
                        item.labelMemoryOutlet.stringValue = "\(System.physicalMemory())GB"

                        item.labelStorageOutlet.stringValue = "\(DiskStatus.totalDiskSpace)"
                        item.labelOperatingSystemOutlet.stringValue = "Operating System:"
                        item.labelVersionOutlet.stringValue = "Verion:"

                        if AppLanguage.current == .english{
                            item.labelHardwareTitleOutlet.stringValue = "Hardware"
                            item.labelModelTitleOutlet.stringValue = "Model:"
                            item.labelYearTitleOutlet.stringValue = "Year:"
                            item.labelMemoryTitleOutlet.stringValue = "Memory:"
                            item.labelStorageTitleOutlet.stringValue = "Storage:"
                            item.labelSoftwareTitleOutlet.stringValue = "Software"
                            item.labelOperatingSystemTitleOutlet.stringValue = "Operating System:"
                            item.labelVersionTitleOutlet.stringValue = "Verion:"
//                            item.labelModelTitleOutlet.stringValue = ""
//                            item.labelModelTitleOutlet.stringValue = ""
//                            item.labelModelTitleOutlet.stringValue = ""

                        }else if AppLanguage.current == .arabic{
                            item.labelHardwareTitleOutlet.stringValue = "المعدات"
                            item.labelModelTitleOutlet.stringValue = "نموذج:"
                            item.labelYearTitleOutlet.stringValue = "عام:"
                            item.labelMemoryTitleOutlet.stringValue = "ذاكرة:"
                            item.labelStorageTitleOutlet.stringValue = "تخزين:"
                            item.labelSoftwareTitleOutlet.stringValue = "برمجة"
                            item.labelOperatingSystemTitleOutlet.stringValue = "نظام التشغيل:"
                            item.labelVersionTitleOutlet.stringValue = "فيريون:"
                        }else if AppLanguage.current == .spanish{
                            item.labelHardwareTitleOutlet.stringValue = "Hardware"
                            item.labelModelTitleOutlet.stringValue = "Modelo:"
                            item.labelYearTitleOutlet.stringValue = "Año:"
                            item.labelMemoryTitleOutlet.stringValue = "Memoria:"
                            item.labelStorageTitleOutlet.stringValue = "Almacenamiento:"
                            item.labelSoftwareTitleOutlet.stringValue = "Software"
                            item.labelOperatingSystemTitleOutlet.stringValue = "Sistema operativo:"
                            item.labelVersionTitleOutlet.stringValue = "Verión:"
                        }else if AppLanguage.current == .portuguese{
                            item.labelHardwareTitleOutlet.stringValue = "Hardware"
                            item.labelModelTitleOutlet.stringValue = "Modelo:"
                            item.labelYearTitleOutlet.stringValue = "Ano:"
                            item.labelMemoryTitleOutlet.stringValue = "Memória:"
                            item.labelStorageTitleOutlet.stringValue = "Armazenar:"
                            item.labelSoftwareTitleOutlet.stringValue = "Programas"
                            item.labelOperatingSystemTitleOutlet.stringValue = "Sistema operacional:"
                            item.labelVersionTitleOutlet.stringValue = "Verão:"
                        }
                    }
                    return item
                } else {
                    return NSCollectionViewItem()
                }
            }else{
                var item: NSCollectionViewItem
                if #available(macOS 10.11, *) {
                    item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "THWeekItem"), for: indexPath) as! THWeekItem
                    if let item = item as? THWeekItem {
                        
                        
                        item.imageViewBackgroundOutlet.wantsLayer = true
                        item.imageViewBackgroundOutlet.layer?.borderWidth = 0.0
                        item.imageViewBackgroundOutlet.layer?.borderColor = NSColor.lightGray.cgColor
                        item.imageViewBackgroundOutlet.layer?.cornerRadius = 22.0
                        item.imageViewBackgroundOutlet.layer?.masksToBounds = true
                        item.imageViewBackgroundOutlet.wantsLayer = true
                        item.imageViewBackgroundOutlet.shadow = NSShadow()
                        item.imageViewBackgroundOutlet.layer?.shadowOpacity = 1.0
                        item.imageViewBackgroundOutlet.layer?.shadowColor = NSColor.lightGray.cgColor
                        item.imageViewBackgroundOutlet.layer?.shadowOffset = NSMakeSize(0, 0)
                        item.imageViewBackgroundOutlet.layer?.shadowRadius = 4
                        item.imageViewOutlet.image = arrayTestData[indexPath.item]["image"]
                        as? NSImage
                        var buttonTitleString = ""
                        if AppLanguage.current == .english{
                            item.labelTitleOutlet.stringValue = arrayTestData[indexPath.item]["name"] as? String ?? ""
                            buttonTitleString = self.buttonRunTitleString
                        }else if AppLanguage.current == .arabic{
                            item.labelTitleOutlet.stringValue = arrayTestDataArabic[indexPath.item]["name"] as? String ?? ""
                            buttonTitleString = self.buttonRunTitleString
                        }else if AppLanguage.current == .spanish{
                            item.labelTitleOutlet.stringValue = arrayTestDataSpanish[indexPath.item]["name"] as? String ?? ""
                            buttonTitleString = self.buttonRunTitleString
                        }else if AppLanguage.current == .portuguese{
                            item.labelTitleOutlet.stringValue = arrayTestDataPort[indexPath.item]["name"] as? String ?? ""
                            buttonTitleString = self.buttonRunTitleString
                        }
                        if indexPath.item == 1 {
                            if AppLanguage.current == .english{
                                item.labelSubtitleOutlet.stringValue = "7 Test | Approx, 4 Mins"
                            }else if AppLanguage.current == .arabic{
                                item.labelSubtitleOutlet.stringValue = "7 اختبار | حوالي 4 دقائق"
                            }else if AppLanguage.current == .spanish{
                                item.labelSubtitleOutlet.stringValue = "7 Pruebas | Aproximadamente, 4 Minutos"
                            }else if AppLanguage.current == .portuguese{
                                item.labelSubtitleOutlet.stringValue = "7 Teste | Aprox, 4 Min"
                            }
                        }else if indexPath.item == 2{
                            if AppLanguage.current == .english{
                                item.labelSubtitleOutlet.stringValue = "7 Test | Approx, 5 Mins"
                            }else if AppLanguage.current == .arabic{
                                item.labelSubtitleOutlet.stringValue = "7 اختبار | تقريبًا ، 5 دقائق"
                            }else if AppLanguage.current == .spanish{
                                item.labelSubtitleOutlet.stringValue = "7 Prueba | Aprox, 5 Min"
                            }else if AppLanguage.current == .portuguese{
                                item.labelSubtitleOutlet.stringValue = "7 Teste | Aprox, 5 Min"
                            }
                        }else if indexPath.item == 5 || indexPath.item == 6{
                            if indexPath.item == 5 {
                                if AppLanguage.current == .english{
                                    item.labelSubtitleOutlet.stringValue = "1 Test | Approx, 1 Min"
                                }else if AppLanguage.current == .arabic{
                                    item.labelSubtitleOutlet.stringValue = "اختبار واحد | تقريبًا ، دقيقة واحدة"
                                }else if AppLanguage.current == .spanish{
                                    item.labelSubtitleOutlet.stringValue = "1 prueba | Aproximadamente, 1 minuto"
                                }else if AppLanguage.current == .portuguese{
                                    item.labelSubtitleOutlet.stringValue = "1 Teste | Aprox, 1 Min"
                                }
                            }else{
                                if AppLanguage.current == .english{
                                    item.labelSubtitleOutlet.stringValue = "3 Test | Approx, 1 Min"
                                }else if AppLanguage.current == .arabic{
                                    item.labelSubtitleOutlet.stringValue = "3 اختبار | تقريبًا ، دقيقة واحدة"
                                }else if AppLanguage.current == .spanish{
                                    item.labelSubtitleOutlet.stringValue = "3 pruebas | Aproximadamente, 1 minuto"
                                }else if AppLanguage.current == .portuguese{
                                    item.labelSubtitleOutlet.stringValue = "3 Teste | Aprox, 1 Min"
                                }
                            }
                        }else{
                            if AppLanguage.current == .english{
                                item.labelSubtitleOutlet.stringValue = "1 Test | Approx, 20 Secs"
                            }else if AppLanguage.current == .arabic{
                                item.labelSubtitleOutlet.stringValue = "اختبار واحد | حوالي 20 ثانية"
                            }else if AppLanguage.current == .spanish{
                                item.labelSubtitleOutlet.stringValue = "1 prueba | Aproximadamente, 20 segundos"
                            }else if AppLanguage.current == .portuguese{
                                item.labelSubtitleOutlet.stringValue = "1 teste | Aprox, 20 segundos"
                            }
                        }
                        item.buttonOutlet.bezelStyle = .texturedSquare
                        item.buttonOutlet.isBordered = false //Important
                        item.buttonOutlet.wantsLayer = true
                        item.buttonOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                        item.buttonOutlet.layer?.borderWidth = 0.0
                        item.buttonOutlet.layer?.cornerRadius = 6
                        
                        let pstyle = NSMutableParagraphStyle()
                        pstyle.alignment = .center
                        item.buttonOutlet.attributedTitle = NSAttributedString(string: buttonTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor(hex: SERVER.darkBlue), NSAttributedString.Key.paragraphStyle : pstyle ])
                        
                        item.buttonOutlet.tag = indexPath.item
                        item.buttonOutlet.action = #selector(buttonHomeAction)
                        
                    }
                    return item
                } else {
                    return NSCollectionViewItem()
                }
            }
        }
    }
    @objc func buttonDropDownStartAction(button:NSButton) {
        if SelectArrayString.count > 0 && SelectArrayString[0] != ""{
            if isTestStart == false{
                //                self.isTestStart = true
                print("timerAction stop ---------->>>>>>>>>>>>>>>>>>>>>>>>")
                if SelectArrayString.count > 0{
                    for i in 0..<self.SelectArrayString.count{
                        if self.SelectArrayString[i] == "Keyboard" || self.SelectArrayString[i] == "اختبار لوحة المفاتيح" || self.SelectArrayString[i] == "Teclado" || self.SelectArrayString[i] == "Teclado"{
                            if let monitor = eventMonitor {
                                NSEvent.stopPeriodicEvents()
                                NSEvent.removeMonitor(monitor)
                            }
                            eventMonitor = NSEvent.addLocalMonitorForEvents(matching: [.keyDown, .keyUp, .flagsChanged]) { [unowned self] event in
                                self.onKeyDown(with: event)
                                return event
                            }
                            self.viewPixelsOutlet.wantsLayer = false
                            self.isPixelsTestStart = false
                            self.viewPixelsOutlet.isHidden = true
                            self.isSubMenuSelectedTag = 1
                            self.collectionViewSubmenu.reloadData()
                            self.totalTime = 60
                            self.viewChargerOutlet.isHidden = false
                            self.collectionViewTest.isHidden = false
                            self.interectiveTestIndex = 1
                            self.keyboardTestStatusID = ""
                            self.isChargerStart = 0
                            self.collectionViewTest.reloadData()
                            self.configureCollectionView(tag: 1)
                            self.isKeyboardTest = true
                            self.collectionViewSubmenu.isSelectable = false
                            self.isChargerStart = 1
                            self.audioMeter.isHidden = true
                            timer.invalidate() // just in case this button is tapped multiple times
                            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
                            if let monitor = eventMonitor {
                                NSEvent.stopPeriodicEvents()
                                NSEvent.removeMonitor(monitor)
                            }
                            eventMonitor = NSEvent.addLocalMonitorForEvents(matching: [.keyDown, .keyUp, .flagsChanged]) { [unowned self] event in
                                self.onKeyDown(with: event)
                                return event
                            }
                            self.collectionViewTest.reloadData()
                            break
                        }else if self.SelectArrayString[i] == "Charging Port Test" || self.SelectArrayString[i] == "اختبار منفذ الشحن" || self.SelectArrayString[i] == "Prueba del puerto de carga" || self.SelectArrayString[i] == "Teste da porta de carregamento"{
                            if ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Type") as? String) == "InternalBattery" || ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Transport Type") as? String) == "Internal" || ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Name") as? String) == "InternalBattery-0"{
                                self.isSubMenuSelectedTag = 2
                                self.collectionViewSubmenu.reloadData()
                                self.counter = 0
                                self.totalTime = 20
                                self.viewChargerOutlet.isHidden = false
                                self.collectionViewTest.isHidden = false
                                self.interectiveTestIndex = 3
                                self.keyboardTestStatusID = ""
                                self.isChargerStart = 0
                                self.collectionViewTest.reloadData()
                                self.collectionViewSubmenu.isSelectable = false
                                self.isKeyboardTest = true
                                self.isChargerStart = 1
                                self.audioMeter.isHidden = true
                                timer.invalidate() // just in case this button is tapped multiple times
                                timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(chargerTestTimerAction), userInfo: nil, repeats: true)
                            }else{
                                self.confirmAbletonIsReady(question: "Error", text: "Device has no charger port.")
                                if SelectArrayString.count > 0{
                                    self.SelectArrayString.remove(at: 0)
                                }
                                self.buttonDropDownStartAction(button: NSButton())
                            }
                            break
                        }else if self.SelectArrayString[i] == "Webcam" || self.SelectArrayString[i] == "Webcam" || self.SelectArrayString[i] == "Webcam" || self.SelectArrayString[i] == "Cámara web"{
                            guard (AVCaptureDevice.default(for: AVMediaType.video)) != nil else {
                                self.confirmAbletonIsReady(question: "Error", text: "Device has no camera.")
                                if SelectArrayString.count > 0{
                                    self.SelectArrayString.remove(at: 0)
                                }
                                self.buttonDropDownStartAction(button: NSButton())
                                return
                            }
                            self.isSubMenuSelectedTag = 3
                            self.collectionViewSubmenu.reloadData()
                            self.counter = 0
                            self.totalTime = 20
                            self.viewChargerOutlet.isHidden = false
                            self.collectionViewTest.isHidden = false
                            self.interectiveTestIndex = 4
                            self.keyboardTestStatusID = ""
                            self.isChargerStart = 0
                            self.configureCollectionView(tag: 4)
                            self.collectionViewTest.reloadData()
                            self.isKeyboardTest = true
                            self.collectionViewSubmenu.isSelectable = false
                            session.startRunning()
                            self.isChargerStart = 1
                            timer.invalidate() // just in case this button is tapped multiple times
                            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(webcamTestTimerAction), userInfo: nil, repeats: true)
                            startSession()
                            self.prepareCamera()
                            self.audioMeter.isHidden = true
                            self.collectionViewTest.reloadData()
                            break
                        }else if self.SelectArrayString[i] == "Volume" || self.SelectArrayString[i] == "Volumen" || self.SelectArrayString[i] == "Volumen" || self.SelectArrayString[i] == "Volume"{
                            self.isSubMenuSelectedTag = 4
                            self.collectionViewSubmenu.reloadData()
                            self.counter = 0
                            self.totalTime = 20
                            self.viewChargerOutlet.isHidden = false
                            self.collectionViewTest.isHidden = false
                            self.interectiveTestIndex = 5
                            self.keyboardTestStatusID = ""
                            self.isChargerStart = 0
                            self.audioMeter.isHidden = true
                            self.configureCollectionView(tag: 5)
                            self.isKeyboardTest = true
                            self.collectionViewSubmenu.isSelectable = false
                            self.isChargerStart = 1
                            timer.invalidate() // just in case this button is tapped multiple times
                            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(volumeTestTimerAction), userInfo: nil, repeats: true)
                            self.open_audiofile(testName: "", tag: -1)
                            self.collectionViewTest.reloadData()
                            break
                        }else if self.SelectArrayString[i] == "Microphone" || self.SelectArrayString[i] == "Micrófono" || self.SelectArrayString[i] == "Micrófono" || self.SelectArrayString[i] == "Microfone"{
                            self.isSubMenuSelectedTag = 5
                            self.collectionViewSubmenu.reloadData()
                            self.counter = 0
                            self.totalTime = 10
                            self.viewChargerOutlet.isHidden = false
                            self.collectionViewTest.isHidden = false
                            self.interectiveTestIndex = 6
                            self.audioMeter.isHidden = false
                            self.keyboardTestStatusID = ""
                            self.isChargerStart = 0
                            self.configureCollectionView(tag: 6)
                            self.collectionViewTest.reloadData()
                            self.isKeyboardTest = true
                            self.collectionViewSubmenu.isSelectable = false
                            self.isChargerStart = 1
                            timer.invalidate() // just in case this button is tapped multiple times
                            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(micTestTimerAction), userInfo: nil, repeats: true)
                            print("start recording")
                            self.audioControl?.startRecord()
                            isRecording = true
                            setupUpdateMeter()
                            self.collectionViewTest.reloadData()
                            break
                        }else if self.SelectArrayString[i] == "Defective Pixels" || self.SelectArrayString[i] == "Píxeles defectuosos" || self.SelectArrayString[i] == "Píxeles defectuosos" || self.SelectArrayString[i] == "Pixels defeituosos"{
                            self.isSubMenuSelectedTag = 6
                            self.collectionViewSubmenu.reloadData()
                            self.isPixelsTestStart = true
                            self.counter = 0
                            self.totalTime = 20
                            self.viewPixelsOutlet.isHidden = true
                            self.viewChargerOutlet.isHidden = false
                            self.collectionViewTest.isHidden = false
                            self.interectiveTestIndex = 7
                            self.keyboardTestStatusID = ""
                            self.isChargerStart = 0
                            self.configureCollectionView(tag: 7)
                            self.isKeyboardTest = true
                            self.isChargerStart = 1
                            self.audioMeter.isHidden = true
                            self.collectionViewSubmenu.isSelectable = false
                            timer.invalidate() // just in case this button is tapped multiple times
                            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(DisplayPixelsTestTimerAction), userInfo: nil, repeats: true)
                            self.viewPixelsOutlet.wantsLayer = true
                            self.viewPixelsOutlet.isHidden = false
                            eventMonitor = NSEvent.addLocalMonitorForEvents(matching: .keyDown) { [unowned self] event in
                                self.onKeyDown(with: event)
                                return event
                            }
                            useNextColor()
                            self.collectionViewTest.reloadData()
                            break
                        }else if self.SelectArrayString[i] == "Audio Test" || self.SelectArrayString[i] == "اختبار الصوت" || self.SelectArrayString[i] == "Prueba de sonido" || self.SelectArrayString[i] == "Teste de áudio"{
                            let url = Bundle.main.url(forResource: "sound", withExtension: "mp3")
                            let audioFile = try! AVAudioFile(forReading: url!)
                            if audioFile.fileFormat.channelCount > 0 {
                                self.isSubMenuSelectedTag = 7
                                self.collectionViewSubmenu.reloadData()
                                self.isPixelsTestStart = true
                                self.counter = 0
                                self.totalTime = 60
                                self.viewPixelsOutlet.isHidden = true
                                self.viewChargerOutlet.isHidden = false
                                self.collectionViewTest.isHidden = false
                                self.interectiveTestIndex = 8
                                self.keyboardTestStatusID = ""
                                self.isChargerStart = 0
                                self.configureCollectionView(tag: 8)
                                self.isKeyboardTest = true
                                self.collectionViewSubmenu.isSelectable = false
                                self.isChargerStart = 1
                                self.audioTestIndex = 0
                                self.audioMeter.isHidden = true
                                timer.invalidate() // just in case this button is tapped multiple times
                                timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAudioAction), userInfo: nil, repeats: true)
                                audioEngine.stop()
                                audioPlayer.stop()
                                self.open_audiofile(testName: "audio", tag: 0)
                                self.collectionViewTest.reloadData()
                                break
                            }else{
                                self.confirmAbletonIsReady(question: "Error", text: "System does not have speakers.")
                                if SelectArrayString.count > 0{
                                    self.SelectArrayString.remove(at: 0)
                                }
                                self.buttonDropDownStartAction(button: NSButton())
                                
                            }
                        }
                    }
                }
                self.collectionViewSubmenu.reloadData()
                
                //                self.collectionViewTest.reloadData()
            }else{
                //                self.isTestStart = false
                self.audioMeter.isHidden = true
                self.timer.invalidate()
                self.collectionViewSubmenu.reloadData()
            }
        }else{
            self.audioMeter.isHidden = true

        }
        print("Counter is ---------->>>>>>>>>>>>>>>>>>>>>>>>",counter)
    }
    
    @objc func buttonDropDownAction(button:NSPopUpButton) {
        self.dropDownIndex = button.indexOfSelectedItem
        button.selectItem(at: button.indexOfSelectedItem)
        if button.indexOfSelectedItem == 0{
            //            if AppLanguage.current == .english{
            button.setTitle("Select")
            //            }else if AppLanguage.current == .arabic{
            //                button.setTitle("يختار")
            //            }else if AppLanguage.current == .spanish{
            //                button.setTitle("Seleccione")
            //            }else if AppLanguage.current == .portuguese{
            //                button.setTitle("Selecionar")
            //            }
            self.SelectArrayString.removeAll()
            self.currentSelection = -1
            for i in 0..<self.arrayInterectiveTestData.count{
                self.selectArray.insert(-1, at: i)
            }
            self.isMultipleTestSelect = true
            self.isSelect = true
            self.isSubMenuSelectedTag = 1
            self.collectionViewSubmenu.reloadData()
        }else if button.indexOfSelectedItem == 1{
//            if AppLanguage.current == .english{
                button.setTitle("Select All")
//            }else if AppLanguage.current == .arabic{
//                button.setTitle("اختر الكل")
//            }else if AppLanguage.current == .spanish{
//                button.setTitle("Seleccionar todo")
//            }else if AppLanguage.current == .portuguese{
//                button.setTitle("Selecionar tudo")
//            }
//            button.setTitle("Select All")
            self.selectArray.removeAll()
            self.SelectArrayString.removeAll()
            for i in 0..<self.arrayInterectiveTestData.count{
                self.selectArray.insert(i, at: i)
                self.SelectArrayString.insert(self.arrayInterectiveTestData[i], at: i)
                print("The Test Name IS ------>>>>>>",self.arrayInterectiveTestData[i])
                
            }
            self.isMultipleTestSelect = true
            self.currentSelection = 1
            self.isSelect = true
            self.isSubMenuSelectedTag = 1
            self.collectionViewSubmenu.reloadData()
        }else if button.indexOfSelectedItem == 2{
//            if AppLanguage.current == .english{
                button.setTitle("Unselect")
//            }else if AppLanguage.current == .arabic{
//                button.setTitle("إلغاء التحديد")
//            }else if AppLanguage.current == .spanish{
//                button.setTitle("Deseleccionar")
//            }else if AppLanguage.current == .portuguese{
//                button.setTitle("Desmarcar")
//            }
            self.currentSelection = -1
            self.isSelect = false
            self.SelectArrayString.removeAll()
            self.isMultipleTestSelect = false
            //            if self.isNonInteractive == true{
            //                self.buttonHeightConstraint.constant = 50
            //                self.tableViewBottomConstraint.constant = 20
            //                self.buttonStartOutlet.isHidden = false
            //            }else{
            //                self.buttonHeightConstraint.constant = 0
            //                self.tableViewBottomConstraint.constant = -20
            //                self.buttonStartOutlet.isHidden = true
            //            }
            self.isSubMenuSelectedTag = 1
            self.collectionViewSubmenu.reloadData()
        }
    }
    @objc func buttonHomeAction(button:NSButton) {
        self.labelHeaderEmailTitleOutlet.stringValue = ""
        self.labelHeaderTitleOutlet.stringValue = ""
        self.audioMeter.isHidden = true
        isTableViewTag = 2
        collectionViewSubmenu.isHidden = false
        collectionView.isHidden = true
        collectionViewWidthConstraint.constant = 240
        self.collectionViewSubmenu.reloadData()
        if button.tag == 0{
            self.isPixelsTestStart = true
            self.totalTime = 20
            self.viewPixelsOutlet.isHidden = true
            self.viewChargerOutlet.isHidden = false
            self.collectionViewTest.isHidden = false
            self.interectiveTestIndex = 7
            self.keyboardTestStatusID = ""
            self.isChargerStart = 0
            self.configureCollectionView(tag: 7)
            self.isSelectIndex = 2
            self.isSubMenuSelectedTag = 5
            self.collectionViewSubmenu.reloadData()
            self.tableView.reloadData()
            self.collectionViewTest.reloadData()
        }else if button.tag == 1{
            isTableViewTag = 2
            collectionViewSubmenu.isHidden = false
            collectionView.isHidden = true
            collectionViewWidthConstraint.constant = 240
            self.collectionViewSubmenu.reloadData()
            self.isSelectIndex = 2
            self.tableView.reloadData()
            self.isSubMenuSelectedTag = 0
            self.collectionViewSubmenu.reloadData()
        }else if button.tag == 2 {
            self.timer.invalidate()
            isTableViewTag = 1
            self.isSelectIndex = 3
            collectionViewSubmenu.isHidden = false
            collectionView.isHidden = true
            self.viewChargerOutlet.isHidden = true
            self.collectionViewTest.isHidden = true
            collectionViewWidthConstraint.constant = 240
            self.startNonInterectiveTest()
            self.isSubMenuSelectedTag = 0
            self.nonInterectiveCounter = 0
            self.interectiveTestIndex = 9
            self.isSelectIndex = 3
            self.keyboardTestStatusID = ""
            self.isChargerStart = 0
            self.startNonInterectiveTest()
            self.isSubMenuSelectedTag = 0
            self.collectionViewSubmenu.reloadData()
            self.tableView.reloadData()

        }else if button.tag == 4{
            if ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Type") as? String) == "InternalBattery" || ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Transport Type") as? String) == "Internal" || ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Name") as? String) == "InternalBattery-0"{
                self.timer.invalidate()
                isTableViewTag = 1
                self.isSelectIndex = 3
                collectionViewSubmenu.isHidden = false
                collectionView.isHidden = true
                collectionViewWidthConstraint.constant = 240
                self.startNonInterectiveTest()
                self.isSubMenuSelectedTag = 0
                self.tableView.reloadData()
                self.collectionViewSubmenu.reloadData()
                self.viewChargerOutlet.isHidden = true
                self.collectionViewTest.isHidden = true
                self.nonInterectiveCounter = 0
                self.interectiveTestIndex = 9
                self.isSelectIndex = 3
                self.keyboardTestStatusID = ""
                self.isChargerStart = 0
                self.collectionViewSubmenu.reloadData()
            }else{
                self.confirmAbletonIsReady(question: "Error", text: "Device has no battery.")
            }
        }else if button.tag == 3{
            if ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Type") as? String) == "InternalBattery" || ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Transport Type") as? String) == "Internal" || ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Name") as? String) == "InternalBattery-0"{
                self.totalTime = 20
                self.viewChargerOutlet.isHidden = false
                self.collectionViewTest.isHidden = false
                self.interectiveTestIndex = 3
                self.keyboardTestStatusID = ""
                self.isChargerStart = 0
                self.isSelectIndex = 2
                self.tableView.reloadData()
                self.collectionViewTest.reloadData()
                self.isSubMenuSelectedTag = 1
                self.collectionViewSubmenu.reloadData()
            }else{
                self.confirmAbletonIsReady(question: "Error", text: "Device has no charger port.")
            }
        }else if button.tag == 5{
            
            self.isPixelsTestStart = false
            self.totalTime = 60
            self.viewChargerOutlet.isHidden = false
            self.collectionViewTest.isHidden = false
            self.interectiveTestIndex = 1
            self.keyboardTestStatusID = ""
            self.isChargerStart = 0
            self.isSubMenuSelectedTag = 0
            self.collectionViewSubmenu.reloadData()
            self.collectionViewTest.reloadData()
            self.configureCollectionView(tag: 1)
        }else if button.tag == 6{
            let url = Bundle.main.url(forResource: "sound", withExtension: "mp3")
            let audioFile = try! AVAudioFile(forReading: url!)
            if audioFile.fileFormat.channelCount > 0 {
                self.isPixelsTestStart = true
                self.totalTime = 60
                self.viewPixelsOutlet.isHidden = true
                self.viewChargerOutlet.isHidden = false
                self.collectionViewTest.isHidden = false
                self.interectiveTestIndex = 8
                self.keyboardTestStatusID = ""
                self.isChargerStart = 0
                self.configureCollectionView(tag: 8)
                self.collectionViewTest.reloadData()
                self.isSubMenuSelectedTag = 7
                self.isSelectIndex = 2
                self.tableView.reloadData()
                self.collectionViewSubmenu.reloadData()
            }else{
                self.confirmAbletonIsReady(question: "Error", text: "System does not have speakers.")
            }
        }
        print("Clicked \(button.title)!")
    }
    
    func collectionView(_ collectionView: NSCollectionView, didSelectItemsAt indexPaths: Set<IndexPath>) {
        self.counter = 0
        self.collectionViewTest.reloadData()
        self.timer.invalidate()
        if self.isKeyboardTest == true{
            
        }else{
            
            if collectionView == collectionViewSubmenu{
                if isTableViewTag == 2{
                    if self.isSelect == true{
                        if self.selectArray[(indexPaths.first?.item ?? 0) - 1] == (indexPaths.first!.item - 1){
                            self.selectArray.remove(at: (indexPaths.first?.item ?? 0) - 1)
                            self.selectArray.insert(-1, at: (indexPaths.first?.item ?? 0) - 1)
                            if SelectArrayString.count > 0{
                                for i in 0..<SelectArrayString.count{
                                    if i < SelectArrayString.count{
                                        if arrayInterectiveTestData[(indexPaths.first?.item ?? 0) - 1] == SelectArrayString[i]{
                                            self.SelectArrayString.remove(at: i)
                                        }
                                    }
                                }
                            }
                        }else{
                            self.selectArray.remove(at: (indexPaths.first?.item ?? 0) - 1)
                            self.selectArray.insert((indexPaths.first?.item ?? 0) - 1, at: (indexPaths.first?.item ?? 0) - 1)
                            self.SelectArrayString.append(self.arrayInterectiveTestData[(indexPaths.first?.item ?? 0) - 1])
                            
                        }
                        self.collectionViewSubmenu.reloadData()
                        
                        
                    }else{
                        if ((indexPaths.first?.item ?? 0)) != 0 {
                            self.isSubMenuSelectedTag = ((indexPaths.first?.item ?? 0 ))
                            self.collectionViewSubmenu.reloadData()
                            if indexPaths.first?.item == 1{
                                self.totalTime = 60
                                self.viewChargerOutlet.isHidden = false
                                self.collectionViewTest.isHidden = false
                                self.interectiveTestIndex = 1
                                self.keyboardTestStatusID = ""
                                self.isChargerStart = 0
                                self.audioMeter.isHidden = true
                                self.collectionViewTest.reloadData()
                                self.configureCollectionView(tag: 1)
                            }else if indexPaths.first?.item == 2{
                                if ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Type") as? String) == "InternalBattery" || ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Transport Type") as? String) == "Internal" || ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Name") as? String) == "InternalBattery-0"{
                                    self.totalTime = 20
                                    self.viewChargerOutlet.isHidden = false
                                    self.collectionViewTest.isHidden = false
                                    self.interectiveTestIndex = 3
                                    self.keyboardTestStatusID = ""
                                    self.isChargerStart = 0
                                    self.audioMeter.isHidden = true
                                    self.collectionViewTest.reloadData()
                                }else{
                                    self.confirmAbletonIsReady(question: "Error", text: "Device has no charger port.")
                                }
                            }else if indexPaths.first?.item == 3{
                                guard (AVCaptureDevice.default(for: AVMediaType.video)) != nil else {
                                    self.confirmAbletonIsReady(question: "Error", text: "Device has no camera.")
                                    return
                                }
                                self.totalTime = 20
                                self.viewChargerOutlet.isHidden = false
                                self.collectionViewTest.isHidden = false
                                self.interectiveTestIndex = 4
                                self.keyboardTestStatusID = ""
                                self.isChargerStart = 0
                                self.audioMeter.isHidden = true
                                self.configureCollectionView(tag: 4)
                                self.collectionViewTest.reloadData()
                            }else if indexPaths.first?.item == 4{
                                self.totalTime = 20
                                self.viewChargerOutlet.isHidden = false
                                self.collectionViewTest.isHidden = false
                                self.interectiveTestIndex = 5
                                self.keyboardTestStatusID = ""
                                self.isChargerStart = 0
                                self.audioMeter.isHidden = true
                                self.configureCollectionView(tag: 5)
                                self.collectionViewTest.reloadData()
                            }else if indexPaths.first?.item == 5{
                                self.totalTime = 10
                                self.viewChargerOutlet.isHidden = false
                                self.collectionViewTest.isHidden = false
                                self.interectiveTestIndex = 6
                                self.keyboardTestStatusID = ""
                                self.isChargerStart = 0
                                self.audioMeter.isHidden = false
                                self.configureCollectionView(tag: 6)
                                self.collectionViewTest.reloadData()
                            }else if indexPaths.first?.item == 6{
                                self.isPixelsTestStart = true
                                self.totalTime = 20
                                self.viewPixelsOutlet.isHidden = true
                                self.viewChargerOutlet.isHidden = false
                                self.collectionViewTest.isHidden = false
                                self.interectiveTestIndex = 7
                                self.keyboardTestStatusID = ""
                                self.isChargerStart = 0
                                self.audioMeter.isHidden = true
                                self.configureCollectionView(tag: 7)
                                self.collectionViewTest.reloadData()
                            }else if indexPaths.first?.item == 7{
                                let url = Bundle.main.url(forResource: "sound", withExtension: "mp3")
                                let audioFile = try! AVAudioFile(forReading: url!)
                                if audioFile.fileFormat.channelCount > 0 {
                                    self.isPixelsTestStart = true
                                    self.totalTime = 60
                                    self.viewPixelsOutlet.isHidden = true
                                    self.viewChargerOutlet.isHidden = false
                                    self.collectionViewTest.isHidden = false
                                    self.interectiveTestIndex = 8
                                    self.keyboardTestStatusID = ""
                                    self.isChargerStart = 0
                                    self.audioMeter.isHidden = true
                                    self.configureCollectionView(tag: 8)
                                    self.collectionViewTest.reloadData()
                                }else{
                                    self.confirmAbletonIsReady(question: "Error", text: "System does not have speakers.")
                                }
                            }else{
                                self.audioMeter.isHidden = true
                                self.viewChargerOutlet.isHidden = false
                                self.collectionViewTest.isHidden = false
                            }
                        }
                    }

                }else if isTableViewTag == 3 || isTableViewTag == 4{
                    interectiveTestIndex = 20
                    self.viewPixelsOutlet.isHidden = true
                    self.viewChargerOutlet.isHidden = false
                    self.collectionViewTest.isHidden = false
                    self.keyboardTestStatusID = ""
                    self.isChargerStart = 0
                    self.audioMeter.isHidden = true
                    self.configureCollectionView(tag: 20)
                    if isTableViewTag == 3 {
                        if Reachability.isConnectedToNetwork(){
                            DispatchQueue.main.async {
                                if indexPaths.first?.item != 0{
                                    self.getTestResultAPI(testTypeId: ((self.testTypeArray[(indexPaths.first?.item ?? 0) - 1] as? NSDictionary)?.value(forKey: "Id") as? Int ?? 0))
                                }
                            }
                        }
                    }else{
                        if Reachability.isConnectedToNetwork(){
                            DispatchQueue.main.async {
                                self.getDeviceDiagnosticTestResultsAPI()
                            }
                        }else{
                            if AppLanguage.current == .english{
                                self.confirmAbletonIsReady(question: "Error", text: "Please Check Your Internet Connection Or Wifi")
                            }else if AppLanguage.current == .arabic{
                                self.confirmAbletonIsReady(question: "خطأ", text: "يرجى التحقق من اتصالك بالإنترنت أو شبكة Wifi")
                            }else if AppLanguage.current == .spanish{
                                self.confirmAbletonIsReady(question: "Error", text: "Por favor, compruebe su conexión a Internet o Wifi")
                            }else if AppLanguage.current == .portuguese{
                                self.confirmAbletonIsReady(question: "Erro", text: "Por favor, verifique sua conexão com a Internet ou Wi-Fi")
                            }
                        }
                    }
                }else{
                    self.audioMeter.isHidden = true
                    self.startNonInterectiveTest()
                    self.isSubMenuSelectedTag = indexPaths.first?.item ?? -1
                    self.collectionViewSubmenu.reloadData()
                }
                
            }else if collectionView == collectionViewTest{
                self.audioMeter.isHidden = true

            }else{
                if indexPaths.first?.item == 2{
                    isTableViewTag = 2
                    collectionViewSubmenu.isHidden = false
                    collectionView.isHidden = true
                    collectionViewWidthConstraint.constant = 240
                    collectionViewWidthConstraint.constant = 0
                    self.collectionViewSubmenu.reloadData()
                }else if indexPaths.first?.item == 3{
                    isTableViewTag = 1
                    collectionViewSubmenu.isHidden = false
                    collectionView.isHidden = true
                    collectionViewWidthConstraint.constant = 240
                    self.collectionViewSubmenu.reloadData()
                }else{
                    isTableViewTag = 0
                    collectionViewSubmenu.isHidden = true
                    collectionView.isHidden = false
                    collectionViewWidthConstraint.constant = 0
                }
            }
        }
    }
    
    fileprivate func configureCollectionView(tag:Int) {
        // 1
        let flowLayout = NSCollectionViewFlowLayout()
        if tag == 4{
            flowLayout.itemSize = NSSize(width: (NSScreen.main?.frame.width ?? 500) - 490, height: 650)
            
        }else if tag == 5{
            flowLayout.itemSize = NSSize(width: (NSScreen.main?.frame.width ?? 500) - 490, height: 310)
            
        }else if tag == 1{
            flowLayout.itemSize = NSSize(width: (NSScreen.main?.frame.width ?? 500) - 490, height: 700)
            
        }else if tag == 25{
            flowLayout.itemSize = NSSize(width: (NSScreen.main?.frame.width ?? 500) - 490, height: 700)
        }else if tag == 27{
            flowLayout.itemSize = NSSize(width: (NSScreen.main?.frame.width ?? 500) - 490, height: 700)
        }else if tag == 6{
            flowLayout.itemSize = NSSize(width: (NSScreen.main?.frame.width ?? 500) - 490, height: 450)
            
        }else if tag == 7{
            flowLayout.itemSize = NSSize(width: (NSScreen.main?.frame.width ?? 500) - 490, height: 250)
            
        }else if tag == 8{
            flowLayout.itemSize = NSSize(width: (NSScreen.main?.frame.width ?? 500) - 490, height: 550)
            
        }else if tag == 9{
            flowLayout.itemSize = NSSize(width: (NSScreen.main?.frame.width ?? 500) - 490, height: 550)
            
        }else if tag == 20{
            flowLayout.itemSize = NSSize(width: (NSScreen.main?.frame.width ?? 500) - 490, height: 70)
            
        }else{
//            if isDeviceInfo == true{
//                flowLayout.itemSize = NSSize(width: (NSScreen.main?.frame.width ?? 500) - 490, height: 700)
//            }else{
                flowLayout.itemSize = NSSize(width: (NSScreen.main?.frame.width ?? 500) - 490, height: 250)
//            }
        }
        flowLayout.minimumInteritemSpacing = 0.0
        flowLayout.minimumLineSpacing = 0.0
        collectionViewTest.collectionViewLayout = flowLayout
    }
    
    fileprivate func configureSideMenuCollectionView() {
        let flowLayout = NSCollectionViewFlowLayout()
        flowLayout.itemSize = NSSize(width: 240, height: 70)
        flowLayout.minimumInteritemSpacing = 0.0
        flowLayout.minimumLineSpacing = 0.0
        collectionViewSubmenu.collectionViewLayout = flowLayout
    }
}
extension HomeVC{
    // MARK: - Keyboard Test
    private func onKeyDown(with event: NSEvent) {
        if isPixelsTestStart == true{// for pixels test
            self.collectionViewSubmenu.isSelectable = false
            super.keyUp(with: event)
            if let key = event.specialKey {
                switch key {
                case .leftArrow, .downArrow:
                    usePreviousColor()
                case .rightArrow, .upArrow, .enter, .carriageReturn:
                    useNextColor()
                    
                default:
                    break
                }
            }
            if event.keyCode == kVK_Space {
                useNextColor()
            }
            if event.keyCode == kVK_Escape {
                self.timer.invalidate()
                self.viewPixelsOutlet.isHidden = true
                self.collectionViewSubmenu.isSelectable = true
                self.isPixelsTestFinish = true
                self.collectionViewTest.reloadData()
            }
        }else{
            var keyViews = allKeyViews().filter({ $0.keyCode == event.keyCode })
            if keyViews.isEmpty {
                let keyView = addKeyView(for: event)
                if keyView.keyLabel == "esc"{
                }else{
                    keyViews.append(keyView)
                }
            }else{
                if keyViews.count == 61{
                    keyViews.removeAll()
                }
            }
            keyViews.forEach { keyView in
                if keyboardKeyArray.contains(keyView.keyLabel ?? ""){
                }else{
                    keyboardKeyArray.append(keyView.keyLabel ?? "")
                }
                self.collectionViewTest.reloadData()
                print("--------->>>>>>>>>>>,keyboardKeyArray",keyboardKeyArray)
                if event.type == .flagsChanged, let flag = keyView.key.modifierFlag {
                    keyView.pressed = event.modifierFlags.contains(flag)
                } else {
                    keyView.pressed = (event.type == .keyDown)
                }
            }
        }
    }
    
    private func allKeyViews(rootView: NSView? = nil) -> [KeyView] {
        if isPixelsTestStart == true{// for pixels test
            var result = [KeyView]()
            return result
        }else{
            var result = [KeyView]()
            if result.count == 61{
                result.removeAll()
            }
            (rootView ?? view).subviews.forEach { subview in
                if let keyView = subview as? KeyView {
                    if keyView.keyLabel == "esc"{
                        
                    }else{
                        result.append(keyView)
                    }
                } else {
                    result.append(contentsOf: allKeyViews(rootView: subview))
                }
            }
            
            return result
        }
    }
    
    private func addKeyView(for event: NSEvent) -> KeyView {
        var keyName = Key(code: Int(event.keyCode)).name
        if keyName == "esc"{
            return KeyView()
        }
        if keyName == nil && event.type != .flagsChanged {
            keyName = event.charactersIgnoringModifiers
        }
        
        let keyView = KeyView(frame: .zero)
        keyView.keyLabel = keyName
        keyView.keyCode = Int(event.keyCode)
        stackExtra.addView(keyView, in: .leading)
        stackExtra.isHidden = false
        
        keyView.widthAnchor.constraint(lessThanOrEqualTo: keyView.heightAnchor).isActive = true
        if stackExtra.arrangedSubviews.count > 1 {
            let firstKeyView = stackExtra.arrangedSubviews[0]
            keyView.widthAnchor.constraint(equalTo: firstKeyView.widthAnchor).isActive = true
        }
        
        keyView.needsLayout = true
        
        return keyView
    }
}
extension HomeVC{
    // MARK: - Device Info
    
    func DeviceInfoCollectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        if interectiveTestIndex == 25{
            var item: NSCollectionViewItem
            if #available(macOS 10.11, *) {
                item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "DeviceInfoCell"), for: indexPath) as! DeviceInfoCell
                if let item = item as? DeviceInfoCell {
                    item.labelOutlet.isHidden = true
                    item.labelTitleOutlet.isHidden = true
                    item.labelTestTypeOutlet.isHidden = true
                    let fullNameArr = (CPUandGPUArray[2] as? String)?.components(separatedBy: ":")
                    item.labelModelOutlet.stringValue = "\(fullNameArr?[1] ?? "")"
                    let fullNameArrGPU = (CPUandGPUArray[3] as? String)?.components(separatedBy: ":")
                    item.labelYearOutlet.stringValue = "\(fullNameArrGPU?[1] ?? "")"
                    let fullNameArrCPU = (CPUandGPUArray[4] as? String)?.components(separatedBy: ":")
                    let fullNameArrSpeed = (CPUandGPUArray[5] as? String)?.components(separatedBy: ":")
                    item.labelCPUOutlet.stringValue = "\(fullNameArrSpeed?[1] ?? "") \(fullNameArrCPU?[1] ?? "")"
                    let fullNameArrGPUTest = (CPUandGPUArray[21] as? String)?.components(separatedBy: ":")
                    let fullNameArrVRAM = (CPUandGPUArray[24] as? String)?.components(separatedBy: ":")
                    item.labelGPUOutlet.stringValue = "\(fullNameArrGPUTest?[1] ?? "") \(fullNameArrVRAM?[1] ?? "")"
                    item.labelMemoryOutlet.stringValue = "\(System.physicalMemory()) GB"
                    
                    item.labelStorageOutlet.stringValue = "\(DiskStatus.totalDiskSpace)"
                    //                item.labelOperatingSystemOutlet.stringValue = "Operating System:"
                    //                item.labelVersionOutlet.stringValue = "Verion:"
                    item.labelCPUTitleOutlet.stringValue = "CPU"
                    item.labelGPUTitleOutlet.stringValue = "GPU"

                    if AppLanguage.current == .english{
                        item.labelHardwareTitleOutlet.stringValue = "Hardware"
                        item.labelModelTitleOutlet.stringValue = "Model:"
                        item.labelYearTitleOutlet.stringValue = "Model identifier:"
                        item.labelMemoryTitleOutlet.stringValue = "Memory:"
                        item.labelStorageTitleOutlet.stringValue = "Storage:"
                        item.labelDeviceInfoTitleOutlet.stringValue = "DEVICE INFORMATION"
                        
                        //                    item.labelOperatingSystemTitleOutlet.stringValue = "Operating System:"
                        //                    item.labelVersionTitleOutlet.stringValue = "Verion:"
                        //                            item.labelModelTitleOutlet.stringValue = ""
                        //                            item.labelModelTitleOutlet.stringValue = ""
                        //                            item.labelModelTitleOutlet.stringValue = ""
                        
                    }else if AppLanguage.current == .arabic{
                        item.labelHardwareTitleOutlet.stringValue = "المعدات"
                        item.labelModelTitleOutlet.stringValue = "نموذج:"
                        item.labelYearTitleOutlet.stringValue = "معرّف الطراز:"
                        item.labelMemoryTitleOutlet.stringValue = "ذاكرة:"
                        item.labelStorageTitleOutlet.stringValue = "تخزين:"
                        item.labelDeviceInfoTitleOutlet.stringValue = "معلومات الجهاز"
                        //                    item.labelOperatingSystemTitleOutlet.stringValue = "نظام التشغيل:"
                        //                    item.labelVersionTitleOutlet.stringValue = "فيريون:"
                    }else if AppLanguage.current == .spanish{
                        item.labelHardwareTitleOutlet.stringValue = "Hardware"
                        item.labelModelTitleOutlet.stringValue = "Modelo:"
                        item.labelYearTitleOutlet.stringValue = "Identificador del modelo:"
                        item.labelMemoryTitleOutlet.stringValue = "Memoria:"
                        item.labelStorageTitleOutlet.stringValue = "Almacenamiento:"
                        item.labelDeviceInfoTitleOutlet.stringValue = "INFORMACIÓN DEL DISPOSITIVO"
                        //                    item.labelOperatingSystemTitleOutlet.stringValue = "Sistema operativo:"
                        //                    item.labelVersionTitleOutlet.stringValue = "Verión:"
                    }else if AppLanguage.current == .portuguese{
                        item.labelHardwareTitleOutlet.stringValue = "Hardware"
                        item.labelModelTitleOutlet.stringValue = "Modelo:"
                        item.labelYearTitleOutlet.stringValue = "Identificador de modelo:"
                        item.labelMemoryTitleOutlet.stringValue = "Memória:"
                        item.labelStorageTitleOutlet.stringValue = "Armazenar:"
                        item.labelDeviceInfoTitleOutlet.stringValue = "INFORMAÇÃO DE DISPOSITIVO"
                        //                    item.labelOperatingSystemTitleOutlet.stringValue = "Sistema operacional:"
                        //                    item.labelVersionTitleOutlet.stringValue = "Verão:"
                    }
                }
                return item
            } else {
                return NSCollectionViewItem()
            }
        }else{
            var item: NSCollectionViewItem
            if #available(macOS 10.11, *) {
                item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "DeviceInfoCell"), for: indexPath) as! DeviceInfoCell
                if let item = item as? DeviceInfoCell {
                    item.labelOutlet.isHidden = false
                    item.labelTitleOutlet.isHidden = false
                    item.labelTestTypeOutlet.isHidden = false

                    if AppLanguage.current == .english{
                        item.labelDeviceInfoTitleOutlet.stringValue = "Review Test"
                        item.labelHardwareTitleOutlet.stringValue = "Interactive Test"
                        item.labelTestTypeOutlet.stringValue = "Non Interactive Test"

                        item.labelModelTitleOutlet.stringValue = arrayInterectiveTestData[0]
                        item.labelYearTitleOutlet.stringValue = arrayInterectiveTestData[1]
                        item.labelCPUTitleOutlet.stringValue = arrayInterectiveTestData[2]
                        item.labelGPUTitleOutlet.stringValue = arrayInterectiveTestData[3]
                        item.labelMemoryTitleOutlet.stringValue = arrayInterectiveTestData[4]
                        item.labelStorageTitleOutlet.stringValue = arrayInterectiveTestData[5]
                        item.labelTitleOutlet.stringValue = arrayInterectiveTestData[6]
                        
                        item.labelModelOutlet.stringValue = arrayNonInterectiveTestData[0]
                        item.labelYearOutlet.stringValue = arrayNonInterectiveTestData[1]
                        item.labelCPUOutlet.stringValue = arrayNonInterectiveTestData[2]
                        item.labelGPUOutlet.stringValue = arrayNonInterectiveTestData[3]
                        if ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Type") as? String) == "InternalBattery" || ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Transport Type") as? String) == "Internal" || ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Name") as? String) == "InternalBattery-0"{
                            item.labelMemoryOutlet.stringValue = arrayNonInterectiveTestData[4]
                        }else{
                            item.labelMemoryOutlet.stringValue = ""
                        }
                        item.labelStorageOutlet.stringValue = arrayNonInterectiveTestData[5]
                        item.labelOutlet.stringValue = arrayNonInterectiveTestData[6]

                    }else if AppLanguage.current == .arabic{
                        item.labelDeviceInfoTitleOutlet.stringValue = "مراجعة الاختبار"
                        item.labelHardwareTitleOutlet.stringValue = "اختبار تفاعلي"
                        item.labelTestTypeOutlet.stringValue = "اختبار غير تفاعلي"

                        item.labelModelOutlet.stringValue = arrayInterectiveTestDataArabic[0]
                        item.labelYearOutlet.stringValue = arrayInterectiveTestDataArabic[1]
                        item.labelCPUOutlet.stringValue = arrayInterectiveTestDataArabic[2]
                        item.labelGPUOutlet.stringValue = arrayInterectiveTestDataArabic[3]
                        if ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Type") as? String) == "InternalBattery" || ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Transport Type") as? String) == "Internal" || ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Name") as? String) == "InternalBattery-0"{
                            item.labelMemoryOutlet.stringValue = arrayInterectiveTestDataArabic[4]
                        }else{
                            item.labelMemoryOutlet.stringValue = ""
                        }
                        item.labelStorageOutlet.stringValue = arrayInterectiveTestDataArabic[5]
                        item.labelTitleOutlet.stringValue = arrayInterectiveTestDataArabic[6]
                        
                        item.labelModelOutlet.stringValue = arrayNonInterectiveTestDataArabic[0]
                        item.labelYearOutlet.stringValue = arrayNonInterectiveTestDataArabic[1]
                        item.labelCPUOutlet.stringValue = arrayNonInterectiveTestDataArabic[2]
                        item.labelGPUOutlet.stringValue = arrayNonInterectiveTestDataArabic[3]
                        item.labelMemoryOutlet.stringValue = arrayNonInterectiveTestDataArabic[4]
                        item.labelStorageOutlet.stringValue = arrayNonInterectiveTestDataArabic[5]
                        item.labelOutlet.stringValue = arrayNonInterectiveTestDataArabic[6]

                    }else if AppLanguage.current == .spanish{
                        item.labelDeviceInfoTitleOutlet.stringValue = "Prueba de revisión"
                        item.labelHardwareTitleOutlet.stringValue = "Prueba interactiva"
                        item.labelTestTypeOutlet.stringValue = "Prueba no interactiva"

                        item.labelModelOutlet.stringValue = arrayInterectiveTestDataSpanish[0]
                        item.labelYearOutlet.stringValue = arrayInterectiveTestDataSpanish[1]
                        item.labelCPUOutlet.stringValue = arrayInterectiveTestDataSpanish[2]
                        item.labelGPUOutlet.stringValue = arrayInterectiveTestDataSpanish[3]
                        if ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Type") as? String) == "InternalBattery" || ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Transport Type") as? String) == "Internal" || ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Name") as? String) == "InternalBattery-0"{
                            item.labelMemoryOutlet.stringValue = arrayInterectiveTestDataSpanish[4]
                        }else{
                            item.labelMemoryOutlet.stringValue = ""
                        }
                        item.labelStorageOutlet.stringValue = arrayInterectiveTestDataSpanish[5]
                        item.labelTitleOutlet.stringValue = arrayInterectiveTestDataSpanish[6]
                        
                        item.labelModelOutlet.stringValue = arrayNonInterectiveTestDataSpanish[0]
                        item.labelYearOutlet.stringValue = arrayNonInterectiveTestDataSpanish[1]
                        item.labelCPUOutlet.stringValue = arrayNonInterectiveTestDataSpanish[2]
                        item.labelGPUOutlet.stringValue = arrayNonInterectiveTestDataSpanish[3]
                        item.labelMemoryOutlet.stringValue = arrayNonInterectiveTestDataSpanish[4]
                        item.labelStorageOutlet.stringValue = arrayNonInterectiveTestDataSpanish[5]
                        item.labelOutlet.stringValue = arrayNonInterectiveTestDataSpanish[6]

                    }else if AppLanguage.current == .portuguese{
                        item.labelDeviceInfoTitleOutlet.stringValue = "Teste de Revisão"
                        item.labelHardwareTitleOutlet.stringValue = "Teste interativo"
                        item.labelTestTypeOutlet.stringValue = "Teste não interativo"

                        item.labelModelOutlet.stringValue = arrayInterectiveTestDataPort[0]
                        item.labelYearOutlet.stringValue = arrayInterectiveTestDataPort[1]
                        item.labelCPUOutlet.stringValue = arrayInterectiveTestDataPort[2]
                        item.labelGPUOutlet.stringValue = arrayInterectiveTestDataPort[3]
                        if ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Type") as? String) == "InternalBattery" || ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Transport Type") as? String) == "Internal" || ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Name") as? String) == "InternalBattery-0"{
                            item.labelMemoryOutlet.stringValue = arrayInterectiveTestDataPort[4]
                        }else{
                            item.labelMemoryOutlet.stringValue = ""
                        }
                        item.labelStorageOutlet.stringValue = arrayInterectiveTestDataPort[5]
                        item.labelTitleOutlet.stringValue = arrayInterectiveTestDataPort[6]
                        
                        item.labelModelOutlet.stringValue = arrayNonInterectiveTestDataPort[0]
                        item.labelYearOutlet.stringValue = arrayNonInterectiveTestDataPort[1]
                        item.labelCPUOutlet.stringValue = arrayNonInterectiveTestDataPort[2]
                        item.labelGPUOutlet.stringValue = arrayNonInterectiveTestDataPort[3]
                        item.labelMemoryOutlet.stringValue = arrayNonInterectiveTestDataPort[4]
                        item.labelStorageOutlet.stringValue = arrayNonInterectiveTestDataPort[5]
                        item.labelOutlet.stringValue = arrayNonInterectiveTestDataPort[6]
                    }
                }
                return item
            } else {
                return NSCollectionViewItem()
            }
        }
    }
}
extension HomeVC{
    
    
    override func keyDown(with event: NSEvent) {
        //super.keyDown(event)
    }
    override func keyUp(with event: NSEvent) {
        //super.keyUp(event)
    }
}
extension HomeVC{
    // MARK: - Keyboard Test
    
    func keyboardCollectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        var item: NSCollectionViewItem
        if #available(macOS 10.11, *) {
            item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "KeyboardTestCollectionViewCell"), for: indexPath) as! KeyboardTestCollectionViewCell
            
            if let item = item as? KeyboardTestCollectionViewCell {
                if AppLanguage.current == .english{
                    item.labelStatusOutlet.stringValue = "Please wait untill test finish"
                }else if AppLanguage.current == .arabic{
                    item.labelStatusOutlet.stringValue = "الرجاء الانتظار حتى تنتهي المهمة"
                }else if AppLanguage.current == .spanish{
                    item.labelStatusOutlet.stringValue = "Espere hasta que finalice la tarea"
                }else if AppLanguage.current == .portuguese{
                    item.labelStatusOutlet.stringValue = "Aguarde até que a tarefa termine"
                }
                item.buttonOutlet.bezelStyle = .texturedSquare
                item.buttonOutlet.isBordered = false //Important
                item.buttonOutlet.wantsLayer = true
                item.buttonSendReportOutlet.bezelStyle = .texturedSquare
                item.buttonSendReportOutlet.isBordered = false //Important
                item.buttonSendReportOutlet.wantsLayer = true
                item.buttonRunAgainOUtlet.bezelStyle = .texturedSquare
                item.buttonRunAgainOUtlet.isBordered = false //Important
                item.buttonRunAgainOUtlet.wantsLayer = true
                item.buttonOutlet.action = #selector(buttonKeyboardTestRunAction)
                item.labelTextOutlet.stringValue = ""
                item.labelTextOutlet.stringValue = self.keyboardKeyArray.joined()
                if AppLanguage.current == .english{
                    item.labelOutlet.stringValue = "Keyboard Test"
                }else if AppLanguage.current == .arabic{
                    item.labelOutlet.stringValue = "اختبار لوحة المفاتيح"
                }else if AppLanguage.current == .spanish{
                    item.labelOutlet.stringValue = "Prueba de teclado"
                }else if AppLanguage.current == .portuguese{
                    item.labelOutlet.stringValue = "Teste de teclado"
                }
                if isChargerStart == 0{
                    item.keyboardView.isHidden = true
                    
                    if AppLanguage.current == .english{
                        item.labelTitleOutlet.stringValue = "Press keyboard keys"
                    }else if AppLanguage.current == .arabic{
                        item.labelTitleOutlet.stringValue = "اضغط على مفاتيح لوحة المفاتيح"
                    }else if AppLanguage.current == .spanish{
                        item.labelTitleOutlet.stringValue = "Presione las teclas del teclado"
                    }else if AppLanguage.current == .portuguese{
                        item.labelTitleOutlet.stringValue = "Pressione as teclas do teclado"
                    }
                    item.buttonSendReportOutlet.isHidden = true
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.labelStatusOutlet.isHidden = true
                    item.activityIndicator.isHidden = true
                    item.imageViewOutlet.isHidden = true
                    item.buttonOutlet.isEnabled = true
                    item.buttonOutlet.title = self.buttonRunTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.0
                    item.buttonOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonRunTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    item.labelSubtitleOutlet.stringValue = ""
                    
                }else if keyboardTestStatusID == "1" || keyboardTestStatusID == "2"{
                    item.keyboardView.isHidden = true
                    item.buttonOutlet.isEnabled = false
                    item.buttonOutlet.title = self.buttonFinishTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.0
                    item.buttonOutlet.layer?.borderColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.cornerRadius = 0
                    
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonFinishTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.black, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    
                    item.imageViewOutlet.isHidden = false
                    if keyboardTestStatusID == "1"{
                        item.imageViewOutlet.image = NSImage(named: "tick")
                        if AppLanguage.current == .english{
                            item.labelSubtitleOutlet.stringValue = "Pass"
                        }else if AppLanguage.current == .arabic{
                            item.labelSubtitleOutlet.stringValue = "يمر"
                        }else if AppLanguage.current == .spanish{
                            item.labelSubtitleOutlet.stringValue = "Aprobar"
                        }else if AppLanguage.current == .portuguese{
                            item.labelSubtitleOutlet.stringValue = "Passar"
                        }
                    }else if keyboardTestStatusID == "2"{
                        item.imageViewOutlet.image = NSImage(named: "danger-1")
                        if AppLanguage.current == .english{
                            item.labelSubtitleOutlet.stringValue = "Failed"
                        }else if AppLanguage.current == .arabic{
                            item.labelSubtitleOutlet.stringValue = "باءت بالفشل"
                        }else if AppLanguage.current == .spanish{
                            item.labelSubtitleOutlet.stringValue = "Fallido"
                        }else if AppLanguage.current == .portuguese{
                            item.labelSubtitleOutlet.stringValue = "Fracassado"
                        }
                    }
                    item.activityIndicator.isHidden = true
                    item.activityIndicator.stopAnimation(nil)
                    item.buttonRunAgainOUtlet.isHidden = false
                    item.labelStatusOutlet.isHidden = true
                    item.buttonRunAgainOUtlet.isEnabled = true
                    item.buttonSendReportOutlet.isEnabled = true
                    item.buttonSendReportOutlet.isHidden = false
                    item.buttonRunAgainOUtlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = true
                    item.buttonSendReportOutlet.isEnabled = true
                    item.buttonRunAgainOUtlet.isEnabled = true
                    item.buttonRunAgainOUtlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonRunAgainOUtlet.layer?.borderWidth = 0.0
                    item.buttonRunAgainOUtlet.layer?.borderColor = NSColor.clear.cgColor
                    item.buttonRunAgainOUtlet.layer?.cornerRadius = 0
                    item.buttonRunAgainOUtlet.attributedTitle = NSAttributedString(string: self.buttonRunAgainTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.black, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    //
                    
                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonSendReportOutlet.layer?.borderWidth = 0.0
                    item.buttonSendReportOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                    item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    item.buttonSendReportOutlet.action = #selector(buttonKeyboardTestSendReportsAction)
                    item.buttonRunAgainOUtlet.action = #selector(buttonKeyboardTestRunAgainAction)
                    
                }else if isChargerStart == 1{
                    item.keyboardView.isHidden = false
                    item.labelSubtitleOutlet.stringValue = ""
                    item.buttonOutlet.isEnabled = true
                    item.buttonOutlet.title = self.buttonStopTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.5
                    item.buttonOutlet.layer?.borderColor = NSColor.red.cgColor
                    item.buttonOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonStopTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.red, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    item.imageViewOutlet.isHidden = true
                    item.activityIndicator.isHidden = false
                    item.activityIndicator.isIndeterminate = true
                    item.activityIndicator.style = .spinning
                    item.activityIndicator.startAnimation(nil)
                    
                    
                    
                    
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.labelStatusOutlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = false
                    item.buttonSendReportOutlet.isEnabled = false
                    item.buttonSendReportOutlet.isHidden = false
                    
                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                    item.buttonSendReportOutlet.layer?.borderColor = NSColor.darkGray.cgColor
                    item.buttonSendReportOutlet.layer?.borderWidth = 1
                    item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.darkGray, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    
                }else{
                    item.keyboardView.isHidden = true
                    item.labelSubtitleOutlet.stringValue = ""
                    item.buttonOutlet.isEnabled = true
                    item.buttonOutlet.title = self.buttonRunTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.0
                    item.buttonOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonRunTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    item.activityIndicator.isHidden = true
                    item.activityIndicator.stopAnimation(nil)
                    
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.labelStatusOutlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = false
                    item.buttonSendReportOutlet.isEnabled = false
                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                    item.buttonSendReportOutlet.layer?.borderColor = NSColor.darkGray.cgColor
                    item.buttonSendReportOutlet.layer?.borderWidth = 1
                    item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.darkGray, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    
                }
                
            }
            
            return item
        } else {
            return NSCollectionViewItem()
        }
    }
    
    @objc func buttonKeyboardTestRunAction(button:NSButton) {
        if isChargerStart == 0{
            self.isPixelsTestStart = false
            self.isKeyboardTest = true
            self.collectionViewSubmenu.isSelectable = false
            self.isChargerStart = 1
            timer.invalidate() // just in case this button is tapped multiple times
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAction), userInfo: nil, repeats: true)
            eventMonitor = NSEvent.addLocalMonitorForEvents(matching: [.keyDown, .keyUp, .flagsChanged]) { [unowned self] event in
                self.onKeyDown(with: event)
                return event
            }
        }else{
            self.isKeyboardTest = false
            self.collectionViewSubmenu.isSelectable = true
            self.isChargerStart = 0
            self.timer.invalidate()
            
        }
        self.collectionViewTest.reloadData()
        print("Clicked \(button.title)!")
    }
    
    @objc func buttonKeyboardTestRunAgainAction(button:NSButton) {
        self.keyboardKeyArray.removeAll()
        self.collectionViewSubmenu.isSelectable = true
        self.keyboardTestStatusID = ""
        self.timer.invalidate()
        self.counter = 0
        self.isChargerStart = 0
        self.collectionViewTest.reloadData()
    }
    
    @objc func buttonKeyboardTestSendReportsAction(button:NSButton) {
        self.collectionViewSubmenu.isSelectable = true
        var testName = ""
        if AppLanguage.current == .english{
            testName = "Keyboard Test"
        }else if AppLanguage.current == .arabic{
            testName = "اختبار لوحة المفاتيح"
        }else if AppLanguage.current == .spanish{
            testName = "Prueba de teclado"
        }else if AppLanguage.current == .portuguese{
            testName = "Teste de teclado"
        }
        let keyboardData = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":keyboardTestStatusID,"testTypeId":"119", "TestDatas":[]] as [String : Any]// this is for testing
        self.keyboardTestArray.remove(at: 0)
        self.keyboardTestArray.insert(keyboardData, at: 0)
        UserDefaults.standard.setValue(self.keyboardTestArray, forKey: "Keyboard_Test")
        if Reachability.isConnectedToNetwork(){
            
            self.callSaveTestReportAPI(testArray: self.keyboardTestArray, type: "keyboard")
        }else{
            if AppLanguage.current == .english{
                self.confirmAbletonIsReady(question: "Error", text: "Please Check Your Internet Connection Or Wifi")
            }else if AppLanguage.current == .arabic{
                self.confirmAbletonIsReady(question: "خطأ", text: "يرجى التحقق من اتصالك بالإنترنت أو شبكة Wifi")
            }else if AppLanguage.current == .spanish{
                self.confirmAbletonIsReady(question: "Error", text: "Por favor, compruebe su conexión a Internet o Wifi")
            }else if AppLanguage.current == .portuguese{
                self.confirmAbletonIsReady(question: "Erro", text: "Por favor, verifique sua conexão com a Internet ou Wi-Fi")
            }
        }
    }
    
    @objc func timerAction() {
        print("timerAction stop ---------->>>>>>>>>>>>>>>>>>>>>>>>")
        
        if counter >= 60{
            self.isKeyboardTest = false
            self.collectionViewSubmenu.isSelectable = true
            timer.invalidate()
            self.keyboardTestStatusID = "2"
            self.isKeyboardTest = false
            self.collectionViewSubmenu.isSelectable = true
            self.collectionViewTest.reloadData()
            if isMultipleTestSelect == true{
                if SelectArrayString.count > 0{
                    self.SelectArrayString.remove(at: 0)
                }
                self.buttonKeyboardTestSendReportsAction(button: NSButton())
            }
            print("timer stop ---------->>>>>>>>>>>>>>>>>>>>>>>>",counter)
        }else{
            if keyboardKeyArray.count == 61{
                self.collectionViewSubmenu.isSelectable = true
                self.timer.invalidate()
                self.keyboardTestStatusID = "1"
                self.isKeyboardTest = false
                self.collectionViewSubmenu.isSelectable = true
                self.collectionViewTest.reloadData()
                if isMultipleTestSelect == true{
                    if SelectArrayString.count > 0{
                        self.SelectArrayString.remove(at: 0)
                    }
                    self.buttonKeyboardTestSendReportsAction(button: NSButton())
                }
            }
            counter += 1
        }
        print("Counter is ---------->>>>>>>>>>>>>>>>>>>>>>>>",counter)
    }
}
extension HomeVC{
    // MARK: - Charger Test
    
    func chargerCollectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        var item: NSCollectionViewItem
        if #available(macOS 10.11, *) {
            item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "TestHeaderCollectionViewCell"), for: indexPath) as! TestHeaderCollectionViewCell
            
            if let item = item as? TestHeaderCollectionViewCell {
                if AppLanguage.current == .english{
                    item.labelStatusOutlet.stringValue = "Please wait untill test finish"
                }else if AppLanguage.current == .arabic{
                    item.labelStatusOutlet.stringValue = "الرجاء الانتظار حتى تنتهي المهمة"
                }else if AppLanguage.current == .spanish{
                    item.labelStatusOutlet.stringValue = "Espere hasta que finalice la tarea"
                }else if AppLanguage.current == .portuguese{
                    item.labelStatusOutlet.stringValue = "Aguarde até que a tarefa termine"
                }
                item.buttonOutlet.bezelStyle = .texturedSquare
                item.buttonOutlet.isBordered = false //Important
                item.buttonOutlet.wantsLayer = true
                item.buttonSendReportOutlet.bezelStyle = .texturedSquare
                item.buttonSendReportOutlet.isBordered = false //Important
                item.buttonSendReportOutlet.wantsLayer = true
                item.buttonRunAgainOUtlet.bezelStyle = .texturedSquare
                item.buttonRunAgainOUtlet.isBordered = false //Important
                item.buttonRunAgainOUtlet.wantsLayer = true
                item.buttonOutlet.action = #selector(buttonChargerTestRunAction)
                
                if AppLanguage.current == .english{
                    item.labelOutlet.stringValue = "Charging Port test"
                }else if AppLanguage.current == .arabic{
                    item.labelOutlet.stringValue = "اختبار منفذ الشحن"
                }else if AppLanguage.current == .spanish{
                    item.labelOutlet.stringValue = "Prueba del puerto de carga"
                }else if AppLanguage.current == .portuguese{
                    item.labelOutlet.stringValue = "Teste da porta de carregamento"
                }
                if isChargerStart == 0{
                    if AppLanguage.current == .english{
                        item.labelTitleOutlet.stringValue = "Connect Charger"
                    }else if AppLanguage.current == .arabic{
                        item.labelTitleOutlet.stringValue = "توصيل الشاحن"
                    }else if AppLanguage.current == .spanish{
                        item.labelTitleOutlet.stringValue = "Conectar cargador"
                    }else if AppLanguage.current == .portuguese{
                        item.labelTitleOutlet.stringValue = "Conectar cargador"
                    }
                    item.buttonSendReportOutlet.isHidden = true
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.labelStatusOutlet.isHidden = true
                    item.activityIndicator.isHidden = true
                    item.imageViewOutlet.isHidden = true
                    item.buttonOutlet.isEnabled = true
                    item.buttonOutlet.title = self.buttonRunTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.0
                    item.buttonOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonRunTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    item.labelSubtitleOutlet.stringValue = ""
                    
                }else if keyboardTestStatusID == "1" || keyboardTestStatusID == "2"{
                    if AppLanguage.current == .english{
                        item.labelTitleOutlet.stringValue = "Connect Charger"
                    }else if AppLanguage.current == .arabic{
                        item.labelTitleOutlet.stringValue = "توصيل الشاحن"
                    }else if AppLanguage.current == .spanish{
                        item.labelTitleOutlet.stringValue = "Conectar cargador"
                    }else if AppLanguage.current == .portuguese{
                        item.labelTitleOutlet.stringValue = "Conectar cargador"
                    }
                    item.buttonOutlet.isEnabled = false
                    item.buttonOutlet.title = self.buttonFinishTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.0
                    item.buttonOutlet.layer?.borderColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.cornerRadius = 0
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonFinishTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.black, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    
                    item.imageViewOutlet.isHidden = false
                    if keyboardTestStatusID == "1"{
                        item.imageViewOutlet.image = NSImage(named: "tick")
                        if AppLanguage.current == .english{
                            item.labelSubtitleOutlet.stringValue = "Pass"
                        }else if AppLanguage.current == .arabic{
                            item.labelSubtitleOutlet.stringValue = "يمر"
                        }else if AppLanguage.current == .spanish{
                            item.labelSubtitleOutlet.stringValue = "Aprobar"
                        }else if AppLanguage.current == .portuguese{
                            item.labelSubtitleOutlet.stringValue = "Passar"
                        }
                    }else if keyboardTestStatusID == "2"{
                        item.imageViewOutlet.image = NSImage(named: "danger-1")
                        if AppLanguage.current == .english{
                            item.labelSubtitleOutlet.stringValue = "Failed"
                        }else if AppLanguage.current == .arabic{
                            item.labelSubtitleOutlet.stringValue = "باءت بالفشل"
                        }else if AppLanguage.current == .spanish{
                            item.labelSubtitleOutlet.stringValue = "Fallido"
                        }else if AppLanguage.current == .portuguese{
                            item.labelSubtitleOutlet.stringValue = "Fracassado"
                        }
                        
                    }
                    item.activityIndicator.isHidden = true
                    item.activityIndicator.stopAnimation(nil)
                    item.buttonRunAgainOUtlet.isHidden = false
                    item.labelStatusOutlet.isHidden = true
                    item.buttonRunAgainOUtlet.isEnabled = true
                    item.buttonSendReportOutlet.isEnabled = true
                    item.buttonSendReportOutlet.isHidden = false
                    item.buttonRunAgainOUtlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = true
                    item.buttonSendReportOutlet.isEnabled = true
                    item.buttonRunAgainOUtlet.isEnabled = true
                    item.buttonRunAgainOUtlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonRunAgainOUtlet.layer?.borderWidth = 0.0
                    item.buttonRunAgainOUtlet.layer?.borderColor = NSColor.clear.cgColor
                    item.buttonRunAgainOUtlet.layer?.cornerRadius = 0
                    item.buttonRunAgainOUtlet.attributedTitle = NSAttributedString(string: self.buttonRunAgainTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.black, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonSendReportOutlet.layer?.borderWidth = 0.0
                    item.buttonSendReportOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                    item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    item.buttonSendReportOutlet.action = #selector(buttonChargerTestSendReportAction)
                    item.buttonRunAgainOUtlet.action = #selector(buttonChargerTestRunAgainAction)
                    
                }else if isChargerStart == 1{
                    item.labelSubtitleOutlet.stringValue = ""
                    item.buttonOutlet.isEnabled = true
                    item.buttonOutlet.title = self.buttonStopTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.5
                    item.buttonOutlet.layer?.borderColor = NSColor.red.cgColor
                    item.buttonOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonStopTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.red, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    item.imageViewOutlet.isHidden = true
                    item.activityIndicator.isHidden = false
                    item.activityIndicator.isIndeterminate = true
                    item.activityIndicator.style = .spinning
                    item.activityIndicator.startAnimation(nil)
                    
                    
                    
                    
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.labelStatusOutlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = false
                    item.buttonSendReportOutlet.isEnabled = false
                    item.buttonSendReportOutlet.isHidden = false
                    
                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                    item.buttonSendReportOutlet.layer?.borderColor = NSColor.darkGray.cgColor
                    item.buttonSendReportOutlet.layer?.borderWidth = 1
                    item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.darkGray, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    
                }else{
                    item.labelSubtitleOutlet.stringValue = ""
                    item.buttonOutlet.isEnabled = true
                    item.buttonOutlet.title = self.buttonRunTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.0
                    item.buttonOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonRunTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    item.activityIndicator.isHidden = true
                    item.activityIndicator.stopAnimation(nil)
                    
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.labelStatusOutlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = false
                    item.buttonSendReportOutlet.isEnabled = false
                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                    item.buttonSendReportOutlet.layer?.borderColor = NSColor.darkGray.cgColor
                    item.buttonSendReportOutlet.layer?.borderWidth = 1
                    item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.darkGray, NSAttributedString.Key.paragraphStyle : pstyle ])
                }
                
            }
            
            return item
        } else {
            return NSCollectionViewItem()
        }
    }
    
    @objc func buttonChargerTestRunAction(button:NSButton) {
        
        if isChargerStart == 0{
            self.collectionViewSubmenu.isSelectable = false
            self.isKeyboardTest = true
            self.isChargerStart = 1
            timer.invalidate() // just in case this button is tapped multiple times
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(chargerTestTimerAction), userInfo: nil, repeats: true)
            
        }else{
            self.collectionViewSubmenu.isSelectable = true
            self.isKeyboardTest = false
            self.isChargerStart = 0
            self.timer.invalidate()
        }
        self.collectionViewTest.reloadData()
        print("Clicked \(button.title)!")
    }
    
    @objc func buttonChargerTestRunAgainAction(button:NSButton) {
        self.timer.invalidate()
        self.counter = 0
        self.keyboardTestStatusID = ""
        self.isChargerStart = 0
        self.collectionViewTest.reloadData()
        
    }
    @objc func buttonChargerTestSendReportAction(button:NSButton) {
        var testName = ""
        if AppLanguage.current == .english{
            testName = "Charging Port test"
        }else if AppLanguage.current == .arabic{
            testName = "اختبار منفذ الشحن"
        }else if AppLanguage.current == .spanish{
            testName = "Prueba del puerto de carga"
        }else if AppLanguage.current == .portuguese{
            testName = "Teste da porta de carregamento"
        }
        let chargerData = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":keyboardTestStatusID,"testTypeId":"113",  "TestDatas":[]] as [String : Any]// this is for testing
        self.chargerTestArray.remove(at: 0)
        self.chargerTestArray.insert(chargerData, at: 0)
        UserDefaults.standard.setValue(self.keyboardTestArray, forKey: "Charger_Test")
        //        self.TakeScreensShots(folderName: "Desktop")
        if Reachability.isConnectedToNetwork(){
            self.callSaveTestReportAPI(testArray: chargerTestArray, type: "charger")
        }else{
            if AppLanguage.current == .english{
                self.confirmAbletonIsReady(question: "Error", text: "Please Check Your Internet Connection Or Wifi")
            }else if AppLanguage.current == .arabic{
                self.confirmAbletonIsReady(question: "خطأ", text: "يرجى التحقق من اتصالك بالإنترنت أو شبكة Wifi")
            }else if AppLanguage.current == .spanish{
                self.confirmAbletonIsReady(question: "Error", text: "Por favor, compruebe su conexión a Internet o Wifi")
            }else if AppLanguage.current == .portuguese{
                self.confirmAbletonIsReady(question: "Erro", text: "Por favor, verifique sua conexão com a Internet ou Wi-Fi")
            }
        }
    }
    
    @objc func chargerTestTimerAction() {
        if counter >= 20{
            timer.invalidate()
            self.keyboardTestStatusID = "2"
            self.isKeyboardTest = false
            self.isKeyboardTest = false
            self.collectionViewSubmenu.isSelectable = true
            self.collectionViewTest.reloadData()
            if isMultipleTestSelect == true{
                if SelectArrayString.count > 0{
                    self.SelectArrayString.remove(at: 0)
                }
                self.buttonChargerTestSendReportAction(button: NSButton())
            }
            print("timer stop ---------->>>>>>>>>>>>>>>>>>>>>>>>",counter)
        }else{
            counter += 1
            var battery = Battery()
            if battery.open() != kIOReturnSuccess { exit(0) }
            if (battery.isACPowered()) == true{
                self.timer.invalidate()
                self.keyboardTestStatusID = "1"
                self.isKeyboardTest = false
                self.collectionViewSubmenu.isSelectable = true
                self.collectionViewTest.reloadData()
                if isMultipleTestSelect == true{
                    if SelectArrayString.count > 0{
                        self.SelectArrayString.remove(at: 0)
                    }
                    self.buttonChargerTestSendReportAction(button: NSButton())
                }
            }
        }
        print("Counter is ---------->>>>>>>>>>>>>>>>>>>>>>>>",counter)
    }
}


extension HomeVC{
    // MARK: - Camera Test
    
    func cameraCollectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        var item: NSCollectionViewItem
        if #available(macOS 10.11, *) {
            item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "HomeTestCollectionViewCell"), for: indexPath) as! HomeTestCollectionViewCell
            
            
            
            if let item = item as? HomeTestCollectionViewCell {
                if AppLanguage.current == .english{
                    item.labelStatusOutlet.stringValue = "Check the clarity. Take a picture"
                }else if AppLanguage.current == .arabic{
                    item.labelStatusOutlet.stringValue = "تحقق من الوضوح. التقط صورة"
                }else if AppLanguage.current == .spanish{
                    item.labelStatusOutlet.stringValue = "Comprueba la claridad. Toma una foto"
                }else if AppLanguage.current == .portuguese{
                    item.labelStatusOutlet.stringValue = "Verifique a clareza. Tire uma foto"
                }
                item.buttonOutlet.bezelStyle = .texturedSquare
                item.buttonOutlet.isBordered = false //Important
                item.buttonOutlet.wantsLayer = true
                item.buttonSendReportOutlet.bezelStyle = .texturedSquare
                item.buttonSendReportOutlet.isBordered = false //Important
                item.buttonSendReportOutlet.wantsLayer = true
                item.buttonRunAgainOUtlet.bezelStyle = .texturedSquare
                item.buttonRunAgainOUtlet.isBordered = false //Important
                item.buttonRunAgainOUtlet.wantsLayer = true
                item.buttonOutlet.action = #selector(buttonCameraTestRunAction)
                item.buttonCaptureAction.action = #selector(buttonCaptureAction)
                item.buttonCaptureAction.layer?.borderWidth = 1.0
                item.buttonCaptureAction.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                item.buttonCaptureAction.layer?.cornerRadius = 6

                if captureSession.isRunning {
                    item.buttonCaptureAction.isHidden = false
                }else{
                    item.buttonCaptureAction.isHidden = true
                }
                if let image = NSImage(data: imageData) {
                    item.imageViewPhotoOutlet.isHidden = false
                    item.imageViewPhotoOutlet.image = image
                }else{
                    item.imageViewPhotoOutlet.isHidden = true
                }

                if AppLanguage.current == .english{
                    item.labelOutlet.stringValue = "Webcam Test"
                }else if AppLanguage.current == .arabic{
                    item.labelOutlet.stringValue = "اختبار كاميرا الويب"
                }else if AppLanguage.current == .spanish{
                    item.labelOutlet.stringValue = "Prueba de cámara web"
                }else if AppLanguage.current == .portuguese{
                    item.labelOutlet.stringValue = "Teste de webcam"
                }
                
                item.cameraView?.wantsLayer = true
                item.cameraView?.layer?.backgroundColor = NSColor.black.cgColor
                
                
                session.sessionPreset = AVCaptureSession.Preset.high
                
                if AVCaptureDevice.default(for: AVMediaType.video) != nil{
                    let input: AVCaptureInput = try! AVCaptureDeviceInput(device: AVCaptureDevice.default(for: .video)!)
                    session.addInput(input)
                    let previewLayer: AVCaptureVideoPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
                    previewLayer.videoGravity = .resizeAspectFill
                    previewLayer.frame = item.cameraView!.bounds
                    item.cameraView?.layer?.addSublayer(previewLayer)
                }
                
                item.cameraView?.wantsLayer = true
                item.cameraView?.layer?.backgroundColor = NSColor.black.cgColor
                session.sessionPreset = AVCaptureSession.Preset.high
                let input: AVCaptureInput = try! AVCaptureDeviceInput(device: AVCaptureDevice.default(for: .video)!)
                session.addInput(input)
                let previewLayer: AVCaptureVideoPreviewLayer = AVCaptureVideoPreviewLayer(session: session)
                previewLayer.videoGravity = .resizeAspectFill
                previewLayer.frame = item.cameraView!.bounds
                item.cameraView?.layer?.addSublayer(previewLayer)
                
                
                if isChargerStart == 0{// 1234
                    if AppLanguage.current == .english{
                        item.labelTitleOutlet.stringValue = "Click run to begin webcam test"
                    }else if AppLanguage.current == .arabic{
                        item.labelTitleOutlet.stringValue = "انقر فوق تشغيل لبدء اختبار كاميرا الويب"
                    }else if AppLanguage.current == .spanish{
                        item.labelTitleOutlet.stringValue = "Haga clic en ejecutar para comenzar la prueba de la cámara web"
                    }else if AppLanguage.current == .portuguese{
                        item.labelTitleOutlet.stringValue = "Clique em executar para iniciar o teste da webcam"
                    }
                    item.buttonSendReportOutlet.isHidden = true
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.labelStatusOutlet.isHidden = true
                    item.activityIndicator.isHidden = true
                    item.imageViewOutlet.isHidden = true
                    item.buttonOutlet.isEnabled = true
                    item.buttonOutlet.title = self.buttonRunTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.0
                    item.buttonOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonRunTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    
                    item.labelSubtitleOutlet.stringValue = ""
                    
                }else if keyboardTestStatusID == "1" || keyboardTestStatusID == "2"{
                    if AppLanguage.current == .english{
                        item.labelTitleOutlet.stringValue = "Click run to begin webcam test"
                    }else if AppLanguage.current == .arabic{
                        item.labelTitleOutlet.stringValue = "انقر فوق تشغيل لبدء اختبار كاميرا الويب"
                    }else if AppLanguage.current == .spanish{
                        item.labelTitleOutlet.stringValue = "Haga clic en ejecutar para comenzar la prueba de la cámara web"
                    }else if AppLanguage.current == .portuguese{
                        item.labelTitleOutlet.stringValue = "Clique em executar para iniciar o teste da webcam"
                    }
                    item.buttonOutlet.isEnabled = false
                    item.buttonOutlet.title = self.buttonFinishTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.0
                    item.buttonOutlet.layer?.borderColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.cornerRadius = 0
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonFinishTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.black, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    
                    
                    item.imageViewOutlet.isHidden = false
                    if keyboardTestStatusID == "1"{
                        item.imageViewOutlet.image = NSImage(named: "tick")
                        if AppLanguage.current == .english{
                            item.labelSubtitleOutlet.stringValue = "Pass"
                        }else if AppLanguage.current == .arabic{
                            item.labelSubtitleOutlet.stringValue = "يمر"
                        }else if AppLanguage.current == .spanish{
                            item.labelSubtitleOutlet.stringValue = "Aprobar"
                        }else if AppLanguage.current == .portuguese{
                            item.labelSubtitleOutlet.stringValue = "Passar"
                        }
                    }else if keyboardTestStatusID == "2"{
                        item.imageViewOutlet.image = NSImage(named: "danger-1")
                        if AppLanguage.current == .english{
                            item.labelSubtitleOutlet.stringValue = "Failed"
                        }else if AppLanguage.current == .arabic{
                            item.labelSubtitleOutlet.stringValue = "باءت بالفشل"
                        }else if AppLanguage.current == .spanish{
                            item.labelSubtitleOutlet.stringValue = "Fallido"
                        }else if AppLanguage.current == .portuguese{
                            item.labelSubtitleOutlet.stringValue = "Fracassado"
                        }
                        
                    }
                    item.activityIndicator.isHidden = true
                    item.activityIndicator.stopAnimation(nil)
                    item.buttonRunAgainOUtlet.isHidden = false
                    item.labelStatusOutlet.isHidden = true
                    item.buttonRunAgainOUtlet.isEnabled = true
                    item.buttonSendReportOutlet.isEnabled = true
                    item.buttonSendReportOutlet.isHidden = false
                    item.buttonRunAgainOUtlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = true
                    item.buttonSendReportOutlet.isEnabled = true
                    item.buttonRunAgainOUtlet.isEnabled = true
                    item.buttonRunAgainOUtlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonRunAgainOUtlet.layer?.borderWidth = 0.0
                    item.buttonRunAgainOUtlet.layer?.borderColor = NSColor.clear.cgColor
                    item.buttonRunAgainOUtlet.layer?.cornerRadius = 0
                    item.buttonRunAgainOUtlet.attributedTitle = NSAttributedString(string: self.buttonRunAgainTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.black, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonSendReportOutlet.layer?.borderWidth = 0.0
                    item.buttonSendReportOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                    item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    item.buttonSendReportOutlet.action = #selector(buttonCameraTestSendReportAction)
                    item.buttonRunAgainOUtlet.action = #selector(buttonCameraTestRunAgainAction)
                    
                }else if isChargerStart == 1{
                    item.labelSubtitleOutlet.stringValue = ""
                    item.buttonOutlet.isEnabled = true
                    item.buttonOutlet.title = self.buttonStopTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.5
                    item.buttonOutlet.layer?.borderColor = NSColor.red.cgColor
                    item.buttonOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonStopTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.red, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    item.imageViewOutlet.isHidden = true
                    item.activityIndicator.isHidden = false
                    item.activityIndicator.isIndeterminate = true
                    item.activityIndicator.style = .spinning
                    item.activityIndicator.startAnimation(nil)
                    
                    
                    
                    
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.labelStatusOutlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = false
                    item.buttonSendReportOutlet.isEnabled = false
                    item.buttonSendReportOutlet.isHidden = false
                    
                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                    item.buttonSendReportOutlet.layer?.borderColor = NSColor.darkGray.cgColor
                    item.buttonSendReportOutlet.layer?.borderWidth = 1
                    item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.darkGray, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                }else{
                    item.labelSubtitleOutlet.stringValue = ""
                    item.buttonOutlet.isEnabled = true
                    item.buttonOutlet.title = self.buttonRunTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.0
                    item.buttonOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonRunTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    item.activityIndicator.isHidden = true
                    item.activityIndicator.stopAnimation(nil)
                    
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.labelStatusOutlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = false
                    item.buttonSendReportOutlet.isEnabled = false
                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                    item.buttonSendReportOutlet.layer?.borderColor = NSColor.darkGray.cgColor
                    item.buttonSendReportOutlet.layer?.borderWidth = 1
                    item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.darkGray, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                }
                
            }
            
            return item
        } else {
            return NSCollectionViewItem()
        }
    }
    
    @objc func buttonCaptureAction(button:NSButton) {
        print(captureConnection?.isActive)
        let photoSettings = AVCapturePhotoSettings()
        photoOutput?.capturePhoto(with: photoSettings, delegate: self)
    }
    
    @objc func buttonCameraTestRunAction(button:NSButton) {
        
        if isChargerStart == 0{
            self.isKeyboardTest = true
            self.collectionViewSubmenu.isSelectable = false
            session.startRunning()
            self.isChargerStart = 1
            timer.invalidate() // just in case this button is tapped multiple times
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(webcamTestTimerAction), userInfo: nil, repeats: true)
            startSession()
            self.prepareCamera()
        }else{
            self.isKeyboardTest = false
            self.collectionViewSubmenu.isSelectable = true
            session.stopRunning()
//            stopSession()
            captureSession.stopRunning()
            self.isChargerStart = 0
            self.timer.invalidate()
        }
        self.collectionViewTest.reloadData()
        print("Clicked \(button.title)!")
    }
    
    @objc func buttonCameraTestRunAgainAction(button:NSButton) {
        self.timer.invalidate()
        self.counter = 0
        self.keyboardTestStatusID = ""
        self.imageData.removeAll()
        self.isChargerStart = 0
        self.collectionViewTest.reloadData()
    }
    
    @objc func buttonCameraTestSendReportAction(button:NSButton) {
        var testName = ""
        if AppLanguage.current == .english{
            testName = "Webcam Test"
        }else if AppLanguage.current == .arabic{
            testName = "اختبار كاميرا الويب"
        }else if AppLanguage.current == .spanish{
            testName = "Prueba de cámara web"
        }else if AppLanguage.current == .portuguese{
            testName = "Teste de webcam"
        }
        let chargerData = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":keyboardTestStatusID,"testTypeId":"122",  "TestDatas":[]] as [String : Any]// this is for testing
        self.webcamTestArray.remove(at: 0)
        self.webcamTestArray.insert(chargerData, at: 0)
        UserDefaults.standard.setValue(self.webcamTestArray, forKey: "Webcam_Test")
        if Reachability.isConnectedToNetwork(){
            self.callSaveTestReportAPI(testArray: webcamTestArray, type: "webcam")
//       MARK: CALL UPLOAD API
            self.upload(testArray: webcamTestArray as NSArray)
            
            
        }else{
            if AppLanguage.current == .english{
                self.confirmAbletonIsReady(question: "Error", text: "Please Check Your Internet Connection Or Wifi")
            }else if AppLanguage.current == .arabic{
                self.confirmAbletonIsReady(question: "خطأ", text: "يرجى التحقق من اتصالك بالإنترنت أو شبكة Wifi")
            }else if AppLanguage.current == .spanish{
                self.confirmAbletonIsReady(question: "Error", text: "Por favor, compruebe su conexión a Internet o Wifi")
            }else if AppLanguage.current == .portuguese{
                self.confirmAbletonIsReady(question: "Erro", text: "Por favor, verifique sua conexão com a Internet ou Wi-Fi")
            }
        }
    }
    
    func upload(testArray:NSArray){

        let url = SERVER.saveTestReportByWithImageMobile
        print("The Url is",url)
       // self.loader.animationController(view: self.view)
        //        var parameters = Dictionary<String,Any>()
        var isTest = Int()
        let resultArray = NSMutableArray()
        if ((testArray[0] as? NSDictionary)?.value(forKey: "testStatus") as? Bool) == false{
            isTest = 2
        }else{
            isTest = 1
        }
//        let imgData = (self.imageViewCameraPictureOutlet.image ?? UIImage()).jpegData(compressionQuality: 1)!
        let imgData = self.imageData
        let  parameters = ["TestName":((testArray[0] as? NSDictionary)?.value(forKey: "name") as? String) ?? "","macAddress":getMacSerialNumber() ,"email":DataStore.shared.email ?? "","testStatusID":isTest,"TestTypeId":((testArray[0] as? NSDictionary)?.value(forKey: "testTypeId") as? Int) ?? 0,"Images":imgData] as [String : Any]
        print("-------->>>>>>>>>IS",parameters)
        NetworkManager.shared.requestWithSingleMultipart(mimeType: "", fileExtension: "", type: "", urlStr: url, imageData: imgData, "", parameters: parameters){ (res) in
//            self.loader.hideAnimationLoader(view: self.view)
            let dict = self.convertStringToDictionary(text: res as! String)
            print("Url is-------->>>>>>>>>IS",dict)
            if (dict?["status"] as? Int) == 200{
                
            }else{
//                self.showError((dict?["message"] as? String ?? ""))
            }
        } onError: { (err) in
//            self.loader.hideAnimationLoader(view: self.view)
        }
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {
                print("Something went wrong")
            }
        }
        return nil
    }
    
    @objc func webcamTestTimerAction() {
        if counter >= 20{
            timer.invalidate()
            session.stopRunning()
            captureSession.stopRunning()
            self.keyboardTestStatusID = "2"
            self.isKeyboardTest = false
            self.collectionViewSubmenu.isSelectable = true
            self.collectionViewTest.reloadData()
            if isMultipleTestSelect == true{
                if SelectArrayString.count > 0{
                    self.SelectArrayString.remove(at: 0)
                }
                self.buttonCameraTestSendReportAction(button: NSButton())
            }
            print("timer stop ---------->>>>>>>>>>>>>>>>>>>>>>>>",counter)
        }else{
            counter += 1
        }
        print("Counter is ---------->>>>>>>>>>>>>>>>>>>>>>>>",counter)
    }
    
    private func setupFrontCamera() {
        captureSession.sessionPreset = .high
        guard (AVCaptureDevice.default(for: AVMediaType.video)) != nil else {
            self.confirmAbletonIsReady(question: "Error", text: "Device has no camera.")
            return
        }
        let input: AVCaptureInput = try! AVCaptureDeviceInput(device: AVCaptureDevice.default(for: .video)!)
        captureSession.addInput(input)
        captureSession.startRunning()
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        view.layer?.addSublayer(previewLayer)
        previewLayer.frame = view.frame
        let dataOutput = AVCaptureVideoDataOutput()
        dataOutput.setSampleBufferDelegate(self, queue: DispatchQueue(label: "videoQueue"))
        captureSession.addOutput(dataOutput)
    }
    
    func captureOutput(_ output: AVCaptureOutput, didOutput sampleBuffer: CMSampleBuffer, from connection: AVCaptureConnection) {
        if #available(macOS 10.13, *) {
            
            guard let pixelBuffer: CVPixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else { return }
            let request = VNDetectFaceRectanglesRequest { req, err in
                if let err = err {
                    debugPrint("error in \(#function) -->>>", err)
                    return
                }
                
                DispatchQueue.main.async {
                    if let results = req.results {
                        print("\(results.count) face(s)------------->>>>>>>>>>>>>>>")
                        //                        if results.count > 0 {
                        //                            if self.titleString == "Rear Face Detector Test" || self.titleString == "اختبار كاشف الوجه الخلفي" || self.titleString == "Prueba del detector de la cara trasera" || self.titleString == "Teste do detector de rosto traseiro"{
                        //                                self.isRearFaceDetect = true
                        //                                self.previewLayer.removeFromSuperlayer()
                        //                                self.captureSession.stopRunning()
                        ////                                self.rearFaceDetectTestComplete()
                        //                            }else{
                        ////                                self.isFrontFaceDetect = true
                        //                                self.previewLayer.removeFromSuperlayer()
                        //                                self.captureSession.stopRunning()
                        ////                                self.frontFaceDetectTestComplete()
                        //                            }
                        //                        }
                    }
                }
            }
            
            DispatchQueue.global(qos: .userInteractive).async {
                let handler = VNImageRequestHandler(cvPixelBuffer: pixelBuffer, options: [:])
                do {
                    try handler.perform([request])
                } catch {
                    self.previewLayer.removeFromSuperlayer()
                    self.captureSession.stopRunning()
                    debugPrint("failed to perform request in \(#function)")
                }
            }
        }
    }

    func startSession() {
        if !captureSession.isRunning {
            captureSession.startRunning()
        }
    }
    
    func stopSession() {
        if captureSession.isRunning {
            captureSession.stopRunning()
        }
    }
    
    internal func photoOutput(_ output: AVCapturePhotoOutput, didFinishProcessingPhoto photo: AVCapturePhoto, error: Error?) {
        let imageData = photo.fileDataRepresentation()
        if let data = imageData{
            timer.invalidate()
            self.imageData = data
            session.stopRunning()
            captureSession.stopRunning()
            self.keyboardTestStatusID = "1"
            self.isKeyboardTest = false
            self.collectionViewSubmenu.isSelectable = true
            self.collectionViewTest.reloadData()
            if isMultipleTestSelect == true{
                if SelectArrayString.count > 0{
                    self.SelectArrayString.remove(at: 0)
                }
                self.buttonCameraTestSendReportAction(button: NSButton())
            }
        }else{
            timer.invalidate()
            session.stopRunning()
            captureSession.stopRunning()
            self.keyboardTestStatusID = "2"
            self.isKeyboardTest = false
            self.collectionViewSubmenu.isSelectable = true
            self.collectionViewTest.reloadData()
            if isMultipleTestSelect == true{
                if SelectArrayString.count > 0{
                    self.SelectArrayString.remove(at: 0)
                }
                self.buttonCameraTestSendReportAction(button: NSButton())
            }
        }
        print(photo)
    }
    
    func takePhoto() {
        print(captureConnection?.isActive)
        let photoSettings = AVCapturePhotoSettings()
        photoOutput?.capturePhoto(with: photoSettings, delegate: self)
    }
    
    func prepareCamera() {
        photoOutput = AVCapturePhotoOutput()
        captureSession = AVCaptureSession()
        captureSession.sessionPreset = AVCaptureSession.Preset.photo
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.videoGravity = AVLayerVideoGravity.resizeAspectFill
        do {
            let deviceDiscoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: [AVCaptureDevice.DeviceType.builtInWideAngleCamera], mediaType: AVMediaType.video, position: AVCaptureDevice.Position.front)
            let cameraDevice = deviceDiscoverySession.devices[0]
            let videoInput = try AVCaptureDeviceInput(device: cameraDevice)
            captureSession.beginConfiguration()
            if captureSession.canAddInput(videoInput) {
                print("Adding videoInput to captureSession")
                captureSession.addInput(videoInput)
            } else {
                print("Unable to add videoInput to captureSession")
            }
            if captureSession.canAddOutput(photoOutput!) {
                captureSession.addOutput(photoOutput!)
                print("Adding videoOutput to captureSession")
            } else {
                print("Unable to add videoOutput to captureSession")
            }
            captureConnection = AVCaptureConnection(inputPorts: videoInput.ports, output: photoOutput!)
            captureSession.commitConfiguration()
            //            if let previewLayer = previewLayer {
            if ((previewLayer.connection?.isVideoMirroringSupported) != nil) {
                previewLayer.connection?.automaticallyAdjustsVideoMirroring = false
                previewLayer.connection?.isVideoMirrored = true
            }
            //            previewLayer.frame = view.bounds
            //            view.layer = previewLayer
            //            view.wantsLayer = true
            //            }
            captureSession.startRunning()
        } catch {
            print(error.localizedDescription)
        }
    }
}
extension HomeVC{
    // MARK: - Volume Test
    func volumeCollectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        var item: NSCollectionViewItem
        if #available(macOS 10.11, *) {
            item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "VolumeTestCollectionViewCell"), for: indexPath) as! VolumeTestCollectionViewCell
            
            
            
            if let item = item as? VolumeTestCollectionViewCell {
                if AppLanguage.current == .english{
                    item.labelStatusOutlet.stringValue = "Please wait untill test finish"
                }else if AppLanguage.current == .arabic{
                    item.labelStatusOutlet.stringValue = "الرجاء الانتظار حتى تنتهي المهمة"
                }else if AppLanguage.current == .spanish{
                    item.labelStatusOutlet.stringValue = "Espere hasta que finalice la tarea"
                }else if AppLanguage.current == .portuguese{
                    item.labelStatusOutlet.stringValue = "Aguarde até que a tarefa termine"
                }
                item.buttonOutlet.bezelStyle = .texturedSquare
                item.buttonOutlet.isBordered = false //Important
                item.buttonOutlet.wantsLayer = true
                item.buttonSendReportOutlet.bezelStyle = .texturedSquare
                item.buttonSendReportOutlet.isBordered = false //Important
                item.buttonSendReportOutlet.wantsLayer = true
                item.buttonRunAgainOUtlet.bezelStyle = .texturedSquare
                item.buttonRunAgainOUtlet.isBordered = false //Important
                item.buttonRunAgainOUtlet.wantsLayer = true
                item.buttonOutlet.action = #selector(buttonVolumeTestRunAction(button:))
                if AppLanguage.current == .english{
                    item.labelOutlet.stringValue = "Volume Test"
                }else if AppLanguage.current == .arabic{
                    item.labelOutlet.stringValue = "اختبار الحجم"
                }else if AppLanguage.current == .spanish{
                    item.labelOutlet.stringValue = "Prueba de volumen"
                }else if AppLanguage.current == .portuguese{
                    item.labelOutlet.stringValue = "Teste de volume"
                }
                
                let currentVol = Double(currentVolume * 100)
                //                String(format: "%.2f", (currentVolume * 100))
                
                item.labelPercentageOutlet.stringValue = String(format: "%.0f", (currentVolume * 100))
                item.progressIndicator.doubleValue = currentVol
                if isChargerStart == 0{// 1234
                    item.viewOutlet.isHidden = true
                    item.viewOutlet?.wantsLayer = true
                    item.viewOutlet?.layer?.backgroundColor = NSColor(hex: "F4F5F9").cgColor
                    if AppLanguage.current == .english{
                        item.labelTitleOutlet.stringValue = "Use the volume keys on the keyboard to increase and decrease the volume."
                    }else if AppLanguage.current == .arabic{
                        item.labelTitleOutlet.stringValue = "استخدم مفاتيح مستوى الصوت على لوحة المفاتيح لرفع مستوى الصوت وخفضه."
                    }else if AppLanguage.current == .spanish{
                        item.labelTitleOutlet.stringValue = "Utilice las teclas de volumen del teclado para aumentar y disminuir el volumen"
                    }else if AppLanguage.current == .portuguese{
                        item.labelTitleOutlet.stringValue = "Use as teclas de volume do teclado para aumentar e diminuir o volume."
                    }
                    item.buttonSendReportOutlet.isHidden = true
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.labelStatusOutlet.isHidden = true
                    item.activityIndicator.isHidden = true
                    item.imageViewOutlet.isHidden = true
                    item.buttonOutlet.isEnabled = true
                    item.buttonOutlet.title = self.buttonRunTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.0
                    item.buttonOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonRunTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    item.labelSubtitleOutlet.stringValue = ""
                    
                }else if keyboardTestStatusID == "1" || keyboardTestStatusID == "2"{
                    if AppLanguage.current == .english{
                        item.labelTitleOutlet.stringValue = "Use the volume keys on the keyboard to increase and decrease the volume."
                    }else if AppLanguage.current == .arabic{
                        item.labelTitleOutlet.stringValue = "استخدم مفاتيح مستوى الصوت على لوحة المفاتيح لرفع مستوى الصوت وخفضه."
                    }else if AppLanguage.current == .spanish{
                        item.labelTitleOutlet.stringValue = "Utilice las teclas de volumen del teclado para aumentar y disminuir el volumen"
                    }else if AppLanguage.current == .portuguese{
                        item.labelTitleOutlet.stringValue = "Use as teclas de volume do teclado para aumentar e diminuir o volume."
                    }
                    item.viewOutlet.isHidden = true
                    item.buttonOutlet.isEnabled = false
                    item.buttonOutlet.title = self.buttonFinishTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.0
                    item.buttonOutlet.layer?.borderColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.cornerRadius = 0
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonFinishTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.black, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    
                    item.imageViewOutlet.isHidden = false
                    if keyboardTestStatusID == "1"{
                        item.imageViewOutlet.image = NSImage(named: "tick")
                        if AppLanguage.current == .english{
                            item.labelSubtitleOutlet.stringValue = "Pass"
                        }else if AppLanguage.current == .arabic{
                            item.labelSubtitleOutlet.stringValue = "يمر"
                        }else if AppLanguage.current == .spanish{
                            item.labelSubtitleOutlet.stringValue = "Aprobar"
                        }else if AppLanguage.current == .portuguese{
                            item.labelSubtitleOutlet.stringValue = "Passar"
                        }
                    }else if keyboardTestStatusID == "2"{
                        item.imageViewOutlet.image = NSImage(named: "danger-1")
                        if AppLanguage.current == .english{
                            item.labelSubtitleOutlet.stringValue = "Failed"
                        }else if AppLanguage.current == .arabic{
                            item.labelSubtitleOutlet.stringValue = "باءت بالفشل"
                        }else if AppLanguage.current == .spanish{
                            item.labelSubtitleOutlet.stringValue = "Fallido"
                        }else if AppLanguage.current == .portuguese{
                            item.labelSubtitleOutlet.stringValue = "Fracassado"
                        }
                        
                    }
                    item.activityIndicator.isHidden = true
                    item.activityIndicator.stopAnimation(nil)
                    item.buttonRunAgainOUtlet.isHidden = false
                    item.labelStatusOutlet.isHidden = true
                    item.buttonRunAgainOUtlet.isEnabled = true
                    item.buttonSendReportOutlet.isEnabled = true
                    item.buttonSendReportOutlet.isHidden = false
                    item.buttonRunAgainOUtlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = true
                    item.buttonSendReportOutlet.isEnabled = true
                    item.buttonRunAgainOUtlet.isEnabled = true
                    item.buttonRunAgainOUtlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonRunAgainOUtlet.layer?.borderWidth = 0.0
                    item.buttonRunAgainOUtlet.layer?.borderColor = NSColor.clear.cgColor
                    item.buttonRunAgainOUtlet.layer?.cornerRadius = 0
                    item.buttonRunAgainOUtlet.attributedTitle = NSAttributedString(string: self.buttonRunAgainTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.black, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonSendReportOutlet.layer?.borderWidth = 0.0
                    item.buttonSendReportOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                    item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonSendReportOutlet.action = #selector(buttonVolumeTestSendReportAction(button:))
                    item.buttonRunAgainOUtlet.action = #selector(buttonVolumeTestRunAgainAction(button:))
                    
                }else if isChargerStart == 1{
                    if currentVolume == 0.0{
                        item.imageViewStatusOutlet.image = NSImage(named: "mute")
                    }else if currentVolume == 1.0{
                        item.imageViewStatusOutlet.image = NSImage(named: "volume-1")
                    }else{
                        item.imageViewStatusOutlet.image = NSImage(named: "volume-1")
                    }
                    item.viewOutlet.isHidden = false
                    item.labelSubtitleOutlet.stringValue = ""
                    item.buttonOutlet.isEnabled = true
                    item.buttonOutlet.title = self.buttonStopTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.5
                    item.buttonOutlet.layer?.borderColor = NSColor.red.cgColor
                    item.buttonOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonStopTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.red, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.imageViewOutlet.isHidden = true
                    item.activityIndicator.isHidden = false
                    item.activityIndicator.isIndeterminate = true
                    item.activityIndicator.style = .spinning
                    item.activityIndicator.startAnimation(nil)
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.labelStatusOutlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = false
                    item.buttonSendReportOutlet.isEnabled = false
                    item.buttonSendReportOutlet.isHidden = false
                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                    item.buttonSendReportOutlet.layer?.borderColor = NSColor.darkGray.cgColor
                    item.buttonSendReportOutlet.layer?.borderWidth = 1
                    item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.darkGray, NSAttributedString.Key.paragraphStyle : pstyle ])
                }else{
                    item.viewOutlet.isHidden = true
                    item.labelSubtitleOutlet.stringValue = ""
                    item.buttonOutlet.isEnabled = true
                    item.buttonOutlet.title = self.buttonRunTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.0
                    item.buttonOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonRunTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    
                    item.activityIndicator.isHidden = true
                    item.activityIndicator.stopAnimation(nil)
                    
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.labelStatusOutlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = false
                    item.buttonSendReportOutlet.isEnabled = false
                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                    item.buttonSendReportOutlet.layer?.borderColor = NSColor.darkGray.cgColor
                    item.buttonSendReportOutlet.layer?.borderWidth = 1
                    item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.darkGray, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                }
                
            }
            
            return item
        } else {
            return NSCollectionViewItem()
        }
    }
    
    @objc func buttonVolumeTestRunAction(button:NSButton) {
        
        if isChargerStart == 0{
            self.isKeyboardTest = true
            self.collectionViewSubmenu.isSelectable = false
            self.isChargerStart = 1
            timer.invalidate() // just in case this button is tapped multiple times
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(volumeTestTimerAction), userInfo: nil, repeats: true)
            self.open_audiofile(testName: "", tag: -1)
        }else{
            self.isKeyboardTest = false
            self.collectionViewSubmenu.isSelectable = true
            self.isChargerStart = 0
            audioEngine.stop()
            audioPlayer.stop()
            self.timer.invalidate()
        }
        self.collectionViewTest.reloadData()
        print("Clicked \(button.title)!")
    }
    
    @objc func buttonVolumeTestRunAgainAction(button:NSButton) {
        self.timer.invalidate()
        audioEngine.stop()
        audioPlayer.stop()
        self.isVolumeLow = false
        self.isVolumeHigh = false
        self.counter = 0
        self.keyboardTestStatusID = ""
        self.isChargerStart = 0
        self.collectionViewTest.reloadData()
        
    }
    @objc func buttonVolumeTestSendReportAction(button:NSButton) {
        audioEngine.stop()
        audioPlayer.stop()
        
        var testName = ""
        if AppLanguage.current == .english{
            testName = "Volume Test"
        }else if AppLanguage.current == .arabic{
            testName = "اختبار الحجم"
        }else if AppLanguage.current == .spanish{
            testName = "Prueba de volumen"
        }else if AppLanguage.current == .portuguese{
            testName = "Teste de Volume"
        }
        
        let chargerData = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":keyboardTestStatusID,"testTypeId":"123",  "TestDatas":[]] as [String : Any]// this is for testing
        self.volumeTestArray.remove(at: 0)
        self.volumeTestArray.insert(chargerData, at: 0)
        UserDefaults.standard.setValue(self.volumeTestArray, forKey: "Webcam_Test")
        if Reachability.isConnectedToNetwork(){
            self.callSaveTestReportAPI(testArray: volumeTestArray, type: "volume")
        }else{
            if AppLanguage.current == .english{
                self.confirmAbletonIsReady(question: "Error", text: "Please Check Your Internet Connection Or Wifi")
            }else if AppLanguage.current == .arabic{
                self.confirmAbletonIsReady(question: "خطأ", text: "يرجى التحقق من اتصالك بالإنترنت أو شبكة Wifi")
            }else if AppLanguage.current == .spanish{
                self.confirmAbletonIsReady(question: "Error", text: "Por favor, compruebe su conexión a Internet o Wifi")
            }else if AppLanguage.current == .portuguese{
                self.confirmAbletonIsReady(question: "Erro", text: "Por favor, verifique sua conexão com a Internet ou Wi-Fi")
            }
        }
    }
    
    @objc func volumeTestTimerAction() {
        if counter >= 10{
            audioEngine.stop()
            audioPlayer.stop()
            timer.invalidate()
            self.keyboardTestStatusID = "2"
            self.isKeyboardTest = false
            self.collectionViewSubmenu.isSelectable = true
            self.collectionViewTest.reloadData()
            if isMultipleTestSelect == true{
                if SelectArrayString.count > 0{
                    self.SelectArrayString.remove(at: 0)
                }
                self.buttonVolumeTestSendReportAction(button: NSButton())
            }
            print("timer stop ---------->>>>>>>>>>>>>>>>>>>>>>>>",counter)
        }else{
            counter += 1
            currentVolume = NSSound.systemVolume
            print("currentVolume ---------->>>>>>>>>>>>>>>>>>>>>>>>",currentVolume)
            
            if currentVolume == 0.0{
                self.isVolumeLow = true
            }else if currentVolume == 1.0{
                self.isVolumeHigh = true
            }
            if isVolumeLow == true && isVolumeHigh == true{
                self.keyboardTestStatusID = "1"
                audioEngine.stop()
                audioPlayer.stop()
                timer.invalidate()
                if isMultipleTestSelect == true{
                    if SelectArrayString.count > 0{
                        self.SelectArrayString.remove(at: 0)
                    }
                    self.buttonVolumeTestSendReportAction(button: NSButton())
                }
            }
            self.collectionViewTest.reloadData()
        }
        print("Counter is ---------->>>>>>>>>>>>>>>>>>>>>>>>",counter)
    }
    
    
    func open_audiofile(testName:String,tag:Int) {
        //get where the file is
        let url = Bundle.main.url(forResource: "sound", withExtension: "mp3")
        //put it in an AVAudioFile
        let audioFile = try! AVAudioFile(forReading: url!)
        //Get the audio file format
        //let format = AVAudioFormat(commonFormat: .pcmFormatFloat32, sampleRate: file.fileFormat.sampleRate, channels: file.fileFormat.channelCount, interleaved: false)
        let audioFormat = audioFile.processingFormat
        let audioFrameCount = UInt32(audioFile.length)
        //how many channels?
        print(audioFile.fileFormat.channelCount)
        print(audioFrameCount)
        
        if audioFile.fileFormat.channelCount > 0 {
            //Setup the buffer for audio data
            let audioFileBuffer = AVAudioPCMBuffer(pcmFormat: audioFormat, frameCapacity: UInt32(audioFile.length))
            //put audio data in the buffer
            try! audioFile.read(into: audioFileBuffer!)
            //readFile.arrayFloatValues = Array(UnsafeBufferPointer(start: audioFileBuffer!.floatChannelData?[0], count:Int(audioFileBuffer!.frameLength)))
            
            //Init engine and player
            let mainMixer = audioEngine.mainMixerNode
            audioEngine.attach(audioPlayer)
            audioEngine.connect(audioPlayer, to:mainMixer, format: audioFileBuffer!.format)
            audioPlayer.scheduleBuffer(audioFileBuffer!, completionHandler: nil)
            audioEngine.prepare()
            do {
                try audioEngine.start()
                print("engine started")
            } catch let error {
                print(error.localizedDescription)
            }
            if testName == "audio"{
                if tag == 0 {
                    audioPlayer.pan = -1.0
                    audioPlayer.play()
                }else if tag == 1{
                    audioPlayer.pan =  1.0
                    audioPlayer.play()
                }else if tag == 2{
                    self.audioEngine.stop()
                    self.audioPlayer.stop()
                    self.playSound()
                }
            }else{
                audioPlayer.play()
            }
        }else{
            self.confirmAbletonIsReady(question: "Error", text: "System does not have speekers.")
        }
    }
}
extension HomeVC{
    // MARK: - Mic Test
    
    func MicCollectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        var item: NSCollectionViewItem
        if #available(macOS 10.11, *) {
            item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "MicTestCollectionViewCell"), for: indexPath) as! MicTestCollectionViewCell
            
            if let item = item as? MicTestCollectionViewCell {
                if AppLanguage.current == .english{
                    item.labelStatusOutlet.stringValue = "Please wait untill test finish"
                }else if AppLanguage.current == .arabic{
                    item.labelStatusOutlet.stringValue = "الرجاء الانتظار حتى تنتهي المهمة"
                }else if AppLanguage.current == .spanish{
                    item.labelStatusOutlet.stringValue = "Espere hasta que finalice la tarea"
                }else if AppLanguage.current == .portuguese{
                    item.labelStatusOutlet.stringValue = "Aguarde até que a tarefa termine"
                }
                item.buttonOutlet.bezelStyle = .texturedSquare
                item.buttonOutlet.isBordered = false //Important
                item.buttonOutlet.wantsLayer = true
                item.buttonSendReportOutlet.bezelStyle = .texturedSquare
                item.buttonSendReportOutlet.isBordered = false //Important
                item.buttonSendReportOutlet.wantsLayer = true
                item.buttonRunAgainOUtlet.bezelStyle = .texturedSquare
                item.buttonRunAgainOUtlet.isBordered = false //Important
                item.buttonRunAgainOUtlet.wantsLayer = true
                item.buttonOutlet.action = #selector(buttonMicTestRunAction)
                if AppLanguage.current == .english{
                    item.labelOutlet.stringValue = "Mic Test"
                }else if AppLanguage.current == .arabic{
                    item.labelOutlet.stringValue = "اختبار الميكروفون"
                }else if AppLanguage.current == .spanish{
                    item.labelOutlet.stringValue = "Prueba de micrófono"
                }else if AppLanguage.current == .portuguese{
                    item.labelOutlet.stringValue = "Teste de microfone"
                }
                if isChargerStart == 0{
                    item.imageViewStatusOutlet.isHidden = true
                    if AppLanguage.current == .english{
                        item.labelTitleOutlet.stringValue = "Speak to check microphone"
                    }else if AppLanguage.current == .arabic{
                        item.labelTitleOutlet.stringValue = "تحدث للتحقق من الميكروفون"
                    }else if AppLanguage.current == .spanish{
                        item.labelTitleOutlet.stringValue = "Hablar para comprobar el micrófono"
                    }else if AppLanguage.current == .portuguese{
                        item.labelTitleOutlet.stringValue = "Fale para verificar o microfone"
                    }
                    item.buttonSendReportOutlet.isHidden = true
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.labelStatusOutlet.isHidden = true
                    item.activityIndicator.isHidden = true
                    item.imageViewOutlet.isHidden = true
                    item.buttonOutlet.isEnabled = true
                    item.buttonOutlet.title = self.buttonRunTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.0
                    item.buttonOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonRunTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.labelSubtitleOutlet.stringValue = ""
                    
                }else if keyboardTestStatusID == "1" || keyboardTestStatusID == "2"{
                    item.imageViewStatusOutlet.isHidden = true
                    item.buttonOutlet.isEnabled = false
                    item.buttonOutlet.title = self.buttonFinishTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.0
                    item.buttonOutlet.layer?.borderColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.cornerRadius = 0
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonFinishTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.black, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    
                    item.imageViewOutlet.isHidden = false
                    if keyboardTestStatusID == "1"{
                        item.imageViewOutlet.image = NSImage(named: "tick")
                        if AppLanguage.current == .english{
                            item.labelSubtitleOutlet.stringValue = "Pass"
                        }else if AppLanguage.current == .arabic{
                            item.labelSubtitleOutlet.stringValue = "يمر"
                        }else if AppLanguage.current == .spanish{
                            item.labelSubtitleOutlet.stringValue = "Aprobar"
                        }else if AppLanguage.current == .portuguese{
                            item.labelSubtitleOutlet.stringValue = "Passar"
                        }
                    }else if keyboardTestStatusID == "2"{
                        item.imageViewOutlet.image = NSImage(named: "danger-1")
                        if AppLanguage.current == .english{
                            item.labelSubtitleOutlet.stringValue = "Failed"
                        }else if AppLanguage.current == .arabic{
                            item.labelSubtitleOutlet.stringValue = "باءت بالفشل"
                        }else if AppLanguage.current == .spanish{
                            item.labelSubtitleOutlet.stringValue = "Fallido"
                        }else if AppLanguage.current == .portuguese{
                            item.labelSubtitleOutlet.stringValue = "Fracassado"
                        }
                        
                    }
                    item.activityIndicator.isHidden = true
                    item.activityIndicator.stopAnimation(nil)
                    item.buttonRunAgainOUtlet.isHidden = false
                    item.labelStatusOutlet.isHidden = true
                    item.buttonRunAgainOUtlet.isEnabled = true
                    item.buttonSendReportOutlet.isEnabled = true
                    item.buttonSendReportOutlet.isHidden = false
                    item.buttonRunAgainOUtlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = true
                    item.buttonSendReportOutlet.isEnabled = true
                    item.buttonRunAgainOUtlet.isEnabled = true
                    item.buttonRunAgainOUtlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonRunAgainOUtlet.layer?.borderWidth = 0.0
                    item.buttonRunAgainOUtlet.layer?.borderColor = NSColor.clear.cgColor
                    item.buttonRunAgainOUtlet.layer?.cornerRadius = 0
                    item.buttonRunAgainOUtlet.attributedTitle = NSAttributedString(string: self.buttonRunAgainTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.black, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    //
                    
                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonSendReportOutlet.layer?.borderWidth = 0.0
                    item.buttonSendReportOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                    item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonSendReportOutlet.action = #selector(buttonMicTestSendReportAction)
                    item.buttonRunAgainOUtlet.action = #selector(buttonMicTestRunAgainAction)
                    
                }else if isChargerStart == 1{
//                    item.imageViewStatusOutlet.isHidden = false
                    item.imageViewStatusOutlet.isHidden = true

                    item.imageViewStatusOutlet.canDrawSubviewsIntoLayer = true
                    item.imageViewStatusOutlet.imageScaling = .scaleProportionallyDown
                    item.imageViewStatusOutlet.animates = true
                    item.imageViewStatusOutlet.image = NSImage(named: "support")
                    item.labelSubtitleOutlet.stringValue = ""
                    item.buttonOutlet.isEnabled = true
                    item.buttonOutlet.title = self.buttonStopTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.5
                    item.buttonOutlet.layer?.borderColor = NSColor.red.cgColor
                    item.buttonOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonStopTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.red, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.imageViewOutlet.isHidden = true
                    item.activityIndicator.isHidden = false
                    item.activityIndicator.isIndeterminate = true
                    item.activityIndicator.style = .spinning
                    item.activityIndicator.startAnimation(nil)
                    
                    
                    
                    
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.labelStatusOutlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = false
                    item.buttonSendReportOutlet.isEnabled = false
                    item.buttonSendReportOutlet.isHidden = false
                    
                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                    item.buttonSendReportOutlet.layer?.borderColor = NSColor.darkGray.cgColor
                    item.buttonSendReportOutlet.layer?.borderWidth = 1
                    item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.darkGray, NSAttributedString.Key.paragraphStyle : pstyle ])
                }else{
                    item.imageViewStatusOutlet.isHidden = true
                    item.labelSubtitleOutlet.stringValue = ""
                    item.buttonOutlet.isEnabled = true
                    item.buttonOutlet.title = self.buttonRunTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.0
                    item.buttonOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonRunTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    item.activityIndicator.isHidden = true
                    item.activityIndicator.stopAnimation(nil)
                    
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.labelStatusOutlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = false
                    item.buttonSendReportOutlet.isEnabled = false
                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                    item.buttonSendReportOutlet.layer?.borderColor = NSColor.darkGray.cgColor
                    item.buttonSendReportOutlet.layer?.borderWidth = 1
                    item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.darkGray, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                }
                
            }
            
            return item
        } else {
            return NSCollectionViewItem()
        }
    }
    
    @objc func buttonMicTestRunAction(button:NSButton) {
        if isChargerStart == 0{
            self.isKeyboardTest = true
            self.collectionViewSubmenu.isSelectable = false
            self.isChargerStart = 1
            timer.invalidate() // just in case this button is tapped multiple times
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(micTestTimerAction), userInfo: nil, repeats: true)
            print("start recording")
            self.audioControl?.startRecord()
            isRecording = true
            setupUpdateMeter()
        }else{
            self.isKeyboardTest = false
            self.collectionViewSubmenu.isSelectable = true
            self.isChargerStart = 0
            self.timer.invalidate()
            self.audioControl?.pauseRecord()
        }
        self.collectionViewTest.reloadData()
        print("Clicked \(button.title)!")
    }

    func setupUpdateMeter() {
        self.timerNew = DispatchSource.makeTimerSource(flags: [], queue: DispatchQueue.main)
        self.timerNew!.schedule(deadline: .now(), repeating: .microseconds(100))
        self.timerNew!.setEventHandler
        {
            self.audioMeter.decibel = (self.audioControl?.getValue() ?? 0.0)
            print("(self.audioControl?.getValue() ?? 0.0)",(self.audioControl?.getValue() ?? 0.0))
            if (self.audioControl?.getValue() ?? 0.0) >= (-60.0){
                self.keyboardTestStatusID = "1"
            }else{
                self.keyboardTestStatusID = "2"
            }
            self.audioMeter.display()
        }
        self.timerNew!.resume()
    }
    
    @objc func buttonMicTestRunAgainAction(button:NSButton) {
        self.timer.invalidate()
        self.counter = 0
        self.keyboardTestStatusID = ""
        self.isChargerStart = 0
        self.audioControl?.stopRecord()
        self.timerNew?.cancel()
        self.audioMeter.decibel = -120
        self.audioMeter.display()
        self.collectionViewTest.reloadData()
        
    }
    @objc func buttonMicTestSendReportAction(button:NSButton) {
        var testName = ""
        if AppLanguage.current == .english{
             testName = "Microphone Test"
        }else if AppLanguage.current == .arabic{
             testName = "اختبار الميكروفون"
        }else if AppLanguage.current == .spanish{
             testName = "Prueba de micrófono"
        }else if AppLanguage.current == .portuguese{
             testName = "Teste do microfone"
        }
        
        let chargerData = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":keyboardTestStatusID,"testTypeId":"124",  "TestDatas":[]] as [String : Any]// this is for testing
        self.micTestArray.remove(at: 0)
        self.micTestArray.insert(chargerData, at: 0)
        UserDefaults.standard.setValue(self.micTestArray, forKey: "Mic_Test")
        if Reachability.isConnectedToNetwork(){
            self.callSaveTestReportAPI(testArray: micTestArray, type: "mic")
        }else{
            if AppLanguage.current == .english{
                self.confirmAbletonIsReady(question: "Error", text: "Please Check Your Internet Connection Or Wifi")
            }else if AppLanguage.current == .arabic{
                self.confirmAbletonIsReady(question: "خطأ", text: "يرجى التحقق من اتصالك بالإنترنت أو شبكة Wifi")
            }else if AppLanguage.current == .spanish{
                self.confirmAbletonIsReady(question: "Error", text: "Por favor, compruebe su conexión a Internet o Wifi")
            }else if AppLanguage.current == .portuguese{
                self.confirmAbletonIsReady(question: "Erro", text: "Por favor, verifique sua conexão com a Internet ou Wi-Fi")
            }
        }
    }
    
    @objc func micTestTimerAction() {
        
        if counter >= 10{
            timer.invalidate()
            if self.keyboardTestStatusID == "1"{
                self.keyboardTestStatusID = "1"
            }else{
                self.keyboardTestStatusID = "2"
            }
            self.audioControl?.stopRecord()
            self.timerNew?.cancel()
            self.audioMeter.decibel = -120
            self.audioMeter.display()
            self.isKeyboardTest = false
            self.collectionViewSubmenu.isSelectable = true
            self.collectionViewTest.reloadData()
            if isMultipleTestSelect == true{
                if SelectArrayString.count > 0{
                    self.SelectArrayString.remove(at: 0)
                }
                self.buttonMicTestSendReportAction(button: NSButton())
            }
            print("timer stop ---------->>>>>>>>>>>>>>>>>>>>>>>>",counter)
        }else{
            counter += 1
        }
        print("Counter is ---------->>>>>>>>>>>>>>>>>>>>>>>>",counter)
    }
}

extension HomeVC{
    // MARK: - Pixels Test
    
    func pixelsCollectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        var item: NSCollectionViewItem
        if #available(macOS 10.11, *) {
            item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "PixelsTestCollectionViewCell"), for: indexPath) as! PixelsTestCollectionViewCell
            
            
            
            if let item = item as? PixelsTestCollectionViewCell {
                if AppLanguage.current == .english{
                    item.labelStatusOutlet.stringValue = "Please wait untill test finish"
                }else if AppLanguage.current == .arabic{
                    item.labelStatusOutlet.stringValue = "الرجاء الانتظار حتى تنتهي المهمة"
                }else if AppLanguage.current == .spanish{
                    item.labelStatusOutlet.stringValue = "Espere hasta que finalice la tarea"
                }else if AppLanguage.current == .portuguese{
                    item.labelStatusOutlet.stringValue = "Aguarde até que a tarefa termine"
                }
                item.buttonOutlet.bezelStyle = .texturedSquare
                item.buttonOutlet.isBordered = false //Important
                item.buttonOutlet.wantsLayer = true
                item.buttonYesOutlet.bezelStyle = .texturedSquare
                item.buttonYesOutlet.isBordered = false //Important
                item.buttonYesOutlet.wantsLayer = true
                item.buttonNoOutlet.bezelStyle = .texturedSquare
                item.buttonNoOutlet.isBordered = false //Important
                item.buttonNoOutlet.wantsLayer = true
                item.buttonSendReportOutlet.bezelStyle = .texturedSquare
                item.buttonSendReportOutlet.isBordered = false //Important
                item.buttonSendReportOutlet.wantsLayer = true
                item.buttonRunAgainOUtlet.bezelStyle = .texturedSquare
                item.buttonRunAgainOUtlet.isBordered = false //Important
                item.buttonRunAgainOUtlet.wantsLayer = true
                item.buttonOutlet.action = #selector(buttonDisplayPixelsTestRunAction)
                if AppLanguage.current == .english{
                    item.labelOutlet.stringValue = "Defective Pixels Test"
                }else if AppLanguage.current == .arabic{
                    item.labelOutlet.stringValue = "اختبار البكسل المعيب"
                }else if AppLanguage.current == .spanish{
                    item.labelOutlet.stringValue = "Prueba de píxeles defectuosos"
                }else if AppLanguage.current == .portuguese{
                    item.labelOutlet.stringValue = "Teste de pixel com defeito"
                }

                item.buttonYesOutlet.action = #selector(buttonDisplayPixelsYesAction)
                item.buttonOutlet.action = #selector(buttonDisplayPixelsTestRunAction)
                item.buttonNoOutlet.action = #selector(buttonDisplayPixelsNoAction)
                if isChargerStart == 0{// 1234
                    if AppLanguage.current == .english{
                        item.labelTitleOutlet.stringValue = "Press the run button, to change color use arrow keys and press ESC to exit."
                    }else if AppLanguage.current == .arabic{
                        item.labelTitleOutlet.stringValue = "اضغط على زر التشغيل لتغيير لون استخدام مفاتيح الأسهم واضغط على ESC للخروج."
                    }else if AppLanguage.current == .spanish{
                        item.labelTitleOutlet.stringValue = "Presione el botón Ejecutar, para cambiar el color use las teclas de flecha y presione ESC para salir"
                    }else if AppLanguage.current == .portuguese{
                        item.labelTitleOutlet.stringValue = "Pressione o botão de execução, para alterar a cor, use as teclas de seta e pressione ESC para sair."
                    }
                    item.buttonOutlet.isHidden = false
                    item.buttonYesOutlet.isHidden = true
                    item.buttonNoOutlet.isHidden = true
                    item.buttonSendReportOutlet.isHidden = true
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.labelStatusOutlet.isHidden = true
                    item.activityIndicator.isHidden = true
                    item.imageViewOutlet.isHidden = true
                    item.buttonOutlet.isEnabled = true
                    item.buttonOutlet.title = self.buttonRunTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.0
                    item.buttonOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonRunTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    item.labelSubtitleOutlet.stringValue = ""
                    
                }else if isPixelsTestFinish == true{
                    item.buttonOutlet.isHidden = true
                    item.buttonYesOutlet.isHidden = false
                    item.buttonNoOutlet.isHidden = false
                    item.buttonOutlet.isEnabled = true
                    item.labelStatusOutlet.isHidden = true
                    item.imageViewOutlet.isHidden = true
                    item.activityIndicator.isHidden = true
                    item.activityIndicator.stopAnimation(nil)
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.buttonSendReportOutlet.isHidden = true
                    
                    
                    item.buttonYesOutlet.isEnabled = true
                    item.buttonYesOutlet.layer?.backgroundColor = NSColor(hex: "58D28F").cgColor
                    item.buttonYesOutlet.layer?.borderWidth = 0.0
                    item.buttonYesOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    
                    //                    item.buttonNoOutlet.isEnabled = true
                    item.buttonNoOutlet.layer?.backgroundColor = NSColor.red.cgColor
                    item.buttonNoOutlet.layer?.borderWidth = 0.0
                    item.buttonNoOutlet.layer?.cornerRadius = 6
                    
                    
                    if AppLanguage.current == .english{
                        item.labelTitleOutlet.stringValue = "Pass Test if you did not see small black or blue dots."
                        item.buttonYesOutlet.attributedTitle = NSAttributedString(string: "YES", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                        item.buttonNoOutlet.attributedTitle = NSAttributedString(string: "No", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])

                    }else if AppLanguage.current == .arabic{
                        item.labelTitleOutlet.stringValue = "اجتياز الاختبار إذا لم ترَ نقاطًا صغيرة سوداء أو زرقاء."
                        item.buttonYesOutlet.attributedTitle = NSAttributedString(string: "نعم", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                        item.buttonNoOutlet.attributedTitle = NSAttributedString(string: "رقم", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])

                    }else if AppLanguage.current == .spanish{
                        item.labelTitleOutlet.stringValue = "Pase la prueba si no vio pequeños puntos negros o azules"
                        item.buttonYesOutlet.attributedTitle = NSAttributedString(string: "SÍ", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                        item.buttonNoOutlet.attributedTitle = NSAttributedString(string: "No", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])

                    }else if AppLanguage.current == .portuguese{
                        item.labelTitleOutlet.stringValue = "Passe no teste se você não viu pequenos pontos pretos ou azuis."
                        item.buttonYesOutlet.attributedTitle = NSAttributedString(string: "SIM", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                        item.buttonNoOutlet.attributedTitle = NSAttributedString(string: "Não", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])

                    }

                    
                }else if keyboardTestStatusID == "1" || keyboardTestStatusID == "2"{
                    item.buttonOutlet.isEnabled = false
                    item.buttonYesOutlet.isHidden = true
                    item.buttonNoOutlet.isHidden = true
                    item.buttonOutlet.title = self.buttonFinishTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.0
                    item.buttonOutlet.layer?.borderColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.cornerRadius = 0
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonFinishTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.black, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    item.imageViewOutlet.isHidden = false
                    if keyboardTestStatusID == "1"{
                        item.imageViewOutlet.image = NSImage(named: "tick")
                        if AppLanguage.current == .english{
                            item.labelSubtitleOutlet.stringValue = "Pass"
                        }else if AppLanguage.current == .arabic{
                            item.labelSubtitleOutlet.stringValue = "يمر"
                        }else if AppLanguage.current == .spanish{
                            item.labelSubtitleOutlet.stringValue = "Aprobar"
                        }else if AppLanguage.current == .portuguese{
                            item.labelSubtitleOutlet.stringValue = "Passar"
                        }
                    }else if keyboardTestStatusID == "2"{
                        item.imageViewOutlet.image = NSImage(named: "danger-1")
                        if AppLanguage.current == .english{
                            item.labelSubtitleOutlet.stringValue = "Failed"
                        }else if AppLanguage.current == .arabic{
                            item.labelSubtitleOutlet.stringValue = "باءت بالفشل"
                        }else if AppLanguage.current == .spanish{
                            item.labelSubtitleOutlet.stringValue = "Fallido"
                        }else if AppLanguage.current == .portuguese{
                            item.labelSubtitleOutlet.stringValue = "Fracassado"
                        }
                        
                    }
                    item.activityIndicator.isHidden = true
                    item.activityIndicator.stopAnimation(nil)
                    item.buttonRunAgainOUtlet.isHidden = false
                    item.labelStatusOutlet.isHidden = true
                    item.buttonRunAgainOUtlet.isEnabled = true
                    item.buttonSendReportOutlet.isEnabled = true
                    item.buttonSendReportOutlet.isHidden = false
                    item.buttonRunAgainOUtlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = true
                    item.buttonSendReportOutlet.isEnabled = true
                    item.buttonRunAgainOUtlet.isEnabled = true
                    item.buttonRunAgainOUtlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonRunAgainOUtlet.layer?.borderWidth = 0.0
                    item.buttonRunAgainOUtlet.layer?.borderColor = NSColor.clear.cgColor
                    item.buttonRunAgainOUtlet.layer?.cornerRadius = 0
                    item.buttonRunAgainOUtlet.attributedTitle = NSAttributedString(string: self.buttonRunAgainTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.black, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    
                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonSendReportOutlet.layer?.borderWidth = 0.0
                    item.buttonSendReportOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                    item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonSendReportOutlet.action = #selector(buttonDisplayPixelsTestSendReportAction)
                    item.buttonRunAgainOUtlet.action = #selector(buttonDisplayPixelsRunAgainAction)
                    
                }else if isChargerStart == 1{
                    item.buttonYesOutlet.isHidden = true
                    item.buttonNoOutlet.isHidden = true
                    item.labelSubtitleOutlet.stringValue = ""
                    item.buttonOutlet.isEnabled = true
                    item.buttonOutlet.title = self.buttonStopTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.5
                    item.buttonOutlet.layer?.borderColor = NSColor.red.cgColor
                    item.buttonOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonStopTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.red, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.imageViewOutlet.isHidden = true
                    item.activityIndicator.isHidden = false
                    item.activityIndicator.isIndeterminate = true
                    item.activityIndicator.style = .spinning
                    item.activityIndicator.startAnimation(nil)
                    
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.labelStatusOutlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = false
                    item.buttonSendReportOutlet.isEnabled = false
                    item.buttonSendReportOutlet.isHidden = false
                    
                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                    item.buttonSendReportOutlet.layer?.borderColor = NSColor.darkGray.cgColor
                    item.buttonSendReportOutlet.layer?.borderWidth = 1
                    item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.darkGray, NSAttributedString.Key.paragraphStyle : pstyle ])
                }else{
                    item.buttonYesOutlet.isHidden = true
                    item.buttonNoOutlet.isHidden = true
                    item.labelSubtitleOutlet.stringValue = ""
                    item.buttonOutlet.isEnabled = true
                    item.buttonOutlet.title = self.buttonRunTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.0
                    item.buttonOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonRunTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    item.activityIndicator.isHidden = true
                    item.activityIndicator.stopAnimation(nil)
                    
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.labelStatusOutlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = false
                    item.buttonSendReportOutlet.isEnabled = false
                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                    item.buttonSendReportOutlet.layer?.borderColor = NSColor.darkGray.cgColor
                    item.buttonSendReportOutlet.layer?.borderWidth = 1
                    item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.darkGray, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                }
                
            }
            
            return item
        } else {
            return NSCollectionViewItem()
        }
    }
    @objc func buttonDisplayPixelsYesAction(button:NSButton) {
        self.keyboardTestStatusID = "1"
        self.isPixelsTestFinish = false
        self.timer.invalidate()
        self.isKeyboardTest = false
        self.collectionViewSubmenu.isSelectable = true
        self.collectionViewTest.reloadData()
        if isMultipleTestSelect == true{
            if SelectArrayString.count > 0{
                self.SelectArrayString.remove(at: 0)
            }
            self.buttonDisplayPixelsTestSendReportAction(button: NSButton())
        }
    }
    @objc func buttonDisplayPixelsNoAction(button:NSButton) {
        self.keyboardTestStatusID = "2"
        self.isPixelsTestFinish = false
        self.timer.invalidate()
        self.isKeyboardTest = false
        self.collectionViewSubmenu.isSelectable = true
        self.collectionViewTest.reloadData()
        if isMultipleTestSelect == true{
            if SelectArrayString.count > 0{
                self.SelectArrayString.remove(at: 0)
            }
            self.buttonDisplayPixelsTestSendReportAction(button: NSButton())
        }
    }
    @objc func buttonDisplayPixelsTestRunAction(button:NSButton) {
        if isChargerStart == 0{
            self.isKeyboardTest = true
            self.isChargerStart = 1
            self.collectionViewSubmenu.isSelectable = false
            timer.invalidate() // just in case this button is tapped multiple times
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(DisplayPixelsTestTimerAction), userInfo: nil, repeats: true)
            self.viewPixelsOutlet.wantsLayer = true
            self.viewPixelsOutlet.isHidden = false
            eventMonitor = NSEvent.addLocalMonitorForEvents(matching: .keyDown) { [unowned self] event in
                self.onKeyDown(with: event)
                return event
            }
            useNextColor()
        }else{
            self.isKeyboardTest = false
            self.collectionViewSubmenu.isSelectable = true
            self.viewPixelsOutlet.isHidden = false
            self.isChargerStart = 0
            self.timer.invalidate()
        }
        self.collectionViewTest.reloadData()
        print("Clicked \(button.title)!")
    }
    
    @objc func buttonDisplayPixelsRunAgainAction(button:NSButton) {
        self.keyboardTestStatusID = ""
        self.isChargerStart = 0
        self.collectionViewTest.reloadData()
        
    }
    @objc func buttonDisplayPixelsTestSendReportAction(button:NSButton) {
        var testName = ""
        if AppLanguage.current == .english{
            testName = "Defective Pixels Test"
        }else if AppLanguage.current == .arabic{
            testName = "اختبار البكسل المعيب"
        }else if AppLanguage.current == .spanish{
            testName = "Prueba de píxeles defectuosos"
        }else if AppLanguage.current == .portuguese{
            testName = "Teste de pixel com defeito"
        }
        let chargerData = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":keyboardTestStatusID,"testTypeId":"125",  "TestDatas":[]] as [String : Any]// this is for testing
        self.pixelsTestArray.remove(at: 0)
        self.pixelsTestArray.insert(chargerData, at: 0)
        UserDefaults.standard.setValue(self.pixelsTestArray, forKey: "Defective_Pixels_Test")
        if Reachability.isConnectedToNetwork(){
            self.callSaveTestReportAPI(testArray: pixelsTestArray, type: "pixels")
        }else{
            if AppLanguage.current == .english{
                self.confirmAbletonIsReady(question: "Error", text: "Please Check Your Internet Connection Or Wifi")
            }else if AppLanguage.current == .arabic{
                self.confirmAbletonIsReady(question: "خطأ", text: "يرجى التحقق من اتصالك بالإنترنت أو شبكة Wifi")
            }else if AppLanguage.current == .spanish{
                self.confirmAbletonIsReady(question: "Error", text: "Por favor, compruebe su conexión a Internet o Wifi")
            }else if AppLanguage.current == .portuguese{
                self.confirmAbletonIsReady(question: "Erro", text: "Por favor, verifique sua conexão com a Internet ou Wi-Fi")
            }
        }
    }
    
    @objc func DisplayPixelsTestTimerAction() {
        if counter >= 20{
            self.isKeyboardTest = false
            timer.invalidate()
            self.isPixelsTestFinish = true
            self.viewPixelsOutlet.isHidden = true
            self.isKeyboardTest = false
            self.collectionViewSubmenu.isSelectable = true
            self.collectionViewTest.reloadData()
            if isMultipleTestSelect == true{
                if self.keyboardTestStatusID == ""{
                    self.keyboardTestStatusID = "2"
                }

                if SelectArrayString.count > 0{
                    self.SelectArrayString.remove(at: 0)
                }
                self.buttonDisplayPixelsTestSendReportAction(button: NSButton())
            }
            print("timer stop ---------->>>>>>>>>>>>>>>>>>>>>>>>",counter)
        }else{
            counter += 1
        }
        print("Counter is ---------->>>>>>>>>>>>>>>>>>>>>>>>",counter)
    }
    
    private func changeColor(indexChange: Int) {
        currentColorIndex = (currentColorIndex + indexChange + colors.count) % colors.count
        self.viewPixelsOutlet.layer?.backgroundColor = currentColor.cgColor
    }
    
    private func useNextColor() {
        changeColor(indexChange: +1)
    }
    
    private func usePreviousColor() {
        changeColor(indexChange: -1)
    }
    
}


extension HomeVC{
    // MARK: - Audio Test
    
    func audioCollectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        var item: NSCollectionViewItem
        if #available(macOS 10.11, *) {
            item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "AudioTestCollectionViewCell"), for: indexPath) as! AudioTestCollectionViewCell
            
            
            
            if let item = item as? AudioTestCollectionViewCell {
                if AppLanguage.current == .english{
                    item.labelStatusOutlet.stringValue = "Please wait untill test finish"
                    item.labelLeftTitleOutlet.stringValue = "1 - Left speaker"
                    item.labelRightTitleOutlet.stringValue = "2 - Right speaker"
                    item.labelBalanceTitleOutlet.stringValue = "3 - Balance"
                }else if AppLanguage.current == .arabic{
                    item.labelLeftTitleOutlet.stringValue = "1 - مكبر الصوت الأيسر"
                    item.labelRightTitleOutlet.stringValue = "2 - مكبر الصوت الأيمن"
                    item.labelBalanceTitleOutlet.stringValue = "3 - التوازن"
                    item.labelStatusOutlet.stringValue = "الرجاء الانتظار حتى تنتهي المهمة"
                }else if AppLanguage.current == .spanish{
                    item.labelLeftTitleOutlet.stringValue = "1 - Altavoz izquierdo"
                    item.labelRightTitleOutlet.stringValue = "2 - Altavoz derecho"
                    item.labelBalanceTitleOutlet.stringValue = "3 - Equilibrio"
                    item.labelStatusOutlet.stringValue = "Espere hasta que finalice la tarea"
                }else if AppLanguage.current == .portuguese{
                    item.labelLeftTitleOutlet.stringValue = "1 - Alto-falante esquerdo"
                    item.labelRightTitleOutlet.stringValue = "2 - Altifalante direito"
                    item.labelBalanceTitleOutlet.stringValue = "3 - Equilíbrio"
                    item.labelStatusOutlet.stringValue = "Aguarde até que a tarefa termine"
                }
                item.buttonOutlet.bezelStyle = .texturedSquare
                item.buttonOutlet.isBordered = false //Important
                item.buttonOutlet.wantsLayer = true
                item.buttonLeftYesOutlet.isHidden = true
                item.buttonLeftNoOutlet.isHidden = true
                item.buttonLeftBalanceIssueOutlet.isHidden = true
                item.leftImageViewOutlet.isHidden = true
                item.buttonRightYesOutlet.isHidden = true
                item.buttonRightNoOutlet.isHidden = true
                item.buttonRightBalanceIssueOutlet.isHidden = true
                item.rightImageViewOutlet.isHidden = true
                item.buttonBalanceYesOutlet.isHidden = true
                item.buttonBalanceNoOutlet.isHidden = true
                item.buttonBalanceIssueOutlet.isHidden = true
                item.balanceImageViewOutlet.isHidden = true
                item.labelLeftSubtitleOutlet.stringValue = ""
                item.labelRightSubtitleOutlet.stringValue = ""
                item.labelBalanceSubtitleOutlet.stringValue = ""
                item.buttonLeftYesOutlet.bezelStyle = .texturedSquare
                item.buttonLeftYesOutlet.isBordered = false //Important
                item.buttonLeftYesOutlet.wantsLayer = true
                item.buttonLeftYesOutlet.layer?.borderWidth = 0.0
                item.buttonLeftYesOutlet.layer?.borderColor = NSColor.clear.cgColor
                item.buttonLeftYesOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                item.buttonLeftYesOutlet.layer?.cornerRadius = 0
                let pstyle = NSMutableParagraphStyle()
                pstyle.alignment = .center
                
                item.buttonLeftNoOutlet.bezelStyle = .texturedSquare
                item.buttonLeftNoOutlet.isBordered = false //Important
                item.buttonLeftNoOutlet.wantsLayer = true
                item.buttonLeftNoOutlet.layer?.borderWidth = 0.0
                item.buttonLeftNoOutlet.layer?.borderColor = NSColor.clear.cgColor
                item.buttonLeftNoOutlet.layer?.cornerRadius = 0
                item.buttonLeftNoOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                
                item.buttonLeftBalanceIssueOutlet.bezelStyle = .texturedSquare
                item.buttonLeftBalanceIssueOutlet.isBordered = false //Important
                item.buttonLeftBalanceIssueOutlet.wantsLayer = true
                item.buttonLeftBalanceIssueOutlet.layer?.borderWidth = 0.0
                item.buttonLeftBalanceIssueOutlet.layer?.borderColor = NSColor.clear.cgColor
                item.buttonLeftBalanceIssueOutlet.layer?.cornerRadius = 6
                item.buttonLeftBalanceIssueOutlet.layer?.backgroundColor = NSColor(hex: "E8B80D").cgColor
                
                
                item.buttonRightYesOutlet.bezelStyle = .texturedSquare
                item.buttonRightYesOutlet.isBordered = false //Important
                item.buttonRightYesOutlet.wantsLayer = true
                item.buttonRightYesOutlet.layer?.borderWidth = 0.0
                item.buttonRightYesOutlet.layer?.borderColor = NSColor.clear.cgColor
                item.buttonRightYesOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                item.buttonRightYesOutlet.layer?.cornerRadius = 0
                item.buttonRightNoOutlet.bezelStyle = .texturedSquare
                item.buttonRightNoOutlet.isBordered = false //Important
                item.buttonRightNoOutlet.wantsLayer = true
                item.buttonRightNoOutlet.layer?.borderWidth = 0.0
                item.buttonRightNoOutlet.layer?.borderColor = NSColor.clear.cgColor
                item.buttonRightNoOutlet.layer?.cornerRadius = 0
                item.buttonRightNoOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                item.buttonRightBalanceIssueOutlet.bezelStyle = .texturedSquare
                item.buttonRightBalanceIssueOutlet.isBordered = false //Important
                item.buttonRightBalanceIssueOutlet.wantsLayer = true
                item.buttonRightBalanceIssueOutlet.layer?.borderWidth = 0.0
                item.buttonRightBalanceIssueOutlet.layer?.borderColor = NSColor.clear.cgColor
                item.buttonRightBalanceIssueOutlet.layer?.cornerRadius = 6
                item.buttonRightBalanceIssueOutlet.layer?.backgroundColor = NSColor(hex: "E8B80D").cgColor
                item.buttonBalanceYesOutlet.bezelStyle = .texturedSquare
                item.buttonBalanceYesOutlet.isBordered = false //Important
                item.buttonBalanceYesOutlet.wantsLayer = true
                item.buttonBalanceYesOutlet.layer?.borderWidth = 0.0
                item.buttonBalanceYesOutlet.layer?.borderColor = NSColor.clear.cgColor
                item.buttonBalanceYesOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                item.buttonBalanceYesOutlet.layer?.cornerRadius = 0
               
                item.buttonBalanceNoOutlet.bezelStyle = .texturedSquare
                item.buttonBalanceNoOutlet.isBordered = false //Important
                item.buttonBalanceNoOutlet.wantsLayer = true
                item.buttonBalanceNoOutlet.layer?.borderWidth = 0.0
                item.buttonBalanceNoOutlet.layer?.borderColor = NSColor.clear.cgColor
                item.buttonBalanceNoOutlet.layer?.cornerRadius = 0
                item.buttonBalanceNoOutlet.layer?.backgroundColor = NSColor.clear.cgColor
               
                item.buttonBalanceIssueOutlet.bezelStyle = .texturedSquare
                item.buttonBalanceIssueOutlet.isBordered = false //Important
                item.buttonBalanceIssueOutlet.wantsLayer = true
                item.buttonBalanceIssueOutlet.layer?.borderWidth = 0.0
                item.buttonBalanceIssueOutlet.layer?.borderColor = NSColor.clear.cgColor
                item.buttonBalanceIssueOutlet.layer?.cornerRadius = 6
                item.buttonBalanceIssueOutlet.layer?.backgroundColor = NSColor(hex: "E8B80D").cgColor

                if AppLanguage.current == .english{
                    item.buttonLeftYesOutlet.attributedTitle = NSAttributedString(string: "YES", attributes: [ NSAttributedString.Key.foregroundColor : NSColor(hex: "58D28F"), NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonLeftNoOutlet.attributedTitle = NSAttributedString(string: "NO", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.red, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonLeftBalanceIssueOutlet.attributedTitle = NSAttributedString(string: "BALANCE ISSUES", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonRightYesOutlet.attributedTitle = NSAttributedString(string: "YES", attributes: [ NSAttributedString.Key.foregroundColor : NSColor(hex: "58D28F"), NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonRightNoOutlet.attributedTitle = NSAttributedString(string: "NO", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.red, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonRightBalanceIssueOutlet.attributedTitle = NSAttributedString(string: "BALANCE ISSUES", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonBalanceYesOutlet.attributedTitle = NSAttributedString(string: "YES", attributes: [ NSAttributedString.Key.foregroundColor : NSColor(hex: "58D28F"), NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonBalanceNoOutlet.attributedTitle = NSAttributedString(string: "NO", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.red, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonBalanceIssueOutlet.attributedTitle = NSAttributedString(string: "BALANCE ISSUES", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                }else if AppLanguage.current == .arabic{
                    item.buttonLeftYesOutlet.attributedTitle = NSAttributedString(string: "نعم", attributes: [ NSAttributedString.Key.foregroundColor : NSColor(hex: "58D28F"), NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonLeftNoOutlet.attributedTitle = NSAttributedString(string: "رقم", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.red, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonLeftBalanceIssueOutlet.attributedTitle = NSAttributedString(string: "قضايا التوازن", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonRightYesOutlet.attributedTitle = NSAttributedString(string: "نعم", attributes: [ NSAttributedString.Key.foregroundColor : NSColor(hex: "58D28F"), NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonRightNoOutlet.attributedTitle = NSAttributedString(string: "رقم", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.red, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonRightBalanceIssueOutlet.attributedTitle = NSAttributedString(string: "قضايا التوازن", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonBalanceYesOutlet.attributedTitle = NSAttributedString(string: "نعم", attributes: [ NSAttributedString.Key.foregroundColor : NSColor(hex: "58D28F"), NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonBalanceNoOutlet.attributedTitle = NSAttributedString(string: "رقم", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.red, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonBalanceIssueOutlet.attributedTitle = NSAttributedString(string: "قضايا التوازن", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                }else if AppLanguage.current == .spanish{
                    item.buttonLeftYesOutlet.attributedTitle = NSAttributedString(string: "SÍ", attributes: [ NSAttributedString.Key.foregroundColor : NSColor(hex: "58D28F"), NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonLeftNoOutlet.attributedTitle = NSAttributedString(string: "NO", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.red, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonLeftBalanceIssueOutlet.attributedTitle = NSAttributedString(string: "PROBLEMAS DE EQUILIBRIO", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonRightYesOutlet.attributedTitle = NSAttributedString(string: "SÍ", attributes: [ NSAttributedString.Key.foregroundColor : NSColor(hex: "58D28F"), NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonRightNoOutlet.attributedTitle = NSAttributedString(string: "NO", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.red, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonRightBalanceIssueOutlet.attributedTitle = NSAttributedString(string: "PROBLEMAS DE EQUILIBRIO", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonBalanceYesOutlet.attributedTitle = NSAttributedString(string: "SÍ", attributes: [ NSAttributedString.Key.foregroundColor : NSColor(hex: "58D28F"), NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonBalanceNoOutlet.attributedTitle = NSAttributedString(string: "NO", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.red, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonBalanceIssueOutlet.attributedTitle = NSAttributedString(string: "PROBLEMAS DE EQUILIBRIO", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                }else if AppLanguage.current == .portuguese{
                    item.buttonLeftYesOutlet.attributedTitle = NSAttributedString(string: "SIM", attributes: [ NSAttributedString.Key.foregroundColor : NSColor(hex: "58D28F"), NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonLeftNoOutlet.attributedTitle = NSAttributedString(string: "NÃO", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.red, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonLeftBalanceIssueOutlet.attributedTitle = NSAttributedString(string: "QUESTÕES DE EQUILÍBRIO", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonRightYesOutlet.attributedTitle = NSAttributedString(string: "SIM", attributes: [ NSAttributedString.Key.foregroundColor : NSColor(hex: "58D28F"), NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonRightNoOutlet.attributedTitle = NSAttributedString(string: "NÃO", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.red, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonRightBalanceIssueOutlet.attributedTitle = NSAttributedString(string: "QUESTÕES DE EQUILÍBRIO", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonBalanceYesOutlet.attributedTitle = NSAttributedString(string: "SIM", attributes: [ NSAttributedString.Key.foregroundColor : NSColor(hex: "58D28F"), NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonBalanceNoOutlet.attributedTitle = NSAttributedString(string: "NÃO", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.red, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonBalanceIssueOutlet.attributedTitle = NSAttributedString(string: "QUESTÕES DE EQUILÍBRIO", attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                }
                
                item.buttonSendReportOutlet.bezelStyle = .texturedSquare
                item.buttonSendReportOutlet.isBordered = false //Important
                item.buttonSendReportOutlet.wantsLayer = true
                item.buttonRunAgainOUtlet.bezelStyle = .texturedSquare
                item.buttonRunAgainOUtlet.isBordered = false //Important
                item.buttonRunAgainOUtlet.wantsLayer = true
                if AppLanguage.current == .english{
                    item.labelOutlet.stringValue = "Audio Test"
                }else if AppLanguage.current == .arabic{
                    item.labelOutlet.stringValue = "اختبار الصوت"
                }else if AppLanguage.current == .spanish{
                    item.labelOutlet.stringValue = "Prueba de sonido"
                }else if AppLanguage.current == .portuguese{
                    item.labelOutlet.stringValue = "Teste de áudio"
                }
                item.buttonOutlet.action = #selector(buttonAudioTestRunAction)
                if isChargerStart == 0{// 1234
                    self.isKeyboardTest = false
                    self.collectionViewSubmenu.isSelectable = true
                    item.leftActivityIndicator.isHidden = true
                    item.leftActivityIndicator.stopAnimation(nil)
                    item.rightActivityIndicator.isHidden = true
                    item.rightActivityIndicator.stopAnimation(nil)
                    item.balanceActivityIndicator.isHidden = true
                    item.balanceActivityIndicator.stopAnimation(nil)
                    item.buttonOutlet.isHidden = false
                    item.buttonSendReportOutlet.isHidden = true
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.labelStatusOutlet.isHidden = true
                    item.buttonOutlet.isEnabled = true
                    item.buttonOutlet.title = self.buttonRunTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.0
                    item.buttonOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonRunTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                }else if audioTestIndex == 0{
                    item.buttonLeftYesOutlet.isHidden = false
                    item.buttonLeftNoOutlet.isHidden = false
                    item.buttonLeftBalanceIssueOutlet.isHidden = false
                    item.leftImageViewOutlet.isHidden = true
                    item.buttonRightYesOutlet.isHidden = true
                    item.buttonRightNoOutlet.isHidden = true
                    item.buttonRightBalanceIssueOutlet.isHidden = true
                    item.rightImageViewOutlet.isHidden = true
                    item.buttonBalanceYesOutlet.isHidden = true
                    item.buttonBalanceNoOutlet.isHidden = true
                    item.buttonBalanceIssueOutlet.isHidden = true
                    item.balanceImageViewOutlet.isHidden = true
                    if AppLanguage.current == .english{
                        item.labelLeftSubtitleOutlet.stringValue = "Do you hear sound from left ear?"
                    }else if AppLanguage.current == .arabic{
                        item.labelLeftSubtitleOutlet.stringValue = "هل تسمع صوتًا من الأذن اليسرى؟"
                    }else if AppLanguage.current == .spanish{
                        item.labelLeftSubtitleOutlet.stringValue = "¿Oyes sonido del oído izquierdo?"
                    }else if AppLanguage.current == .portuguese{
                        item.labelLeftSubtitleOutlet.stringValue = "Você ouve o som do ouvido esquerdo?"
                    }
                    item.labelRightSubtitleOutlet.stringValue = ""
                    item.labelBalanceSubtitleOutlet.stringValue = ""
                    item.buttonLeftYesOutlet.tag = 1
                    item.buttonLeftNoOutlet.tag = 2
                    item.buttonLeftBalanceIssueOutlet.tag = 3
                    item.buttonLeftYesOutlet.action = #selector(buttonLeftYesAction)
                    item.buttonLeftNoOutlet.action = #selector(buttonLeftNoAction)
                    item.buttonLeftBalanceIssueOutlet.action = #selector(buttonLeftBalanceIssueAction)
                    item.leftActivityIndicator.isHidden = false
                    item.leftActivityIndicator.isIndeterminate = true
                    item.leftActivityIndicator.style = .spinning
                    item.leftActivityIndicator.startAnimation(nil)
                    item.buttonOutlet.isEnabled = true
                    item.buttonOutlet.title = self.buttonStopTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.5
                    item.buttonOutlet.layer?.borderColor = NSColor.red.cgColor
                    item.buttonOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonStopTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.red, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.labelStatusOutlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = false
                    item.buttonSendReportOutlet.isEnabled = false
                    item.buttonSendReportOutlet.isHidden = false
                    
                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                    item.buttonSendReportOutlet.layer?.borderColor = NSColor.darkGray.cgColor
                    item.buttonSendReportOutlet.layer?.borderWidth = 1
                    
                    item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.darkGray, NSAttributedString.Key.paragraphStyle : pstyle ])
                }else if audioTestIndex == 1 && audioTestStatus != ""{
                    item.leftActivityIndicator.isHidden = true
                    item.leftActivityIndicator.stopAnimation(nil)
                    item.leftImageViewOutlet.isHidden = false
                    if audioTestStatus == "1"{
                        item.leftImageViewOutlet.image = NSImage(named: "tick")
                        if AppLanguage.current == .english{
                            item.labelLeftSubtitleOutlet.stringValue = "Pass"
                        }else if AppLanguage.current == .arabic{
                            item.labelLeftSubtitleOutlet.stringValue = "يمر"
                        }else if AppLanguage.current == .spanish{
                            item.labelLeftSubtitleOutlet.stringValue = "Aprobar"
                        }else if AppLanguage.current == .portuguese{
                            item.labelLeftSubtitleOutlet.stringValue = "Passar"
                        }
                    }else if audioTestStatus == "2"{
                        item.leftImageViewOutlet.image = NSImage(named: "danger-1")
                        if AppLanguage.current == .english{
                            item.labelLeftSubtitleOutlet.stringValue = "Failed"
                        }else if AppLanguage.current == .arabic{
                            item.labelLeftSubtitleOutlet.stringValue = "باءت بالفشل"
                        }else if AppLanguage.current == .spanish{
                            item.labelLeftSubtitleOutlet.stringValue = "Fallido"
                        }else if AppLanguage.current == .portuguese{
                            item.labelLeftSubtitleOutlet.stringValue = "Fracassado"
                        }
                    }
                    
                    item.buttonLeftYesOutlet.isHidden = true
                    item.buttonLeftNoOutlet.isHidden = true
                    item.buttonLeftBalanceIssueOutlet.isHidden = true
                    item.buttonRightYesOutlet.isHidden = false
                    item.buttonRightNoOutlet.isHidden = false
                    item.buttonRightBalanceIssueOutlet.isHidden = false
                    item.rightImageViewOutlet.isHidden = true
                    item.buttonBalanceYesOutlet.isHidden = true
                    item.buttonBalanceNoOutlet.isHidden = true
                    item.buttonBalanceIssueOutlet.isHidden = true
                    item.balanceImageViewOutlet.isHidden = true
                    //                    item.labelLeftSubtitleOutlet.stringValue = "Do you hear sound from left ears?"
                    
                    if AppLanguage.current == .english{
                        item.labelRightSubtitleOutlet.stringValue = "Do you hear sound from right ears"
                    }else if AppLanguage.current == .arabic{
                        item.labelRightSubtitleOutlet.stringValue = "هل تسمع صوتًا من الأذن اليمنى؟"
                    }else if AppLanguage.current == .spanish{
                        item.labelRightSubtitleOutlet.stringValue = "¿Oyes sonido del oído derecho?"
                    }else if AppLanguage.current == .portuguese{
                        item.labelRightSubtitleOutlet.stringValue = "Você ouve o som do ouvido direito?"
                    }
                    item.labelBalanceSubtitleOutlet.stringValue = ""
                    item.buttonRightYesOutlet.tag = 4
                    item.buttonRightNoOutlet.tag = 5
                    item.buttonRightBalanceIssueOutlet.tag = 6
                    
                    item.buttonRightYesOutlet.action = #selector(buttonLeftYesAction)
                    item.buttonRightNoOutlet.action = #selector(buttonLeftNoAction)
                    item.buttonRightBalanceIssueOutlet.action = #selector(buttonLeftBalanceIssueAction)
                    item.rightActivityIndicator.isHidden = false
                    item.rightActivityIndicator.isIndeterminate = true
                    item.rightActivityIndicator.style = .spinning
                    item.rightActivityIndicator.startAnimation(nil)
                    item.buttonOutlet.isEnabled = true
                    item.buttonOutlet.title = self.buttonStopTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.5
                    item.buttonOutlet.layer?.borderColor = NSColor.red.cgColor
                    item.buttonOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonStopTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.red, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.labelStatusOutlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = false
                    item.buttonSendReportOutlet.isEnabled = false
                    item.buttonSendReportOutlet.isHidden = false
                    
                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                    item.buttonSendReportOutlet.layer?.borderColor = NSColor.darkGray.cgColor
                    item.buttonSendReportOutlet.layer?.borderWidth = 1
                    item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.darkGray, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                }else if audioTestIndex == 2 && audioTestRightStatus != ""{
                    item.rightActivityIndicator.isHidden = true
                    item.rightActivityIndicator.stopAnimation(nil)
                    item.leftImageViewOutlet.isHidden = false
                    item.rightImageViewOutlet.isHidden = false
                    
                    
                    if audioTestStatus == "1"{
                        item.leftImageViewOutlet.image = NSImage(named: "tick")
                        if AppLanguage.current == .english{
                            item.labelLeftSubtitleOutlet.stringValue = "Pass"
                        }else if AppLanguage.current == .arabic{
                            item.labelLeftSubtitleOutlet.stringValue = "يمر"
                        }else if AppLanguage.current == .spanish{
                            item.labelLeftSubtitleOutlet.stringValue = "Aprobar"
                        }else if AppLanguage.current == .portuguese{
                            item.labelLeftSubtitleOutlet.stringValue = "Passar"
                        }
                    }else if audioTestStatus == "2"{
                        item.leftImageViewOutlet.image = NSImage(named: "danger-1")
                        if AppLanguage.current == .english{
                            item.labelLeftSubtitleOutlet.stringValue = "Failed"
                        }else if AppLanguage.current == .arabic{
                            item.labelLeftSubtitleOutlet.stringValue = "باءت بالفشل"
                        }else if AppLanguage.current == .spanish{
                            item.labelLeftSubtitleOutlet.stringValue = "Fallido"
                        }else if AppLanguage.current == .portuguese{
                            item.labelLeftSubtitleOutlet.stringValue = "Fracassado"
                        }
                    }
                    if audioTestRightStatus == "3"{
                        item.rightImageViewOutlet.image = NSImage(named: "tick")
                        if AppLanguage.current == .english{
                            item.labelRightSubtitleOutlet.stringValue = "Pass"
                        }else if AppLanguage.current == .arabic{
                            item.labelRightSubtitleOutlet.stringValue = "يمر"
                        }else if AppLanguage.current == .spanish{
                            item.labelRightSubtitleOutlet.stringValue = "Aprobar"
                        }else if AppLanguage.current == .portuguese{
                            item.labelRightSubtitleOutlet.stringValue = "Passar"
                        }
                        
                    }else if audioTestRightStatus == "4"{
                        item.rightImageViewOutlet.image = NSImage(named: "danger-1")
                        if AppLanguage.current == .english{
                            item.labelRightSubtitleOutlet.stringValue = "Failed"
                        }else if AppLanguage.current == .arabic{
                            item.labelRightSubtitleOutlet.stringValue = "باءت بالفشل"
                        }else if AppLanguage.current == .spanish{
                            item.labelRightSubtitleOutlet.stringValue = "Fallido"
                        }else if AppLanguage.current == .portuguese{
                            item.labelRightSubtitleOutlet.stringValue = "Fracassado"
                        }
                    }
                    
                    item.buttonLeftYesOutlet.isHidden = true
                    item.buttonLeftNoOutlet.isHidden = true
                    item.buttonLeftBalanceIssueOutlet.isHidden = true
                    item.buttonRightYesOutlet.isHidden = true
                    item.buttonRightNoOutlet.isHidden = true
                    item.buttonRightBalanceIssueOutlet.isHidden = true
                    item.buttonBalanceYesOutlet.isHidden = false
                    item.buttonBalanceNoOutlet.isHidden = false
                    item.buttonBalanceIssueOutlet.isHidden = false
                    if AppLanguage.current == .english{
                        item.labelBalanceSubtitleOutlet.stringValue = "Do you hear sound from both ears?"
                    }else if AppLanguage.current == .arabic{
                        item.labelBalanceSubtitleOutlet.stringValue = "هل تسمع صوتًا من كلتا الأذنين؟"
                    }else if AppLanguage.current == .spanish{
                        item.labelBalanceSubtitleOutlet.stringValue = "¿Oyes sonido de ambos oídos?"
                    }else if AppLanguage.current == .portuguese{
                        item.labelBalanceSubtitleOutlet.stringValue = "Você ouve o som de ambas as orelhas?"
                    }
                    item.buttonBalanceYesOutlet.tag = 7
                    item.buttonBalanceNoOutlet.tag = 8
                    item.buttonBalanceIssueOutlet.tag = 9
                    
                    item.buttonBalanceYesOutlet.action = #selector(buttonLeftYesAction)
                    item.buttonBalanceNoOutlet.action = #selector(buttonLeftNoAction)
                    item.buttonBalanceIssueOutlet.action = #selector(buttonLeftBalanceIssueAction)
                    item.balanceActivityIndicator.isHidden = false
                    item.balanceActivityIndicator.isIndeterminate = true
                    item.balanceActivityIndicator.style = .spinning
                    item.balanceActivityIndicator.startAnimation(nil)
                    item.buttonOutlet.isEnabled = true
                    item.buttonOutlet.title = self.buttonStopTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.5
                    item.buttonOutlet.layer?.borderColor = NSColor.red.cgColor
                    item.buttonOutlet.layer?.cornerRadius = 6
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonStopTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.red, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonRunAgainOUtlet.isHidden = true
                    item.labelStatusOutlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = false
                    item.buttonSendReportOutlet.isEnabled = false
                    item.buttonSendReportOutlet.isHidden = false
                    
                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                    item.buttonSendReportOutlet.layer?.borderColor = NSColor.darkGray.cgColor
                    item.buttonSendReportOutlet.layer?.borderWidth = 1
                    item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.darkGray, NSAttributedString.Key.paragraphStyle : pstyle ])
                }else if audioTestIndex == 3 && audioTestBalanceStatus != ""{
                    self.isKeyboardTest = false
                    self.collectionViewSubmenu.isSelectable = true
                    item.balanceActivityIndicator.isHidden = true
                    item.balanceActivityIndicator.stopAnimation(nil)
                    item.leftImageViewOutlet.isHidden = false
                    item.rightImageViewOutlet.isHidden = false
                    item.balanceImageViewOutlet.isHidden = false
                    
                    
                    if audioTestStatus == "1"{
                        item.leftImageViewOutlet.image = NSImage(named: "tick")
                        if AppLanguage.current == .english{
                            item.labelLeftSubtitleOutlet.stringValue = "Pass"
                        }else if AppLanguage.current == .arabic{
                            item.labelLeftSubtitleOutlet.stringValue = "يمر"
                        }else if AppLanguage.current == .spanish{
                            item.labelLeftSubtitleOutlet.stringValue = "Aprobar"
                        }else if AppLanguage.current == .portuguese{
                            item.labelLeftSubtitleOutlet.stringValue = "Passar"
                        }
                    }else if audioTestStatus == "2"{
                        item.leftImageViewOutlet.image = NSImage(named: "danger-1")
                        if AppLanguage.current == .english{
                            item.labelLeftSubtitleOutlet.stringValue = "Failed"
                        }else if AppLanguage.current == .arabic{
                            item.labelLeftSubtitleOutlet.stringValue = "باءت بالفشل"
                        }else if AppLanguage.current == .spanish{
                            item.labelLeftSubtitleOutlet.stringValue = "Fallido"
                        }else if AppLanguage.current == .portuguese{
                            item.labelLeftSubtitleOutlet.stringValue = "Fracassado"
                        }
                    }
                    if audioTestRightStatus == "3"{
                        item.rightImageViewOutlet.image = NSImage(named: "tick")
                        if AppLanguage.current == .english{
                            item.labelRightSubtitleOutlet.stringValue = "Pass"
                        }else if AppLanguage.current == .arabic{
                            item.labelRightSubtitleOutlet.stringValue = "يمر"
                        }else if AppLanguage.current == .spanish{
                            item.labelRightSubtitleOutlet.stringValue = "Aprobar"
                        }else if AppLanguage.current == .portuguese{
                            item.labelRightSubtitleOutlet.stringValue = "Passar"
                        }
                    }else if audioTestRightStatus == "4"{
                        item.rightImageViewOutlet.image = NSImage(named: "danger-1")
                        if AppLanguage.current == .english{
                            item.labelRightSubtitleOutlet.stringValue = "Failed"
                        }else if AppLanguage.current == .arabic{
                            item.labelRightSubtitleOutlet.stringValue = "باءت بالفشل"
                        }else if AppLanguage.current == .spanish{
                            item.labelRightSubtitleOutlet.stringValue = "Fallido"
                        }else if AppLanguage.current == .portuguese{
                            item.labelRightSubtitleOutlet.stringValue = "Fracassado"
                        }
                    }
                    
                    if audioTestBalanceStatus == "5"{
                        item.balanceImageViewOutlet.image = NSImage(named: "tick")
                        if AppLanguage.current == .english{
                            item.labelBalanceSubtitleOutlet.stringValue = "Pass"
                        }else if AppLanguage.current == .arabic{
                            item.labelBalanceSubtitleOutlet.stringValue = "يمر"
                        }else if AppLanguage.current == .spanish{
                            item.labelBalanceSubtitleOutlet.stringValue = "Aprobar"
                        }else if AppLanguage.current == .portuguese{
                            item.labelBalanceSubtitleOutlet.stringValue = "Passar"
                        }
                    }else if audioTestBalanceStatus == "6"{
                        item.balanceImageViewOutlet.image = NSImage(named: "danger-1")
                        if AppLanguage.current == .english{
                            item.labelBalanceSubtitleOutlet.stringValue = "Failed"
                        }else if AppLanguage.current == .arabic{
                            item.labelBalanceSubtitleOutlet.stringValue = "باءت بالفشل"
                        }else if AppLanguage.current == .spanish{
                            item.labelBalanceSubtitleOutlet.stringValue = "Fallido"
                        }else if AppLanguage.current == .portuguese{
                            item.labelBalanceSubtitleOutlet.stringValue = "Fracassado"
                        }
                    }
                    
                    item.buttonLeftYesOutlet.isHidden = true
                    item.buttonLeftNoOutlet.isHidden = true
                    item.buttonLeftBalanceIssueOutlet.isHidden = true
                    item.buttonRightYesOutlet.isHidden = true
                    item.buttonRightNoOutlet.isHidden = true
                    item.buttonRightBalanceIssueOutlet.isHidden = true
                    item.buttonBalanceYesOutlet.isHidden = true
                    item.buttonBalanceNoOutlet.isHidden = true
                    item.buttonBalanceIssueOutlet.isHidden = true
                    
                    
                    
                    item.buttonOutlet.isEnabled = false
                    item.buttonOutlet.title = self.buttonFinishTitleString
                    item.buttonOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.borderWidth = 0.0
                    item.buttonOutlet.layer?.borderColor = NSColor.clear.cgColor
                    item.buttonOutlet.layer?.cornerRadius = 0
                    let pstyle = NSMutableParagraphStyle()
                    pstyle.alignment = .center
                    item.buttonOutlet.attributedTitle = NSAttributedString(string: self.buttonFinishTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.black, NSAttributedString.Key.paragraphStyle : pstyle ])
                    
                    item.buttonRunAgainOUtlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = true
                    item.buttonSendReportOutlet.isEnabled = true
                    item.buttonSendReportOutlet.isHidden = false
                    item.buttonRunAgainOUtlet.isHidden = false
                    item.buttonRunAgainOUtlet.isEnabled = true
                    item.buttonSendReportOutlet.isEnabled = true
                    item.buttonRunAgainOUtlet.isEnabled = true
                    item.buttonRunAgainOUtlet.layer?.backgroundColor = NSColor.clear.cgColor
                    item.buttonRunAgainOUtlet.layer?.borderWidth = 0.0
                    item.buttonRunAgainOUtlet.layer?.borderColor = NSColor.clear.cgColor
                    item.buttonRunAgainOUtlet.layer?.cornerRadius = 0
                    item.buttonRunAgainOUtlet.attributedTitle = NSAttributedString(string: self.buttonRunAgainTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.black, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonSendReportOutlet.layer?.borderWidth = 0.0
                    item.buttonSendReportOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                    item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                    item.labelStatusOutlet.isHidden = true
                    if isMultipleTestSelect == true{
                        if SelectArrayString.count > 0{
                            self.SelectArrayString.remove(at: 0)
                        }
                        self.buttonAudioSendReportAction(button: NSButton())
                    }
                    item.buttonSendReportOutlet.action = #selector(buttonAudioSendReportAction)
                    item.buttonRunAgainOUtlet.action = #selector(buttonAudioTestRunAgainAction)
                }
                //                else if audioTestIndex == -1{
                //                    item.leftActivityIndicator.isHidden = true
                //                    item.leftActivityIndicator.stopAnimation(nil)
                //                    item.rightActivityIndicator.isHidden = true
                //                    item.rightActivityIndicator.stopAnimation(nil)
                //                    item.balanceActivityIndicator.isHidden = true
                //                    item.balanceActivityIndicator.stopAnimation(nil)
                //
                //                    item.buttonOutlet.isEnabled = true
                //                    item.buttonOutlet.title = "STOP"
                //                    item.buttonOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                //                    item.buttonOutlet.layer?.borderWidth = 0.5
                //                    item.buttonOutlet.layer?.borderColor = NSColor.red.cgColor
                //                    item.buttonOutlet.layer?.cornerRadius = 6
                //                    if #available(macOS 10.14, *) {
                //                        item.buttonOutlet.contentTintColor = NSColor.red
                //                    } else {
                //                        // Fallback on earlier versions
                //                    }
                //                    item.buttonRunAgainOUtlet.isHidden = true
                //                    item.labelStatusOutlet.isHidden = false
                //                    item.buttonRunAgainOUtlet.isEnabled = false
                //                    item.buttonSendReportOutlet.isEnabled = false
                //                    item.buttonSendReportOutlet.isHidden = false
                //                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                //                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                //                    item.buttonSendReportOutlet.layer?.borderColor = NSColor.darkGray.cgColor
                //                    item.buttonSendReportOutlet.layer?.borderWidth = 1
                //                    if #available(macOS 10.14, *) {
                //                        item.buttonSendReportOutlet.contentTintColor = NSColor.darkGray
                //                    } else {
                //                        // Fallback on earlier versions
                //                    }
                //                }else{
                //                    item.leftActivityIndicator.isHidden = true
                //                    item.leftActivityIndicator.stopAnimation(nil)
                //                    item.rightActivityIndicator.isHidden = true
                //                    item.rightActivityIndicator.stopAnimation(nil)
                //                    item.balanceActivityIndicator.isHidden = true
                //                    item.balanceActivityIndicator.stopAnimation(nil)
                //                    item.buttonOutlet.isEnabled = true
                //                    item.buttonOutlet.title = "Run"
                //                    item.buttonOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                //                    item.buttonOutlet.layer?.borderWidth = 0.0
                //                    item.buttonOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                //                    item.buttonOutlet.layer?.cornerRadius = 6
                //                    if #available(macOS 10.14, *) {
                //                        item.buttonOutlet.contentTintColor = NSColor.white
                //                    } else {
                //                        // Fallback on earlier versions
                //                    }
                //                    item.buttonRunAgainOUtlet.isHidden = true
                //                    item.labelStatusOutlet.isHidden = false
                //                    item.buttonRunAgainOUtlet.isEnabled = false
                //                    item.buttonSendReportOutlet.isEnabled = false
                //                    item.buttonSendReportOutlet.layer?.backgroundColor = NSColor.clear.cgColor
                //                    item.buttonSendReportOutlet.layer?.cornerRadius = 6
                //                    item.buttonSendReportOutlet.layer?.borderColor = NSColor.darkGray.cgColor
                //                    item.buttonSendReportOutlet.layer?.borderWidth = 1
                //                    if #available(macOS 10.14, *) {
                //                        item.buttonSendReportOutlet.contentTintColor = NSColor.darkGray
                //                    } else {
                //                        // Fallback on earlier versions
                //                    }
                //                }
            }
            return item
        } else {
            return NSCollectionViewItem()
        }
    }
    
    @objc func buttonAudioSendReportAction(button:NSButton) {
        //        let chargerData = ["testName":"Charger Test","macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":keyboardTestStatusID,"testTypeId":"113"] as [String : Any]// this is for testing
        //        self.chargerTestArray.remove(at: 0)
        //        self.chargerTestArray.insert(chargerData, at: 0)
        //        UserDefaults.standard.setValue(self.keyboardTestArray, forKey: "Charger_Test")
        
        print("------->>>>>>>>>>>>>>>>",audioTestArray)
        if Reachability.isConnectedToNetwork(){
            self.callSaveTestReportAPI(testArray: audioTestArray as! [[String : Any]], type: "audio")
        }else{
            if AppLanguage.current == .english{
                self.confirmAbletonIsReady(question: "Error", text: "Please Check Your Internet Connection Or Wifi")
            }else if AppLanguage.current == .arabic{
                self.confirmAbletonIsReady(question: "خطأ", text: "يرجى التحقق من اتصالك بالإنترنت أو شبكة Wifi")
            }else if AppLanguage.current == .spanish{
                self.confirmAbletonIsReady(question: "Error", text: "Por favor, compruebe su conexión a Internet o Wifi")
            }else if AppLanguage.current == .portuguese{
                self.confirmAbletonIsReady(question: "Erro", text: "Por favor, verifique sua conexão com a Internet ou Wi-Fi")
            }
        }
    }
    @objc func buttonAudioTestRunAgainAction(button:NSButton) {
        self.timer.invalidate()
        self.counter = 0
        self.isChargerStart = -1
        self.audioTestBalanceStatus = ""
        self.audioTestRightStatus = ""
        self.audioTestStatus = ""
        self.isChargerStart = 0
        self.audioTestArray.removeAllObjects()
        self.collectionViewTest.reloadData()
        
    }
    
    @objc func buttonAudioTestRunAction(button:NSButton) {
        
        if isChargerStart == 0{
            self.isKeyboardTest = true
            self.collectionViewSubmenu.isSelectable = false
            self.isChargerStart = 1
            self.audioTestIndex = 0
            timer.invalidate() // just in case this button is tapped multiple times
            timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(timerAudioAction), userInfo: nil, repeats: true)
            audioEngine.stop()
            audioPlayer.stop()
            self.open_audiofile(testName: "audio", tag: 0)
            
        }else{
            self.isKeyboardTest = false
            self.collectionViewSubmenu.isSelectable = true
            self.isChargerStart = 0
            self.audioEngine.stop()
            self.audioPlayer.stop()
            self.audioEngine2.stop()
            self.audioPlayer2.stop()
            self.audioTestIndex = -1
            self.timer.invalidate()
            self.collectionViewTest.reloadData()
            
        }
        self.collectionViewTest.reloadData()
        print("Clicked \(button.title)!")
    }
    
    
    @objc func timerAudioAction() {
        print("timerAction stop ---------->>>>>>>>>>>>>>>>>>>>>>>>")
        
        if counter >= 60{
            self.isKeyboardTest = false
            self.collectionViewSubmenu.isSelectable = true
            timer.invalidate()
            self.keyboardTestStatusID = "2"
            self.isKeyboardTest = false
            self.collectionViewSubmenu.isSelectable = true
            self.collectionViewTest.reloadData()
            if isMultipleTestSelect == true{
                if SelectArrayString.count > 0{
                    self.SelectArrayString.remove(at: 0)
                }
                self.buttonAudioSendReportAction(button: NSButton())
            }
            print("timer stop ---------->>>>>>>>>>>>>>>>>>>>>>>>",counter)
        }else{
            if counter == 19{
                self.audioTestStatus = "2"
                var testName = ""
                if AppLanguage.current == .english{
                    testName = "Audio Test - Left Channel"
                }else if AppLanguage.current == .arabic{
                    testName = "اختبار الصوت - القناة اليسرى"
                }else if AppLanguage.current == .spanish{
                    testName = "Prueba de audio - Canal izquierdo"
                }else if AppLanguage.current == .portuguese{
                    testName = "Teste de Áudio - Canal Esquerdo"
                }
                let data = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":"2","testTypeId":"135", "TestDatas":[]] as [String : Any]// this is for testing
                self.audioTestArray.add(data)
                self.audioTestIndex = 1
                audioEngine.stop()
                audioPlayer.stop()
                self.open_audiofile(testName: "audio", tag: 1)
                self.collectionViewTest.reloadData()
            }else  if counter == 39{
                self.audioTestRightStatus = "4"
                var testName = ""
                if AppLanguage.current == .english{
                    testName = "Audio Test - Right Channel"
                }else if AppLanguage.current == .arabic{
                    testName = "اختبار الصوت - القناة اليمنى"
                }else if AppLanguage.current == .spanish{
                    testName = "Prueba de audio: canal derecho"
                }else if AppLanguage.current == .portuguese{
                    testName = "Teste de Áudio - Canal Direito"
                }
                let data = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":"2","testTypeId":"135", "TestDatas":[]] as [String : Any]// this is for testing
                self.audioTestArray.add(data)
                self.audioTestIndex = 2
                audioEngine.stop()
                audioPlayer.stop()
                self.open_audiofile(testName: "audio", tag: 2)
                self.collectionViewTest.reloadData()
            }else  if counter == 59{
                self.audioTestBalanceStatus = "6"
                var testName = ""
                if AppLanguage.current == .english{
                    testName = "Audio Test - Balance"
                }else if AppLanguage.current == .arabic{
                    testName = "اختبار الصوت - التوازن"
                }else if AppLanguage.current == .spanish{
                    testName = "Prueba de audio - Equilibrio"
                }else if AppLanguage.current == .portuguese{
                    testName = "Teste de Áudio - Equilíbrio"
                }
                let data = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":"2","testTypeId":"135", "TestDatas":[]] as [String : Any]// this is for testing
                self.audioTestArray.add(data)
                self.audioTestIndex = 3
                print("nonInterectiveArray is ---------->>>>>>>>>>>>>>>>>>>>>>>>",nonInterectiveArray)
                self.collectionViewSubmenu.isSelectable = true
                self.timer.invalidate()
                self.collectionViewTest.reloadData()
                audioPlayer2.stop()
                audioEngine2.stop()
                audioEngine.stop()
                audioPlayer.stop()
            }
            counter += 1
        }
        print("Counter is ---------->>>>>>>>>>>>>>>>>>>>>>>>",counter)
    }
    @objc func buttonLeftYesAction(button:NSButton) {
        if button.tag == 1{
            counter = 20
            self.audioTestStatus = "1"
            audioTestIndex = 1
            var testName = ""
            if AppLanguage.current == .english{
                testName = "Audio Test - Left Channel"
            }else if AppLanguage.current == .arabic{
                testName = "اختبار الصوت - القناة اليسرى"
            }else if AppLanguage.current == .spanish{
                testName = "Prueba de audio - Canal izquierdo"
            }else if AppLanguage.current == .portuguese{
                testName = "Teste de Áudio - Canal Esquerdo"
            }
            let data = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":"1","testTypeId":"135", "TestDatas":[]] as [String : Any]// this is for testing
            self.audioTestArray.add(data)
            audioEngine.stop()
            audioPlayer.stop()
            self.open_audiofile(testName: "audio", tag: 1)
            self.collectionViewTest.reloadData()
        }else if button.tag == 4{
            audioTestIndex = 2
            counter = 40
            self.audioTestRightStatus = "3"
            var testName = ""
            if AppLanguage.current == .english{
                testName = "Audio Test - Right Channel"
            }else if AppLanguage.current == .arabic{
                testName = "اختبار الصوت - القناة اليمنى"
            }else if AppLanguage.current == .spanish{
                testName = "Prueba de audio: canal derecho"
            }else if AppLanguage.current == .portuguese{
                testName = "Teste de Áudio - Canal Direito"
            }
            let data = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":"1","testTypeId":"135", "TestDatas":[]] as [String : Any]// this is for testing
            self.audioTestArray.add(data)
            audioEngine.stop()
            audioPlayer.stop()
            self.open_audiofile(testName: "audio", tag: 2)
            self.collectionViewTest.reloadData()
        }else if button.tag == 7{
            audioTestIndex = 3
            counter = 60
            self.timer.invalidate()
            self.audioTestBalanceStatus = "5"
            var testName = ""
            if AppLanguage.current == .english{
                testName = "Audio Test - Balance"
            }else if AppLanguage.current == .arabic{
                testName = "اختبار الصوت - التوازن"
            }else if AppLanguage.current == .spanish{
                testName = "Prueba de audio - Equilibrio"
            }else if AppLanguage.current == .portuguese{
                testName = "Teste de Áudio - Equilíbrio"
            }
            let data = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":"1","testTypeId":"135", "TestDatas":[]] as [String : Any]// this is for testing
            self.audioTestArray.add(data)
            audioEngine.stop()
            audioPlayer.stop()
            audioEngine2.stop()
            audioPlayer2.stop()
            
            print("nonInterectiveArray is ---------->>>>>>>>>>>>>>>>>>>>>>>>",nonInterectiveArray)
            self.collectionViewTest.reloadData()
        }
    }
    
    @objc func buttonLeftNoAction(button:NSButton) {
        if button.tag == 2{
            audioTestIndex = 1
            counter = 20
            self.audioTestStatus = "2"
            self.collectionViewTest.reloadData()
            var testName = ""
            if AppLanguage.current == .english{
                testName = "Audio Test - Left Channel"
            }else if AppLanguage.current == .arabic{
                testName = "اختبار الصوت - القناة اليسرى"
            }else if AppLanguage.current == .spanish{
                testName = "Prueba de audio - Canal izquierdo"
            }else if AppLanguage.current == .portuguese{
                testName = "Teste de Áudio - Canal Esquerdo"
            }
            let data = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":"2","testTypeId":"135", "TestDatas":[]] as [String : Any]// this is for testing
            self.audioTestArray.add(data)
            self.collectionViewTest.reloadData()
            audioEngine.stop()
            audioPlayer.stop()
            self.open_audiofile(testName: "audio", tag: 1)
        }else if button.tag == 5{
            audioTestIndex = 2
            counter = 40
            self.audioTestRightStatus = "4"
            self.collectionViewTest.reloadData()
            var testName = ""
            if AppLanguage.current == .english{
                testName = "Audio Test - Right Channel"
            }else if AppLanguage.current == .arabic{
                testName = "اختبار الصوت - القناة اليمنى"
            }else if AppLanguage.current == .spanish{
                testName = "Prueba de audio: canal derecho"
            }else if AppLanguage.current == .portuguese{
                testName = "Teste de Áudio - Canal Direito"
            }
            let data = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":"2","testTypeId":"135", "TestDatas":[]] as [String : Any]// this is for testing
            self.audioTestArray.add(data)
            self.collectionViewTest.reloadData()
            audioEngine.stop()
            audioPlayer.stop()
            self.open_audiofile(testName: "audio", tag: 2)
        }else if button.tag == 8{
            audioTestIndex = 3
            counter = 60
            self.timer.invalidate()
            self.audioTestBalanceStatus = "6"
            self.collectionViewTest.reloadData()
            var testName = ""
            if AppLanguage.current == .english{
                testName = "Audio Test - Balance"
            }else if AppLanguage.current == .arabic{
                testName = "اختبار الصوت - التوازن"
            }else if AppLanguage.current == .spanish{
                testName = "Prueba de audio - Equilibrio"
            }else if AppLanguage.current == .portuguese{
                testName = "Teste de Áudio - Equilíbrio"
            }
            let data = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":"2","testTypeId":"135", "TestDatas":[]] as [String : Any]// this is for testing
            self.audioTestArray.add(data)
            audioEngine.stop()
            audioPlayer.stop()
            audioEngine2.stop()
            audioPlayer2.stop()
            self.collectionViewTest.reloadData()
        }
    }
    
    @objc func buttonLeftBalanceIssueAction(button:NSButton) {
        if button.tag == 3{
            audioTestIndex = 1
            counter = 20
            self.audioTestStatus = "2"
            self.collectionViewTest.reloadData()
            var testName = ""
            if AppLanguage.current == .english{
                testName = "Audio Test - Left Channel"
            }else if AppLanguage.current == .arabic{
                testName = "اختبار الصوت - القناة اليسرى"
            }else if AppLanguage.current == .spanish{
                testName = "Prueba de audio - Canal izquierdo"
            }else if AppLanguage.current == .portuguese{
                testName = "Teste de Áudio - Canal Esquerdo"
            }
            let data = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":"2","testTypeId":"135", "TestDatas":[]] as [String : Any]// this is for testing
            self.audioTestArray.add(data)
            self.collectionViewTest.reloadData()
            audioEngine.stop()
            audioPlayer.stop()
            self.open_audiofile(testName: "audio", tag: 1)
            
        }else if button.tag == 6{
            counter = 40
            audioTestIndex = 2
            self.audioTestRightStatus = "4"
            self.collectionViewTest.reloadData()
            var testName = ""
            if AppLanguage.current == .english{
                testName = "Audio Test - Right Channel"
            }else if AppLanguage.current == .arabic{
                testName = "اختبار الصوت - القناة اليمنى"
            }else if AppLanguage.current == .spanish{
                testName = "Prueba de audio: canal derecho"
            }else if AppLanguage.current == .portuguese{
                testName = "Teste de Áudio - Canal Direito"
            }
            let data = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":"2","testTypeId":"135", "TestDatas":[]] as [String : Any]// this is for testing
            self.audioTestArray.add(data)
            self.collectionViewTest.reloadData()
            audioEngine.stop()
            audioPlayer.stop()
            self.open_audiofile(testName: "audio", tag: 2)
        }else if button.tag == 9{
            audioTestIndex = 3
            counter = 60
            self.timer.invalidate()
            self.audioTestBalanceStatus = "6"
            self.collectionViewTest.reloadData()
            var testName = ""
            if AppLanguage.current == .english{
                testName = "Audio Test - Balance"
            }else if AppLanguage.current == .arabic{
                testName = "اختبار الصوت - التوازن"
            }else if AppLanguage.current == .spanish{
                testName = "Prueba de audio - Equilibrio"
            }else if AppLanguage.current == .portuguese{
                testName = "Teste de Áudio - Equilíbrio"
            }
            let data = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":"2","testTypeId":"135", "TestDatas":[]] as [String : Any]// this is for testing
            self.audioTestArray.add(data)
            audioEngine.stop()
            audioPlayer.stop()
            audioEngine2.stop()
            audioPlayer2.stop()
            self.collectionViewTest.reloadData()
        }
    }
    func playSound() {
        let url = Bundle.main.url(forResource: "sound", withExtension: "mp3")
        let audioFile = try! AVAudioFile(forReading: url!)
        let audioFormat = audioFile.processingFormat
        let audioFrameCount = UInt32(audioFile.length)
        print(audioFile.fileFormat.channelCount)
        print(audioFrameCount)
        let audioFileBuffer = AVAudioPCMBuffer(pcmFormat: audioFormat, frameCapacity: UInt32(audioFile.length))
        try! audioFile.read(into: audioFileBuffer!)
        let mainMixer = audioEngine2.mainMixerNode
        audioEngine2.attach(audioPlayer2)
        audioEngine2.connect(audioPlayer2, to:mainMixer, format: audioFileBuffer!.format)
        audioPlayer2.scheduleBuffer(audioFileBuffer!, completionHandler: nil)
        audioEngine2.prepare()
        do {
            try audioEngine2.start()
            print("engine started")
        } catch let error {
            print(error.localizedDescription)
        }
        audioPlayer2.play()
    }
}
extension HomeVC{
    // MARK: - Non Interective Test
    
    func startNonInterectiveTest(){
        timer.invalidate() // just in case this button is tapped multiple times
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(NoninterectiveTimerAction), userInfo: nil, repeats: true)
    }
    
    @objc func NoninterectiveTimerAction() {
        print("timer stop ---------->>>>>>>>>>>>>>>>>>>>>>>>",nonInterectiveCounter)
        
        if nonInterectiveCounter >= 360{
            timer.invalidate()
            if self.keyboardTestStatusID == "1"{
                self.keyboardTestStatusID = "1"
            }else{
                self.keyboardTestStatusID = "2"
            }
            self.collectionViewTest.reloadData()
            print("timer stop ---------->>>>>>>>>>>>>>>>>>>>>>>>",nonInterectiveCounter)
        }else{
            nonInterectiveCounter += 1
            print("timer stop ---------->>>>>>>>>>>>>>>>>>>>>>>>",nonInterectiveCounter)
            //            self.collectionViewTest.reloadData()
            
            if  nonInterectiveCounter == 0  || nonInterectiveCounter == 1 ||                 self.isSelectIndex == 3{
                self.viewChargerOutlet.isHidden = false
                self.collectionViewTest.isHidden = false
                self.interectiveTestIndex = 9
                self.keyboardTestStatusID = ""
                self.isChargerStart = 0
                self.collectionViewTest.reloadData()
                self.configureCollectionView(tag: 9)
                
            }else  if nonInterectiveCounter == 60 {
                
                
            }else if nonInterectiveCounter >= 70 {
                //                let chargerData = ["testName":"Test","macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":"1","testTypeId":"111"] as [String : Any]// this is for testing
                //                self.nonInterectiveArray.add(chargerData)
                //                print("nonInterectiveArray is ---------->>>>>>>>>>>>>>>>>>>>>>>>",nonInterectiveArray)
                //                self.viewChargerOutlet.isHidden = false
                //                self.collectionViewTest.isHidden = false
                //                self.interectiveTestIndex = 11
                //                self.keyboardTestStatusID = ""
                //                self.isChargerStart = 0
                //                self.collectionViewTest.reloadData()
                //                self.configureCollectionView(tag: 9)
                
            }else{
                
            }
            
        }
        print("Counter is ---------->>>>>>>>>>>>>>>>>>>>>>>>",nonInterectiveCounter)
        self.collectionViewTest.reloadData()
        
    }
}
extension HomeVC{
    // MARK: - CPU Test
    
    func CPUCollectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        var item: NSCollectionViewItem
        if #available(macOS 10.11, *) {
            item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "CPUTestCollectionViewCell"), for: indexPath) as! CPUTestCollectionViewCell
            if let item = item as? CPUTestCollectionViewCell {
                item.viewOutlet.wantsLayer = true
                item.viewOutlet.layer?.borderWidth = 0.0
                item.viewOutlet.layer?.backgroundColor = NSColor(hex: "F4F5F9").cgColor
                item.viewOutlet.layer?.cornerRadius = 12
                item.imageViewOutlet.isHidden = true
                if AppLanguage.current == .english{
                    item.labelOutlet.stringValue = "CPU Test"
                }else if AppLanguage.current == .arabic{
                    item.labelOutlet.stringValue = "اختبار وحدة المعالجة المركزية"
                }else if AppLanguage.current == .spanish{
                    item.labelOutlet.stringValue = "Prueba de CPU"
                }else if AppLanguage.current == .portuguese{
                    item.labelOutlet.stringValue = "Teste de CPU"
                }
                if AppLanguage.current == .arabic{
                    item.labelTitleOutlet.alignment = .right
                    item.labelTwoOutlet.alignment = .right
                    item.labelThreeOutlet.alignment = .right
                    item.labelFourOutlet.alignment = .right
                    item.labelFiveOutlet.alignment = .right
                    item.labelSixOutlet.alignment = .right
                    item.labelSevenOutlet.alignment = .right
                    item.labelEightOutlet.alignment = .right
                    item.labelNineOutlet.alignment = .right
                    item.labelTenOutlet.alignment = .right
                }else{
                    item.labelTitleOutlet.alignment = .left
                    item.labelTwoOutlet.alignment = .left
                    item.labelThreeOutlet.alignment = .left
                    item.labelFourOutlet.alignment = .left
                    item.labelFiveOutlet.alignment = .left
                    item.labelSixOutlet.alignment = .left
                    item.labelSevenOutlet.alignment = .left
                    item.labelEightOutlet.alignment = .left
                    item.labelNineOutlet.alignment = .left
                    item.labelTenOutlet.alignment = .left
                }
                if nonInterectiveCounter == 0 || nonInterectiveCounter == 1{
                    item.labelTitleOutlet.stringValue = ""
                    item.labelTwoOutlet.stringValue = ""
                    item.labelThreeOutlet.stringValue = ""
                    item.labelFourOutlet.stringValue = ""
                    item.labelFiveOutlet.stringValue = ""
                    item.labelSixOutlet.stringValue = ""
                    item.labelSevenOutlet.stringValue = ""
                    item.labelEightOutlet.stringValue = ""
                    item.labelNineOutlet.stringValue = ""
                    item.labelTenOutlet.stringValue = ""
                }
                if nonInterectiveCounter == 0 || nonInterectiveCounter == 1{
                    item.activityIndicator.isHidden = false
                    item.activityIndicator.isIndeterminate = true
                    item.activityIndicator.style = .spinning
                    item.activityIndicator.startAnimation(nil)
                    let fullNameArr = (CPUandGPUArray[2] as? String)?.components(separatedBy: ":")
                    if AppLanguage.current == .english{
                        item.labelTitleOutlet.stringValue = "Model Name : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .arabic{
                        item.labelTitleOutlet.stringValue = "اسم النموذج : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .spanish{
                        item.labelTitleOutlet.stringValue = "Nombre del modelo : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .portuguese{
                        item.labelTitleOutlet.stringValue = "Nome do modelo : \(fullNameArr?[1] ?? "")"
                    }
                    
                }else if nonInterectiveCounter == 5{
                    let fullNameArr = (CPUandGPUArray[3] as? String)?.components(separatedBy: ":")
                    if AppLanguage.current == .english{
                        item.labelTwoOutlet.stringValue = "Model Identifier : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .arabic{
                        item.labelTwoOutlet.stringValue = "معرّف الطراز: \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .spanish{
                        item.labelTwoOutlet.stringValue = "Identificador del modelo: \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .portuguese{
                        item.labelTwoOutlet.stringValue = "Identificador do modelo: \(fullNameArr?[1] ?? "")"
                    }
                }else if nonInterectiveCounter == 10{
                    let fullNameArr = (CPUandGPUArray[4] as? String)?.components(separatedBy: ":")
                    if AppLanguage.current == .english{
                        item.labelThreeOutlet.stringValue = "Processor Name : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .arabic{
                        item.labelThreeOutlet.stringValue = "اسم المعالج: \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .spanish{
                        item.labelThreeOutlet.stringValue = "Nombre del procesador : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .portuguese{
                        item.labelThreeOutlet.stringValue = "Nome do processador : \(fullNameArr?[1] ?? "")"
                    }
                }else if nonInterectiveCounter == 15{
                    let fullNameArr = (CPUandGPUArray[5] as? String)?.components(separatedBy: ":")
                    if AppLanguage.current == .english{
                        item.labelFourOutlet.stringValue = "Processor Speed : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .arabic{
                        item.labelFourOutlet.stringValue = "سرعة المعالج : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .spanish{
                        item.labelFourOutlet.stringValue = "Velocidad del procesador : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .portuguese{
                        item.labelFourOutlet.stringValue = "Velocidade do processador : \(fullNameArr?[1] ?? "")"
                    }
                }else if nonInterectiveCounter == 20{
                    let fullNameArr = (CPUandGPUArray[6] as? String)?.components(separatedBy: ":")
                    if AppLanguage.current == .english{
                        item.labelFiveOutlet.stringValue = "Number of Processors :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .arabic{
                        item.labelFiveOutlet.stringValue = "عدد المعالجات: \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .spanish{
                        item.labelFiveOutlet.stringValue = "Número de procesadores : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .portuguese{
                        item.labelFiveOutlet.stringValue = "Número de Processadores : \(fullNameArr?[1] ?? "")"
                    }
                    
                }else if nonInterectiveCounter == 25{
                    let fullNameArr = (CPUandGPUArray[7] as? String)?.components(separatedBy: ":")
                    if AppLanguage.current == .english{
                        item.labelSixOutlet.stringValue = "Total Number of Cores :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .arabic{
                        item.labelSixOutlet.stringValue = "إجمالي عدد النوى:  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .spanish{
                        item.labelSixOutlet.stringValue = "Número total de núcleos: \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .portuguese{
                        item.labelSixOutlet.stringValue = "Número total de núcleos : \(fullNameArr?[1] ?? "")"
                    }
                }else if nonInterectiveCounter == 30{
                    let fullNameArr = (CPUandGPUArray[8] as? String)?.components(separatedBy: ":")
                    if AppLanguage.current == .english{
                        item.labelSevenOutlet.stringValue = "L2 Cache (per Core) :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .arabic{
                        item.labelSevenOutlet.stringValue = "ذاكرة التخزين المؤقت L2 (لكل مركز) :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .spanish{
                        item.labelSevenOutlet.stringValue = "Caché L2 (por núcleo) : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .portuguese{
                        item.labelSevenOutlet.stringValue = "Cache L2 (por Núcleo) : \(fullNameArr?[1] ?? "")"
                    }

                }else if nonInterectiveCounter == 35{
                    let fullNameArr = (CPUandGPUArray[9] as? String)?.components(separatedBy: ":")
                    if AppLanguage.current == .english{
                        item.labelEightOutlet.stringValue = "L3 cache :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .arabic{
                        item.labelEightOutlet.stringValue = "ذاكرة التخزين المؤقت L3 :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .spanish{
                        item.labelEightOutlet.stringValue = "caché L3 : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .portuguese{
                        item.labelEightOutlet.stringValue = "Cache L3 : \(fullNameArr?[1] ?? "")"
                    }
                }else if nonInterectiveCounter == 40{
                    let fullNameArr = (CPUandGPUArray[10] as? String)?.components(separatedBy: ":")
                    if AppLanguage.current == .english{
                        item.labelNineOutlet.stringValue = "Hyper-Threading Technology :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .arabic{
                        item.labelNineOutlet.stringValue = "فرط خيوط التكنولوجيا :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .spanish{
                        item.labelNineOutlet.stringValue = "Tecnología Hyper-Threading : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .portuguese{
                        item.labelNineOutlet.stringValue = "Tecnologia Hyper-Threading : \(fullNameArr?[1] ?? "")"
                    }

                }else if nonInterectiveCounter == 45{
                    let fullNameArr = (CPUandGPUArray[11] as? String)?.components(separatedBy: ":")
                    if AppLanguage.current == .english{
                        item.labelTenOutlet.stringValue = "Enabled Memory :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .arabic{
                        item.labelTenOutlet.stringValue = "الذاكرة الممكّنة:  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .spanish{
                        item.labelTenOutlet.stringValue = "Memoria habilitada : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .portuguese{
                        item.labelTenOutlet.stringValue = "Memória habilitada : \(fullNameArr?[1] ?? "")"
                    }
                    item.imageViewOutlet.isHidden = false
                    item.imageViewOutlet.image = NSImage(named: "tick")
                    item.activityIndicator.isHidden = true
                    item.activityIndicator.stopAnimation(nil)
                    self.getGPUDetails()
                }
            }
            return item
        } else {
            return NSCollectionViewItem()
        }
    }
    
    func shell(launchPath: String, arguments: [String]) -> String
    {
        let task = Process()
        task.launchPath = launchPath
        task.arguments = arguments
        
        let pipe = Pipe()
        task.standardOutput = pipe
        task.launch()
        
        let data = pipe.fileHandleForReading.readDataToEndOfFile()
        let output: String = NSString(data: data, encoding: String.Encoding.utf8.rawValue)! as String
        
        return output
    }
}
extension HomeVC{
    // MARK: - GPU Test
    func GPUCollectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        var item: NSCollectionViewItem
        if #available(macOS 10.11, *) {
            item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "CPUTestCollectionViewCell"), for: indexPath) as! CPUTestCollectionViewCell
            if let item = item as? CPUTestCollectionViewCell {
                item.viewOutlet.wantsLayer = true
                item.viewOutlet.layer?.borderWidth = 0.0
                item.viewOutlet.layer?.backgroundColor = NSColor(hex: "F4F5F9").cgColor
                item.viewOutlet.layer?.cornerRadius = 12
                item.imageViewOutlet.isHidden = true
                if AppLanguage.current == .english{
                    item.labelOutlet.stringValue = "GPU Test"
                }else if AppLanguage.current == .arabic{
                    item.labelOutlet.stringValue = "اختبار GPU"
                }else if AppLanguage.current == .spanish{
                    item.labelOutlet.stringValue = "Prueba de GPU"
                }else if AppLanguage.current == .portuguese{
                    item.labelOutlet.stringValue = "GPU Test"
                }
                if AppLanguage.current == .arabic{
                    item.labelTitleOutlet.alignment = .right
                    item.labelTwoOutlet.alignment = .right
                    item.labelThreeOutlet.alignment = .right
                    item.labelFourOutlet.alignment = .right
                    item.labelFiveOutlet.alignment = .right
                    item.labelSixOutlet.alignment = .right
                    item.labelSevenOutlet.alignment = .right
                    item.labelEightOutlet.alignment = .right
                    item.labelNineOutlet.alignment = .right
                    item.labelTenOutlet.alignment = .right
                }else{
                    item.labelTitleOutlet.alignment = .left
                    item.labelTwoOutlet.alignment = .left
                    item.labelThreeOutlet.alignment = .left
                    item.labelFourOutlet.alignment = .left
                    item.labelFiveOutlet.alignment = .left
                    item.labelSixOutlet.alignment = .left
                    item.labelSevenOutlet.alignment = .left
                    item.labelEightOutlet.alignment = .left
                    item.labelNineOutlet.alignment = .left
                    item.labelTenOutlet.alignment = .left
                }
                if nonInterectiveCounter == 0 || nonInterectiveCounter == 1{
                    item.labelTitleOutlet.stringValue = ""
                    item.labelTwoOutlet.stringValue = ""
                    item.labelThreeOutlet.stringValue = ""
                    item.labelFourOutlet.stringValue = ""
                    item.labelFiveOutlet.stringValue = ""
                    item.labelSixOutlet.stringValue = ""
                    item.labelSevenOutlet.stringValue = ""
                    item.labelEightOutlet.stringValue = ""
                    item.labelNineOutlet.stringValue = ""
                    item.labelTenOutlet.stringValue = ""
                    item.activityIndicator.isHidden = false
                    item.activityIndicator.isIndeterminate = true
                    item.activityIndicator.style = .spinning
                    item.activityIndicator.startAnimation(nil)
                    let fullNameArr = (CPUandGPUArray[21] as? String)?.components(separatedBy: ":")
                    if AppLanguage.current == .english{
                        item.labelTitleOutlet.stringValue = "Chipset Model : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .arabic{
                        item.labelTitleOutlet.stringValue = "نموذج الشرائح : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .spanish{
                        item.labelTitleOutlet.stringValue = "Modelo de conjunto de chips : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .portuguese{
                        item.labelTitleOutlet.stringValue = "Modelo do chipset : \(fullNameArr?[1] ?? "")"
                    }
                }else if nonInterectiveCounter == 5{
                    let fullNameArr = (CPUandGPUArray[22] as? String)?.components(separatedBy: ":")
                    if AppLanguage.current == .english{
                        item.labelTwoOutlet.stringValue = "Type : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .arabic{
                        item.labelTwoOutlet.stringValue = "يكتب : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .spanish{
                        item.labelTwoOutlet.stringValue = "Escribe : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .portuguese{
                        item.labelTwoOutlet.stringValue = "Modelo : \(fullNameArr?[1] ?? "")"
                    }
                }else if nonInterectiveCounter == 10{
                    let fullNameArr = (CPUandGPUArray[23] as? String)?.components(separatedBy: ":")
                    if AppLanguage.current == .english{
                        item.labelThreeOutlet.stringValue = "BUS :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .arabic{
                        item.labelThreeOutlet.stringValue = "أوتوبيس :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .spanish{
                        item.labelThreeOutlet.stringValue = "AUTOBÚS : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .portuguese{
                        item.labelThreeOutlet.stringValue = "ÔNIBUS : \(fullNameArr?[1] ?? "")"
                    }
                }else if nonInterectiveCounter == 15{
                    let fullNameArr = (CPUandGPUArray[24] as? String)?.components(separatedBy: ":")
                    if AppLanguage.current == .english{
                        item.labelFourOutlet.stringValue = "VRAM (Dynamic, Max) :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .arabic{
                        item.labelFourOutlet.stringValue = "VRAM (Dynamic, Max) :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .spanish{
                        item.labelFourOutlet.stringValue = "VRAM (Dynamic, Max) :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .portuguese{
                        item.labelFourOutlet.stringValue = "VRAM (Dynamic, Max) :  \(fullNameArr?[1] ?? "")"
                    }
                }else if nonInterectiveCounter == 20{
                    let fullNameArr = (CPUandGPUArray[25] as? String)?.components(separatedBy: ":")
                    if AppLanguage.current == .english{
                        item.labelFiveOutlet.stringValue = "Vendor :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .arabic{
                        item.labelFiveOutlet.stringValue = "بائع :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .spanish{
                        item.labelFiveOutlet.stringValue = "Vendedor : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .portuguese{
                        item.labelFiveOutlet.stringValue = "Fornecedor : \(fullNameArr?[1] ?? "")"
                    }
                    
                }else if nonInterectiveCounter == 25{
                    let fullNameArr = (CPUandGPUArray[26] as? String)?.components(separatedBy: ":")
                    if AppLanguage.current == .english{
                        item.labelSixOutlet.stringValue = "Device ID :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .arabic{
                        item.labelSixOutlet.stringValue = "معرف الجهاز :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .spanish{
                        item.labelSixOutlet.stringValue = "Identificación del dispositivo : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .portuguese{
                        item.labelSixOutlet.stringValue = "ID de dispositivo : \(fullNameArr?[1] ?? "")"
                    }
                }else if nonInterectiveCounter == 30{
                    let fullNameArr = (CPUandGPUArray[27] as? String)?.components(separatedBy: ":")
                    if AppLanguage.current == .english{
                        item.labelSevenOutlet.stringValue = "Revision ID :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .arabic{
                        item.labelSevenOutlet.stringValue = "معرف المراجعة :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .spanish{
                        item.labelSevenOutlet.stringValue = "Identificación de revisión: \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .portuguese{
                        item.labelSevenOutlet.stringValue = "Código da revisão : \(fullNameArr?[1] ?? "")"
                    }

                }else if nonInterectiveCounter == 35{
                    let fullNameArr = (CPUandGPUArray[28] as? String)?.components(separatedBy: ":")
                    if AppLanguage.current == .english{
                        item.labelEightOutlet.stringValue = "Metal Family : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .arabic{
                        item.labelEightOutlet.stringValue = "عائلة المعادن : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .spanish{
                        item.labelEightOutlet.stringValue = "Familia de metales : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .portuguese{
                        item.labelEightOutlet.stringValue = "Família de metais :  \(fullNameArr?[1] ?? "")"
                    }
                }else if nonInterectiveCounter == 40{
                    let fullNameArr = (CPUandGPUArray[31] as? String)?.components(separatedBy: ":")
                    if AppLanguage.current == .english{
                        item.labelNineOutlet.stringValue = "Display Type :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .arabic{
                        item.labelNineOutlet.stringValue = "نوع العرض :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .spanish{
                        item.labelNineOutlet.stringValue = "Tipo de visualización : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .portuguese{
                        item.labelNineOutlet.stringValue = "Tipo de exibição : \(fullNameArr?[1] ?? "")"
                    }

                }else if nonInterectiveCounter == 45{
                    let fullNameArr = (CPUandGPUArray[32] as? String)?.components(separatedBy: ":")
                    if AppLanguage.current == .english{
                        item.labelTenOutlet.stringValue = "Resolution :  \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .arabic{
                        item.labelTenOutlet.stringValue = "القرار : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .spanish{
                        item.labelTenOutlet.stringValue = "Resolución : \(fullNameArr?[1] ?? "")"
                    }else if AppLanguage.current == .portuguese{
                        item.labelTenOutlet.stringValue = "Resolução: \(fullNameArr?[1] ?? "")"
                    }
                    item.imageViewOutlet.isHidden = false
                    item.imageViewOutlet.image = NSImage(named: "tick")
                    item.activityIndicator.isHidden = true
                    item.activityIndicator.stopAnimation(nil)
                    self.getMemoryDetails()
                    
                }
            }
            return item
        } else {
            return NSCollectionViewItem()
        }
    }
    func getGPUDetails(){
        self.nonInterectiveCounter = 0
        self.isSubMenuSelectedTag = 1
        timer.invalidate() // just in case this button is tapped multiple times
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(GPUTimerAction), userInfo: nil, repeats: true)
        self.collectionViewSubmenu.reloadData()
    }
    
    @objc func GPUTimerAction() {
        nonInterectiveCounter += 1
        if  nonInterectiveCounter == 0  || nonInterectiveCounter == 1{
            
            var testName = ""
            if AppLanguage.current == .english{
                testName = "CPU Test"
            }else if AppLanguage.current == .arabic{
                testName = "اختبار وحدة المعالجة المركزية"
            }else if AppLanguage.current == .spanish{
                testName = "Prueba de CPU"
            }else if AppLanguage.current == .portuguese{
                testName = "Teste de CPU"
            }
            
            let chargerData = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":"1","testTypeId":"111", "TestDatas":[]] as [String : Any]// this is for testing
            self.nonInterectiveArray.add(chargerData)
            self.viewChargerOutlet.isHidden = false
            self.collectionViewTest.isHidden = false
            self.interectiveTestIndex = 10
            self.keyboardTestStatusID = ""
            self.isChargerStart = 0
            self.collectionViewTest.reloadData()
            self.configureCollectionView(tag: 9)
        }
        print("Counter is ---------->>>>>>>>>>>>>>>>>>>>>>>>",nonInterectiveCounter)
        self.collectionViewTest.reloadData()
    }
}


extension HomeVC{
    // MARK: - Memory Test
    func MemoryCollectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        var item: NSCollectionViewItem
        if #available(macOS 10.11, *) {
            item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "CPUTestCollectionViewCell"), for: indexPath) as! CPUTestCollectionViewCell
            if let item = item as? CPUTestCollectionViewCell {
                item.viewOutlet.wantsLayer = true
                item.viewOutlet.layer?.borderWidth = 0.0
                item.viewOutlet.layer?.backgroundColor = NSColor(hex: "F4F5F9").cgColor
                item.viewOutlet.layer?.cornerRadius = 12
                item.imageViewOutlet.isHidden = true
                if AppLanguage.current == .english{
                    item.labelOutlet.stringValue = "Memory Test"
                }else if AppLanguage.current == .arabic{
                    item.labelOutlet.stringValue = "إختبار ذاكرة"
                }else if AppLanguage.current == .spanish{
                    item.labelOutlet.stringValue = "Prueba de Memoria"
                }else if AppLanguage.current == .portuguese{
                    item.labelOutlet.stringValue = "Teste de memória"
                }
                
                let memoryUsage = System.memoryUsage()
                func memoryUnit(_ value: Double) -> String {
                    if value < 1.0 { return String(Int(value * 1000.0))    + "MB" }
                    else           { return NSString(format:"%.2f", value) as String + "GB" }
                }
                if nonInterectiveCounter == 0 || nonInterectiveCounter == 1{
                    item.labelTitleOutlet.stringValue = ""
                    item.labelTwoOutlet.stringValue = ""
                    item.labelThreeOutlet.stringValue = ""
                    item.labelFourOutlet.stringValue = ""
                    item.labelFiveOutlet.stringValue = ""
                    item.labelSixOutlet.stringValue = ""
                    item.labelSevenOutlet.stringValue = ""
                    item.labelEightOutlet.stringValue = ""
                    item.labelNineOutlet.stringValue = ""
                    item.labelTenOutlet.stringValue = ""
                    item.activityIndicator.isHidden = false
                    item.activityIndicator.isIndeterminate = true
                    item.activityIndicator.style = .spinning
                    item.activityIndicator.startAnimation(nil)
                    if AppLanguage.current == .english{
                        item.labelTitleOutlet.stringValue = "PHYSICAL SIZE: \(System.physicalMemory())GB"
                    }else if AppLanguage.current == .arabic{
                        item.labelTitleOutlet.stringValue = "قياس فيزيائي: \(System.physicalMemory())GB"
                    }else if AppLanguage.current == .spanish{
                        item.labelTitleOutlet.stringValue = "TAMAÑO FÍSICO: \(System.physicalMemory())GB"
                    }else if AppLanguage.current == .portuguese{
                        item.labelTitleOutlet.stringValue = "TAMANHO FÍSICO: \(System.physicalMemory())GB"
                    }
                    
                }else if nonInterectiveCounter == 5{
                    if AppLanguage.current == .english{
                        item.labelTwoOutlet.stringValue = "FREE: \(memoryUnit(memoryUsage.free))"
                    }else if AppLanguage.current == .arabic{
                        item.labelTwoOutlet.stringValue = "مجانا: \(memoryUnit(memoryUsage.free))"
                    }else if AppLanguage.current == .spanish{
                        item.labelTwoOutlet.stringValue = "LIBRE: \(memoryUnit(memoryUsage.free))"
                    }else if AppLanguage.current == .portuguese{
                        item.labelTwoOutlet.stringValue = "GRATUITAMENTE: \(memoryUnit(memoryUsage.free))"
                    }
                }else if nonInterectiveCounter == 10{
                    if AppLanguage.current == .english{
                        item.labelThreeOutlet.stringValue = "WIRED: \(memoryUnit(memoryUsage.wired))"
                    }else if AppLanguage.current == .arabic{
                        item.labelThreeOutlet.stringValue = "سلكي: \(memoryUnit(memoryUsage.wired))"
                    }else if AppLanguage.current == .spanish{
                        item.labelThreeOutlet.stringValue = "CABLEADO: \(memoryUnit(memoryUsage.wired))"
                    }else if AppLanguage.current == .portuguese{
                        item.labelThreeOutlet.stringValue = "COM CABO: \(memoryUnit(memoryUsage.wired))"
                    }
                }else if nonInterectiveCounter == 15{
                    if AppLanguage.current == .english{
                        item.labelFourOutlet.stringValue = "ACTIVE: \(memoryUnit(memoryUsage.active))"
                    }else if AppLanguage.current == .arabic{
                        item.labelFourOutlet.stringValue = "نشيط: \(memoryUnit(memoryUsage.active))"
                    }else if AppLanguage.current == .spanish{
                        item.labelFourOutlet.stringValue = "ACTIVO: \(memoryUnit(memoryUsage.active))"
                    }else if AppLanguage.current == .portuguese{
                        item.labelFourOutlet.stringValue = "ATIVO: \(memoryUnit(memoryUsage.active))"
                    }
                }else if nonInterectiveCounter == 20{
                    if AppLanguage.current == .english{
                        item.labelFiveOutlet.stringValue = "INACTIVE: \(memoryUnit(memoryUsage.inactive))"
                    }else if AppLanguage.current == .arabic{
                        item.labelFiveOutlet.stringValue = "غير نشط: \(memoryUnit(memoryUsage.inactive))"
                    }else if AppLanguage.current == .spanish{
                        item.labelFiveOutlet.stringValue = "INACTIVO: \(memoryUnit(memoryUsage.inactive))"
                    }else if AppLanguage.current == .portuguese{
                        item.labelFiveOutlet.stringValue = "INATIVO: \(memoryUnit(memoryUsage.inactive))"
                    }
                }else if nonInterectiveCounter == 25{
                    if AppLanguage.current == .english{
                        item.labelSixOutlet.stringValue = ("COMPRESSED: \(memoryUnit(memoryUsage.compressed))")
                    }else if AppLanguage.current == .arabic{
                        item.labelSixOutlet.stringValue = ("مضغوط: \(memoryUnit(memoryUsage.compressed))")
                    }else if AppLanguage.current == .spanish{
                        item.labelSixOutlet.stringValue = ("COMPRIMIDO: \(memoryUnit(memoryUsage.compressed))")
                    }else if AppLanguage.current == .portuguese{
                        item.labelSixOutlet.stringValue = ("COMPRIMIDO: \(memoryUnit(memoryUsage.compressed))")
                    }
                    item.imageViewOutlet.isHidden = false
                    item.imageViewOutlet.image = NSImage(named: "tick")
                    item.activityIndicator.isHidden = true
                    item.activityIndicator.stopAnimation(nil)
                    self.getStorageDetails()
                }
            }
            return item
        } else {
            return NSCollectionViewItem()
        }
    }
    func getMemoryDetails(){
        self.nonInterectiveCounter = 0
        self.isSubMenuSelectedTag = 2
        timer.invalidate() // just in case this button is tapped multiple times
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(MemoryTimerAction), userInfo: nil, repeats: true)
        self.collectionViewSubmenu.reloadData()
    }
    
    @objc func MemoryTimerAction() {
        nonInterectiveCounter += 1
        if  nonInterectiveCounter == 0  || nonInterectiveCounter == 1{
            
            var testName = ""
            if AppLanguage.current == .english{
                testName = "GPU Test"
            }else if AppLanguage.current == .arabic{
                testName = "اختبار GPU"
            }else if AppLanguage.current == .spanish{
                testName = "Prueba de GPU"
            }else if AppLanguage.current == .portuguese{
                testName = "GPU Test"
            }
            let chargerData = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":"1","testTypeId":"112", "TestDatas":[]] as [String : Any]// this is for testing
            self.nonInterectiveArray.add(chargerData)
            self.viewChargerOutlet.isHidden = false
            self.collectionViewTest.isHidden = false
            self.interectiveTestIndex = 11
            self.keyboardTestStatusID = ""
            self.isChargerStart = 0
            self.collectionViewTest.reloadData()
            self.configureCollectionView(tag: 9)
        }
        print("Counter is ---------->>>>>>>>>>>>>>>>>>>>>>>>",nonInterectiveCounter)
        self.collectionViewTest.reloadData()
    }
}
extension HomeVC{
    // MARK: - Storage Test
    func StorageCollectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        var item: NSCollectionViewItem
        if #available(macOS 10.11, *) {
            item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "CPUTestCollectionViewCell"), for: indexPath) as! CPUTestCollectionViewCell
            if let item = item as? CPUTestCollectionViewCell {
                item.viewOutlet.wantsLayer = true
                item.viewOutlet.layer?.borderWidth = 0.0
                item.viewOutlet.layer?.backgroundColor = NSColor(hex: "F4F5F9").cgColor
                item.viewOutlet.layer?.cornerRadius = 12
                item.imageViewOutlet.isHidden = true
                if AppLanguage.current == .english{
                    item.labelOutlet.stringValue = "Storage Test"
                }else if AppLanguage.current == .arabic{
                    item.labelOutlet.stringValue = "اختبار التخزين"
                }else if AppLanguage.current == .spanish{
                    item.labelOutlet.stringValue = "Prueba de almacenamiento"
                }else if AppLanguage.current == .portuguese{
                    item.labelOutlet.stringValue = "Teste de armazenamento"
                }
                if nonInterectiveCounter == 0 || nonInterectiveCounter == 1{
                    item.labelTitleOutlet.stringValue = ""
                    item.labelTwoOutlet.stringValue = ""
                    item.labelThreeOutlet.stringValue = ""
                    item.labelFourOutlet.stringValue = ""
                    item.labelFiveOutlet.stringValue = ""
                    item.labelSixOutlet.stringValue = ""
                    item.labelSevenOutlet.stringValue = ""
                    item.labelEightOutlet.stringValue = ""
                    item.labelNineOutlet.stringValue = ""
                    item.labelTenOutlet.stringValue = ""
                    item.activityIndicator.isHidden = false
                    item.activityIndicator.isIndeterminate = true
                    item.activityIndicator.style = .spinning
                    item.activityIndicator.startAnimation(nil)
                    if AppLanguage.current == .english{
                        item.labelTitleOutlet.stringValue = "Total Disk Space: \(DiskStatus.totalDiskSpace)"
                    }else if AppLanguage.current == .arabic{
                        item.labelTitleOutlet.stringValue = "مساحة القرص الإجمالية: \(DiskStatus.totalDiskSpace)"
                    }else if AppLanguage.current == .spanish{
                        item.labelTitleOutlet.stringValue = "Espacio total en disco: \(DiskStatus.totalDiskSpace)"
                    }else if AppLanguage.current == .portuguese{
                        item.labelTitleOutlet.stringValue = "Espaço total em disco: \(DiskStatus.totalDiskSpace)"
                    }
                }else if nonInterectiveCounter == 5 {
                    if AppLanguage.current == .english{
                        item.labelTwoOutlet.stringValue = "Used: \(DiskStatus.usedDiskSpace)"
                    }else if AppLanguage.current == .arabic{
                        item.labelTwoOutlet.stringValue = "تستخدم: \(DiskStatus.usedDiskSpace)"
                    }else if AppLanguage.current == .spanish{
                        item.labelTwoOutlet.stringValue = "Usó: \(DiskStatus.usedDiskSpace)"
                    }else if AppLanguage.current == .portuguese{
                        item.labelTwoOutlet.stringValue = "Usado: \(DiskStatus.usedDiskSpace)"
                    }
                }else if nonInterectiveCounter == 10 {
                    if AppLanguage.current == .english{
                        item.labelThreeOutlet.stringValue = "Free: \(DiskStatus.freeDiskSpace)"
                    }else if AppLanguage.current == .arabic{
                        item.labelThreeOutlet.stringValue = "حر: \(DiskStatus.freeDiskSpace)"
                    }else if AppLanguage.current == .spanish{
                        item.labelThreeOutlet.stringValue = "Libre: \(DiskStatus.freeDiskSpace)"
                    }else if AppLanguage.current == .portuguese{
                        item.labelThreeOutlet.stringValue = "Livre: \(DiskStatus.freeDiskSpace)"
                    }
                }else if nonInterectiveCounter == 12 {
                    item.imageViewOutlet.isHidden = false
                    item.imageViewOutlet.image = NSImage(named: "tick")
                    item.activityIndicator.isHidden = true
                    item.activityIndicator.stopAnimation(nil)
                    if ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Type") as? String) == "InternalBattery" || ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Transport Type") as? String) == "Internal" || ((self.batteryCheckArray[0] as? NSDictionary)?.value(forKey: "Name") as? String) == "InternalBattery-0"{
                        self.getBatteryDetails()
                    }else{
                        self.confirmAbletonIsReady(question: "Error", text: "Device has no battery.")
                    }
                }
                return item
            } else {
                return NSCollectionViewItem()
            }
        }
    }
    func getStorageDetails(){
        self.nonInterectiveCounter = 0
        self.isSubMenuSelectedTag = 3
        timer.invalidate() // just in case this button is tapped multiple times
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(StorageTimerAction), userInfo: nil, repeats: true)
        self.collectionViewSubmenu.reloadData()
    }
    
    @objc func StorageTimerAction() {
        nonInterectiveCounter += 1
        if  nonInterectiveCounter == 0  || nonInterectiveCounter == 1{
            var testName = ""
            if AppLanguage.current == .english{
                testName = "Memory Test"
            }else if AppLanguage.current == .arabic{
                testName = "إختبار ذاكرة"
            }else if AppLanguage.current == .spanish{
                testName = "Prueba de Memoria"
            }else if AppLanguage.current == .portuguese{
                testName = "Teste de memória"
            }
            
            let chargerData = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":"1","testTypeId":"114", "TestDatas":[]] as [String : Any]// this is for testing
            self.nonInterectiveArray.add(chargerData)
            self.viewChargerOutlet.isHidden = false
            self.collectionViewTest.isHidden = false
            self.interectiveTestIndex = 12
            self.keyboardTestStatusID = ""
            self.isChargerStart = 0
            self.collectionViewTest.reloadData()
            self.configureCollectionView(tag: 9)
        }
        print("Counter is ---------->>>>>>>>>>>>>>>>>>>>>>>>",nonInterectiveCounter)
        self.collectionViewTest.reloadData()
    }
    func deviceRemainingFreeSpaceInBytes() -> Int64? {
        let documentDirectory = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true).last!
        guard
            let systemAttributes = try? FileManager.default.attributesOfFileSystem(forPath: documentDirectory),
            let freeSize = systemAttributes[.systemFreeSize] as? NSNumber
        else {
            // something failed
            return nil
        }
        return freeSize.int64Value
    }
}
extension HomeVC{
    // MARK: - Battery Test
    func BatteryCollectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        var item: NSCollectionViewItem
        if #available(macOS 10.11, *) {
            item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "CPUTestCollectionViewCell"), for: indexPath) as! CPUTestCollectionViewCell
            if let item = item as? CPUTestCollectionViewCell {
                item.viewOutlet.wantsLayer = true
                item.viewOutlet.layer?.borderWidth = 0.0
                item.viewOutlet.layer?.backgroundColor = NSColor(hex: "F4F5F9").cgColor
                item.viewOutlet.layer?.cornerRadius = 12
                item.imageViewOutlet.isHidden = true
                
                if AppLanguage.current == .english{
                    item.labelOutlet.stringValue = "Battery Test"
                }else if AppLanguage.current == .arabic{
                    item.labelOutlet.stringValue = "اختبار البطارية"
                }else if AppLanguage.current == .spanish{
                    item.labelOutlet.stringValue = "Prueba de batería"
                }else if AppLanguage.current == .portuguese{
                    item.labelOutlet.stringValue = "Teste de bateria"
                }
                var battery = Battery()
                if battery.open() != kIOReturnSuccess { exit(0) }
                if nonInterectiveCounter == 0 || nonInterectiveCounter == 1{
                    item.labelTitleOutlet.stringValue = ""
                    item.labelTwoOutlet.stringValue = ""
                    item.labelThreeOutlet.stringValue = ""
                    item.labelFourOutlet.stringValue = ""
                    item.labelFiveOutlet.stringValue = ""
                    item.labelSixOutlet.stringValue = ""
                    item.labelSevenOutlet.stringValue = ""
                    item.labelEightOutlet.stringValue = ""
                    item.labelNineOutlet.stringValue = ""
                    item.labelTenOutlet.stringValue = ""
                    item.activityIndicator.isHidden = false
                    item.activityIndicator.isIndeterminate = true
                    item.activityIndicator.style = .spinning
                    item.activityIndicator.startAnimation(nil)
                    if (battery.isACPowered()){
                        if AppLanguage.current == .english{
                            item.labelTitleOutlet.stringValue = "AC POWERED: YES"
                        }else if AppLanguage.current == .arabic{
                            item.labelTitleOutlet.stringValue = "التيار المتردد: نعم"
                        }else if AppLanguage.current == .spanish{
                            item.labelTitleOutlet.stringValue = "ALIMENTADO POR CA: SÍ"
                        }else if AppLanguage.current == .portuguese{
                            item.labelTitleOutlet.stringValue = "ALIMENTADO AC: SIM"
                        }
                    }else{
                        if AppLanguage.current == .english{
                            item.labelTitleOutlet.stringValue = "AC POWERED: NO"
                        }else if AppLanguage.current == .arabic{
                            item.labelTitleOutlet.stringValue = "التيار المتردد: لا"
                        }else if AppLanguage.current == .spanish{
                            item.labelTitleOutlet.stringValue = "ALIMENTADO POR CA: NO"
                        }else if AppLanguage.current == .portuguese{
                            item.labelTitleOutlet.stringValue = "ALIMENTADO AC: NÃO"
                        }
                    }
                }else if nonInterectiveCounter == 5{
                    if (battery.isCharged()){
                        if AppLanguage.current == .english{
                            item.labelTwoOutlet.stringValue = "CHARGED: YES"
                        }else if AppLanguage.current == .arabic{
                            item.labelTwoOutlet.stringValue = "مشحونة: نعم"
                        }else if AppLanguage.current == .spanish{
                            item.labelTwoOutlet.stringValue = "COBRADO: SI"
                        }else if AppLanguage.current == .portuguese{
                            item.labelTwoOutlet.stringValue = "CARREGADO: SIM"
                        }
                    }else{
                        if AppLanguage.current == .english{
                            item.labelTwoOutlet.stringValue = "CHARGED: NO"
                        }else if AppLanguage.current == .arabic{
                            item.labelTwoOutlet.stringValue = "مشحونة: لا"
                        }else if AppLanguage.current == .spanish{
                            item.labelTwoOutlet.stringValue = "COBRADO: NO"
                        }else if AppLanguage.current == .portuguese{
                            item.labelTwoOutlet.stringValue = "CARREGADO: NÃO"
                        }
                    }
                }else if nonInterectiveCounter == 10{
                    if (battery.isCharging()){
                        if AppLanguage.current == .english{
                            item.labelThreeOutlet.stringValue = "CHARGING: YES"
                        }else if AppLanguage.current == .arabic{
                            item.labelThreeOutlet.stringValue = "الشحن: نعم"
                        }else if AppLanguage.current == .spanish{
                            item.labelThreeOutlet.stringValue = "CARGANDO: SI"
                        }else if AppLanguage.current == .portuguese{
                            item.labelThreeOutlet.stringValue = "CARREGANDO: SIM"
                        }
                    }else{
                        if AppLanguage.current == .english{
                            item.labelThreeOutlet.stringValue = "CHARGING: NO"
                        }else if AppLanguage.current == .arabic{
                            item.labelThreeOutlet.stringValue = "الشحن: لا"
                        }else if AppLanguage.current == .spanish{
                            item.labelThreeOutlet.stringValue = "CARGANDO: NO"
                        }else if AppLanguage.current == .portuguese{
                            item.labelThreeOutlet.stringValue = "CARREGANDO: NÃO"
                        }
                    }
                }else if nonInterectiveCounter == 15{
                    if AppLanguage.current == .english{
                        item.labelFourOutlet.stringValue = "CHARGE: \(battery.charge())%"
                    }else if AppLanguage.current == .arabic{
                        item.labelFourOutlet.stringValue = "تكلفة: \(battery.charge())%"
                    }else if AppLanguage.current == .spanish{
                        item.labelFourOutlet.stringValue = "COBRAR: \(battery.charge())%"
                    }else if AppLanguage.current == .portuguese{
                        item.labelFourOutlet.stringValue = "CARREGAR: \(battery.charge())%"
                    }
                }else if nonInterectiveCounter == 20{
                    if AppLanguage.current == .english{
                        item.labelFiveOutlet.stringValue = "CAPACITY: \(battery.currentCapacity()) mAh"
                    }else if AppLanguage.current == .arabic{
                        item.labelFiveOutlet.stringValue = "الاهلية: \(battery.currentCapacity()) mAh"
                    }else if AppLanguage.current == .spanish{
                        item.labelFiveOutlet.stringValue = "CAPACIDAD: \(battery.currentCapacity()) mAh"
                    }else if AppLanguage.current == .portuguese{
                        item.labelFiveOutlet.stringValue = "CAPACIDADE: \(battery.currentCapacity()) mAh"
                    }
                }else if nonInterectiveCounter == 25{

                    if AppLanguage.current == .english{
                        item.labelSixOutlet.stringValue = "BATTERY HEALTH %: \(Double((Double(battery.currentCapacity()) / Double(battery.designCapacity())) * 100.0).twoDigits)"
                    }else if AppLanguage.current == .arabic{
                        item.labelSixOutlet.stringValue = "صحة البطارية %: \(Double((Double(battery.currentCapacity()) / Double(battery.designCapacity())) * 100.0).twoDigits)"
                    }else if AppLanguage.current == .spanish{
                        item.labelSixOutlet.stringValue = "SALUD DE LA BATERÍA %: \(Double((Double(battery.currentCapacity()) / Double(battery.designCapacity())) * 100.0).twoDigits)"
                    }else if AppLanguage.current == .portuguese{
                        item.labelSixOutlet.stringValue = "SAÚDE DA BATERIA %: \(Double((Double(battery.currentCapacity()) / Double(battery.designCapacity())) * 100.0).twoDigits)"
                    }
                    
                }else if nonInterectiveCounter == 30{
                    let blob = IOPSCopyPowerSourcesInfo()
                    let list = IOPSCopyPowerSourcesList(blob?.takeRetainedValue())
                    if AppLanguage.current == .english{
                        item.labelSevenOutlet.stringValue = "BATTERY HEALTH: \((((list?.takeRetainedValue()) as? NSArray ?? [])[0] as? NSDictionary ?? [:]).value(forKey: "BatteryHealth") ?? "")"
                    }else if AppLanguage.current == .arabic{
                        item.labelSevenOutlet.stringValue = "صحة البطارية: \((((list?.takeRetainedValue()) as? NSArray ?? [])[0] as? NSDictionary ?? [:]).value(forKey: "BatteryHealth") ?? "")"
                    }else if AppLanguage.current == .spanish{
                        item.labelSevenOutlet.stringValue = "SALUD DE LA BATERÍA: \((((list?.takeRetainedValue()) as? NSArray ?? [])[0] as? NSDictionary ?? [:]).value(forKey: "BatteryHealth") ?? "")"
                    }else if AppLanguage.current == .portuguese{
                        item.labelSevenOutlet.stringValue = "SAÚDE DA BATERIA: \((((list?.takeRetainedValue()) as? NSArray ?? [])[0] as? NSDictionary ?? [:]).value(forKey: "BatteryHealth") ?? "")"
                    }
                }else if nonInterectiveCounter == 35{
                    if AppLanguage.current == .english{
                        item.labelEightOutlet.stringValue = "CYCLES: \(battery.cycleCount())"
                    }else if AppLanguage.current == .arabic{
                        item.labelEightOutlet.stringValue = "الدورات: \(battery.cycleCount())"
                    }else if AppLanguage.current == .spanish{
                        item.labelEightOutlet.stringValue = "CICLOS: \(battery.cycleCount())"
                    }else if AppLanguage.current == .portuguese{
                        item.labelEightOutlet.stringValue = "CICLOS: \(battery.cycleCount())"
                    }
                }else if nonInterectiveCounter == 40{
                    if AppLanguage.current == .english{
                        item.labelNineOutlet.stringValue = "MAX CYCLES: \(battery.designCycleCount())"
                    }else if AppLanguage.current == .arabic{
                        item.labelNineOutlet.stringValue = "ماكس CYCLES: \(battery.designCycleCount())"
                    }else if AppLanguage.current == .spanish{
                        item.labelNineOutlet.stringValue = "CICLOS MÁX.: \(battery.designCycleCount())"
                    }else if AppLanguage.current == .portuguese{
                        item.labelNineOutlet.stringValue = "CICLOS MÁX.: \(battery.designCycleCount())"
                    }
                }else if nonInterectiveCounter == 45{
                    if AppLanguage.current == .english{
                        item.labelTenOutlet.stringValue = "TEMPERATURE: \(battery.temperature())°C"
                    }else if AppLanguage.current == .arabic{
                        item.labelTenOutlet.stringValue = "درجة الحرارة: \(battery.temperature())°C"
                    }else if AppLanguage.current == .spanish{
                        item.labelTenOutlet.stringValue = "LA TEMPERATURA: \(battery.temperature())°C"
                    }else if AppLanguage.current == .portuguese{
                        item.labelTenOutlet.stringValue = "TEMPERATURA: \(battery.temperature())°C"
                    }
                    item.imageViewOutlet.isHidden = false
                    item.imageViewOutlet.image = NSImage(named: "tick")
                    item.activityIndicator.isHidden = true
                    item.activityIndicator.stopAnimation(nil)
                    self.getWifiDetails()
                    _ = battery.close()
                }
            }
            return item
        } else {
            return NSCollectionViewItem()
        }
    }
    
    func getBatteryDetails(){
        self.nonInterectiveCounter = 0
        self.isSubMenuSelectedTag = 4
        timer.invalidate() // just in case this button is tapped multiple times
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(BatteryTimerAction), userInfo: nil, repeats: true)
        self.collectionViewSubmenu.reloadData()
    }
    
    @objc func BatteryTimerAction() {
        nonInterectiveCounter += 1
        if  nonInterectiveCounter == 0  || nonInterectiveCounter == 1{
            
            var testName = ""
            if AppLanguage.current == .english{
                testName = "Storage Test"
            }else if AppLanguage.current == .arabic{
                testName = "اختبار التخزين"
            }else if AppLanguage.current == .spanish{
                testName = "Prueba de almacenamiento"
            }else if AppLanguage.current == .portuguese{
                testName = "Teste de armazenamento"
            }
            let chargerData = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":"1","testTypeId":"115", "TestDatas":[]] as [String : Any]// this is for testing
            self.nonInterectiveArray.add(chargerData)
            self.viewChargerOutlet.isHidden = false
            self.collectionViewTest.isHidden = false
            self.interectiveTestIndex = 13
            self.keyboardTestStatusID = ""
            self.isChargerStart = 0
            self.collectionViewTest.reloadData()
            self.configureCollectionView(tag: 9)
        }
        print("Counter is ---------->>>>>>>>>>>>>>>>>>>>>>>>",nonInterectiveCounter)
        self.collectionViewTest.reloadData()
    }
}

extension HomeVC{
    // MARK: - Wifi Test
    func wifiCollectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        var item: NSCollectionViewItem
        if #available(macOS 10.11, *) {
            item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "CPUTestCollectionViewCell"), for: indexPath) as! CPUTestCollectionViewCell
            if let item = item as? CPUTestCollectionViewCell {
                item.viewOutlet.wantsLayer = true
                item.viewOutlet.layer?.borderWidth = 0.0
                item.viewOutlet.layer?.backgroundColor = NSColor(hex: "F4F5F9").cgColor
                item.viewOutlet.layer?.cornerRadius = 12
                item.imageViewOutlet.isHidden = true
                if AppLanguage.current == .english{
                    item.labelOutlet.stringValue = "Network Test"
                }else if AppLanguage.current == .arabic{
                    item.labelOutlet.stringValue = "اختبار Ethernet / wifi"
                }else if AppLanguage.current == .spanish{
                    item.labelOutlet.stringValue = "Prueba de red"
                }else if AppLanguage.current == .portuguese{
                    item.labelOutlet.stringValue = "Teste de rede"
                }
                if nonInterectiveCounter == 0 || nonInterectiveCounter == 1{
                    item.labelTitleOutlet.stringValue = ""
                    item.labelTwoOutlet.stringValue = ""
                    item.labelThreeOutlet.stringValue = ""
                    item.labelFourOutlet.stringValue = ""
                    item.labelFiveOutlet.stringValue = ""
                    item.labelSixOutlet.stringValue = ""
                    item.labelSevenOutlet.stringValue = ""
                    item.labelEightOutlet.stringValue = ""
                    item.labelNineOutlet.stringValue = ""
                    item.labelTenOutlet.stringValue = ""
                    item.activityIndicator.isHidden = false
                    item.activityIndicator.isIndeterminate = true
                    item.activityIndicator.style = .spinning
                    item.activityIndicator.startAnimation(nil)
                    
                }else if nonInterectiveCounter == 5{
                    switch Networking.networkInterfaceType{
                    case .Ethernet:
                        if AppLanguage.current == .english{
                            item.labelTitleOutlet.stringValue = "Connected with ethernet"
                        }else if AppLanguage.current == .arabic{
                            item.labelTitleOutlet.stringValue = "متصل بشبكة إيثرنت"
                        }else if AppLanguage.current == .spanish{
                            item.labelTitleOutlet.stringValue = "Conectado con ethernet"
                        }else if AppLanguage.current == .portuguese{
                            item.labelTitleOutlet.stringValue = "Conectado com ethernet"
                        }
                        if let cellular = getAddress(for: .cellular){
                            if AppLanguage.current == .english{
                                item.labelTwoOutlet.stringValue = "IP Address: \(cellular)"
                            }else if AppLanguage.current == .arabic{
                                item.labelTwoOutlet.stringValue = "عنوان IP: \(cellular)"
                            }else if AppLanguage.current == .spanish{
                                item.labelTwoOutlet.stringValue = "Dirección IP: \(cellular)"
                            }else if AppLanguage.current == .portuguese{
                                item.labelTwoOutlet.stringValue = "Endereço de IP: \(cellular)"
                            }
                        }
                        item.imageViewOutlet.isHidden = false
                        item.imageViewOutlet.image = NSImage(named: "tick")
                        item.activityIndicator.isHidden = true
                        item.activityIndicator.stopAnimation(nil)
                        self.getbluethoothDetails()
                    case .Wifi:
                        if AppLanguage.current == .english{
                            item.labelTitleOutlet.stringValue = "Connected with Wifi"
                        }else if AppLanguage.current == .arabic{
                            item.labelTitleOutlet.stringValue = "متصل بشبكة Wifi"
                        }else if AppLanguage.current == .spanish{
                            item.labelTitleOutlet.stringValue = "Conectado con Wi-Fi"
                        }else if AppLanguage.current == .portuguese{
                            item.labelTitleOutlet.stringValue = "Conectado com Wi-Fi"
                        }
                        item.imageViewOutlet.isHidden = false
                        item.imageViewOutlet.image = NSImage(named: "tick")
                        item.activityIndicator.isHidden = true
                        item.activityIndicator.stopAnimation(nil)
                        if let wifiIP = getAddress(for: .wifi){
                            if AppLanguage.current == .english{
                                item.labelTwoOutlet.stringValue = "IP Address: \(wifiIP)"
                            }else if AppLanguage.current == .arabic{
                                item.labelTwoOutlet.stringValue = "عنوان IP: \(wifiIP)"
                            }else if AppLanguage.current == .spanish{
                                item.labelTwoOutlet.stringValue = "Dirección IP: \(wifiIP)"
                            }else if AppLanguage.current == .portuguese{
                                item.labelTwoOutlet.stringValue = "Endereço de IP: \(wifiIP)"
                            }
                        }
                        self.getbluethoothDetails()
                    case .Unknown:
                        if AppLanguage.current == .english{
                            item.labelTitleOutlet.stringValue = "Not connected to network"
                        }else if AppLanguage.current == .arabic{
                            item.labelTitleOutlet.stringValue = "غير متصل بالشبكة"
                        }else if AppLanguage.current == .spanish{
                            item.labelTitleOutlet.stringValue = "No conectado a la red"
                        }else if AppLanguage.current == .portuguese{
                            item.labelTitleOutlet.stringValue = "Não conectado à rede"
                        }
                        item.imageViewOutlet.isHidden = false
                        item.imageViewOutlet.image = NSImage(named: "danger-1")
                        item.activityIndicator.isHidden = true
                        item.activityIndicator.stopAnimation(nil)
                        self.getbluethoothDetails()
                    }
                }else if nonInterectiveCounter == 7{
                    item.imageViewOutlet.image = NSImage(named: "danger-1")
                    self.timer.invalidate()
                    
                }
            }
            return item
        } else {
            return NSCollectionViewItem()
        }
    }
    
    func getWifiDetails(){
        self.nonInterectiveCounter = 0
        self.isSubMenuSelectedTag = 5
        timer.invalidate() // just in case this button is tapped multiple times
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(WifiTimerAction), userInfo: nil, repeats: true)
        self.collectionViewSubmenu.reloadData()
    }
    
    @objc func WifiTimerAction() {
        nonInterectiveCounter += 1
        if  nonInterectiveCounter == 0  || nonInterectiveCounter == 1{
            var testName = ""
            if AppLanguage.current == .english{
                testName = "Battery Test"
            }else if AppLanguage.current == .arabic{
                testName = "اختبار البطارية"
            }else if AppLanguage.current == .spanish{
                testName = "Prueba de batería"
            }else if AppLanguage.current == .portuguese{
                testName = "Teste de bateria"
            }
            let chargerData = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":"1","testTypeId":"116", "TestDatas":[]] as [String : Any]// this is for testing
            self.nonInterectiveArray.add(chargerData)
            self.viewChargerOutlet.isHidden = false
            self.collectionViewTest.isHidden = false
            self.interectiveTestIndex = 14
            self.keyboardTestStatusID = ""
            self.isChargerStart = 0
            self.collectionViewTest.reloadData()
            self.configureCollectionView(tag: 9)
        }
        print("Counter is ---------->>>>>>>>>>>>>>>>>>>>>>>>",nonInterectiveCounter)
        self.collectionViewTest.reloadData()
    }
    func currentSSIDs() -> [String] {
        let client = CWWiFiClient.shared()
        return client.interfaces()?.compactMap { interface in
            return interface.ssid()
        } ?? []
    }
    
    func isWIFIActive() -> Bool {
        guard let interfaceNames = CWWiFiClient.interfaceNames() else {
            return false
        }
        
        for interfaceName in interfaceNames {
            let interface = CWWiFiClient.shared().interface(withName: interfaceName)
            
            if interface?.ssid() != nil {
                return true
            }
        }
        return false
    }
    
    func getAddress(for network: Network) -> String? {
        var address: String?
        
        // Get list of all interfaces on the local machine:
        var ifaddr: UnsafeMutablePointer<ifaddrs>?
        guard getifaddrs(&ifaddr) == 0 else { return nil }
        guard let firstAddr = ifaddr else { return nil }
        
        // For each interface ...
        for ifptr in sequence(first: firstAddr, next: { $0.pointee.ifa_next }) {
            let interface = ifptr.pointee
            
            // Check for IPv4 or IPv6 interface:
            let addrFamily = interface.ifa_addr.pointee.sa_family
            if addrFamily == UInt8(AF_INET) || addrFamily == UInt8(AF_INET6) {
                
                // Check interface name:
                let name = String(cString: interface.ifa_name)
                if name == network.rawValue {
                    
                    // Convert interface address to a human readable string:
                    var hostname = [CChar](repeating: 0, count: Int(NI_MAXHOST))
                    getnameinfo(interface.ifa_addr, socklen_t(interface.ifa_addr.pointee.sa_len),
                                &hostname, socklen_t(hostname.count),
                                nil, socklen_t(0), NI_NUMERICHOST)
                    address = String(cString: hostname)
                }
            }
        }
        freeifaddrs(ifaddr)
        
        return address
    }
    
}

extension HomeVC{
    // MARK: - bluethooth Test
    func bluethoothCollectionView(_ collectionView: NSCollectionView, itemForRepresentedObjectAt indexPath: IndexPath) -> NSCollectionViewItem {
        var item: NSCollectionViewItem
        if #available(macOS 10.11, *) {
            item = collectionView.makeItem(withIdentifier: NSUserInterfaceItemIdentifier(rawValue: "CPUTestCollectionViewCell"), for: indexPath) as! CPUTestCollectionViewCell
            if let item = item as? CPUTestCollectionViewCell {
                item.buttonSendReportOutlet.isHidden = true
                item.buttonSendReportTrailingConstraint.constant = -90
                item.viewOutlet.wantsLayer = true
                item.viewOutlet.layer?.borderWidth = 0.0
                item.viewOutlet.layer?.backgroundColor = NSColor(hex: "F4F5F9").cgColor
                item.viewOutlet.layer?.cornerRadius = 12
                item.imageViewOutlet.isHidden = true
                
                if AppLanguage.current == .english{
                    item.labelOutlet.stringValue = "Bluetooth Test"
                }else if AppLanguage.current == .arabic{
                    item.labelOutlet.stringValue = "اختبار البلوتوث"
                }else if AppLanguage.current == .spanish{
                    item.labelOutlet.stringValue = "Prueba Bluetooth"
                }else if AppLanguage.current == .portuguese{
                    item.labelOutlet.stringValue = "Teste Bluetooth"
                }
                if nonInterectiveCounter == 0 || nonInterectiveCounter == 1{
                    item.labelTitleOutlet.stringValue = ""
                    item.labelTwoOutlet.stringValue = ""
                    item.labelThreeOutlet.stringValue = ""
                    item.labelFourOutlet.stringValue = ""
                    item.labelFiveOutlet.stringValue = ""
                    item.labelSixOutlet.stringValue = ""
                    item.labelSevenOutlet.stringValue = ""
                    item.labelEightOutlet.stringValue = ""
                    item.labelNineOutlet.stringValue = ""
                    item.labelTenOutlet.stringValue = ""
                    item.activityIndicator.isHidden = false
                    item.activityIndicator.isIndeterminate = true
                    item.activityIndicator.style = .spinning
                    item.activityIndicator.startAnimation(nil)
                }else if nonInterectiveCounter == 5{
                    
                    if self.bluethoothStatus == "Bluetooth On" || bluethoothStatus == "البلوتوث قيد التشغيل" || bluethoothStatus == "Bluetooth activado" || bluethoothStatus == "Bluetooth ligado"{
                        if AppLanguage.current == .english{
                            item.labelTitleOutlet.stringValue = "Bluetooth On"
                        }else if AppLanguage.current == .arabic{
                            item.labelTitleOutlet.stringValue = "البلوتوث قيد التشغيل"
                        }else if AppLanguage.current == .spanish{
                            item.labelTitleOutlet.stringValue = "Bluetooth activado"
                        }else if AppLanguage.current == .portuguese{
                            item.labelTitleOutlet.stringValue = "Bluetooth ligado"
                        }
                        item.imageViewOutlet.isHidden = false
                        item.imageViewOutlet.image = NSImage(named: "tick")
                        item.activityIndicator.isHidden = true
                        item.activityIndicator.stopAnimation(nil)
                        self.timer.invalidate()
                        item.buttonSendReportOutlet.isHidden = false
                        item.buttonSendReportTrailingConstraint.constant = 35
                        item.buttonSendReportOutlet.bezelStyle = .texturedSquare
                        item.buttonSendReportOutlet.isBordered = false //Important
                        item.buttonSendReportOutlet.wantsLayer = true
                        item.buttonSendReportOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                        item.buttonSendReportOutlet.layer?.borderWidth = 0.0
                        item.buttonSendReportOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                        item.buttonSendReportOutlet.layer?.cornerRadius = 6
                        let pstyle = NSMutableParagraphStyle()
                        pstyle.alignment = .center
                        item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                        item.buttonSendReportOutlet.action = #selector(buttonSendReportsAction)
                    }else{
                        item.labelTitleOutlet.stringValue = self.bluethoothStatus
                        item.imageViewOutlet.isHidden = false
                        item.imageViewOutlet.image = NSImage(named: "danger-1")
                        item.activityIndicator.isHidden = true
                        item.activityIndicator.stopAnimation(nil)
                        self.timer.invalidate()
                        item.buttonSendReportOutlet.isHidden = false
                        item.buttonSendReportTrailingConstraint.constant = 35
                        item.buttonSendReportOutlet.bezelStyle = .texturedSquare
                        item.buttonSendReportOutlet.isBordered = false //Important
                        item.buttonSendReportOutlet.wantsLayer = true
                        item.buttonSendReportOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
                        item.buttonSendReportOutlet.layer?.borderWidth = 0.0
                        item.buttonSendReportOutlet.layer?.borderColor = NSColor(hex: SERVER.darkBlue).cgColor
                        item.buttonSendReportOutlet.layer?.cornerRadius = 6
                        let pstyle = NSMutableParagraphStyle()
                        pstyle.alignment = .center
                        item.buttonSendReportOutlet.attributedTitle = NSAttributedString(string: self.buttonSendReportTitleString, attributes: [ NSAttributedString.Key.foregroundColor : NSColor.white, NSAttributedString.Key.paragraphStyle : pstyle ])
                        item.buttonSendReportOutlet.action = #selector(buttonSendReportsAction)
                    }
                }
            }
            return item
        } else {
            return NSCollectionViewItem()
        }
    }
    
    @objc func buttonSendReportsAction(button:NSButton) {
        var isTestSuccess = ""
        if self.bluethoothStatus == "Bluetooth On" || bluethoothStatus == "البلوتوث قيد التشغيل" || bluethoothStatus == "Bluetooth activado" || bluethoothStatus == "Bluetooth ligado"{
            isTestSuccess = "1"
        }else{
            isTestSuccess = "2"
            
        }
        var testName = ""
        if AppLanguage.current == .english{
            testName = "Bluetooth Test"
        }else if AppLanguage.current == .arabic{
            testName = "اختبار البلوتوث"
        }else if AppLanguage.current == .spanish{
            testName = "Prueba Bluetooth"
        }else if AppLanguage.current == .portuguese{
            testName = "Teste Bluetooth"
        }
        
        let chargerData = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":isTestSuccess,"testTypeId":"118", "TestDatas":[]] as [String : Any]// this is for testing
        self.nonInterectiveArray.add(chargerData)
        if Reachability.isConnectedToNetwork(){
            self.callSaveTestReportAPI(testArray: self.nonInterectiveArray as! [[String : Any]], type: "keyboard")
        }else{
            if AppLanguage.current == .english{
                self.confirmAbletonIsReady(question: "Error", text: "Please Check Your Internet Connection Or Wifi")
            }else if AppLanguage.current == .arabic{
                self.confirmAbletonIsReady(question: "خطأ", text: "يرجى التحقق من اتصالك بالإنترنت أو شبكة Wifi")
            }else if AppLanguage.current == .spanish{
                self.confirmAbletonIsReady(question: "Error", text: "Por favor, compruebe su conexión a Internet o Wifi")
            }else if AppLanguage.current == .portuguese{
                self.confirmAbletonIsReady(question: "Erro", text: "Por favor, verifique sua conexão com a Internet ou Wi-Fi")
            }
        }
        print("nonInterectiveArray is ---------->>>>>>>>>>>>>>>>>>>>>>>>",nonInterectiveArray)
        print("nonInterectiveArray is ---------->>>>>>>>>>>>>>>>>>>>>>>>",nonInterectiveArray)
    }
    func getbluethoothDetails(){
        self.nonInterectiveCounter = 0
        self.isSubMenuSelectedTag = 6
        timer.invalidate() // just in case this button is tapped multiple times
        timer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(BluethoothTimerAction), userInfo: nil, repeats: true)
        self.collectionViewSubmenu.reloadData()
    }
    
    @objc func BluethoothTimerAction() {
        nonInterectiveCounter += 1
        if  nonInterectiveCounter == 0  || nonInterectiveCounter == 1{
            var isTestSuccess = ""
            switch Networking.networkInterfaceType{
            case .Ethernet:
                isTestSuccess = "1"
            case .Wifi:
                isTestSuccess = "1"
            case .Unknown:
                isTestSuccess = "2"
            }
            
            var testName = ""
            if AppLanguage.current == .english{
                testName = "Network Test"
            }else if AppLanguage.current == .arabic{
                testName = "اختبار Ethernet / wifi"
            }else if AppLanguage.current == .spanish{
                testName = "Prueba de red"
            }else if AppLanguage.current == .portuguese{
                testName = "Teste de rede"
            }
            let chargerData = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":isTestSuccess,"testTypeId":"117", "TestDatas":[]] as [String : Any]// this is for testing
            self.nonInterectiveArray.add(chargerData)
            self.viewChargerOutlet.isHidden = false
            self.collectionViewTest.isHidden = false
            self.interectiveTestIndex = 15
            self.keyboardTestStatusID = ""
            self.isChargerStart = 0
            self.collectionViewTest.reloadData()
            self.configureCollectionView(tag: 9)
        }
        print("Counter is ---------->>>>>>>>>>>>>>>>>>>>>>>>",nonInterectiveCounter)
        self.collectionViewTest.reloadData()
    }
    
    func saveTestReports(){
        var isTestSuccess = ""
        if self.bluethoothStatus == "Bluetooth On" || bluethoothStatus == "البلوتوث قيد التشغيل" || bluethoothStatus == "Bluetooth activado" || bluethoothStatus == "Bluetooth ligado"{
            isTestSuccess = "1"
        }else{
            isTestSuccess = "2"
            
        }
        var testName = ""
        if AppLanguage.current == .english{
            testName = "Bluetooth Test"
        }else if AppLanguage.current == .arabic{
            testName = "اختبار البلوتوث"
        }else if AppLanguage.current == .spanish{
            testName = "Prueba Bluetooth"
        }else if AppLanguage.current == .portuguese{
            testName = "Teste Bluetooth"
        }
        

        let chargerData = ["testName":testName,"macAddress": getMacSerialNumber(),"email":DataStore.shared.email ?? "","testStatusID":isTestSuccess,"testTypeId":"118", "TestDatas":[]] as [String : Any]// this is for testing
        self.nonInterectiveArray.add(chargerData)
        
        let a: NSAlert = NSAlert()
        a.messageText = "Send Report ?"
        a.informativeText = "Are you sure send report"
        a.addButton(withTitle: "YES")
        a.addButton(withTitle: "Cancel")
        a.alertStyle = NSAlert.Style.informational
        
        a.beginSheetModal(for: self.view.window ?? NSWindow(), completionHandler: { (modalResponse: NSApplication.ModalResponse) -> Void in
            if(modalResponse == NSApplication.ModalResponse.alertFirstButtonReturn){
                print("Document deleted")
                if Reachability.isConnectedToNetwork(){
                    self.callSaveTestReportAPI(testArray: self.nonInterectiveArray as! [[String : Any]], type: "keyboard")
                }else{
                    if AppLanguage.current == .english{
                        self.confirmAbletonIsReady(question: "Error", text: "Please Check Your Internet Connection Or Wifi")
                    }else if AppLanguage.current == .arabic{
                        self.confirmAbletonIsReady(question: "خطأ", text: "يرجى التحقق من اتصالك بالإنترنت أو شبكة Wifi")
                    }else if AppLanguage.current == .spanish{
                        self.confirmAbletonIsReady(question: "Error", text: "Por favor, compruebe su conexión a Internet o Wifi")
                    }else if AppLanguage.current == .portuguese{
                        self.confirmAbletonIsReady(question: "Erro", text: "Por favor, verifique sua conexão com a Internet ou Wi-Fi")
                    }
                }
            }
        })
        print("nonInterectiveArray is ---------->>>>>>>>>>>>>>>>>>>>>>>>",nonInterectiveArray)
        print("nonInterectiveArray is ---------->>>>>>>>>>>>>>>>>>>>>>>>",nonInterectiveArray)
    }
    
    func centralManager(_ central: CBCentralManager, didDiscover peripheral: CBPeripheral, advertisementData: [String : Any], rssi RSSI: NSNumber) {
        let localName: String = advertisementData[CBAdvertisementDataLocalNameKey] as? String ?? ""
    }
    
    func centralManager(_ central: CBCentralManager, didConnect peripheral: CBPeripheral) {
        print("1")
    }
    
    func centralManager(_ central: CBCentralManager, didDisconnectPeripheral peripheral: CBPeripheral, error: Error?) {
        print("2")
    }
    
    func centralManagerDidUpdateState(_ central: CBCentralManager) {
        switch (central.state) {
        case .poweredOff:
            if AppLanguage.current == .english{
                self.bluethoothStatus = "Bluetooth Off"
            }else if AppLanguage.current == .arabic{
                self.bluethoothStatus = "بلوتوث متوقف"
            }else if AppLanguage.current == .spanish{
                self.bluethoothStatus = "Bluetooth desactivado"
            }else if AppLanguage.current == .portuguese{
                self.bluethoothStatus = "Bluetooth desligado"
            }
            print("Powered Off")
        case .poweredOn:
            if AppLanguage.current == .english{
                self.bluethoothStatus = "Bluetooth On"
            }else if AppLanguage.current == .arabic{
                self.bluethoothStatus = "البلوتوث قيد التشغيل"
            }else if AppLanguage.current == .spanish{
                self.bluethoothStatus = "Bluetooth activado"
            }else if AppLanguage.current == .portuguese{
                self.bluethoothStatus = "Bluetooth ligado"
            }
            print("Powered On")
            self.centralManager.scanForPeripherals(withServices: nil, options: nil)
        case .unauthorized:
            if AppLanguage.current == .english{
                self.bluethoothStatus = "Unauthorized"
            }else if AppLanguage.current == .arabic{
                self.bluethoothStatus = "غير مصرح"
            }else if AppLanguage.current == .spanish{
                self.bluethoothStatus = "No autorizado"
            }else if AppLanguage.current == .portuguese{
                self.bluethoothStatus = "Não autorizado"
            }
            print("Unauthorized")
        case .unknown:
            if AppLanguage.current == .english{
                self.bluethoothStatus = "Unknown"
            }else if AppLanguage.current == .arabic{
                self.bluethoothStatus = "مجهول"
            }else if AppLanguage.current == .spanish{
                self.bluethoothStatus = "Desconocido"
            }else if AppLanguage.current == .portuguese{
                self.bluethoothStatus = "Desconhecido"
            }
            print("Unknown")
        case .unsupported:
            if AppLanguage.current == .english{
                self.bluethoothStatus = "Unsupported"
            }else if AppLanguage.current == .arabic{
                self.bluethoothStatus = "غير مدعوم"
            }else if AppLanguage.current == .spanish{
                self.bluethoothStatus = "No compatible"
            }else if AppLanguage.current == .portuguese{
                self.bluethoothStatus = "Sem suporte"
            }
        default:
            print("Default")
        }
    }
}


extension HomeVC{
    // MARK: - API  Calling
    func callSaveTestReportAPI(testArray:[[String:Any]],type:String) {
        print("-------->>>>>>>>>>>>",testArray)
        self.isKeyboardTest = false
        self.collectionViewSubmenu.isSelectable = true
        self.activityIndicator.isHidden = false
        activityIndicator.isIndeterminate = true
        activityIndicator.style = .spinning
        activityIndicator.startAnimation(nil)
        
        var request = URLRequest(url: URL(string: SERVER.saveTestReportByMobile)!)
        request.httpMethod = "POST"
        request.setValue("application/json", forHTTPHeaderField: "Content-Type")
        request.httpBody = try! JSONSerialization.data(withJSONObject: testArray)
        print("-------->>>>>testArray>>>>>>>",testArray)
        Alamofire.request(request)                               // Or `Alamofire.request(request)` in prior versions of Alamofire
            .responseJSON { response in
                DispatchQueue.main.async {
                    self.activityIndicator.isHidden = true
                    self.activityIndicator.stopAnimation(nil)
                }
                switch response.result {
                case .failure(let error):
                    print(error)
                    
                    if let data = response.data, let responseString = String(data: data, encoding: .utf8) {
                        print("-------->>>>>>>>>>>>>>>>>",responseString)
                    }
                case .success(let responseObject):
                    print("-------->>>>>>>>>>>>>>>>>",responseObject)
                    if (responseObject as? NSDictionary)?.value(forKey: "Status") as? Int == 1{
                        if self.isMultipleTestSelect == false{
                            self.timer.invalidate()
                            self.keyboardTestStatusID = ""
                            self.counter = 0
                            self.isChargerStart = 0
                            if type == "volume"{
                                self.audioEngine.stop()
                                self.audioPlayer.stop()
                                self.isVolumeLow = false
                                self.isVolumeHigh = false
                            }else if type == "keyboard"{
                                self.keyboardKeyArray.removeAll()
                                //                            self.collectionViewSubmenu.isSelectable = true
                            }else if type == "charger"{
                                self.isChargerStart = 0
                            }else if type == "webcam"{
                                self.isChargerStart = 0
                            }else if type == "mic"{
                                self.isChargerStart = 0
                            }else if type == "pixels"{
                                self.isChargerStart = 0
                            }else if type == "audio"{
                                self.isChargerStart = -1
                                self.audioTestBalanceStatus = ""
                                self.audioTestRightStatus = ""
                                self.audioTestStatus = ""
                                self.isChargerStart = 0
                                self.audioTestArray.removeAllObjects()
                            }
                            self.isKeyboardTest = false
                            self.tableView.selectRowIndexes(NSIndexSet(index: 0) as IndexSet, byExtendingSelection: false)
                            self.collectionViewTest.reloadData()
                            self.isSelectIndex = 0
                            self.tableView.reloadData()
                        }else{
                            if self.SelectArrayString.count == 0 {
                                self.isMultipleTestSelect = false
                                self.selectArray.removeAll()
                                self.SelectArrayString.removeAll()
                                self.currentSelection = -1
                                self.isSelect = false
                                self.timer.invalidate()
                                self.keyboardTestStatusID = ""
                                self.counter = 0
                                self.isChargerStart = 0
                                self.audioEngine.stop()
                                self.audioPlayer.stop()
                                self.isVolumeLow = false
                                self.isVolumeHigh = false
                                self.keyboardKeyArray.removeAll()
                                    self.isChargerStart = 0
                                    self.isChargerStart = 0
                                    self.isChargerStart = 0
                                    self.isChargerStart = 0
                                    self.isChargerStart = -1
                                    self.audioTestBalanceStatus = ""
                                    self.audioTestRightStatus = ""
                                    self.audioTestStatus = ""
                                    self.isChargerStart = 0
                                    self.audioTestArray.removeAllObjects()
                                self.isKeyboardTest = false
                                self.tableView.selectRowIndexes(NSIndexSet(index: 0) as IndexSet, byExtendingSelection: false)
                                self.imageData.removeAll()
                                self.collectionViewTest.reloadData()
                                self.isSelectIndex = 0
                                self.tableView.reloadData()
                                self.collectionViewSubmenu.reloadData()
                            }else{
                                self.buttonDropDownStartAction(button: NSButton())
                            }
                        }
                    }else{
                        self.confirmAbletonIsReady(question: "Error", text: ((responseObject as? NSDictionary)?.value(forKey: "Msg") as? String ?? ""))
                    }
                }
            }
    }
    
    func getTestResultAPI(testTypeId:Int) {
        self.testArray.removeAllObjects()
        self.reportsActivityIndicator.isHidden = false
        reportsActivityIndicator.isIndeterminate = true
        reportsActivityIndicator.style = .spinning
        reportsActivityIndicator.startAnimation(nil)
        let url = SERVER.getDeviceTestResultsByMobile
        let param = ["MacAddress":getMacSerialNumber(),"SerialNumber":getMacSerialNumber() ,"testTypeId":testTypeId,"testingDate":self.dateString,"email":DataStore.shared.email ?? ""] as [String : Any]
        NetworkManager.shared.callApi(url, param, .post) { (err, res) in
            DispatchQueue.main.async {
                self.reportsActivityIndicator.isHidden = true
                self.reportsActivityIndicator.stopAnimation(nil)
            }
            if (res?["Status"] as? Int) == 1{
                self.testArray = (res?["data"] as? NSArray)?.mutableCopy() as? NSMutableArray ?? []
                self.collectionViewTest.reloadData()
                self.collectionViewSubmenu.reloadData()

            }else{
                self.collectionViewTest.reloadData()
                self.collectionViewSubmenu.reloadData()
                
                //                self.autoHideAlert(title: "", message: (res?["Msg"] as? String ?? "")) {
                //                    self.navigationController?.popViewController(animated: true)
            }
        }
    }
    
    func getDeviceTestResultsAPI(TestingGroupId:Int) {
        self.reportsActivityIndicator.isHidden = false
        reportsActivityIndicator.isIndeterminate = true
        reportsActivityIndicator.style = .spinning
        reportsActivityIndicator.startAnimation(nil)
        let url = "\(SERVER.BASE_URL)Org/GetTestingTypes?DeviceTypeId=\(6)&TestingGroupId=\(TestingGroupId)"
        print("------>>>>>>>>",url)
        NetworkManager.shared.callApi(url, nil, .get) { (err, res) in
            DispatchQueue.main.async {
                self.reportsActivityIndicator.isHidden = true
                self.reportsActivityIndicator.stopAnimation(nil)
            }
            print("res----->>>>>",res?["Status"] as? Int)
            if (res?["Status"] as? Int) == 1{
                self.testTypeArray = (res?["data"] as? NSArray)?.mutableCopy() as? NSMutableArray ?? []
                self.tableView.reloadData()
                self.collectionViewSubmenu.reloadData()
                self.collectionViewTest.reloadData()
            }else{
                self.collectionViewTest.reloadData()
                self.collectionViewSubmenu.reloadData()
                //                        self.showError((res?["Msg"] as? String ?? ""))
                //                        self.tableView.reloadData()
            }
        }
    }
    
    func getDeviceDiagnosticTestResultsAPI() {
        self.reportsActivityIndicator.isHidden = false
        reportsActivityIndicator.isIndeterminate = true
        reportsActivityIndicator.style = .spinning
        reportsActivityIndicator.startAnimation(nil)
        let url = SERVER.getDeviceDiagnosticTestResults
        let param = ["MacAddress":getMacSerialNumber(),"SerialNumber":getMacSerialNumber() ,"groupId":1,"email":DataStore.shared.email ?? ""] as [String : Any]
        print("PARAMS IS ------>>>>>>>",param)
        print("PARAMS IS ------>>>>>>>",url)
        NetworkManager.shared.callApi(url, param, .post) { (err, res) in
            DispatchQueue.main.async {
                self.reportsActivityIndicator.isHidden = true
                self.reportsActivityIndicator.stopAnimation(nil)
            }
            print("res----->>>>>",res)
            print("res----->>>>>",res?["Status"] as? Int)
            if (res?["Status"] as? Int) == 1{
                self.testArray = (res?["data"] as? NSArray)?.mutableCopy() as? NSMutableArray ?? []
                self.tableView.reloadData()
                self.collectionViewSubmenu.reloadData()
                self.collectionViewTest.reloadData()

            }else{
                self.collectionViewTest.reloadData()
                self.collectionViewSubmenu.reloadData()
                
            }
        }
    }
    
    func getDeviceTestResultsSixTestAPI() {
        
       // self.pdfViewOpenPDF_URL()
        
        self.reportsActivityIndicator.isHidden = false
        reportsActivityIndicator.isIndeterminate = true
        reportsActivityIndicator.style = .spinning
        reportsActivityIndicator.startAnimation(nil)
        let url = SERVER.getDeviceTestResultsSixTest
        
            let param = ["MacAddress":getMacSerialNumber() ?? ""] as Dictionary<String,Any>
            print("PARAMS IS ------>>>>>>>",param)
            print("PARAMS IS ------>>>>>>>",url)
            
            var request = URLRequest(url: URL(string: SERVER.getDeviceTestResultsSixTest)!)
            request.httpMethod = "POST"
            request.setValue("application/json", forHTTPHeaderField: "Content-Type")
            request.httpBody = try! JSONSerialization.data(withJSONObject: param)
            print("-------->>>>>>>>>>>>",param)
            Alamofire.request(request)                               // Or `Alamofire.request(request)` in prior versions of Alamofire
                .responseJSON { response in
                    DispatchQueue.main.async {
                        self.reportsActivityIndicator.isHidden = true
                        self.reportsActivityIndicator.stopAnimation(nil)
                        
                    }
                    switch response.result {
                    case .failure(let error):
                        print(error)
                        
                        if let data = response.data, let responseString = String(data: data, encoding: .utf8) {
                            print("-------->>>>>>>>>>>>>>>>>",responseString)
                        }
                    case .success(let responseObject):
                        print("-------->>>>>>>>>>>>>>>>>",responseObject)
                 //       self.dataSource = (responseObject as? NSArray)?.mutableCopy() as? NSMutableArray ?? []
                        self.testArray = (responseObject as? NSArray)?.mutableCopy() as? NSMutableArray ?? []
                        //self.getpdfdataAPI(Date: "08-04-2023")
                        self.tableView.reloadData()
                        self.collectionViewSubmenu.reloadData()
                        self.collectionViewTest.reloadData()
                    }
                }
            
        
//            DispatchQueue.main.async {
//                self.reportsActivityIndicator.isHidden = true
        //                self.reportsActivityIndicator.stopAnimation(nil)
//            }
//            print("res----->>>>>",res)
//            print("res----->>>>>",res?["Status"] as? Int)
//            if (res?["Status"] as? Int) == 1{
//                self.testArray = (res?["data"] as? NSArray)?.mutableCopy() as? NSMutableArray ?? []
//                self.tableView.reloadData()
//                self.collectionViewSubmenu.reloadData()
//                self.collectionViewTest.reloadData()
//
//            }else{
//                self.collectionViewTest.reloadData()
//                self.collectionViewSubmenu.reloadData()
//
//            }
//        }
    }
    
    func getpdfdataAPI(Date:String) {
        self.reportsActivityIndicator.isHidden = false
        reportsActivityIndicator.isIndeterminate = true
        reportsActivityIndicator.style = .spinning
        reportsActivityIndicator.startAnimation(nil)
//        let url = "\(SERVER.Getpdfdata)?MacAddress=\(118)&datereport=\(Date)"
      let url = "https://nexaglobal.azurewebsites.net/api/Org/Getpdfdata?MacAddress=118&datereport=08-04-2023"
        print("------>>>>>>>>",url)
        NetworkManager.shared.callApi(url, nil, .post) { (err, res) in
            DispatchQueue.main.async {
                self.reportsActivityIndicator.isHidden = true
                self.reportsActivityIndicator.stopAnimation(nil)
            }
            print("res----->>>>>",res?["Status"] as? Int)
            if (res?["Status"] as? Int) == 1{
                self.testTypeArray = (res?["data"] as? NSArray)?.mutableCopy() as? NSMutableArray ?? []
                self.tableView.reloadData()
                self.collectionViewSubmenu.reloadData()
                self.collectionViewTest.reloadData()
            }else{
                self.collectionViewTest.reloadData()
                self.collectionViewSubmenu.reloadData()
                //                        self.showError((res?["Msg"] as? String ?? ""))
                //                        self.tableView.reloadData()
            }
        }
    }
    
}
extension NSColor {
    
    convenience init(hex: String) {
        let trimHex = hex.trimmingCharacters(in: .whitespacesAndNewlines)
        let dropHash = String(trimHex.dropFirst()).trimmingCharacters(in: .whitespacesAndNewlines)
        let hexString = trimHex.starts(with: "#") ? dropHash : trimHex
        let ui64 = UInt64(hexString, radix: 16)
        let value = ui64 != nil ? Int(ui64!) : 0
        // #RRGGBB
        var components = (
            R: CGFloat((value >> 16) & 0xff) / 255,
            G: CGFloat((value >> 08) & 0xff) / 255,
            B: CGFloat((value >> 00) & 0xff) / 255,
            a: CGFloat(1)
        )
        if String(hexString).count == 8 {
            // #RRGGBBAA
            components = (
                R: CGFloat((value >> 24) & 0xff) / 255,
                G: CGFloat((value >> 16) & 0xff) / 255,
                B: CGFloat((value >> 08) & 0xff) / 255,
                a: CGFloat((value >> 00) & 0xff) / 255
            )
        }
        self.init(red: components.R, green: components.G, blue: components.B, alpha: components.a)
    }
    
    func toHex(alpha: Bool = false) -> String? {
        guard let components = cgColor.components, components.count >= 3 else {
            return nil
        }
        
        let r = Float(components[0])
        let g = Float(components[1])
        let b = Float(components[2])
        var a = Float(1.0)
        
        if components.count >= 4 {
            a = Float(components[3])
        }
        
        if alpha {
            return String(format: "%02lX%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255), lroundf(a * 255))
        } else {
            return String(format: "%02lX%02lX%02lX", lroundf(r * 255), lroundf(g * 255), lroundf(b * 255))
        }
    }
}

extension NSSound {
    
    //
    // PUBLIC INTERFACE
    // to be swifty, public interface is mostly gettable-settable properties,
    // only few convenience functions
    //
    
    
    // managing the system volume by setting "systemVolume" property (Float between 0 and 1.0)
    public static var systemVolume: Float {
        get {return self.getSystemVolume()}
        set {self.setSystemVolume(theVolume:newValue)}
    }
    
    // getting and setting the "mute" state of the system volume
    // note convenience function "systemVolumeFadeToMute(seconds:Float)" to fade out to mute
    public static var systemVolumeIsMuted: Bool {
        get {return self.getSystemVolumeIsMuted()}
        set {self.systemVolumeSetMuted(newValue)}
    }
    
    // convenience function to fade system volume out to mute over (Float) seconds
    // NOTE: blocking specifies if function returns immediately or only after muting completed!
    // If blocking = false the task will be scheduled in a global queue for asynchronous execution
    // by GDC– any manual volume control while fading out will be overridden! Can be desired or not...
    public class func systemVolumeFadeToMute(seconds: Float = 3, blocking: Bool = true) {
        // return if already muted
        if systemVolumeIsMuted {return}
        
        if blocking {fadeSystemVolumeToMutePrivate(seconds: seconds)}
        else {DispatchQueue.global().async {self.fadeSystemVolumeToMutePrivate(seconds: seconds)} }
    }
    
    //
    // PRIVATE INTERFACE
    // these functions cannot be called publicly but are for internal use only!
    // C-centric CoreAudioKit is hidden behind the above swifty public interface
    //
    
    
    
    //    Return the ID of the default audio device; this is a C routine
    //
    //    IN:        none
    //    OUT:    the ID of the default device or AudioObjectUnknown
    //
    private class func obtainDefaultOutputDevice() -> AudioDeviceID
    {
        var  theAnswer : AudioDeviceID = kAudioObjectUnknown
        var  theSize = UInt32(MemoryLayout.size(ofValue: theAnswer)) // needs to be converted to UInt32?
        var  theAddress : AudioObjectPropertyAddress
        
        theAddress = AudioObjectPropertyAddress.init(mSelector: kAudioHardwarePropertyDefaultOutputDevice, mScope: kAudioObjectPropertyScopeGlobal, mElement: kAudioObjectPropertyElementMaster)
        
        //first be sure that a default device exists
        if (!AudioObjectHasProperty(AudioObjectID(kAudioObjectSystemObject), &theAddress) )    {
            print("Unable to get default audio device")
            return theAnswer
        }
        
        //get the property 'default output device'
        let theError : OSStatus = AudioObjectGetPropertyData(AudioObjectID(kAudioObjectSystemObject), &theAddress, UInt32(0), nil, &theSize, &theAnswer)
        if (theError != noErr) {
            print("Unable to get output audio device")
            return theAnswer
        }
        return theAnswer
    }
    
    
    //
    //    Return the system sound volume as a float in the range [0...1]
    //
    //    IN:        none
    //    OUT:    (float) the volume of the default device
    //
    private class func getSystemVolume() -> Float
    {
        var defaultDevID: AudioDeviceID = kAudioObjectUnknown
        var theSize = UInt32(MemoryLayout.size(ofValue: defaultDevID))
        var theError: OSStatus
        var theVolume: Float32 = 0
        var theAddress: AudioObjectPropertyAddress
        
        defaultDevID = obtainDefaultOutputDevice()
        if (defaultDevID == kAudioObjectUnknown) {
            print("Audio device not found!")
            return 0.0
        }        //device not found: return 0
        
        theAddress = AudioObjectPropertyAddress.init(mSelector: kAudioHardwareServiceDeviceProperty_VirtualMainVolume, mScope: kAudioDevicePropertyScopeOutput, mElement: kAudioObjectPropertyElementMaster)
        //be sure that the default device has the volume property
        if (!AudioObjectHasProperty(defaultDevID, &theAddress) ) {
            print("No volume control for device 0x%0x",defaultDevID)
            return 0.0
        }
        
        //now read the property and correct it, if outside [0...1]
        theError = AudioObjectGetPropertyData(defaultDevID, &theAddress, 0, nil, &theSize, &theVolume)
        if ( theError != noErr )    {
            print("Unable to read volume for device 0x%0x", defaultDevID)
            return 0.0
        }
        
        theVolume = theVolume > 1.0 ? 1.0 : (theVolume < 0.0 ? 0.0 : theVolume)
        
        return theVolume
    }
    
    
    
    //
    //    Set the volume of the default device
    //
    //    IN:        (float)the new volume
    //    OUT:    none
    //
    private class func setSystemVolume(theVolume: Float, muteOff: Bool = true)
    {
        var newValue: Float = theVolume
        var theAddress: AudioObjectPropertyAddress
        var defaultDevID: AudioDeviceID
        var theError: OSStatus = noErr
        var muted: UInt32
        var canSetVol: DarwinBoolean = true
        var muteValue: Bool
        var hasMute:Bool = true
        var canMute: DarwinBoolean = true
        
        defaultDevID = obtainDefaultOutputDevice()
        if (defaultDevID == kAudioObjectUnknown) {
            //device not found: return without trying to set
            print("Audio Device unknown")
            return
        }
        
        //check if the new value is in the correct range - normalize it if not
        newValue = theVolume > 1.0 ? 1.0 : (theVolume < 0.0 ? 0.0 : theVolume)
        if (newValue != theVolume) {
            print("Tentative volume (%5.2f) was out of range; reset to %5.2f", theVolume, newValue)
        }
        
        theAddress = AudioObjectPropertyAddress.init(mSelector: kAudioDevicePropertyMute, mScope: kAudioDevicePropertyScopeOutput, mElement: kAudioObjectPropertyElementMaster)
        
        //set the selector to mute or not by checking if under threshold (5% here)
        //and check if a mute command is available
        muteValue = (newValue < 0.05)
        if (muteValue) {
            theAddress.mSelector = kAudioDevicePropertyMute
            hasMute = AudioObjectHasProperty(defaultDevID, &theAddress)
            if (hasMute) {
                theError = AudioObjectIsPropertySettable(defaultDevID, &theAddress, &canMute)
                if (theError != noErr || !(canMute.boolValue))
                {
                    canMute = false
                    print("Should mute device 0x%0x but did not succeed",defaultDevID)
                }
            }
            else {canMute = false}
        } else {
            theAddress.mSelector = kAudioHardwareServiceDeviceProperty_VirtualMainVolume
        }
        
        // **** now manage the volume following the what we found ****
        
        //be sure the device has a volume command
        if (!AudioObjectHasProperty(defaultDevID, &theAddress)) {
            print("The device 0x%0x does not have a volume to set", defaultDevID)
            return
        }
        
        //be sure the device can set the volume
        theError = AudioObjectIsPropertySettable(defaultDevID, &theAddress, &canSetVol)
        if ( theError != noErr || !canSetVol.boolValue ) {
            print("The volume of device 0x%0x cannot be set", defaultDevID)
            return
        }
        
        //if under the threshold then mute it, only if possible - done/exit
        if (muteValue && hasMute && canMute.boolValue) {
            muted = 1
            theError = AudioObjectSetPropertyData(defaultDevID, &theAddress, 0, nil, UInt32(MemoryLayout.size(ofValue: muted)), &muted)
            
            if (theError != noErr) {
                print("The device 0x%0x was not muted",defaultDevID)
                return
            }
        } else {       //else set it
            theError = AudioObjectSetPropertyData(defaultDevID, &theAddress, 0, nil, UInt32(MemoryLayout.size(ofValue: newValue)), &newValue)
            if (theError != noErr) {
                print("The device 0x%0x was unable to set volume", defaultDevID)
            }
            //if device is able to handle muting, maybe it was muted, so unlock it
            if (muteOff && hasMute && canMute.boolValue) {
                theAddress.mSelector = kAudioDevicePropertyMute
                muted = 0
                theError = AudioObjectSetPropertyData(defaultDevID, &theAddress, 0, nil, UInt32(MemoryLayout.size(ofValue: muted)), &muted)
            }
        }
        if (theError != noErr) {
            print("Unable to set volume for device 0x%0x", defaultDevID)
        }
    }
    
    
    
    //
    //    IN:        (Boolean) if true the device is muted, false it is unmated
    //    OUT:        none
    //
    private class func systemVolumeSetMuted(_ m:Bool) {
        var defaultDevID: AudioDeviceID = kAudioObjectUnknown
        var theAddress: AudioObjectPropertyAddress
        var hasMute: Bool
        var canMute: DarwinBoolean = true
        var theError: OSStatus = noErr
        var muted: UInt32 = 0
        
        defaultDevID = obtainDefaultOutputDevice()
        if (defaultDevID == kAudioObjectUnknown) {
            //device not found
            print("Audio device unknown")
            return
        }
        
        theAddress = AudioObjectPropertyAddress.init(mSelector: kAudioDevicePropertyMute, mScope: kAudioDevicePropertyScopeOutput, mElement: kAudioObjectPropertyElementMaster)
        
        muted = m ? 1 : 0
        
        hasMute = AudioObjectHasProperty(defaultDevID, &theAddress)
        
        if (hasMute)
        {
            theError = AudioObjectIsPropertySettable(defaultDevID, &theAddress, &canMute)
            if (theError == noErr && canMute.boolValue)
            {
                theError = AudioObjectSetPropertyData(defaultDevID, &theAddress, 0, nil, UInt32(MemoryLayout.size(ofValue: muted)), &muted)
                if (theError != noErr) {
                    print("Cannot change mute status of device 0x%0x", defaultDevID)
                }
            }
        }
    }
    
    
    
    //
    //    IN:        (float) number of seconds during which volume is faded out to mute
    //    OUT:        none
    //
    private class func fadeSystemVolumeToMutePrivate(seconds:Float) {
        
        // prevent muting times longer than 10 seconds
        var secs = (seconds > 0) ? seconds : (seconds*(-1.0))
        secs = (secs > 10.0) ? 10.0 : secs
        
        let currentVolume = self.systemVolume
        let delta = currentVolume / (seconds*2)
        var secondsLeft = secs
        
        while (secondsLeft > 0) {
            self.systemVolume -= delta
            Thread.sleep(forTimeInterval: 0.5)
            secondsLeft -= 0.5
        }
        systemVolumeIsMuted = true
        setSystemVolume(theVolume: currentVolume, muteOff: false)
    }
    
    
    //
    //    IN:        none
    //    OUT:       (Bool) state of system mute (NOTE: this is different from system volume = 0!
    //
    private class func getSystemVolumeIsMuted() -> Bool
    {
        var defaultDevID: AudioDeviceID = kAudioObjectUnknown
        var theAddress: AudioObjectPropertyAddress
        var hasMute: Bool
        var canMute: DarwinBoolean = true
        var theError: OSStatus = noErr
        var muted: UInt32 = 0
        var mutedSize = UInt32(MemoryLayout.size(ofValue: muted))
        
        defaultDevID = obtainDefaultOutputDevice()
        if (defaultDevID == kAudioObjectUnknown) {
            //device not found
            print("Audio device unknown")
            return false                       // works, but not the best return code for this
        }
        
        theAddress = AudioObjectPropertyAddress.init(mSelector: kAudioDevicePropertyMute, mScope: kAudioDevicePropertyScopeOutput, mElement: kAudioObjectPropertyElementMaster)
        
        hasMute = AudioObjectHasProperty(defaultDevID, &theAddress)
        
        if (hasMute) {
            theError = AudioObjectIsPropertySettable(defaultDevID, &theAddress, &canMute)
            if (theError == noErr && canMute.boolValue) {
                theError = AudioObjectGetPropertyData(defaultDevID, &theAddress, 0, nil, &mutedSize, &muted)
                if (muted != 0) {
                    return true
                }
            }
        }
        return false
    }
    
}
extension Float {
    var clean: String {
        return self.truncatingRemainder(dividingBy: 1) == 0 ? String(format: "%.0f", self) : String(self)
    }
}

class DiskStatus {
    
    //MARK: Formatter MB only
    class func MBFormatter(_ bytes: Int64) -> String {
        let formatter = ByteCountFormatter()
        formatter.allowedUnits = ByteCountFormatter.Units.useMB
        formatter.countStyle = ByteCountFormatter.CountStyle.decimal
        formatter.includesUnit = false
        return formatter.string(fromByteCount: bytes) as String
    }
    
    
    //MARK: Get String Value
    class var totalDiskSpace:String {
        get {
            return ByteCountFormatter.string(fromByteCount: totalDiskSpaceInBytes, countStyle: ByteCountFormatter.CountStyle.file)
        }
    }
    
    class var freeDiskSpace:String {
        get {
            return ByteCountFormatter.string(fromByteCount: freeDiskSpaceInBytes, countStyle: ByteCountFormatter.CountStyle.file)
        }
    }
    
    class var usedDiskSpace:String {
        get {
            return ByteCountFormatter.string(fromByteCount: usedDiskSpaceInBytes, countStyle: ByteCountFormatter.CountStyle.file)
        }
    }
    
    
    //MARK: Get raw value
    class var totalDiskSpaceInBytes:Int64 {
        get {
            do {
                let systemAttributes = try FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory() as String)
                let space = (systemAttributes[FileAttributeKey.systemSize] as? NSNumber)?.int64Value
                return space!
            } catch {
                return 0
            }
        }
    }
    
    class var freeDiskSpaceInBytes:Int64 {
        get {
            do {
                let systemAttributes = try FileManager.default.attributesOfFileSystem(forPath: NSHomeDirectory() as String)
                let freeSpace = (systemAttributes[FileAttributeKey.systemFreeSize] as? NSNumber)?.int64Value
                return freeSpace!
            } catch {
                return 0
            }
        }
    }
    
    class var usedDiskSpaceInBytes:Int64 {
        get {
            let usedSpace = totalDiskSpaceInBytes - freeDiskSpaceInBytes
            return usedSpace
        }
    }
}

class ReplacePresentationAnimator: NSObject, NSViewControllerPresentationAnimator {
    func animatePresentation(of viewController: NSViewController, from fromViewController: NSViewController) {
        if let window = fromViewController.view.window {
            NSAnimationContext.runAnimationGroup({ (context) -> Void in
                fromViewController.view.animator().alphaValue = 0
            }, completionHandler: { () -> Void in
                viewController.view.alphaValue = 0
                window.contentViewController = viewController
                viewController.view.animator().alphaValue = 1.0
            })
        }
    }
    
    func animateDismissal(of viewController: NSViewController, from fromViewController: NSViewController) {
        if let window = viewController.view.window {
            NSAnimationContext.runAnimationGroup({ (context) -> Void in
                viewController.view.animator().alphaValue = 0
            }, completionHandler: { () -> Void in
                fromViewController.view.alphaValue = 0
                window.contentViewController = fromViewController
                fromViewController.view.animator().alphaValue = 1.0
            })
        }
    }
}
extension Double {
    var threeDigits: Double {
        return (self * 1000).rounded(.toNearestOrEven) / 1000
    }
    
    var twoDigits: Double {
        return (self * 100).rounded(.toNearestOrEven) / 100
    }
    
    var oneDigit: Double {
        return (self * 10).rounded(.toNearestOrEven) / 10
    }
}
