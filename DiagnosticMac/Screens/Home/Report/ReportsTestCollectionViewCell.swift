//
//  ReportsTestCollectionViewCell.swift
//  DiagnosticMac
//
//  Created by MacOS on 29/03/22.
//

import Cocoa

class ReportsTestCollectionViewCell: NSCollectionViewItem {
    @IBOutlet weak var viewOutlet: NSView!
    @IBOutlet weak var labelTitleOutlet: NSTextField!
    
    @IBOutlet weak var labelNameOutlet: NSTextField!
    @IBOutlet weak var labelHeaderTitleOutlet: NSTextField!
    @IBOutlet weak var labelOutlet: NSTextField!
    
    @IBOutlet weak var labelStatusOutlet: NSTextField!
    @IBOutlet weak var imageViewStatusOutlet: NSImageView!
    
    @IBOutlet weak var labelTestNameOutlet: NSTextField!
    @IBOutlet weak var viewReportResultOutlet: NSView!
    
    @IBOutlet weak var buttonSendReportTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonDropDownOutlet: NSPopUpButton!
    
    @IBOutlet weak var buttonSendReportWidthConstraint: NSLayoutConstraint!
    @IBOutlet weak var buttonSendReportOutlet: NSButton!
    
    @IBOutlet weak var buttonDropDownCenterConstraint: NSLayoutConstraint!
    
    @IBOutlet weak var buttonDropDownStartOutlet: NSButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
}
