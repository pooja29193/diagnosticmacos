//
//  TestHeaderCollectionViewCell.swift
//  Example NSTableView macOS Swift
//
//  Created by MacOS on 17/03/22.
//  Copyright © 2022 Bình Nguyễn. All rights reserved.
//

import Cocoa

class TestHeaderCollectionViewCell: NSCollectionViewItem {
    @IBOutlet weak var buttonOutlet: NSButton!
    
    @IBOutlet weak var labelOutlet: NSTextField!

    @IBOutlet weak var labelTitleOutlet: NSTextField!

    @IBOutlet weak var buttonRunAgainOUtlet: NSButton!
    @IBOutlet weak var labelStatusOutlet: NSTextField!
    @IBOutlet weak var buttonSendReportOutlet: NSButton!
    @IBOutlet weak var activityIndicator: NSProgressIndicator!
    @IBOutlet weak var imageViewOutlet: NSImageView!
    @IBOutlet weak var labelSubtitleOutlet: NSTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
}
