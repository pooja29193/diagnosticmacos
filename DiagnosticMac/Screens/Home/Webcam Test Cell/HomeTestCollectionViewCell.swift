//
//  HomeTestCollectionViewCell.swift
//  DiagnosticMac
//
//  Created by MacOS on 17/03/22.
//

import Cocoa

class HomeTestCollectionViewCell: NSCollectionViewItem {
    
    @IBOutlet weak var buttonCaptureAction: NSButton!
    @IBOutlet weak var labelOutlet: NSTextField!
    @IBOutlet weak var buttonOutlet: NSButton!
    @IBOutlet weak var labelTitleOutlet: NSTextField!
    @IBOutlet weak var labelSubtitleOutlet: NSTextField!
    @IBOutlet weak var imageViewOutlet: NSImageView!
    @IBOutlet weak var activityIndicator: NSProgressIndicator!
    @IBOutlet weak var cameraView: NSView!
    @IBOutlet weak var buttonSendReportOutlet: NSButton!
    @IBOutlet weak var buttonRunAgainOUtlet: NSButton!
    @IBOutlet weak var labelStatusOutlet: NSTextField!
    @IBOutlet weak var imageViewPhotoOutlet: NSImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
}
