//
//  SubMenuCollectionViewCell.swift
//  DiagnosticMac
//
//  Created by MacOS on 17/03/22.
//

import Cocoa

class SubMenuCollectionViewCell: NSCollectionViewItem {
    @IBOutlet weak var labelSubtitleOutlet: NSTextField!
    
    @IBOutlet weak var viewOutlet: NSView!
    @IBOutlet weak var imageViewOutlet: NSImageView!
    @IBOutlet weak var labelTitleOutlet: NSTextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
}
