//
//  VolumeTestCollectionViewCell.swift
//  DiagnosticMac
//
//  Created by MacOS on 22/03/22.
//

import Cocoa

class VolumeTestCollectionViewCell: NSCollectionViewItem {
    @IBOutlet weak var buttonOutlet: NSButton!
    
    @IBOutlet weak var labelPercentageOutlet: NSTextField!
    @IBOutlet weak var progressIndicator: NSProgressIndicator!
    @IBOutlet weak var imageViewStatusOutlet: NSImageView!
    @IBOutlet weak var viewOutlet: NSView!
    @IBOutlet weak var labelOutlet: NSTextField!

    @IBOutlet weak var labelTitleOutlet: NSTextField!

    @IBOutlet weak var buttonRunAgainOUtlet: NSButton!
    @IBOutlet weak var labelStatusOutlet: NSTextField!
    @IBOutlet weak var buttonSendReportOutlet: NSButton!
    @IBOutlet weak var activityIndicator: NSProgressIndicator!
    @IBOutlet weak var imageViewOutlet: NSImageView!
    @IBOutlet weak var labelSubtitleOutlet: NSTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
}
