//
//  PurchasesDetailView.swift
//  TableDemo
//
//  Created by Gabriel Theodoropoulos.
//  Copyright © 2019 Appcoda. All rights reserved.
//

import Cocoa

class HomeSubMenuCell: NSView, LoadableView {
    
    // MARK: - IBOutlet Properties
    
    @IBOutlet weak var labelOutlet: NSTextField!
    
    @IBOutlet weak var ImageViewOutlet: NSImageView!
    
    @IBOutlet weak var idLabel: NSTextField!
    
    @IBOutlet weak var creditCardNumberLabel: NSTextField!
    
    @IBOutlet weak var creditCardTypeLabel: NSTextField!
    
    @IBOutlet weak var purchasesLabel: NSTextField!
    
    @IBOutlet weak var amountLabel: NSTextField!
    
    @IBOutlet weak var labelLeadingConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    
    var mainView: NSView?
    
    
    
    // MARK: - Init
    
    init() {
        super.init(frame: NSRect.zero)
        
        _ = load(fromNIBNamed: "HomeSubMenuCell")
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
