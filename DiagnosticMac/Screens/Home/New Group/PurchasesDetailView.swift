//
//  PurchasesDetailView.swift
//  TableDemo
//
//  Created by Gabriel Theodoropoulos.
//  Copyright © 2019 Appcoda. All rights reserved.
//

import Cocoa

class PurchasesDetailView: NSView, LoadableView {
    
    // MARK: - IBOutlet Properties
    
    @IBOutlet weak var buttonOutlet: NSPopUpButton!
    @IBOutlet weak var viewOutlet: NSView!
    @IBOutlet weak var labelOutlet: NSTextField!
    
    @IBOutlet weak var avatarImageView: NSImageView!
    @IBOutlet weak var labelLeadingConstraint: NSLayoutConstraint!
    
    // MARK: - Properties
    
    var mainView: NSView?

    // MARK: - Init
    
    init() {
        super.init(frame: NSRect.zero)
        
        _ = load(fromNIBNamed: "PurchasesDetailView")
    }
    
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
