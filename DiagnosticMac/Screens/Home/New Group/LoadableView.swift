//
//  LoadableView.swift
//  TableDemo
//
//  Created by Gabriel Theodoropoulos.
//  Copyright © 2019 Appcoda. All rights reserved.
//

import Cocoa

protocol LoadableView: class {
    var mainView: NSView? { get set }
    func load(fromNIBNamed nibName: String) -> Bool
}


extension LoadableView where Self: NSView {
    func load(fromNIBNamed nibName: String) -> Bool {
        var nibObjects: NSArray?
        let nibName = NSNib.Name(stringLiteral: nibName)
        
        if Bundle.main.loadNibNamed(nibName, owner: self, topLevelObjects: &nibObjects) {
            guard let nibObjects = nibObjects else { return false }
            
            let viewObjects = nibObjects.filter { $0 is NSView }
            
            if viewObjects.count > 0 {
                guard let view = viewObjects[0] as? NSView else { return false }
                mainView = view
                self.addSubview(mainView!)
                
                mainView?.translatesAutoresizingMaskIntoConstraints = false
                if #available(macOS 10.11, *) {
                    mainView?.leadingAnchor.constraint(equalTo: self.leadingAnchor).isActive = true
                } else {
                    // Fallback on earlier versions
                }
                if #available(macOS 10.11, *) {
                    mainView?.trailingAnchor.constraint(equalTo: self.trailingAnchor).isActive = true
                } else {
                    // Fallback on earlier versions
                }
                if #available(macOS 10.11, *) {
                    mainView?.topAnchor.constraint(equalTo: self.topAnchor).isActive = true
                } else {
                    // Fallback on earlier versions
                }
                if #available(macOS 10.11, *) {
                    mainView?.bottomAnchor.constraint(equalTo: self.bottomAnchor).isActive = true
                } else {
                    // Fallback on earlier versions
                }
                
                return true
            }
        }
        
        return false
    }
    
}
