//
//  DeviceInfoCell.swift
//  NexaGlobal macOS
//
//  Created by MacOS on 28/12/22.
//

import Cocoa

class DeviceInfoCell: NSCollectionViewItem {

    @IBOutlet weak var labelDeviceInfoTitleOutlet: NSTextField!
    @IBOutlet weak var labelHardwareTitleOutlet: NSTextField!
    
    @IBOutlet weak var labelModelTitleOutlet: NSTextField!
    @IBOutlet weak var labelModelOutlet: NSTextField!

    
    @IBOutlet weak var labelYearTitleOutlet: NSTextField!
    @IBOutlet weak var labelYearOutlet: NSTextField!

    
    @IBOutlet weak var labelCPUTitleOutlet: NSTextField!
    @IBOutlet weak var labelCPUOutlet: NSTextField!
    
    @IBOutlet weak var labelGPUTitleOutlet: NSTextField!
    @IBOutlet weak var labelGPUOutlet: NSTextField!

    @IBOutlet weak var labelMemoryTitleOutlet: NSTextField!
    @IBOutlet weak var labelMemoryOutlet: NSTextField!

    @IBOutlet weak var labelStorageTitleOutlet: NSTextField!
    @IBOutlet weak var labelStorageOutlet: NSTextField!
    
    
    @IBOutlet weak var labelTestTypeOutlet: NSTextField!
    @IBOutlet weak var labelTitleOutlet: NSTextField!
    @IBOutlet weak var labelOutlet: NSTextField!

    
    @IBOutlet weak var labelSoftwareTitleOutlet: NSTextField!


    @IBOutlet weak var labelOperatingSystemTitleOutlet: NSTextField!
    @IBOutlet weak var labelOperatingSystemOutlet: NSTextField!

    @IBOutlet weak var labelVersionTitleOutlet: NSTextField!
    @IBOutlet weak var labelVersionOutlet: NSTextField!

    
    @IBOutlet weak var labelBuildTitleOutlet: NSTextField!
    @IBOutlet weak var labelBuildOutlet: NSTextField!

    @IBOutlet weak var labelUpdatesTitleOutlet: NSTextField!
    @IBOutlet weak var labelUpdatesOutlet: NSTextField!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do view setup here.
    }
    
}
