//
//  AppDelegate.swift
//  calendar
//
//  Created by thierryH24 on 12/10/2017.
//  Copyright © 2017 thierryH24. All rights reserved.
//

import Cocoa

class THWeekItem: NSCollectionViewItem {
    @IBOutlet weak var labelBackgroundOutlet: NSTextField!
    
    @IBOutlet weak var imageViewBackgroundOutlet: NSImageView!
    @IBOutlet weak var buttonOutlet: NSButton!
    @IBOutlet weak var labelSubtitleOutlet: NSTextField!
    @IBOutlet weak var labelTitleOutlet: NSTextField!
    @IBOutlet weak var imageViewOutlet: NSImageView!
    @IBOutlet weak var labelImageViewBackgroundOutlet: NSTextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do view setup here.
    }
    
    func configure(week: String) {
        labelBackgroundOutlet.stringValue = String(week.prefix(1)).uppercased() + String(week.dropFirst())
//        weekField.textColor = THCalendarView.globalPreferences.calendar.textColor
//        weekField.alignment = .right
    }


}
