//
//  ViewController.swift
//  DiagnosticMac
//
//  Created by MacOS on 04/03/22.
//

import Cocoa
import Alamofire
//import JSNavigationController

//private extension Selector {
//    static let pushToNextViewController = #selector(ViewController.pushToNextViewController)
//    static let popViewController = #selector(ViewController.popViewController)
//}

class ViewController: NSViewController {

    //MARK: - Outlets
    @IBOutlet weak var labelPasswordTitleOutlet: NSTextField!
    @IBOutlet weak var labelEmailTitleOutlet: NSTextField!
    @IBOutlet weak var buttonLoginOutlet: NSButton!
    @IBOutlet weak var buttonSignUpOutlet: NSButton!
    @IBOutlet weak var LabelOrTitle: NSTextField!


    @IBOutlet weak var viewOutlet: NSView!
    @IBOutlet weak var LabelHeaderTitle: NSTextField!
    @IBOutlet weak var img: NSImageView!
    @IBOutlet weak var activity: NSProgressIndicator!
    @IBOutlet weak var textFieldPasswordOutlet: NSSecureTextField!
    @IBOutlet weak var textFieldEmailOutlet: NSTextField!

    
    //MARK: - SignUp Outlets
    @IBOutlet weak var viewSignUpOutlet: NSView!
    @IBOutlet weak var textFieldSignUpEmailOutlet: NSTextField!
    @IBOutlet weak var LabelSignUpEmailTitleOutlet: NSTextField!
    @IBOutlet weak var labelSignUpPasswordTitleOutlet: NSTextField!
    @IBOutlet weak var labelSignUpConfirmPasswordTitleOutlet: NSTextField!
    @IBOutlet weak var buttonSignUpLoginOutlet: NSButton!
    @IBOutlet weak var buttonSignUpViewOutlet: NSButton!
    @IBOutlet weak var LabelSignUpOrTitle: NSTextField!
    @IBOutlet weak var imgView: NSImageView!
    @IBOutlet weak var textFieldSignUpPasswordOutlet: NSSecureTextField!
    @IBOutlet weak var textFieldSignUpConfirmPasswordOutlet: NSSecureTextField!
    @IBOutlet weak var textFieldSignUpAddressOutlet: NSTextField!
    @IBOutlet weak var LabelSignUpAddressTitle: NSTextField!
    @IBOutlet weak var scrollViewOutlet: NSScrollView!
    
    
    
    //    open weak var navigationController: JSNavigationController?

    //MARK: - Controller life cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        
        TheApp.mimimized.toggle()
        self.scrollViewOutlet.isHidden = true
        self.viewOutlet.isHidden = false
        self.activity.isHidden = true
        viewOutlet.wantsLayer = true
        viewOutlet.layer?.borderWidth = 0.0
        self.LabelOrTitle.isHidden = true
        self.buttonSignUpOutlet.isHidden = true
        //        viewOutlet.layer?.borderColor = NSColor(hex: "F4F5F9").cgColor
        viewOutlet.layer?.backgroundColor = NSColor(hex: "F4F5F9").cgColor
        viewOutlet.layer?.cornerRadius = 12
        buttonLoginOutlet.bezelStyle = .texturedSquare
        buttonLoginOutlet.isBordered = false //Important
        buttonLoginOutlet.wantsLayer = true
        buttonLoginOutlet.bezelStyle = .texturedSquare
        buttonLoginOutlet.isBordered = false //Important
        buttonLoginOutlet.wantsLayer = true
        buttonLoginOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
        buttonLoginOutlet.layer?.cornerRadius = 6
        
        buttonSignUpOutlet.bezelStyle = .texturedSquare
        buttonSignUpOutlet.isBordered = false //Important
        buttonSignUpOutlet.wantsLayer = true
        buttonSignUpOutlet.bezelStyle = .texturedSquare
        buttonSignUpOutlet.isBordered = false //Important
        buttonSignUpOutlet.wantsLayer = true
        buttonSignUpOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
        buttonSignUpOutlet.layer?.cornerRadius = 6
        
        
        textFieldEmailOutlet.focusRingType =  .none
        textFieldPasswordOutlet.focusRingType =  .none
        
        
        
        scrollViewOutlet.wantsLayer = true
        scrollViewOutlet.layer?.borderWidth = 0.0
        //        viewOutlet.layer?.borderColor = NSColor(hex: "F4F5F9").cgColor
        scrollViewOutlet.layer?.backgroundColor = NSColor(hex: "F4F5F9").cgColor
        scrollViewOutlet.layer?.cornerRadius = 0
        
        
        
        viewSignUpOutlet.wantsLayer = true
        viewSignUpOutlet.layer?.borderWidth = 0.0
        //        viewOutlet.layer?.borderColor = NSColor(hex: "F4F5F9").cgColor
        viewSignUpOutlet.layer?.backgroundColor = NSColor(hex: "F4F5F9").cgColor
        viewSignUpOutlet.layer?.cornerRadius = 12
        buttonSignUpLoginOutlet.bezelStyle = .texturedSquare
        buttonSignUpLoginOutlet.isBordered = false //Important
        buttonSignUpLoginOutlet.wantsLayer = true
        buttonSignUpLoginOutlet.bezelStyle = .texturedSquare
        buttonSignUpLoginOutlet.isBordered = false //Important
        buttonSignUpLoginOutlet.wantsLayer = true
        buttonSignUpLoginOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
        buttonSignUpLoginOutlet.layer?.cornerRadius = 6
        buttonSignUpViewOutlet.bezelStyle = .texturedSquare
        buttonSignUpViewOutlet.isBordered = false //Important
        buttonSignUpViewOutlet.wantsLayer = true
        buttonSignUpViewOutlet.bezelStyle = .texturedSquare
        buttonSignUpViewOutlet.isBordered = false //Important
        buttonSignUpViewOutlet.wantsLayer = true
        buttonSignUpViewOutlet.layer?.backgroundColor = NSColor(hex: SERVER.darkBlue).cgColor
        buttonSignUpViewOutlet.layer?.cornerRadius = 6
        textFieldSignUpEmailOutlet.focusRingType =  .none
        textFieldSignUpAddressOutlet.focusRingType =  .none
        textFieldSignUpPasswordOutlet.focusRingType =  .none
        textFieldSignUpConfirmPasswordOutlet.focusRingType =  .none
        
        
        
        if #available(macOS 10.14, *) {
            buttonLoginOutlet.contentTintColor = NSColor.white
            buttonSignUpLoginOutlet.contentTintColor = NSColor.white
            buttonSignUpViewOutlet.contentTintColor = NSColor.white
            buttonSignUpOutlet.contentTintColor = NSColor.white
        } else {
            // Fallback on earlier versions
        }
        
        
        
        
        if AppLanguage.current == .english{
            self.labelEmailTitleOutlet.stringValue = "Email"
            self.textFieldEmailOutlet.placeholderString = "Email"
            self.labelPasswordTitleOutlet.stringValue = "Password"
            self.textFieldPasswordOutlet.placeholderString = "Password"
            self.buttonLoginOutlet.title = "Login"
            self.buttonSignUpLoginOutlet.title = "Login"
            self.LabelSignUpEmailTitleOutlet.stringValue = "Email"
            self.textFieldSignUpEmailOutlet.placeholderString = "Email"
            self.labelSignUpPasswordTitleOutlet.stringValue = "Password"
            self.textFieldSignUpPasswordOutlet.placeholderString = "Password"
            
            self.LabelOrTitle.stringValue = "or"
            self.buttonSignUpViewOutlet.title = "Sign Up"
            self.LabelSignUpAddressTitle.stringValue = "Address"
            self.textFieldSignUpAddressOutlet.placeholderString = "Address"
            self.labelSignUpConfirmPasswordTitleOutlet.stringValue = "Confirm password"
            self.textFieldSignUpConfirmPasswordOutlet.placeholderString = "Confirm password"
            self.LabelSignUpOrTitle.stringValue = "or"
            self.buttonSignUpOutlet.title = "Sign Up"
            
            
            //            self.label
            
            
        }else if AppLanguage.current == .arabic{
            self.labelEmailTitleOutlet.stringValue = "البريد الإلكتروني"
            self.textFieldEmailOutlet.placeholderString = "البريد الإلكتروني"
            self.labelPasswordTitleOutlet.stringValue = "كلمه السر"
            self.textFieldPasswordOutlet.placeholderString = "كلمه السر"
            self.buttonLoginOutlet.title = "تسجيل الدخول"
            self.buttonSignUpLoginOutlet.title = "تسجيل الدخول"
            self.LabelSignUpEmailTitleOutlet.stringValue = "البريد الإلكتروني"
            self.textFieldSignUpEmailOutlet.placeholderString = "البريد الإلكتروني"
            self.labelSignUpPasswordTitleOutlet.stringValue = "كلمه السر"
            self.textFieldSignUpPasswordOutlet.placeholderString = "كلمه السر"
            
            
            self.LabelOrTitle.stringValue = "أو"
            self.buttonSignUpViewOutlet.title = "اشتراك"
            
            
            self.LabelSignUpOrTitle.stringValue = "أو"
            self.buttonSignUpOutlet.title = "اشتراك"
            
            
            self.LabelSignUpAddressTitle.stringValue = "تبوك"
            self.textFieldSignUpAddressOutlet.placeholderString = "تبوك"
            self.labelSignUpConfirmPasswordTitleOutlet.stringValue = "تأكيد كلمة المرور"
            self.textFieldSignUpConfirmPasswordOutlet.placeholderString = "تأكيد كلمة المرور"
            
        }else if AppLanguage.current == .spanish{
            self.labelEmailTitleOutlet.stringValue = "Email"
            self.textFieldEmailOutlet.placeholderString = "Email"
            self.labelPasswordTitleOutlet.stringValue = "Clave"
            self.textFieldPasswordOutlet.placeholderString = "Clave"
            self.buttonLoginOutlet.title = "Acceso"
            self.buttonSignUpLoginOutlet.title = "Acceso"
            
            
            self.LabelSignUpEmailTitleOutlet.stringValue = "Email"
            self.textFieldSignUpEmailOutlet.placeholderString = "Email"
            self.labelSignUpPasswordTitleOutlet.stringValue = "Clave"
            self.textFieldSignUpPasswordOutlet.placeholderString = "Clave"
            
            
            self.LabelOrTitle.stringValue = "o"
            self.buttonSignUpViewOutlet.title = "Inscribirse"
            
            
            self.LabelSignUpOrTitle.stringValue = "o"
            self.buttonSignUpOutlet.title = "Inscribirse"
            
            self.LabelSignUpAddressTitle.stringValue = "Dirección"
            self.textFieldSignUpAddressOutlet.placeholderString = "Dirección"
            self.labelSignUpConfirmPasswordTitleOutlet.stringValue = "Confirmar contraseña"
            self.textFieldSignUpConfirmPasswordOutlet.placeholderString = "Confirmar contraseña"
        }else if AppLanguage.current == .portuguese{
            self.labelEmailTitleOutlet.stringValue = "o email"
            self.textFieldEmailOutlet.placeholderString = "o email"
            self.labelPasswordTitleOutlet.stringValue = "senha"
            self.textFieldPasswordOutlet.placeholderString = "senha"
            self.buttonLoginOutlet.title = "Conecte-se"
            self.buttonSignUpLoginOutlet.title = "Conecte-se"
            
            
            self.textFieldSignUpEmailOutlet.stringValue = "o email"
            self.textFieldSignUpEmailOutlet.placeholderString = "o email"
            self.labelSignUpPasswordTitleOutlet.stringValue = "senha"
            self.textFieldSignUpPasswordOutlet.placeholderString = "senha"
            
            self.LabelOrTitle.stringValue = "ou"
            self.buttonSignUpViewOutlet.title = "Inscrever-se"
            
            self.LabelSignUpOrTitle.stringValue = "ou"
            self.buttonSignUpOutlet.title = "Inscrever-se"
            
            self.LabelSignUpAddressTitle.stringValue = "Endereço"
            self.textFieldSignUpAddressOutlet.placeholderString = "Endereço"
            self.labelSignUpConfirmPasswordTitleOutlet.stringValue = "Confirme a Senha"
            self.textFieldSignUpConfirmPasswordOutlet.placeholderString = "Confirme a Senha"
            
        }
        
        if #available(macOS 10.14, *) {
            buttonLoginOutlet.contentTintColor = NSColor.white
        } else {
            // Fallback on earlier versions
        }
        
    }
    override func viewDidAppear() {
        self.view.window?.toggleFullScreen(self)
        self.view.window?.standardWindowButton(.zoomButton)?.isEnabled = false
        self.view.window?.styleMask.remove(NSWindow.StyleMask.resizable)

//        let presOptions: NSApplication.PresentationOptions = ([.fullScreen])
//        /*These are all of the options for NSApplicationPresentationOptions
//         .Default
//         .AutoHideDock              |   /
//         .AutoHideMenuBar           |   /
//         .DisableForceQuit          |   /
//         .DisableMenuBarTransparency|   /
//         .FullScreen                |   /
//         .HideDock                  |   /
//         .HideMenuBar               |   /
//         .DisableAppleMenu          |   /
//         .DisableProcessSwitching   |   /
//         .DisableSessionTermination |   /
//         .DisableHideApplication    |   /
//         .AutoHideToolbar
//         .HideMenuBar               |   /
//         .DisableAppleMenu          |   /
//         .DisableProcessSwitching   |   /
//         .DisableSessionTermination |   /
//         .DisableHideApplication    |   /
//         .AutoHideToolbar */
//
//        let optionsDictionary = [NSView.FullScreenModeOptionKey.fullScreenModeApplicationPresentationOptions :
//                                    UInt64(presOptions.rawValue)]
//
//        self.view.enterFullScreenMode(NSScreen.main!, withOptions:optionsDictionary)
//
//        self.view.wantsLayer = true
        
    }
    override var representedObject: Any? {
        didSet {
        // Update the view, if already loaded.
        }
    }
    
    @IBAction func buttonSignUpAction(_ sender: Any) {
        self.scrollViewOutlet.isHidden = false
        self.viewOutlet.isHidden = true
        }
    
    @IBAction func buttonLoginAction(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            if self.textFieldEmailOutlet.stringValue == ""{
                if AppLanguage.current == .english{
                    self.confirmAbletonIsReady(question: "Error", text: "Please Enter Email.")
                }else if AppLanguage.current == .arabic{
                    self.confirmAbletonIsReady(question: "خطأ", text: "الرجاء إدخال البريد الإلكتروني.")
                }else if AppLanguage.current == .spanish{
                    self.confirmAbletonIsReady(question: "Error", text: "Ingrese el correo electrónico.")
                }else if AppLanguage.current == .portuguese{
                    self.confirmAbletonIsReady(question: "Erro", text: "Por favor, digite o e-mail.")
                }
            }else if isValidEmail(self.textFieldEmailOutlet.stringValue) == false{
                if AppLanguage.current == .english{
                    self.confirmAbletonIsReady(question: "Error", text: "Please Enter Valid Email.")
                }else if AppLanguage.current == .arabic{
                    self.confirmAbletonIsReady(question: "خطأ", text: "الرجاء إدخال بريد إلكتروني صحيح.")
                }else if AppLanguage.current == .spanish{
                    self.confirmAbletonIsReady(question: "Error", text: "Por favor introduzca un correo electrónico válido.")
                }else if AppLanguage.current == .portuguese{
                    self.confirmAbletonIsReady(question: "Erro", text: "Por favor, insira um e-mail válido.")
                }
            }else if self.textFieldPasswordOutlet.stringValue == ""{
                if AppLanguage.current == .english{
                    self.confirmAbletonIsReady(question: "Error", text: "Please Enter Password.")
                }else if AppLanguage.current == .arabic{
                    self.confirmAbletonIsReady(question: "خطأ", text: "الرجاء إدخال كلمة المرور.")
                }else if AppLanguage.current == .spanish{
                    self.confirmAbletonIsReady(question: "Error", text: "Por favor, ingrese contraseña.")
                }else if AppLanguage.current == .portuguese{
                    self.confirmAbletonIsReady(question: "Erro", text: "Por favor, digite a senha.")
                }
            }
//            else if isValidated(self.textFieldSignUpPasswordOutlet.stringValue) == false{
//                if AppLanguage.current == .english{
//                    self.confirmAbletonIsReady(question: "Error", text: "Must contain at least one number & one uppercase & special characters & lowercase letter, & min. 8 characters")
//                }else if AppLanguage.current == .arabic{
//                    self.confirmAbletonIsReady(question: "خطأ", text: "يجب أن يحتوي على رقم واحد على الأقل وأحرف كبيرة وأحرف خاصة وحرف صغير ، & 8 أحرف على الأقل")
//                }else if AppLanguage.current == .spanish{
//                    self.confirmAbletonIsReady(question: "Error", text: "Debe contener al menos un número, una mayúscula y caracteres especiales, una letra minúscula y un mínimo de 8 caracteres")
//                }else if AppLanguage.current == .portuguese{
//                    self.confirmAbletonIsReady(question: "Erro", text: "Deve conter pelo menos um número e uma maiúscula e caracteres especiais e letra minúscula e no mínimo 8 caracteres")
//                }
//            }
            else{
                self.CallLoginAPI()
            }
        }
        else{
            if AppLanguage.current == .english{
                self.confirmAbletonIsReady(question: "Error", text: "Please Check Your Internet Connection Or Wifi")
            }else if AppLanguage.current == .arabic{
                self.confirmAbletonIsReady(question: "خطأ", text: "يرجى التحقق من اتصالك بالإنترنت أو شبكة Wifi")
            }else if AppLanguage.current == .spanish{
                self.confirmAbletonIsReady(question: "Error", text: "Por favor, compruebe su conexión a Internet o Wifi")
            }else if AppLanguage.current == .portuguese{
                self.confirmAbletonIsReady(question: "Erro", text: "Por favor, verifique sua conexão com a Internet ou Wi-Fi")
            }
        }
    }
    
    
    
    
    //MARK: - Sign Up Button Action
    
    @IBAction func buttonSignUpViewAction(_ sender: Any) {
        if Reachability.isConnectedToNetwork(){
            if self.textFieldSignUpEmailOutlet.stringValue == ""{
                if AppLanguage.current == .english{
                    self.confirmAbletonIsReady(question: "Error", text: "Please Enter Email.")
                }else if AppLanguage.current == .arabic{
                    self.confirmAbletonIsReady(question: "خطأ", text: "الرجاء إدخال البريد الإلكتروني.")
                }else if AppLanguage.current == .spanish{
                    self.confirmAbletonIsReady(question: "Error", text: "Ingrese el correo electrónico.")
                }else if AppLanguage.current == .portuguese{
                    self.confirmAbletonIsReady(question: "Erro", text: "Por favor, digite o e-mail.")
                }
            }else if isValidEmail(self.textFieldSignUpEmailOutlet.stringValue) == false{
                if AppLanguage.current == .english{
                    self.confirmAbletonIsReady(question: "Error", text: "Please Enter Valid Email.")
                }else if AppLanguage.current == .arabic{
                    self.confirmAbletonIsReady(question: "خطأ", text: "الرجاء إدخال بريد إلكتروني صحيح.")
                }else if AppLanguage.current == .spanish{
                    self.confirmAbletonIsReady(question: "Error", text: "Por favor introduzca un correo electrónico válido.")
                }else if AppLanguage.current == .portuguese{
                    self.confirmAbletonIsReady(question: "Erro", text: "Por favor, insira um e-mail válido.")
                }
            }else if self.textFieldSignUpAddressOutlet.stringValue == ""{
                if AppLanguage.current == .english{
                    self.confirmAbletonIsReady(question: "Error", text: "Please Enter Address.")
                }else if AppLanguage.current == .arabic{
                    self.confirmAbletonIsReady(question: "خطأ", text: "الرجاء إدخال العنوان.")
                }else if AppLanguage.current == .spanish{
                    self.confirmAbletonIsReady(question: "Error", text: "Por favor ingrese la dirección")
                }else if AppLanguage.current == .portuguese{
                    self.confirmAbletonIsReady(question: "Erro", text: "Por favor, digite o endereço.")
                }
            }else if self.textFieldSignUpPasswordOutlet.stringValue == ""{
                if AppLanguage.current == .english{
                    self.confirmAbletonIsReady(question: "Error", text: "Please Enter Password.")
                }else if AppLanguage.current == .arabic{
                    self.confirmAbletonIsReady(question: "خطأ", text: "الرجاء إدخال كلمة المرور.")
                }else if AppLanguage.current == .spanish{
                    self.confirmAbletonIsReady(question: "Error", text: "Por favor, ingrese contraseña.")
                }else if AppLanguage.current == .portuguese{
                    self.confirmAbletonIsReady(question: "Erro", text: "Por favor, digite a senha.")
                }
            }else if isValidated(self.textFieldSignUpPasswordOutlet.stringValue) == false{
                if AppLanguage.current == .english{
                    self.confirmAbletonIsReady(question: "Error", text: "Must contain at least one number & one uppercase & special characters & lowercase letter, & min. 8 characters")
                }else if AppLanguage.current == .arabic{
                    self.confirmAbletonIsReady(question: "خطأ", text: "يجب أن يحتوي على رقم واحد على الأقل وأحرف كبيرة وأحرف خاصة وحرف صغير ، & 8 أحرف على الأقل")
                }else if AppLanguage.current == .spanish{
                    self.confirmAbletonIsReady(question: "Error", text: "Debe contener al menos un número, una mayúscula y caracteres especiales, una letra minúscula y un mínimo de 8 caracteres")
                }else if AppLanguage.current == .portuguese{
                    self.confirmAbletonIsReady(question: "Erro", text: "Deve conter pelo menos um número e uma maiúscula e caracteres especiais e letra minúscula e no mínimo 8 caracteres")
                }
            }else if self.textFieldSignUpConfirmPasswordOutlet.stringValue == ""{
                if AppLanguage.current == .english{
                    self.confirmAbletonIsReady(question: "Error", text: "Please Enter Confirm Password.")
                }else if AppLanguage.current == .arabic{
                    self.confirmAbletonIsReady(question: "خطأ", text: "الرجاء إدخال تأكيد كلمة المرور.")
                }else if AppLanguage.current == .spanish{
                    self.confirmAbletonIsReady(question: "Error", text: "Por favor ingrese Confirmar contraseña")
                }else if AppLanguage.current == .portuguese{
                    self.confirmAbletonIsReady(question: "Erro", text: "Por favor, confirme a senha.")
                }
            }else if isValidated(self.textFieldSignUpConfirmPasswordOutlet.stringValue) == false{
                if AppLanguage.current == .english{
                    self.confirmAbletonIsReady(question: "Error", text: "Must contain at least one number & one uppercase & special characters & lowercase letter, & min. 8 characters")
                }else if AppLanguage.current == .arabic{
                    self.confirmAbletonIsReady(question: "خطأ", text: "يجب أن يحتوي على رقم واحد على الأقل وأحرف كبيرة وأحرف خاصة وحرف صغير ، & 8 أحرف على الأقل")
                }else if AppLanguage.current == .spanish{
                    self.confirmAbletonIsReady(question: "Error", text: "Debe contener al menos un número, una mayúscula y caracteres especiales, una letra minúscula y un mínimo de 8 caracteres")
                }else if AppLanguage.current == .portuguese{
                    self.confirmAbletonIsReady(question: "Erro", text: "Deve conter pelo menos um número e uma maiúscula e caracteres especiais e letra minúscula e no mínimo 8 caracteres")
                }
            }else if (self.textFieldSignUpConfirmPasswordOutlet.stringValue) != (self.textFieldSignUpPasswordOutlet.stringValue){
                if AppLanguage.current == .english{
                    self.confirmAbletonIsReady(question: "Error", text: "Password and confirmation password do not match.")
                }else if AppLanguage.current == .arabic{
                    self.confirmAbletonIsReady(question: "خطأ", text: "كلمة المرور وتأكيد كلمة المرور غير متطابقين.")
                }else if AppLanguage.current == .spanish{
                    self.confirmAbletonIsReady(question: "Error", text: "La contraseña y la contraseña de confirmación no coinciden")
                }else if AppLanguage.current == .portuguese{
                    self.confirmAbletonIsReady(question: "Erro", text: "Senha e senha de confirmação não coincidem.")
                }
            }
            else{
                self.CallSignUpAPI()
            }
        }else{
            if AppLanguage.current == .english{
                self.confirmAbletonIsReady(question: "Error", text: "Please Check Your Internet Connection Or Wifi")
            }else if AppLanguage.current == .arabic{
                self.confirmAbletonIsReady(question: "خطأ", text: "يرجى التحقق من اتصالك بالإنترنت أو شبكة Wifi")
            }else if AppLanguage.current == .spanish{
                self.confirmAbletonIsReady(question: "Error", text: "Por favor, compruebe su conexión a Internet o Wifi")
            }else if AppLanguage.current == .portuguese{
                self.confirmAbletonIsReady(question: "Erro", text: "Por favor, verifique sua conexão com a Internet ou Wi-Fi")
            }
        }
    }
    
    @IBAction func buttonSignUpLoginAction(_ sender: Any) {
        self.scrollViewOutlet.isHidden = true
        self.viewOutlet.isHidden = false

    }

}
extension ViewController{
    
    
    func CallLoginAPI() {
        self.activity.isHidden = false
        activity.isIndeterminate = true
        activity.style = .spinning
        activity.startAnimation(nil)
        let url = SERVER.login
        let param = ["userName":self.textFieldEmailOutlet.stringValue ,"password":self.textFieldPasswordOutlet.stringValue,"macAddress":getMacSerialNumber(),"serialNumber":getMacSerialNumber()] as [String : Any]
        print("url--------->>>>>>>>>",url)
        print("param--------->>>>>>>>>",param)
        NetworkManager.shared.callApi(url, param , .post) { (err, res) in
            print("--------->>>>>>>>>",res)
            self.activity.isHidden = true
            self.activity.stopAnimation(nil)
            if res?["Status"] as? Int == 1{
                self.validBarCodeAPI(res: res ?? [:])
            }else{
                if AppLanguage.current == .english{
                    self.confirmAbletonIsReady(question: "Error", text: res?["Msg"] as? String ?? "")
                }else if AppLanguage.current == .arabic{
                    self.confirmAbletonIsReady(question: "خطأ", text: res?["Msg"] as? String ?? "")
                }else if AppLanguage.current == .spanish{
                    self.confirmAbletonIsReady(question: "Error", text: res?["Msg"] as? String ?? "")
                }else if AppLanguage.current == .portuguese{
                    self.confirmAbletonIsReady(question: "Erro", text: res?["Msg"] as? String ?? "")
                }
            }
        }
    }
    
    func CallSignUpAPI() {
        self.activity.isHidden = false
        activity.isIndeterminate = true
        activity.style = .spinning
        activity.startAnimation(nil)
        let url = SERVER.ConsumerRegistration
        let param = ["email":textFieldSignUpEmailOutlet.stringValue, "password":textFieldSignUpPasswordOutlet.stringValue,"ConfirmPassword":textFieldSignUpConfirmPasswordOutlet.stringValue,"Title":"Consumer Test","Address":textFieldSignUpAddressOutlet.stringValue] as [String : Any]
        print("url--------->>>>>>>>>",url)

        print("param--------->>>>>>>>>",param)
        NetworkManager.shared.callApi(url, param , .post) { (err, res) in
            print("--------->>>>>>>>>",res)
            self.activity.isHidden = true
            self.activity.stopAnimation(nil)
            if res?["Status"] as? Int == 1{
//                self.validBarCodeAPI(res: res ?? [:])
                self.confirmAbletonIsReady(question: "", text: res?["Msg"] as? String ?? "")

            }else{
                if AppLanguage.current == .english{
                    self.confirmAbletonIsReady(question: "Error", text: res?["Msg"] as? String ?? "")
                }else if AppLanguage.current == .arabic{
                    self.confirmAbletonIsReady(question: "خطأ", text: res?["Msg"] as? String ?? "")
                }else if AppLanguage.current == .spanish{
                    self.confirmAbletonIsReady(question: "Error", text: res?["Msg"] as? String ?? "")
                }else if AppLanguage.current == .portuguese{
                    self.confirmAbletonIsReady(question: "Erro", text: res?["Msg"] as? String ?? "")
                }
            }
        }
    }
    
    func validBarCodeAPI(res:[String:Any]) {
        let url = SERVER.validBarCode
        let param = ["email":res["Email"] as? String ?? "" ,"macAddress":getMacSerialNumber(),"serialNumber":getMacSerialNumber()] as [String : Any]
        print("url-----------",url)

        print("-----------",param)
        NetworkManager.shared.callApi(url, param , .post) { (err, resp) in
            print("Res is ------>>>>>>>.",res)
            if resp?["Status"] as? Int == 1{
                DataStore.shared.email = res["Email"] as? String
                DataStore.shared.loginID = res["UserId"] as? String
                DataStore.shared.Role = res["Role"] as? String
                DataStore.shared.AccessToken = res["AccessToken"] as? String
                DataStore.shared.firstName = res["Name"] as? String
                DataStore.shared.isLogin = true
                
                //  Converted to Swift 5.6 by Swiftify v5.6.21858 - https://swiftify.com/
//                UserDefaults.standard.removeObject(forKey: "AppleTextDirection")
//                UserDefaults.standard.removeObject(forKey: "NSForceRightToLeftWritingDirection")
                if let controller = self.storyboard?.instantiateController(withIdentifier: "HomeVC") as? HomeVC {
//                    UserDefaults.standard.set("YES", forKey: "AppleTextDirection")
//                    UserDefaults.standard.set("YES", forKey: "NSForceRightToLeftWritingDirection")
                    self.view.window?.contentViewController = controller
                }
            }else{
                self.view.window?.close()
            }
        }
    }
    class func isDeviceLanguageRightToLeft() -> Bool {

        let currentLocale = NSLocale.current as NSLocale
        let direction: NSLocale.LanguageDirection = NSLocale.characterDirection(forLanguage: currentLocale.object(forKey: .languageCode) as? String ?? "")
        return direction == .rightToLeft
    }
    
//    class func isAppLanguageRightToLeft() -> Bool {
//
//        let direction = UIApplication.shared.userInterfaceLayoutDirection
//        return direction == UIUserInterfaceLayoutDirection.rightToLeft
//    }
    @objc func pushToNextViewController() {
//        if let destinationViewController = HomeVC() {
//            navigationController?.push(viewController: HomeVC(), animated: true)
//        }
    }

//    @objc func popViewController() {
//
//        navigationController?.popViewController(animated: true)
//    }
}
extension NSViewController{
    func confirmAbletonIsReady(question: String, text: String) {
        let alert = NSAlert()
        alert.messageText = question
        alert.informativeText = text
        alert.alertStyle = NSAlert.Style.informational
        if AppLanguage.current == .english{
            alert.addButton(withTitle: "OK")
        }else if AppLanguage.current == .arabic{
            alert.addButton(withTitle: "نعم")
        }else if AppLanguage.current == .spanish{
            alert.addButton(withTitle: "DE ACUERDO")
        }else if AppLanguage.current == .portuguese{
            alert.addButton(withTitle: "OK")
        }
        //        alert.addButton(withTitle: "Cancel")
        alert.runModal()
    }
}
func getMacSerialNumber() -> String {
    var serialNumber: String? {
        let platformExpert = IOServiceGetMatchingService(kIOMasterPortDefault, IOServiceMatching("IOPlatformExpertDevice") )
        
        guard platformExpert > 0 else {
            return nil
        }
        
        guard let serialNumber = (IORegistryEntryCreateCFProperty(platformExpert, kIOPlatformSerialNumberKey as CFString, kCFAllocatorDefault, 0).takeUnretainedValue() as? String)?.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines) else {
            return nil
        }
        
        IOObjectRelease(platformExpert)

        return serialNumber
    }
    
    return serialNumber ?? "Unknown"
}
extension NSViewController{
    func isValidEmail(_ email: String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailPred = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailPred.evaluate(with: email)
    }
    
    func isValidated(_ password: String) -> Bool {
        var lowerCaseLetter: Bool = false
        var upperCaseLetter: Bool = false
        var digit: Bool = false
        var specialCharacter: Bool = false
        
        if password.count  >= 8 {
            for char in password.unicodeScalars {
                if !lowerCaseLetter {
                    lowerCaseLetter = CharacterSet.lowercaseLetters.contains(char)
                }
                if !upperCaseLetter {
                    upperCaseLetter = CharacterSet.uppercaseLetters.contains(char)
                }
                if !digit {
                    digit = CharacterSet.decimalDigits.contains(char)
                }
                if !specialCharacter {
                    specialCharacter = CharacterSet.punctuationCharacters.contains(char)
                }
            }
            if specialCharacter || (digit && lowerCaseLetter && upperCaseLetter) {
                //do what u want
                return true
            }
            else {
                return false
            }
        }
        return false
    }
}
public class TheApp {
    static var maximized: Bool {
        get {
            guard let visibleFrame = NSScreen.main?.visibleFrame,
                  let window = NSApp.mainWindow
            else { return false }
            
            return window.frame == visibleFrame
        }
        set { NSApp.mainWindow?.zoom(newValue) }
    }
    
    static var fullscreen: Bool {
        get {
            guard let screenFrame = NSScreen.main?.frame,
                  let window = NSApp.mainWindow
            else { return false }
            
            return window.frame == screenFrame
        } set {
            NSApp.mainWindow?.toggleFullScreen(newValue)
        }
    }
    
    static var mimimized: Bool {
        get { NSApp.mainWindow?.isMiniaturized ?? false }
        set { NSApp?.mainWindow?.miniaturize(newValue) }
    }
}

enum VersionError: Error {
    case invalidBundleInfo, invalidResponse
}

class LookupResult: Decodable {
    var results: [AppInfo]
}

class AppInfo: Decodable {
    var version: String
    var trackViewUrl: String
    //let identifier = Bundle.main.infoDictionary?["CFBundleIdentifier"] as? String,
    // You can add many thing based on "http://itunes.apple.com/lookup?bundleId=\(identifier)"  response
    // here version and trackViewUrl are key of URL response
    // so you can add all key beased on your requirement.
    
}

class ArgAppUpdater: NSObject {
    private static var _instance: ArgAppUpdater?;
    
    private override init() {
        
    }
    
    public static func getSingleton() -> ArgAppUpdater {
        if (ArgAppUpdater._instance == nil) {
            ArgAppUpdater._instance = ArgAppUpdater.init();
        }
        return ArgAppUpdater._instance!;
    }
    
    private func getAppInfo(completion: @escaping (AppInfo?, Error?) -> Void) -> URLSessionDataTask? {
        guard let identifier = Bundle.main.infoDictionary?["CFBundleIdentifier"] as? String,
            let url = URL(string: "http://itunes.apple.com/lookup?bundleId=\(identifier)") else {
                DispatchQueue.main.async {
                    completion(nil, VersionError.invalidBundleInfo)
                }
                return nil
        }
        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            do {
                if let error = error { throw error }
                guard let data = data else { throw VersionError.invalidResponse }
                
                print("Data:::",data)
                print("response###",response!)
                
                let result = try JSONDecoder().decode(LookupResult.self, from: data)
                
                let dictionary = try? JSONSerialization.jsonObject(with: data, options: .mutableLeaves)
                
                print("dictionary",dictionary!)
                
                
                guard let info = result.results.first else { throw VersionError.invalidResponse }
                print("result:::",result)
                completion(info, nil)
            } catch {
                completion(nil, error)
            }
        }
        task.resume()
        
        print("task ******", task)
        return task
    }
    private  func checkVersion(force: Bool) {
        let info = Bundle.main.infoDictionary
        let currentVersion = info?["CFBundleShortVersionString"] as? String
        _ = getAppInfo { (info, error) in
            
            let appStoreAppVersion = info?.version
            print("appStoreAppVersion",appStoreAppVersion)
            print("currentVersion",currentVersion)
            print("info?.trackViewUrl",info?.trackViewUrl)
            if let error = error {
                print(error)
            }else if appStoreAppVersion!.compare(currentVersion!, options: .numeric) == .orderedDescending {
                DispatchQueue.main.async {
                    let bundleName = Bundle.main.infoDictionary!["CFBundleDisplayName"] as! String;
                    let a = NSAlert()
                    a.messageText = "Data Eraser"
                    a.informativeText = "\(bundleName) Version \((info?.version)!) is available on AppStore."
                    a.addButton(withTitle: "Update")
                    a.alertStyle = NSAlert.Style.warning
                    let response = a.runModal()
                    if response == .alertFirstButtonReturn {
                        NSWorkspace.shared.open(URL(string: info!.trackViewUrl)!)
                    }
                }
            }
        }
    }
    
    func showUpdateWithConfirmation() {
        checkVersion(force : false)
        
        
    }
    
    func showUpdateWithForce() {
        checkVersion(force : true)
    }
    
    
    
}
