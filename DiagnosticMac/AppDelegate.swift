//
//  AppDelegate.swift
//  DiagnosticMac
//
//  Created by MacOS on 04/03/22.
//

import Cocoa

@main
class AppDelegate: NSObject, NSApplicationDelegate {

    


    func applicationDidFinishLaunching(_ aNotification: Notification) {
        NSApp.appearance = NSAppearance(named: NSAppearance.Name.aqua)
        ArgAppUpdater.getSingleton().showUpdateWithForce()
        ChangeLayout()
    }

    func applicationWillTerminate(_ aNotification: Notification) {
        // Insert code here to tear down your application
    }

    func applicationSupportsSecureRestorableState(_ app: NSApplication) -> Bool {
        return true
    }


}

extension AppDelegate{
    func ChangeLayout(){
        print("APP ------------------>>>>>>>>>>>",AppLanguage.current)
        if AppLanguage.current == .arabic{
            UserDefaults.standard.set("YES", forKey: "AppleTextDirection")
            UserDefaults.standard.set("YES", forKey: "NSForceRightToLeftWritingDirection")
        }else if AppLanguage.current == .portuguese{
            UserDefaults.standard.removeObject(forKey: "AppleTextDirection")
            UserDefaults.standard.removeObject(forKey: "NSForceRightToLeftWritingDirection")
        }else if AppLanguage.current == .spanish{
            UserDefaults.standard.removeObject(forKey: "AppleTextDirection")
            UserDefaults.standard.removeObject(forKey: "NSForceRightToLeftWritingDirection")
        }else{
            UserDefaults.standard.removeObject(forKey: "AppleTextDirection")
            UserDefaults.standard.removeObject(forKey: "NSForceRightToLeftWritingDirection")
        }
    }
}
