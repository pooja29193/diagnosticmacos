//
//  SupportingFiles.swift
//  Diagnostic
//
//  Created by MacOS on 07/10/21.
//

import Foundation
import Alamofire
import SwiftyJSON
import SystemConfiguration

typealias ResponseCompletion = ((_ isSuccess: Bool, _ response: Any?) -> Void)

class DataStore{
    static let shared = DataStore()
    var userDefault:UserDefaults!
    private init(){
        userDefault = UserDefaults.standard
    }
    
    

    
    
    
    var ProfileStatus:String?{
        
        get{
            
            return userDefault.value(forKey: "status") as? String
            
        }
        
        set(newValue){
            
            userDefault.setValue(newValue, forKey: "status")
        }
        
    }
    var appLanguage:AppLanguage?{
        get{
            let rawStr = userDefault.value(forKey: "AppLaunguage") as? String
            return AppLanguage(rawValue: rawStr ?? "")
        }
        set(newValue){
            userDefault.setValue(newValue?.rawValue, forKey: "AppLaunguage")
        }
    }
    
    var email:String?{
        get{
            return userDefault.value(forKey:"email") as? String
        }
        set(newValue){
            userDefault.setValue(newValue, forKey:"email")
        }
    }
    
    var dob:String?{
        get{
            return userDefault.value(forKey:"dob") as? String
        }
        set(newValue){
            userDefault.setValue(newValue, forKey:"dob")
        }
    }
    
    
    var city:String?{
        get{
            return userDefault.value(forKey:"city") as? String
        }
        set(newValue){
            userDefault.setValue(newValue, forKey:"city")
        }
    }
    
    
    
    
    var state:String?{
        get{
            return userDefault.value(forKey: "state") as? String
        }
        set(newValue){
            userDefault.setValue(newValue, forKey: "state")
        }
    }
    
    
    var pincode:String?{
        get{
            return userDefault.value(forKey: "pincode") as? String
        }
        set(newValue){
            userDefault.setValue(newValue, forKey: "pincode")
        }
    }
    
    
    var AccessToken:String?{
        get{
            return userDefault.value(forKey: "AccessToken") as? String
        }
        set(newValue){
            userDefault.setValue(newValue, forKey: "AccessToken")
        }
    }
    

    
    
    var memid:String?{
        get{
            return userDefault.value(forKey: "fkMemID") as? String
        }
        set(newValue){
            userDefault.setValue(newValue, forKey: "fkMemID")
        }
    }
    
    var loginID:String?{
        get{
            return userDefault.value(forKey: "loginID") as? String
        }
        set(newValue){
            userDefault.setValue(newValue, forKey: "loginID")
        }
    }
    
    var Role:String?{
        get{
            return userDefault.value(forKey: "Role") as? String
        }
        set(newValue){
            userDefault.setValue(newValue, forKey: "Role")
        }
    }
    
    
    
    var firstName:String?{
        get{
            return userDefault.value(forKey: "firstName") as? String
        }
        set(newValue){
            userDefault.setValue(newValue, forKey: "firstName")
        }
    }
    var password:String?{
        get{
            return userDefault.value(forKey: "password") as? String
        }
        set(newValue){
            userDefault.setValue(newValue, forKey: "password")
        }
    }
    
    
    
    var MobileNO:String?{
        get{
            return userDefault.value(forKey: "mobile") as? String
        }
        set(newValue){
            userDefault.setValue(newValue, forKey: "mobile")
        }
    }
    var PostVC:Int?{
        get{
            return userDefault.value(forKey: "PostVC") as? Int
        }
        set(newValue){
            userDefault.setValue(newValue, forKey: "PostVC")
        }
    }
    
    var lastName:String?{
        get{
            return userDefault.value(forKey: "lastName") as? String
        }
        set(newValue){
            userDefault.setValue(newValue, forKey: "lastName")
        }
    }
    var suburb:String?{
        get{
            return userDefault.value(forKey: "suburb") as? String
        }
        set(newValue){
            userDefault.setValue(newValue, forKey: "suburb")
        }
    }
    
    
    
    var languageCode:String?{
        
        get{
            return userDefault.value(forKey:"languageCode") as? String
        }
        set(newvalue){
            userDefault.setValue(newvalue, forKey: "languageCode")
        }
    }
    
    
    
    var appOpenFirstTime:Bool{
        get{
            return ((userDefault.value(forKey: "appOpenFirstTime") as? Bool) ?? false)
        }
        set(newValue){
            userDefault.setValue(newValue, forKey: "appOpenFirstTime")
            
        }
    }
    var isLicenceAvailable:Bool{
        get{
            return ((userDefault.value(forKey: "isLicenceAvailable") as? Bool) ?? false)
        }
        set(newValue){
            userDefault.setValue(newValue, forKey: "isLicenceAvailable")
            
        }
    }
    var isWakeUp:Bool{
        get{
            return ((userDefault.value(forKey: "isWakeUp") as? Bool) ?? false)
        }
        set(newValue){
            userDefault.setValue(newValue, forKey: "isWakeUp")
            
        }
    }
    var isTimer:Bool{
        get{
            return ((userDefault.value(forKey: "isTimer") as? Bool) ?? false)
        }
        set(newValue){
            userDefault.setValue(newValue, forKey: "isTimer")
            
        }
    }
    
    var isLogin:Bool{
        get{
            return ((userDefault.value(forKey: "isLogin") as? Bool) ?? false)
        }
        set(newValue){
            userDefault.setValue(newValue, forKey: "isLogin")
            
        }
    }
    
    
    
    var profilePic:String?{
        get{
            return userDefault.value(forKey: "profilePic") as? String
        }
        set(newValue){
            userDefault.setValue(newValue, forKey: "profilePic")
            
        }
    }
    
    
    
    
    func resetDefaults() {
        let defaults = UserDefaults.standard
        let dictionary = defaults.dictionaryRepresentation()
        dictionary.keys.forEach { key in
            if key != "appOpenFirstTime"{
                defaults.removeObject(forKey: key)
            }
            
        }
    }
}
class DictionaryEncoder {
    
    private let encoder = JSONEncoder()
    
    var dateEncodingStrategy: JSONEncoder.DateEncodingStrategy {
        set { encoder.dateEncodingStrategy = newValue }
        get { return encoder.dateEncodingStrategy }
    }
    
    var dataEncodingStrategy: JSONEncoder.DataEncodingStrategy {
        set { encoder.dataEncodingStrategy = newValue }
        get { return encoder.dataEncodingStrategy }
    }
    
    var nonConformingFloatEncodingStrategy: JSONEncoder.NonConformingFloatEncodingStrategy {
        set { encoder.nonConformingFloatEncodingStrategy = newValue }
        get { return encoder.nonConformingFloatEncodingStrategy }
    }
    
    var keyEncodingStrategy: JSONEncoder.KeyEncodingStrategy {
        set { encoder.keyEncodingStrategy = newValue }
        get { return encoder.keyEncodingStrategy }
    }
    
    func encode<T>(_ value: T) throws -> [String: Any] where T : Encodable {
        let data = try encoder.encode(value)
        return try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! [String: Any]
    }
}

class DictionaryDecoder {
    
    private let decoder = JSONDecoder()
    
    var dateDecodingStrategy: JSONDecoder.DateDecodingStrategy {
        set { decoder.dateDecodingStrategy = newValue }
        get { return decoder.dateDecodingStrategy }
    }
    
    var dataDecodingStrategy: JSONDecoder.DataDecodingStrategy {
        set { decoder.dataDecodingStrategy = newValue }
        get { return decoder.dataDecodingStrategy }
    }
    
    var nonConformingFloatDecodingStrategy: JSONDecoder.NonConformingFloatDecodingStrategy {
        set { decoder.nonConformingFloatDecodingStrategy = newValue }
        get { return decoder.nonConformingFloatDecodingStrategy }
    }
    
    var keyDecodingStrategy: JSONDecoder.KeyDecodingStrategy {
        set { decoder.keyDecodingStrategy = newValue }
        get { return decoder.keyDecodingStrategy }
    }
    
    func decode<T:Decodable>(_ type: T.Type, from dictionary: [String: Any]) throws -> T  {
        let data = try JSONSerialization.data(withJSONObject: dictionary, options: [.prettyPrinted])
        return try decoder.decode(type, from: data)
    }
}

class NetworkManager: NSObject {
    static let shared = NetworkManager()
    private override init() {
        super.init()
    }
    
//    func requestGET(requestURL urlStr: String, completion: @escaping ResponseCompletion) {
//        Connection.checkInternet(showLoader: true) { (isConnected) in
//            if isConnected{
//                Alamofire.request(urlStr).response { response in
//                    do {
//                        if let jsonResult = try JSONSerialization.jsonObject(with: response.data!, options: []) as? NSDictionary {
//                            //                        // print(jsonResult)
//                            completion(true, jsonResult)
//                        }
//                    } catch let error as NSError {
//                        completion(false, error.localizedDescription)
//                    }
//                }
//                
//            }else{
//                completion(false,nil)
//            }
//        }
//    }
    
    private func checkInternet(completion:@escaping((_ isConnected:Bool)->Void)){
        let network  = NetworkReachabilityManager.init(host: "https://google.com")
        let isStart = network?.startListening()
        if isStart ?? false{
            network?.listener = {[weak network] status in
                if status != .notReachable && status != .unknown{
                    network?.stopListening()
                    completion(true)
                }else{
                    network?.stopListening()
                    completion(false)
                }
            }
        }else{
            network?.stopListening()
            completion(false)
        }
    }
    
    
    func headers() -> HTTPHeaders {
        var headers: HTTPHeaders = [
            "Content-Type": "application/json",
            "Accept": "application/json"
        ]
//        if let authToken = DataStore.shared.accessToken {
            headers["Authorization"] = "Bearer" + " " + "authToken"
//        }
        return headers
    }
    
    /* Call Api with parameter
     */
    func callApi(_ url:String?, _ parameter:Parameters?,_ method:HTTPMethod? ,completion:@escaping ((_ error:String?,_ resParam:[String:Any]?)->Void)){
        
        Alamofire.request(url ?? "", method: method ?? .post, parameters: parameter, encoding: JSONEncoding.default, headers: self.headers()).responseJSON { (response) in
            if response.result.isSuccess && response.response?.statusCode != 404 && response.response?.statusCode != 500 {
                if let res = response.result.value as? [String:Any]{
                    completion(nil,res)
                    print(">>>>>>>>>>>>>>>>>>",res)
                    
                }
            }else{
                print("Error")
            }
        }
        
    }
    
    func convertStringToDictionary(text: String) -> [String:AnyObject]? {
        if let data = text.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {
                print("Something went wrong")
            }
        }
        return nil
    }
    
//    MARK: ANKUR
    
    func requestWithSingleMultipart(mimeType:String,fileExtension:String,type:String,urlStr: String, imageData: Data,_ fileName:String, parameters: [String : Any], onCompletion: ((Any?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            multipartFormData.append(imageData, withName: "images", fileName: "images.jpg", mimeType: "image/jpeg")

        }, usingThreshold: UInt64.init(), to: urlStr, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseString { response in
                    if let err = response.error{
                        onError?(err)
                        return
                    }
                    let value = response.result.value
                    onCompletion?(value)
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }
    
//    ---------------------------------
    
    func requestWithMultipart(urlStr: String, imageData: [Data?],_ fileName:String, parameters: [String : Any], onCompletion: ((Any?) -> Void)? = nil, onError: ((Error?) -> Void)? = nil){
        let headers: HTTPHeaders = [
            "Content-type": "multipart/form-data"
        ]
        
        Alamofire.upload(multipartFormData: { (multipartFormData) in
            for (key, value) in parameters {
                multipartFormData.append("\(value)".data(using: String.Encoding.utf8)!, withName: key as String)
            }
            
            for i in 0..<imageData.count{
                if let data = imageData[i]{
                    multipartFormData.append(data, withName: "task_files[]", fileName: "\(NSDate().timeIntervalSince1970)" + ".png", mimeType: "image/png")
                    print("\(NSDate().timeIntervalSince1970)" + ".png")
                }
            }
        }, usingThreshold: UInt64.init(), to: urlStr, method: .post, headers: headers) { (result) in
            switch result{
            case .success(let upload, _, _):
                upload.responseString { response in
                    if let err = response.error{
                        onError?(err)
                        return
                    }
                    let value = response.result.value
                    onCompletion?(value)
                    
                }
            case .failure(let error):
                print("Error in upload: \(error.localizedDescription)")
                onError?(error)
            }
        }
    }
}



extension Encodable {
    var dictionary: [String: Any]? {
        guard let data = try? JSONEncoder().encode(self) else { return nil }
        return (try? JSONSerialization.jsonObject(with: data, options: .allowFragments)).flatMap { $0 as? [String: Any] }
    }
}

extension String{
    func toJSONArray() throws -> [[String: Any]?]? {
        let data = self.data(using: .utf8)!
        guard let jsonObject = try JSONSerialization.jsonObject(with: data, options: []) as? [AnyObject] else {
            throw NSError(domain: NSCocoaErrorDomain, code: 1, userInfo: [NSLocalizedDescriptionKey: "Invalid JSON"])
        }
        return jsonObject.map { $0 as? [String: Any]}
    }
    
    func slice(from: String, to: String) -> String? {
        
       return (range(of: from)?.upperBound).flatMap { substringFrom in
            (range(of: to, range: substringFrom..<endIndex)?.lowerBound).map { substringTo in
                String(self[substringFrom..<substringTo])
            }
        }
    }
}

extension String{
    func convertDateFormater() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = "dd/MM/YYYY"
        guard let d = date else { return ""}
        return  dateFormatter.string(from: d)
    }
    
    
    
    func convertDateFormaterFromToDownLine() -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/YYYY"
        let date = dateFormatter.date(from: self)
        dateFormatter.dateFormat = "MMM dd,YYYY"
        guard let d = date else { return ""}
        return  dateFormatter.string(from: d)
    }
    
    
    
}
extension String {
    func convertToDictionary() -> [String: Any]? {
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
}
extension String{
    func isValidEmail() -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: self)
    }
}

extension String{
    func isValidAadharCard()->Bool{
        let aadharRegex = "^[2-9]{1}[0-9]{11}$"
        let emailTest = NSPredicate(format:"SELF MATCHES %@", aadharRegex)
        return emailTest.evaluate(with: self)
    }
}
extension String{
    
}
extension String
{
    func maxLength(length n: Int)->String {
        if (self.count <= n) {
            return self
        }
        return String( Array(self).prefix(upTo: n) )
    }
    func minLength(length n: Int)->String {
        if (self.count >= n) {
            return self
        }
        return String( Array(self).prefix(upTo: n) )
    }
    
}
extension String{
    func isValidPAN() -> Bool{
        let regularExpression = "[A-Z]{5}[0-9]{4}[A-Z]{1}"
        let panCardValidation = NSPredicate(format : "SELF MATCHES %@", regularExpression)
        return panCardValidation.evaluate(with: self)
    }
}

extension String{
    func isValidIFSC() -> Bool{
        let regularExpression = "^[A-Z]{4}0[A-Z0-9]{6}$"
        let panCardValidation = NSPredicate(format : "SELF MATCHES %@", regularExpression)
        return panCardValidation.evaluate(with: self)
    }
}





public class Reachability {
    
    class func isConnectedToNetwork() -> Bool {
        var zeroAddress = sockaddr_in()
        
        zeroAddress.sin_len = UInt8(MemoryLayout.size(ofValue: zeroAddress))
        zeroAddress.sin_family = sa_family_t(AF_INET)
        let defaultRouteReachability = withUnsafePointer(to: &zeroAddress) {
            $0.withMemoryRebound(to: sockaddr.self, capacity: 1) {
                
                SCNetworkReachabilityCreateWithAddress(nil, $0)
                
            }
        }
        var flags = SCNetworkReachabilityFlags()
        if !SCNetworkReachabilityGetFlags(defaultRouteReachability!, &flags) {
            return false
        }
        let isReachable = (flags.rawValue & UInt32(kSCNetworkFlagsReachable)) != 0
        let needsConnection = (flags.rawValue & UInt32(kSCNetworkFlagsConnectionRequired)) != 0
        
        if !isReachable && !needsConnection {
           // ShowAlertwithMsg("The Internet connection appears to be offline.", title: KAppName)
            return (isReachable && needsConnection)
        }
        
        return (isReachable && !needsConnection)
    }
}
