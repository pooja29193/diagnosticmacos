//
//  MCLocalizationDummyDataSource.m
//  MCLocalization
//
//  Created by Baglan on 2/21/15.
//  Copyright (c) 2015 MobileCreators. All rights reserved.
//

#import "MCLocalizationDummyDataSource.h"

@implementation MCLocalizationDummyDataSource

- (NSArray *)supportedLanguages
{
    return @[@"Hindi"];
}

- (NSString *)defaultLanguage
{
    return @"Hindi";
}

- (NSDictionary *)stringsForLanguage:(NSString *)language
{
    return @{};
}

@end
