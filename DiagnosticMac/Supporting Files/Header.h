//
//  Header.h
//  Diagnostic
//
//  Created by MacOS on 02/05/22.
//

#ifndef Header_h
#define Header_h

#import "MCLocalization.h"
#import "LocalizationHelper.h"
#import "NSBundle+Language.h"
#import "LanguageManager.h"

#endif /* Header_h */
