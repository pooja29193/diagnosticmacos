//
//  LocalizationHelper.h
//  SmartCoolerInstallation
//
//  Created by Abhishek Sharma on 02/11/17.
//  Copyright © 2017 Insigma Inc. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface LocalizationHelper : NSObject

+ (instancetype)sharedInstance;
- (void) downloadAndSetLatestFile;
- (NSArray *)getSupportedLanguage : (NSString *)strJsonFilePath;

@end
