//
//  Constants.swift
//  Quick
//
//  Created by Sagar R on 15/08/18.
//  Copyright © 2018 Sagar R. All rights reserved.
//

import Foundation
//let APP_DELEGATE = Notification.shared.delegate! as! AppDelegate
enum LangNames: String {
    case Eng = "en"
    case Arab = "ar"
    case Spanish = "es"
    case Portuguese = "pt"
}
let languageConst = "languageConst"

func language() -> String{
    if let pre = UserDefaults.standard.object(forKey: languageConst) as? String{
        return pre
    }
    return ""
}

let DID_RECEIVED_PUSH_NOTIF = "DID_RECEIVED_PUSH_NOTIF"
let DID_RECEIVED_SILENT_PUSH = "DID_RECEIVED_SILENT_PUSH"





extension String {
    func localized(withComment comment: String? = nil) -> String {
//        return NSLocalizedString(self, tableName: "Localizable", bundle: .main, value: self, comment: self)
        return NSLocalizedString(self, comment: comment ?? "")
    }
    init?(htmlEncodedString: String) {
        
        guard let data = htmlEncodedString.data(using: .utf8) else {
            return nil
        }
        guard let attributedString = try? NSAttributedString(data: data, options: [
            NSAttributedString.DocumentReadingOptionKey(rawValue: NSAttributedString.DocumentAttributeKey.documentType.rawValue): NSAttributedString.DocumentType.html,
            NSAttributedString.DocumentReadingOptionKey(rawValue: NSAttributedString.DocumentAttributeKey.characterEncoding.rawValue): String.Encoding.utf8.rawValue
        ], documentAttributes: nil) else {
            return nil
        }
        self.init(attributedString.string)
    }
    func toDate( dateFormat format  : String) -> Date
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        // dateFormatter.timeZone = NSTimeZone(name: "UTC") as TimeZone?
        return dateFormatter.date(from: self) ?? Date()
    }
    
    func toDateWithDote( dateFormat format  : String) -> Date
    {
        let dateArr : [String] = self.components(separatedBy: ".")
        
        let date = dateArr[0]
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        // dateFormatter.timeZone = NSTimeZone(name: "Asia/Riyadh") as TimeZone?
        return dateFormatter.date(from: date) ?? Date()
        
    }
    
    func toDateWithDateC( dateFormat format  : String) -> Date
    {
        let dateArr : [String] = self.components(separatedBy: ".")
        
        let date = dateArr[0]
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        
        dateFormatter.timeZone = NSTimeZone(name: "Asia/Riyadh") as TimeZone?
        return dateFormatter.date(from: date) ?? Date()
    }
    
    
    
    func changeDateFormateWithDote (sourceDateFormat:String, destinationFormat:String) ->String {
        let dateArr : [String] = self.components(separatedBy: ".")
        let date = dateArr[0]
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = NSTimeZone(name: "Asia/Riyadh") as TimeZone?
        dateFormatter.locale =  Locale(identifier: "en_En")
        let dateObj = date.toDate(dateFormat: sourceDateFormat)
        dateFormatter.dateFormat = destinationFormat
        return (dateFormatter.string(from: dateObj ))
    }
    
}
