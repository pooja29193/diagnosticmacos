

import Foundation
import Alamofire

extension Decodable{
    static func callPOSTApi(_ parameter:[String:Any]?,_ url:String?,completion:@escaping ((_ message:String?,Self?)->Void)){
        
        NetworkManager.shared.callApi(url, parameter ?? [:], .post) { (errMsg, responseDict) in
          
            if errMsg != nil{
                completion(errMsg,nil)
            }else{
                if let res = responseDict{
                    do{
                        let responseModel = try DictionaryDecoder().decode(Self.self, from: res)
                        completion(errMsg,responseModel)
                    }catch(let errorDecode){
                        completion(errorDecode.localizedDescription,nil)
                    }
                }else{
                    completion("Invalid response",nil)
                }
            }
        }
    }
    
    
    static func callGETApi(_ url:String?,_ completion:@escaping ((_ message:String?,Self?)->Void)){
        NetworkManager.shared.callApi(url, nil, .get) { (errMsg, responseDict) in
            if errMsg != nil{
                completion(errMsg,nil)
            }else{
                if let res = responseDict{
                    do{
                        let responseModel = try DictionaryDecoder().decode(Self.self, from: res)
                        completion(errMsg,responseModel)
                    }catch(let errorDecode){
                        completion(errorDecode.localizedDescription,nil)
                    }
                }else{
                    completion("Invalid response",nil)
                }
            }
            
            
        }
        
    }
    
}

extension Double{
    func getDateFromTimeStamp() -> String {
        let date = Date(timeIntervalSince1970: self)
        let dayTimePeriodFormatter = DateFormatter()
        dayTimePeriodFormatter.dateFormat = "dd MMM YY, hh:mm a"
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        return dateString
    }
}





extension Double {
    /// Rounds the double to decimal places value
    func rounded(toPlaces places:Int) -> Double {
        let divisor = pow(10.0, Double(places))
        return (self * divisor).rounded() / divisor
    }
}
extension Date{
    func toString(_ style: DateFormatter.Style) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateStyle = style
        return dateFormatter.string(from: self)
    }
    
    func toString()->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd/MM/YYYY"
        return  dateFormatter.string(from: self)
    }
    
    func toStringDownline()->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MM/dd/YYYY"
        return  dateFormatter.string(from: self)
    }
    
    
   
    
    
    func toStringTheme()->String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        return  dateFormatter.string(from: self)
    }
    
}




extension NSObject {
    class var className: String {
        return String(describing: self)
    }
}

extension Dictionary{
    func toString()->String?{
        let jsonData = try? JSONSerialization.data(withJSONObject: self, options: .prettyPrinted)
        guard let json = jsonData else {
            return nil
        }
        return String(data: json, encoding: .utf8)
    }
    
}

extension Dictionary where Key: ExpressibleByStringLiteral {
    public mutating func lowercaseKeys() {
        for key in self.keys {
            self[String(describing: key).lowercased() as! Key] = self.removeValue(forKey: key)
        }
    }
}











extension Int{
    func toDateString()->String?{
        let date = Date(timeIntervalSince1970: TimeInterval(self))
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "GMT")
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm"
        let strDate = dateFormatter.string(from: date)
        return strDate
    }
}



struct SERVER {
    //MARK: - Colors
    static let darkBlue = "0B274C"


    //MARK: - Live URLS
    //static let BASE_URL = "http://38.17.52.106:6165/api/"
    static let BASE_URL = "https://nexaglobal.azurewebsites.net/api/"
    static let login = BASE_URL+"Auth/Login"
    static let ConsumerRegistration = BASE_URL+"Auth/ConsumerRegistration"
    static let validBarCode = BASE_URL+"Org/ValidBarCode"
    static let saveTestReportByMobile = BASE_URL+"Org/SaveTestReportByMobile"
    static let getDeviceTestResults = BASE_URL+"Org/GetDeviceTestResults"
    static let getTestingTypes = BASE_URL+"Org/GetTestingTypes"
    static let SaveDiagnosticHistory = BASE_URL+"Org/SaveDiagnosticHistory"
    static let getDeviceTestResultsByMobile = BASE_URL+"Org/GetDeviceTestResultsByMobile"
    static let getDiagnosticHistory = BASE_URL+"Org/GetDiagnosticHistory"
    static let getDeviceDiagnosticTestResults = BASE_URL+"Org/GetDeviceDiagnosticTestResults"
    static let saveTestReportByWithImageMobile = BASE_URL+"SaveTestReportByWithImageMobile"
    static let getDeviceTestResultsSixTest = BASE_URL+"Org/GetDeviceTestResultsSixTest"
    static let Getpdfdata = BASE_URL+"Org/Getpdfdata"

}



