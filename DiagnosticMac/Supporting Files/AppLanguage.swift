

import Foundation
enum AppLanguage:String {
    static var current:AppLanguage = {
        return DataStore.shared.appLanguage ?? AppLanguage.english
    }()
    case arabic = "عربى"
    case english = "ENGLISH"
    case spanish = "española"
    case portuguese = "Português"

}

struct arabic{
    var verify = "सत्यापित करें"
    
}

struct English {
    var verify = "VERIFY"

}
