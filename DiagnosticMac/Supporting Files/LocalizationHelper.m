//
//  LocalizationHelper.m
//  SmartCoolerInstallation
//
//  Created by Abhishek Sharma on 02/11/17.
//  Copyright © 2017 Insigma Inc. All rights reserved.
//

#import "LocalizationHelper.h"

@implementation LocalizationHelper

#pragma mark - Shared Instance
+ (instancetype)sharedInstance {
    
    static LocalizationHelper *sharedInstance = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedInstance = [[LocalizationHelper alloc] init];
        
    });
    return sharedInstance;
}


//- (void) downloadAndSetLatestFile {
//    
//    BFLOG(WSLocalization_BugFender);
//    CLog(WSLocalization_BugFender);
//    
//    [AppDelegate_.webapi getLatestLanguageFile:^(id responseValue) {
//         BFLOG(responseValue);
//        
//        if(responseValue && [responseValue isKindOfClass:[NSDictionary class]]){
//            BFLOG(getSuccessWS(WSLocalization_BugFender));
//            
//            BOOL isNeedToDownloadFile = FALSE;
//            if(ReadValue(kDefaultCurrentJsonVersion)){
//                int currentVersion = [ReadValue(kDefaultCurrentJsonVersion) intValue];
//                
//                if(currentVersion < [responseValue[@"version"] intValue]){
//                    isNeedToDownloadFile = TRUE;
//                }
//            }else{
//                isNeedToDownloadFile = TRUE;
//            }
//            
//            if(isNeedToDownloadFile){
//                NSString *stringURL = responseValue[@"fileUrl"];
//                NSURL  *url = [NSURL URLWithString:stringURL];
//                NSData *urlData = [NSData dataWithContentsOfURL:url];
//                
//                if (urlData ){
//                    
//                    NSArray * paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
//                    NSString  *documentsDirectory = [paths objectAtIndex:0];
//                    NSString  *filePath = [NSString stringWithFormat:@"%@/%@", documentsDirectory,@"localization.json"];
//                    if([urlData writeToFile:filePath atomically:YES]) { // Successfully written
//                        StoreValue(kDefaultCurrentJsonVersion, [NSNumber numberWithInt:[responseValue[@"version"] intValue]]);
//                        StoreValue(kDefaultIsJsonFileAvailable, [NSNumber numberWithBool:YES]);
//                        AppDelegate_.arrSupportedLanguage = [self getSupportedLanguage:filePath];
//                        [MCLocalization loadFromURL:[NSURL fileURLWithPath:filePath] defaultLanguage:@"English"];
//                        
//                        if([AppDelegate_.arrSupportedLanguage containsObject:ReadValue(kDefaultSelectedLanguage)]){
//                            [MCLocalization sharedInstance].language = ReadValue(kDefaultSelectedLanguage);
//                        }else{
//                            [AppDelegate_ setDefaultLanguage_For_Localization];
//                        }
//                        
//                        [MCLocalization sharedInstance].noKeyPlaceholder = @"key Not Found";
//                        
//                    }
//                }
//            }
//        }else{
//              BFError(getFailureWS(WSLocalization_BugFender, @"Response is not in format"));
//        }
//        
//        
//    } withErrorBlock:^(NSString *errorMsg, NSInteger errorCode) {
//        
//         BFError(getFailureWS(WSLocalization_BugFender, errorMsg));
//        
//    } withCompletedBlock:^{
//        
//    }];
//}

- (NSArray *)getSupportedLanguage : (NSString *)strJsonFilePath{
    
    NSString *content = [NSString stringWithContentsOfFile:strJsonFilePath encoding:NSUTF8StringEncoding error:nil];
    NSData *objectData = [content dataUsingEncoding:NSUTF8StringEncoding];
    NSError *jsonError;
    NSDictionary *json = [NSJSONSerialization JSONObjectWithData:objectData
                                                         options:NSJSONReadingMutableContainers
                                                           error:&jsonError];
    if(json && [json isKindOfClass:[NSDictionary class]]){
        
        NSMutableArray * arrLang = [NSMutableArray arrayWithArray:[json allKeys]];
        [arrLang sortUsingSelector:@selector(caseInsensitiveCompare:)];
        return [NSArray arrayWithArray:arrLang];
    }
    return @[];
}

@end
